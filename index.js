const express = require("express");
require("dotenv-flow").config();
const bodyParser = require("body-parser");
const serverless = require("serverless-http");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger/swagger.json");
const app = express();
const cors = require("cors");
const router = require("./router");
const masterRouter = require("./master-router");
const configRouter = require("./configRouter");
const setuproute = require("./settingsrouter");
const jwtMiddleWare = require("./jwt-middleware");
const { checkResource } = require("./resources/check-resource");
const fs=require("fs");
const https = require("https");

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const unauthorizedFailureStatus = {
  status: "failure",
  message: "Unauthorized",
};
app.get("/", (req, res) => {
  res.send({ response: "server is working", env: process.env });
});
app.use(async (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin,x-auth-token,authorization, X-Requested-With, Content-Type, Accept"
  );
  // next();
  // return
  const authHeader = req.headers.authorization;
  console.log(req.originalUrl, "original url");
  if (
    req.originalUrl === "/edu/webhook" ||
    req.originalUrl === "/edu/demandNote" ||
    req.originalUrl === "/edu/receiptSend" ||
    req.originalUrl.includes("/edu/createApplication") ||
    req.originalUrl.includes("/edu/sendReceipt?institute=") ||
    req.originalUrl.includes("/edu/pendingFees") ||
    req.originalUrl.includes("/edu/paymentHistory") ||
    req.originalUrl.includes("/config/institute") ||
    req.originalUrl.includes("/edu/ken42datafetch") ||
    req.originalUrl.includes("/edu/ken42datafetchCSV") ||
    req.originalUrl.includes("/edu/uploadMaster")
  ) {
    next();
  } else if (!authHeader) {
    res.status(401).send(unauthorizedFailureStatus);
    return;
  } else {
    const tokenResponse = jwtMiddleWare.checkToken(req, res, next);
    if (!tokenResponse.response) {
      res.status(401).send({ msg: "Token is not valid" });
      return;
    } else {
      const user = tokenResponse.decoded.user
        ? tokenResponse.decoded.user
        : tokenResponse.decoded.email;
      const resource = await checkResource({ user, ...tokenResponse.decoded });
      if (resource.connUri) {
        req.headers.resource = resource.connUri;
        req.headers.orgId = resource._id;
        next();
      } else {
        res.status(401).send({ msg: "Requested resource not found" });
      }
    }
  }
});

app.use("/config", configRouter);
app.use("/edu", router);
app.use("/master", masterRouter);
app.use("/setup", setuproute);

console.log(process.env.stage);
const options = {
  // ca: fs.readFileSync("bundle1.crt"),
  // key: fs.readFileSync("server.key"),
  // cert: fs.readFileSync("cert.crt"),
  // key: fs.readFileSync("key.pem"),
  // cert: fs.readFileSync("server.crt"),
  // ca: fs.readFileSync("bundle1.crt", 'utf8'),
  // ca: [fs.readFileSync("bundle1.crt"), fs.readFileSync("bundle2.crt"), fs.readFileSync("bundle2.crt")],
  // passphrase: "Zenqore@123"
};
let server = https.createServer(options, app);
let port = process.env.PORT;
if (process.env.stage == "prod" || process.env.stage == "uat") {
  server.listen(port, () => {
    console.log(`https server is listening at port ${port}`);
  });
} else {
  app.listen(port, () => {
    // winston.log('info',`server is listening at port ${port}`);
    console.log(`server is listening at port ${port}`);
  });
}
// module.exports = app;

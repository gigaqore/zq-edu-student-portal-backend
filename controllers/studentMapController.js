const transactionsSchema = require("../models/transactionsModel");
const feesLedgerSchema = require("../models/feesLedgerModel");
const StudentSchema = require("../models/studentModel");
const GuardianSchema = require("../models/guardianModel");
const FeeStructureSchema = require("../models/feeStructureModel");
const FeeTypeSchema = require("../models/feeTypeModel");
const StudentFeeMapSchema = require("../models/studentFeeMapModel");
const FeeManagerSchema = require("../models/feesManagerModel");
const { createDatabase } = require("../utils/db_creation");

exports.createStudentMap = async function (req, res) {
  let dbName = "5fa922adb52cca29a4027786";
  let dbConnection = await createDatabase(dbName, req.headers.resource);
  let masterUpladModel = dbConnection.model(
    "masteruploads",
    masterUploadSchema
  );

  masterUpladModel.find({}, async function (err, doc) {
    let studentDetails = doc[0]["data"]["studentDetails"];
    let feeManager = doc[0]["data"]["feeManagers"];
    var feeMapDetails = await Promise.all(
      _.map(studentDetails, async function (x, j) {
        let studentModel = dbConnection.model("students", StudentSchema);
        let check = await studentModel.findOne({ regId: x["Reg No *"] });
        let studentId = check._id;
        let feesManager = x.feeManager;

        var feeManagerId = await Promise.all(
          _.map(feesManager, async function (y) {
            let feeManagerModel = dbConnection.model(
              "feemanagers",
              FeeManagerSchema
            );
            let check = await feeManagerModel.findOne({ id: y });
            let mainId = check._id;
            return mainId;
          })
        );
        let reminder = [];
        let dating = [];
        _.times(12, function (value) {
          let mainDate = new moment().add(value + 1, "months").date(1);
          var remainderDate = new moment(mainDate).subtract(5, "days");
          var obj = {
            dueDate: mainDate,
            id: 1,
            percentage: 8,
          };
          var obj2 = {
            date: remainderDate,
          };
          reminder.push(obj2);
          dating.push(obj);
        }); // 'foo' (4x)

        var newFeeMap = {
          displayName: `SM-${
            String(j).length == 1 ? "00" : String(j).length == 2 ? "0" : ""
          }${Number(j) + 1}`,
          studentId: studentId,
          feeManager: {
            id: feeManagerId,
            paymentSchedule: dating,
            reminderPlan: reminder,
          },
          createdBy: dbName,
        };
        return newFeeMap;
      })
    );

    let feeMapModel = dbConnection.model("studentFeesMap", StudentFeeMapSchema);

    feeMapModel.insertMany(feeMapDetails, async function (error, docs) {
      if (error) {
        if (error.name === "BulkWriteError" && error.code === 11000) {
          // Duplicate username
          return res.status(200).json({
            success: true,
            message: "Fee Student Map already exist!",
            count: 0,
          });
        }
        return res.status(400).json({
          message: "Database Error",
          type: "error",
          data: error,
        });
      } else {
        return res.status(201).json({
          message: "New Fee Map added",
          type: "success",
          data: docs,
          count: docs.length,
        }); // Success
      }
    });
  });
};

exports.getStudentDetails = async function (req, res) {
  var dbUrl = req.headers.resource;
  let id = req.params.id;
  if (!id || !req.query.orgId) {
    res.status(400).json({
      status: "failed",
      message: "Please provide all required parameters.",
      type: "error",
    });
  } else {
    let dbName = req.query.orgId;
    let dbConnection = await createDatabase(dbName, dbUrl);
    let transactionModel = dbConnection.model(
      "transactions",
      transactionsSchema
    );
    let studentModel = dbConnection.model("students", StudentSchema);
    let feeStructureModel = dbConnection.model(
      "feestructures",
      FeeStructureSchema
    );
    let feeTypeModel = dbConnection.model("feetypes", FeeTypeSchema);
    let feeManagerModel = dbConnection.model("feemanagers", FeeManagerSchema);
    let guardianModel = dbConnection.model("guardians", GuardianSchema);
    let feeMapModel = dbConnection.model(
      "studentfeesmaps",
      StudentFeeMapSchema
    );
    var transactionDetails = await transactionModel.find({
      $or: [{ status: "Pending" }, { status: "Partial" }],
      studentRegId: id,
      transactionSubType: "demandNote",
    });
    if (transactionDetails.length == 0) {
      return res
        .status(404)
        .json({ status: "failed", message: "No Active Demand Note" });
    }
    var studentFeeMapDetails = await feeMapModel.findOne({
      studentId: transactionDetails[0].studentId,
    });
    let feeStructureDetails = await feeStructureModel.findOne({
      _id: studentFeeMapDetails.feeStructureId,
    });
    let feeTypesAll = [];
    for (feeTypesI of feeStructureDetails.feeTypeIds) {
      let feeTypesDetails = await feeTypeModel.findOne({
        _id: feeTypesI,
      });
      let feeManagerDetails = await feeManagerModel.findOne({
        feeTypeId: feeTypesI,
      });
      let obj1 = {
        feeTypesDetails: feeTypesDetails,
        feeManagerDetails: feeManagerDetails,
      };
      feeTypesAll.push(obj1);
    }
    if (studentFeeMapDetails == null) {
      return res
        .status(404)
        .json({ status: "failed", message: "Invalid Student ID" });
    }

    let feeLedgerData = [];
    for (oneLedger of transactionDetails) {
      let studentDetails = await studentModel.findOne({
        _id: oneLedger.studentId,
      });
      let guardianDetails = await guardianModel.findOne({
        _id: studentDetails.guardianDetails[0],
      });

      let obj = {
        demandNote: oneLedger,
        guardianDetails: guardianDetails,
        studentDetails: studentDetails,
        studentFeeMapDetails: studentFeeMapDetails,
        pending: studentFeeMapDetails.pending,
        paid: studentFeeMapDetails.paid,
        feeDetails: feeTypesAll,
      };
      feeLedgerData.push(obj);
    }
    res.status(200).json(feeLedgerData);
  }
};

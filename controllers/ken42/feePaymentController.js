const { createDatabase } = require("../../utils/db_creation");
const studentSchema = require("../../models/studentModel");
const mongoose = require("mongoose");
const transactionSchema = require("../../models/transactionsModel");
const orgListSchema = require("../../models/orglists-schema");
const feesLedgerCollectionName = "feesledgers";
const feesLedgerSchema = require("../../models/feesLedgerModel");
const GuardianSchema = require("../../models/guardianModel");
const FeeStructureSchema = require("../../models/feeStructureModel");
const FeeTypeSchema = require("../../models/feeTypeModel");
const StudentFeeMapSchema = require("../../models/studentFeeMapModel");
const FeeManagerSchema = require("../../models/feesManagerModel");

module.exports.getPaymentScheduleById = async (req, res) => {
  let id = req.params.id;
  if (!id || !req.query.orgId) {
    res.status(400).json({
      status: "failed",
      message: "Please provide all required parameters.",
      type: "error",
    });
  } else {
    console.log("env", process.env.central_mongoDbUrl);
    const centralDbConnection = await createDatabase(
      "edu-central",
      process.env.central_mongoDbUrl
    );
    const orgListModel = centralDbConnection.model(
      "orglists",
      orgListSchema,
      "orglists"
    );
    const orgData = await orgListModel.findOne({
      _id: req.query.orgId,
    });

    if (!orgData || orgData == null) {
      centralDbConnection.close();
      res.status(500).send({
        status: "failure",
        message: "Organization not found",
      });
    } else {
      let dbConnection = await createDatabase(
        String(orgData._id),
        orgData.connUri
      );

      let transactionModel = dbConnection.model(
        "transactions",
        transactionSchema
      );
      const feeMapModel = dbConnection.model(
        "studentfeesmaps",
        StudentFeeMapSchema
      );
      // using callback
      transactionModel
        .find({
          $or: [{ status: "Pending" }, { status: "Partial" }],
          studentRegId: id,
          transactionSubType: "demandNote",
        })
        .then(async function (data) {
          if (data) {
            let Result = [];
            for (oneEntry of data) {
              const feeMapData = await feeMapModel.findOne({
                studentId: oneEntry.studentId,
              });
              let finalData = {
                demanNoteId: oneEntry.displayName,
                dueDate: oneEntry.dueDate,
                feesBreakUp: oneEntry.data.feesBreakUp,
                class: oneEntry.class,
                academicYear: oneEntry.academicYear,
                amount: oneEntry.amount,
                fineAmount: feeMapData.fine,
                emailCommunicationRefIds: oneEntry.emailCommunicationRefIds[0],
              };
              Result.push(finalData);
            }
            if (Result.length == 0) {
              centralDbConnection.close();
              res.status(200).json({
                status: "success",
                message: "Data not found",
                data: Result,
              });
            } else {
              centralDbConnection.close();
              res.status(200).json({ status: "success", data: Result });
            }
          } else {
            centralDbConnection.close();
            return res.status(404).json({
              message: "Invalid Student Reg ID",
            });
          }
        })
        .finally(() => {
          centralDbConnection.close();
          dbConnection.close();
        });
    }
  }
};
module.exports.getPaymentHistoryById = async (req, res) => {
  let id = req.params.id;
  if (!id || !req.query.orgId) {
    res.status(400).json({
      status: "failed",
      message: "Please provide all required parameters.",
      type: "error",
    });
  } else {
    const centralDbConnection = await createDatabase(
      "edu-central",
      process.env.central_mongoDbUrl
    );
    const orgListModel = centralDbConnection.model(
      "orglists",
      orgListSchema,
      "orglists"
    );
    const orgData = await orgListModel.findOne({
      _id: req.query.orgId,
    });
    if (!orgData || orgData == null) {
      centralDbConnection.close();
      res.status(500).send({
        status: "failure",
        message: "Organization not found",
      });
    } else {
      let dbConnection = await createDatabase(
        String(orgData._id),
        orgData.connUri
      );

      let transactionModel = dbConnection.model(
        "transactions",
        transactionSchema
      );
      let ledgersModel = dbConnection.model("feesledgers", feesLedgerSchema);
      // using callback
      ledgersModel
        .find({
          $or: [{ status: "Paid" }, { status: "Partial" }],
          studentRegId: id,
          transactionSubType: "feePayment",
        })
        .then(async function (data) {
          if (data) {
            var allData = [];
            for (ledgerOne of data) {
              var transactionDetails = await transactionModel.findOne({
                displayName: ledgerOne.primaryTransaction,
              });
              if (transactionDetails == null) {
                res
                  .status(400)
                  .json({ status: "failed", message: "Invalid Transactions" });
              } else {
                let obj = {
                  feeType: transactionDetails.data.feesBreakUp,
                  class: transactionDetails.class,
                  academicYear: transactionDetails.academicYear,
                  paymentDate: ledgerOne.createdAt,
                  amount: ledgerOne.paidAmount,
                };
                allData.push(obj);
              }
            }
            res.status(200).json({ status: "success", data: allData });
          } else {
            return res.status(404).json({
              message: "Invalid Student Reg ID",
            });
          }
        })
        .finally(() => {
          centralDbConnection.close();
          dbConnection.close();
        });
    }
  }
};

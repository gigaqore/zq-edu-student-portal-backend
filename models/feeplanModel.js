const mongoose = require("mongoose");
const feeplanschema = new mongoose.Schema(
  {
    applicationId: { type: String, unique: false },
    studentRegId: { type: String },
    programPlanHEDAId: { type: String },
    totalAmount: { type: Number },
    plannedAmount: { type: Number },
    feesBreakUp: {type: Array},
    feePlanBreakUp: {type: Array},
    paidAmount: { type: Number },
    year: {type: Number},
    scholarshipAmount: { type: Number },
    pendingAmount: { type: Number },
    currency: { type: String },
    forex: { type: String },
    discountType: { type: String },
    discountPercentage: { type: Number },
    discountAmount: { type: Number },
    discountAmountBreakup: { type: Array },
    installmentPlan: { type: Object },
    campusId: { type: String },
    remarks: { type: Object },
    concessionFees: { type: Number },
    academicYear:{type:String},
    plannedAmountBreakup:[{
        amount:{type:Number},
        feeTypeCode:{type:String},
        title:{type:String}
    }],
    paidAmountBreakup:[{
        amount:{type:Number},
        feeTypeCode:{type:String},
        title:{type:String}
    }],
    pendingAmountBreakup:[{
        amount:{type:Number},
        feeTypeCode:{type:String},
        title:{type:String}
    }],
  },
  { timestamps: true },
  { _id: false }
);
module.exports = feeplanschema;
const FeeManagerSchema = require("../models/feesManagerModel");
const { createDatabase } = require("../utils/db_creation");
exports.createFeesManager = async function (req, res) {
  let inputData = req.body;
  let dbConnection = await createDatabase("admin", req.headers.resource);
  let feeManagerModel = dbConnection.model("feesManager", FeeManagerSchema);
  if (
    !inputData.displayName ||
    !inputData.title ||
    !inputData.types ||
    !inputData.feeDetails
  ) {
    res.status(404).json({
      status: "failed",
      message: "Please provide all required parameters.",
      type: "error",
    });
  } else {
    var newFeeManagerDetails = new feeManagerModel({
      displayName: inputData.displayName,
      title: inputData.title,
      description: inputData.description,
      types: inputData.types,
      feeDetails: inputData.feeDetails,
      default: inputData.default,
      createdBy: inputData.createdBy,
    });
    newFeeManagerDetails.save(function (err, data) {
      if (err) {
        return res.status(400).json({
          message: "Database error",
          type: "error",
          data: err,
        });
      } else {
        return res.status(201).json({
          message: "New Fee Manager added",
          type: "success",
          data: data,
        });
      }
    });
  }
};

exports.getFeeManagerDetails = async function (req, res) {
  let feeTypeId = req.params.id;
  let dbConnection = await createDatabase("admin", req.headers.resource);
  let feeManagerModel = dbConnection.model("feeManager", FeeManagerSchema);
  feeManagerModel.findById(feeTypeId, function (err, doc) {
    if (doc) {
      return res.status(200).json({ status: "success", data: doc });
    } else {
      return res
        .status(400)
        .json({ status: "failure", message: "Fee Manager does not exist" });
    }
  });
};

exports.showAllFeeManager = async function (req, res) {
  let dbConnection = await createDatabase("admin", process.env.database);
  let feeManagerModel = dbConnection.model("feeManager", FeeManagerSchema);
  feeManagerModel.find({}, function (err, doc) {
    if (doc) {
      return res.status(200).json({ status: "success", data: doc });
    } else {
      return res
        .status(400)
        .json({ status: "failure", message: "Fee Manager does not exist" });
    }
  });
};

exports.updatefeeManagerDetails = async function (req, res) {
  let dbConnection = await createDatabase("admin", process.env.database);
  let feeManagerModel = dbConnection.model("feeManager", FeeManagerSchema);
  let id = req.params.id;
  feeManagerModel.updateOne({ _id: id }, req.body, function (err, doc) {
    if (doc.nModified) {
      return res.status(200).json({
        status: "success",
        message: "Fee Manager data has been updated successfully",
      });
    } else {
      return res
        .status(400)
        .json({ status: "failure", message: "Nothing updated" });
    }
  });
};

// exports.deleteStudent = function (req, res) {
//   Role.deleteOne({ _id: req.params.id }).then(function (data) {
//     if (data.deletedCount)
//       return res.json("Role has been deleted successfully");
//     else return res.json({ message: "User does not exist" });
//   });
// };

const feePaymentTemplate = (orgDetails, transaction) => {
  let mode = transaction.mode;

  if (mode == "razorpay") {
    return `
    <div style="display:flex;justify-content:flex-start;"><img  title="logo.jpg" alt="" width="148" height="148" src="${orgDetails.logo.logo}"/><p style="margin-left:20px"><strong>${orgDetails.instituteDetails.instituteName}</strong><br />${orgDetails.instituteDetails.address1},${orgDetails.instituteDetails.address2},${orgDetails.instituteDetails.address3} <br /> ${orgDetails.instituteDetails.cityTown},PIN - ${orgDetails.instituteDetails.pinCode}<br />${orgDetails.instituteDetails.stateName}<br />Contact: ${orgDetails.instituteDetails.email}<br />Ph:${orgDetails.instituteDetails.phoneNumber1}</p></div>
  <br>
  <hr/>
  <br>
  <p><strong>Dear Parent,</strong></p>
  <p>Your fee payment of <strong> Rs${transaction.amount}.00</strong> towards the Demand Note <strong>${transaction.demandNote}</strong> has been initiated through our Payment Gateway. The transaction id is <strong> ${transaction.transactionId}</strong>.</p>
  <p> We will send you the confirmation and the fee receipt once the payment is realized.</p>
  <p>This may take around 2 working days</p>
  <p>Regards,</p>
  <p><strong>${orgDetails.instituteDetails.instituteName} Accounts Team</strong></p>
  <p>&nbsp;</p>`;
  } else if (mode == "cash") {
    return `
    <div style="display:flex;justify-content:flex-start;"><img  title="logo.jpg" alt="" width="148" height="148" src="${orgDetails.logo.logo}"/><p style="margin-left:20px"><strong>${orgDetails.instituteDetails.instituteName}</strong><br />${orgDetails.instituteDetails.address1},${orgDetails.instituteDetails.address2},${orgDetails.instituteDetails.address3} <br /> ${orgDetails.instituteDetails.cityTown},PIN - ${orgDetails.instituteDetails.pinCode}<br />${orgDetails.instituteDetails.stateName}<br />Contact: ${orgDetails.instituteDetails.email}<br />Ph:${orgDetails.instituteDetails.phoneNumber1}</p></div>
  <br>
  <hr/>
  <br>
  <p><strong>Dear Parent,</strong></p>
  <p>Your fee payment of <strong> Rs ${transaction.amount}.00</strong> towards the Demand Note${transaction.demandNote} has been received by Cash. The transaction id is <strong> ${transaction.transactionId}</strong>.</p>
 
  <p>Regards,</p>
  <p><strong>${orgDetails.instituteDetails.instituteName} Accounts Team</strong></p>
  <p>&nbsp;</p>`;
  } else if (mode == "card") {
    return `
    <div style="display:flex;justify-content:flex-start;"><img  title="logo.jpg" alt="" width="148" height="148" src="${orgDetails.logo.logo}"/><p style="margin-left:20px"><strong>${orgDetails.instituteDetails.instituteName}</strong><br />${orgDetails.instituteDetails.address1},${orgDetails.instituteDetails.address2},${orgDetails.instituteDetails.address3} <br /> ${orgDetails.instituteDetails.cityTown},PIN - ${orgDetails.instituteDetails.pinCode}<br />${orgDetails.instituteDetails.stateName}<br />Contact: ${orgDetails.instituteDetails.email}<br />Ph:${orgDetails.instituteDetails.phoneNumber1}</p></div>
  <br>
  <hr/>
  <br>
  <p><strong>Dear Parent,</strong></p>
  <p>Your fee payment of <strong> Rs ${transaction.amount}.00</strong> towards the Demand Note${transaction.demandNote} has been received by Card. The transaction id is <strong> ${transaction.transactionId}</strong>.</p>
 
  <p>Regards,</p>
  <p><strong>${orgDetails.instituteDetails.instituteName} Accounts Team</strong></p>
  <p>&nbsp;</p>`;
  } else if (mode == "cheque") {
    return `
    <div style="display:flex;justify-content:flex-start;"><img  title="logo.jpg" alt="" width="148" height="148" src="${orgDetails.logo.logo}"/><p style="margin-left:20px"><strong>${orgDetails.instituteDetails.instituteName}</strong><br />${orgDetails.instituteDetails.address1},${orgDetails.instituteDetails.address2},${orgDetails.instituteDetails.address3} <br /> ${orgDetails.instituteDetails.cityTown},PIN - ${orgDetails.instituteDetails.pinCode}<br />${orgDetails.instituteDetails.stateName}<br />Contact: ${orgDetails.instituteDetails.email}<br />Ph:${orgDetails.instituteDetails.phoneNumber1}</p></div>
  <br>
  <hr/>
  <br>
  <p><strong>Dear Parent,</strong></p>
  <p>Your fee payment of <strong> Rs ${transaction.amount}.00</strong> towards the Demand Note${transaction.demandNote} has been received by Cheque. The transaction id is <strong> ${transaction.transactionId}</strong>.</p>
 
  <p>Regards,</p>
  <p><strong>${orgDetails.instituteDetails.instituteName} Accounts Team</strong></p>
  <p>&nbsp;</p>`;
  } else if (mode == "netbanking") {
    return `
    <div style="display:flex;justify-content:flex-start;"><img  title="logo.jpg" alt="" width="148" height="148" src="${orgDetails.logo.logo}"/><p style="margin-left:20px"><strong>${orgDetails.instituteDetails.instituteName}</strong><br />${orgDetails.instituteDetails.address1},${orgDetails.instituteDetails.address2},${orgDetails.instituteDetails.address3} <br /> ${orgDetails.instituteDetails.cityTown},PIN - ${orgDetails.instituteDetails.pinCode}<br />${orgDetails.instituteDetails.stateName}<br />Contact: ${orgDetails.instituteDetails.email}<br />Ph:${orgDetails.instituteDetails.phoneNumber1}</p></div>
  <br>
  <hr/>
  <br>
  <p><strong>Dear Parent,</strong></p>
  <p>Your fee payment of <strong> Rs ${transaction.amount}.00</strong> towards the Demand Note${transaction.demandNote} has been received by Netbanking. The transaction id is <strong> ${transaction.transactionId}</strong>.</p>
 
  <p>Regards,</p>
  <p><strong>${orgDetails.instituteDetails.instituteName} Accounts Team</strong></p>
  <p>&nbsp;</p>`;
  } else if (mode == "wallet") {
    return `
    <div style="display:flex;justify-content:flex-start;"><img  title="logo.jpg" alt="" width="148" height="148" src="${orgDetails.logo.logo}"/><p style="margin-left:20px"><strong>${orgDetails.instituteDetails.instituteName}</strong><br />${orgDetails.instituteDetails.address1},${orgDetails.instituteDetails.address2},${orgDetails.instituteDetails.address3} <br /> ${orgDetails.instituteDetails.cityTown},PIN - ${orgDetails.instituteDetails.pinCode}<br />${orgDetails.instituteDetails.stateName}<br />Contact: ${orgDetails.instituteDetails.email}<br />Ph:${orgDetails.instituteDetails.phoneNumber1}</p></div>
  <br>
  <hr/>
  <br>
  <p><strong>Dear Parent,</strong></p>
  <p>Your fee payment of <strong> Rs ${transaction.amount}.00</strong> towards the Demand Note${transaction.demandNote} has been received by Wallet. The transaction id is <strong> ${transaction.transactionId}</strong>.</p>
 
  <p>Regards,</p>
  <p><strong>${orgDetails.instituteDetails.instituteName} Accounts Team</strong></p>
  <p>&nbsp;</p>`;
  }
};

module.exports = { feePaymentTemplate };

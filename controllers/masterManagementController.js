const { createDatabase } = require("../utils/db_creation");
const mongoose = require("mongoose");
// const dbName = "admin";
const msaterCollectionName = "masteruploads";
const StudentSchema = require("../models/studentModel");
const PaymentScheduleSchema = require("../models/paymentScheduleModel");
const InstallmentSchema = require("../models/installmentModel");
const LateFeeSchema = require("../models/lateFeeModel");
const ReminderSchema = require("../models/reminderModel");
const FeeTypeSchema = require("../models/feeTypeModel");
const ProgramPlanSchema = require("../models/programPlanModel");
const FeeManagerSchema = require("../models/feesManagerModel");
const FeeStructureSchema = require("../models/feeStructureModel");
const StudentFeeMapSchema = require("../models/studentFeeMapModel");
const GuardianSchema = require("../models/guardianModel");
const categorySchema = require("../models/categoryModel");
const concessionSchema = require("../models/concessionModel");

var _ = require("lodash");
var moment = require("moment");

async function createMaster(req, res) {
  var dbUrl = req.headers.resource;
  // var dbUrl = "mongodb://a117c6039a7fb4507ae53ec593c83cf3-92be4982110f6c62.elb.us-east-1.amazonaws.com:27017"
  console.log("dburl", dbUrl);
  const { type } = req.params;
  let inputData = req.body;
  let dbName = inputData.orgId;
  if (dbName == undefined) {
    res.status(404).send({
      status: "failure",
      message: "Organization not found",
    });
  } else {
    var masterResponse = await masterManagement(
      type,
      null,
      "post",
      inputData,
      null,
      dbUrl,
      dbName,
      req
    );
    if (masterResponse.status == "success") {
      res.status(201).send(masterResponse);
    } else {
      if (masterResponse.status == "failure") {
        res.status(400).send(masterResponse);
      } else {
        res.status(500).send(masterResponse);
      }
    }
  }
}
async function getMaster(req, res) {
  var dbUrl = req.headers.resource;
  const { type } = req.params;
  const queryData = req.query == undefined ? undefined : req.query;
  let dbName = queryData.orgId;
  var masterResponse = await masterManagement(
    type,
    null,
    "get",
    null,
    queryData,
    dbUrl,
    dbName,
    req
  );
  if (masterResponse.status == "success") {
    res.status(200).send(masterResponse);
  } else {
    if (masterResponse.status == "failure") {
      res.status(400).send(masterResponse);
    } else {
      res.status(500).send(masterResponse);
    }
  }
}
async function updateMaster(req, res) {
  var dbUrl = req.headers.resource;
  const { type } = req.params;
  let inputData = req.body;
  const queryData = req.query == undefined ? undefined : req.query;
  let dbName = inputData.orgId;
  if (dbName == undefined) {
    res.status(404).send({
      status: "failure",
      message: "Organization not found",
    });
  } else {
    var masterResponse = await masterManagement(
      type,
      null,
      "put",
      inputData,
      queryData,
      dbUrl,
      dbName
    );
    if (masterResponse.status == "success") {
      res.status(200).send(masterResponse);
    } else {
      if (masterResponse.status == "failure") {
        res.status(400).send(masterResponse);
      } else {
        res.status(500).send(masterResponse);
      }
    }
  }
}
async function masterManagement(
  type,
  id,
  method,
  inputData,
  queryData,
  dbUrl,
  dbName,
  req
) {
  var responseReturn = {};
  let dbConnection = await createDatabase(dbName, dbUrl);
  switch (type) {
    case "programPlan":
      const programPlanModel = dbConnection.model(
        "programplans",
        ProgramPlanSchema,
        "programplans"
      );
      responseReturn = await masterCRUD(
        type,
        method,
        inputData,
        dbConnection,
        programPlanModel,
        queryData
      );
      break;
    case "paymentSchedule":
      let paymentScheduleModel = dbConnection.model(
        "paymentSchedule",
        PaymentScheduleSchema
      );
      if (method == "get") {
        responseReturn = await masterCRUD(
          type,
          method,
          inputData,
          dbConnection,
          paymentScheduleModel,
          queryData
        );
      } else {
        if (
          !inputData.displayName ||
          !inputData.title ||
          !inputData.description ||
          !inputData.scheduleDetails ||
          !inputData.feesBreakUp ||
          // !inputData.parentId ||
          !inputData.createdBy
        ) {
          responseReturn = {
            status: "failed",
            message: "Please provide all required parameters.",
            type: "error",
          };
        } else {
          responseReturn = await masterCRUD(
            type,
            method,
            inputData,
            dbConnection,
            paymentScheduleModel,
            queryData
          );
        }
      }
      break;
    case "installments":
      let installmentSchema = dbConnection.model(
        "installments",
        InstallmentSchema
      );
      if (method == "get") {
        responseReturn = await masterCRUD(
          type,
          method,
          inputData,
          dbConnection,
          installmentSchema,
          queryData
        );
      } else {
        if (
          !inputData.displayName ||
          !inputData.title ||
          !inputData.description ||
          !inputData.numberOfInstallments ||
          !inputData.frequency ||
          !inputData.feesBreakUp ||
          !inputData.dueDate ||
          !inputData.createdBy
        ) {
          responseReturn = {
            status: "failed",
            message: "Please provide all required parameters.",
            type: "error",
          };
        } else {
          responseReturn = await masterCRUD(
            type,
            method,
            inputData,
            dbConnection,
            installmentSchema,
            queryData
          );
        }
      }
      break;
    case "reminders":
      let reminderSchema = dbConnection.model("reminderPlan", ReminderSchema);
      if (method == "get") {
        responseReturn = await masterCRUD(
          type,
          method,
          inputData,
          dbConnection,
          reminderSchema,
          queryData
        );
      } else {
        if (
          !inputData.displayName ||
          !inputData.title ||
          !inputData.description ||
          !inputData.numberOfReminders ||
          !inputData.scheduleDetails ||
          !inputData.createdBy
        ) {
          responseReturn = {
            status: "failed",
            message: "Please provide all required parameters.",
            type: "error",
          };
        } else {
          responseReturn = await masterCRUD(
            type,
            method,
            inputData,
            dbConnection,
            reminderSchema,
            queryData
          );
        }
      }
      break;
    case "lateFees":
      let lateFeeSchema = dbConnection.model("lateFees", LateFeeSchema);
      if (method == "get") {
        responseReturn = await masterCRUD(
          type,
          method,
          inputData,
          dbConnection,
          lateFeeSchema,
          queryData
        );
      } else {
        if (
          !inputData.displayName ||
          !inputData.title ||
          !inputData.description ||
          !inputData.type ||
          !inputData.amount ||
          !inputData.every ||
          !inputData.createdBy
        ) {
          responseReturn = {
            status: "failed",
            message: "Please provide all required parameters.",
            type: "error",
          };
        } else {
          responseReturn = await masterCRUD(
            type,
            method,
            inputData,
            dbConnection,
            lateFeeSchema,
            queryData
          );
        }
      }
      break;
    case "students":
      let { page, limit } = req.query;
      page = Number(page);
      limit = Number(limit);
      studentResponse = await getStudents(dbConnection, limit, page);
      var pageDetails = studentResponse["0"].metadata["0"];
      responseReturn = {
        status: "success",
        message: `${type} data`,
        data: studentResponse[0].data,
        currentPage: pageDetails.page,
        perPage: limit,
        nextPage: pageDetails.nextPage,
        totalRecord: pageDetails.total,
        totalPages: pageDetails.totalPages,
      };
      break;
    case "studentFeesMapping":
      let feeMapModel = dbConnection.model(
        "studentfeesmaps",
        StudentFeeMapSchema
      );
      responseReturn = await studentFeeMap(
        type,
        method,
        inputData,
        dbConnection,
        feeMapModel,
        queryData
      );
      break;
    case "sendDemandNote":
      let sfmModel = dbConnection.model("studentfeesmaps", StudentFeeMapSchema);
      responseReturn = await sendDemandNote(
        type,
        method,
        inputData,
        dbConnection,
        sfmModel,
        queryData
      );
      break;
    case "feesManager":
      var fmResponse = await getFeeManagers(
        dbConnection,
        Number(queryData.limit),
        Number(queryData.page)
      );
      var pageDetails = fmResponse["0"].metadata["0"];
      const instituteSchema = mongoose.Schema({}, { strict: false });
      let instituteModel = dbConnection.model(
        "institutedetails",
        instituteSchema,
        "institutedetails"
      );
      let instituteData = await instituteModel.find({});
      instituteData = instituteData[0]._doc.instituteContact[0].contactname;
      //data
      let programPlanModel1 = dbConnection.model(
        "programplans",
        ProgramPlanSchema,
        "programplans"
      );

      let paymentScheduleModel1 = dbConnection.model(
        "paymentSchedule",
        PaymentScheduleSchema
      );

      let reminderModel1 = dbConnection.model("reminderPlan", ReminderSchema);
      let lateFeeModel1 = dbConnection.model("lateFees", LateFeeSchema);
      let feeTypeModel1 = dbConnection.model("feeTypes", FeeTypeSchema);
      let paymentScheduleData = await paymentScheduleModel1.find({});
      let programPlanData = await programPlanModel1.find({});
      let reminderData = await reminderModel1.find({});
      let lateFeeData = await lateFeeModel1.find({});
      let feeTypeData = await feeTypeModel1.find({});
      responseReturn = {
        status: "success",
        message: `${type} data`,
        data: fmResponse[0].data.length
          ? fmResponse[0].data.map((item) => ({
              ...item,
              createdBy: instituteData,
            }))
          : fmResponse[0].data,
        allProgramPlan: programPlanData,
        allPaymentSchedule: paymentScheduleData,
        allReminderPlan: reminderData,
        allLateFee: lateFeeData,
        allFeeType: feeTypeData,
        currentPage: pageDetails.page,
        perPage: queryData.limit,
        nextPage: pageDetails.nextPage,
        totalRecord: pageDetails.total,
        totalPages: pageDetails.totalPages,
      };
      break;
    case "feeStructure":
      let feeStructureModel = dbConnection.model(
        "feestructures",
        FeeStructureSchema,
        "feestructures"
      );
      if (method == "get") {
        let feeManagerModel = dbConnection.model(
          "feemanagers",
          FeeManagerSchema,
          "feemanagers"
        );
        let feesTypesModel = dbConnection.model(
          "feetypes",
          FeeTypeSchema,
          "feetypes"
        );
        const allFeesManagers = await feeManagerModel.find();
        const allFeesData = await feesTypesModel.find();
        responseReturn = await masterCRUD(
          type,
          method,
          inputData,
          dbConnection,
          feeStructureModel,
          queryData
        );

        if (responseReturn.data && responseReturn.data.length) {
          responseReturn.data = responseReturn.data.map((item) => {
            let feeTypes = [];
            item._doc.feeTypeIds.forEach((element) => {
              // const feeManager = allFeesManagers.find(
              //   (ele) => ele._doc._id.toString() == element.toString()
              // );
              const feeType = allFeesData.find(
                (ele) => ele._doc._id.toString() == element.toString()
              );
              if (feeType) feeTypes.push(feeType.title);
            });
            return { ...item._doc, feeTypes };
          });
        }
      } else {
        if (
          !inputData.displayName ||
          !inputData.title ||
          !inputData.description ||
          !inputData.feeTypeIds ||
          !inputData.createdBy
        ) {
          responseReturn = {
            status: "failed",
            message: "Please provide all required parameters.",
            type: "error",
          };
        } else {
          responseReturn = await masterCRUD(
            type,
            method,
            inputData,
            dbConnection,
            feeStructureModel,
            queryData
          );
        }
      }
      break;
    case "feeTypes":
      let feeTypeModel = dbConnection.model("feeTypes", FeeTypeSchema);
      if (method == "get") {
        responseReturn = await masterCRUD(
          type,
          method,
          inputData,
          dbConnection,
          feeTypeModel,
          queryData
        );
      } else {
        if (
          !inputData.displayName ||
          !inputData.title ||
          !inputData.description
        ) {
          responseReturn = {
            status: "failed",
            message: "Please provide all required parameters.",
            type: "error",
          };
        } else {
          responseReturn = await masterCRUD(
            type,
            method,
            inputData,
            dbConnection,
            feeTypeModel,
            queryData
          );
        }
      }
      break;
    case "reminderPlan":
      let reminderModel = dbConnection.model("reminders", reminderSchema);
      if (method == "get") {
        responseReturn = await masterCRUD(
          type,
          method,
          inputData,
          dbConnection,
          reminderModel,
          queryData
        );
      }
      break;
    case "category":
      let categoryModel = dbConnection.model("categoryplans", categorySchema);
      if (method == "get") {
        responseReturn = await masterCRUD(
          type,
          method,
          inputData,
          dbConnection,
          categoryModel,
          queryData
        );
      } else {
        if (
          !inputData.displayName ||
          !inputData.title ||
          !inputData.description ||
          !inputData.createdBy
        ) {
          responseReturn = {
            status: "failed",
            message: "Please provide all required parameters.",
            type: "error",
          };
        } else {
          responseReturn = await masterCRUD(
            type,
            method,
            inputData,
            dbConnection,
            categoryModel,
            queryData
          );
        }
      }
      break;
    case "concession":
      let concessionModel = dbConnection.model(
        "concessionplans",
        concessionSchema
      );
      if (method == "get") {
        responseReturn = await masterCRUD(
          type,
          method,
          inputData,
          dbConnection,
          concessionModel,
          queryData
        );
      } else {
        if (
          !inputData.displayName ||
          !inputData.title ||
          !inputData.description ||
          !inputData.categoryId ||
          !inputData.concessionType ||
          !inputData.concessionValue == undefined ||
          !inputData.createdBy
        ) {
          responseReturn = {
            status: "failed",
            message: "Please provide all required parameters.",
            type: "error",
          };
        } else {
          responseReturn = await masterCRUD(
            type,
            method,
            inputData,
            dbConnection,
            concessionModel,
            queryData
          );
        }
      }
      break;
  }
  dbConnection.close();
  return responseReturn;
}

async function masterCRUD(
  master,
  method,
  inputData,
  dbConnection,
  modelData,
  queryData
) {
  try {
    var responseReturn = {};
    if (method == "post") {
      // console.log('**Input Data**',master,inputData)
      var addDetails = new modelData({
        ...inputData,
        updatedBy: inputData.createdBy,
      });
      await addDetails
        .save()
        .then((data) => {
          if (data) {
            responseReturn = {
              message: `New ${master} has been added successfully`,
              status: "success",
              data: data._id,
            };
          } else
            throw {
              message: "Database error",
              status: "error",
              data: data,
            };
        })
        .catch((err) => {
          throw {
            message: "Database error",
            status: "error",
            data: err,
          };
        });
    } else if (method == "put") {
      if (queryData.id == undefined) {
        responseReturn = {
          status: "failure",
          message: `${master} record not found`,
          data: id,
        };
      } else {
        let id = queryData.id;
        var scheduleData = await modelData.findOne({ _id: id });
        var parms = {
          ...inputData,
          updatedBy: inputData.createdBy,
          __v: Number(scheduleData["__v"]) + 1,
        };
        await modelData
          .updateOne({ _id: id }, parms)
          .then((data) => {
            if (data.nModified) {
              responseReturn = {
                status: "success",
                message: `${master} data has been updated successfully`,
                data: id,
              };
            } else
              throw {
                message: "Nothing updated",
                type: "failure",
                data: data,
              };
          })
          .catch((err) => {
            throw {
              message: "Database error",
              type: "error",
              data: err,
            };
          });
      }
    } else if (method == "get") {
      var getDatasDetails = [];
      if (master == "students") {
        // await getStudents(dbConnection);
        // let getD =
        //   queryData.id == undefined
        //     ? await modelData.find({})
        //     : await modelData.findOne({ _id: queryData.id });
        // for (student of getD) {
        //   let requiredData = {};
        //   console.log("student", student);
        //   let guardianModel = dbConnection.model("guardians", GuardianSchema);
        //   let parentData = await guardianModel.findOne({
        //     _id: student.guardianDetails[0],
        //   });
        //   requiredData["parentName"] = parentData._doc.firstName;
        //   // console.log("guardiandetails", parentData);
        //   var programPlanModel = dbConnection.model(
        //     "programplans",
        //     ProgramPlanSchema,
        //     "programplans"
        //   );
        //   let programData = await programPlanModel.findOne({
        //     _id: student.programPlanId,
        //   });
        //   // console.log("prorgamplan", programData);
        //   requiredData["programPlanId"] = programData._doc.title;
        //   let feeStructureModel = dbConnection.model(
        //     "feestructures",
        //     FeeStructureSchema,
        //     "feestructures"
        //   );
        //   // let feeStructureData = await feeStructureModel.findOne({
        //   //   _id: student.feeStructureId,
        //   // });
        //   // requiredData["feeStructureId"] = feeStructureData._doc.title;
        //   getDatasDetails.push({
        //     ...student._doc,
        //     guardianDetails: [requiredData.parentName],
        //     ...requiredData,
        //   });
        // }
      } else if (master == "students") {
      } else {
        getDatasDetails =
          queryData.id == undefined
            ? await modelData.find({})
            : await modelData.findOne({ _id: queryData.id });
      }
      // console.log("getDatasDetails", getDatasDetails);
      const { page, limit } = queryData;
      let paginationDatas = await Paginator(getDatasDetails, page, limit);
      let getDatas = paginationDatas.data;
      const instituteSchema = mongoose.Schema({}, { strict: false });
      let instituteModel = dbConnection.model(
        "institutedetails",
        instituteSchema,
        "institutedetails"
      );
      let instituteData = await instituteModel.find({});
      instituteData = instituteData[0]._doc.instituteContact[0].contactname;
      responseReturn = {
        status: "success",
        message: `${master} data`,
        data: getDatas.length
          ? getDatas.map((item) =>
              master == "feesManager" || master == "feeStructure"
                ? { ...item, _doc: { ...item._doc, createdBy: instituteData } }
                : master == "students"
                ? { ...item, createdBy: instituteData }
                : { ...item._doc, createdBy: instituteData }
            )
          : getDatas,
        currentPage: paginationDatas.page,
        perPage: paginationDatas.perPage,
        nextPage: paginationDatas.next_page,
        totalRecord: paginationDatas.total,
        totalPages: paginationDatas.total_pages,
      };
    } else {
      responseReturn = {
        status: "failure",
        message: "master not found",
      };
    }
    return responseReturn;
  } catch (e) {
    return e;
  }
}

async function studentFeeMap(
  master,
  method,
  inputData,
  dbConnection,
  modelData,
  queryData
) {
  try {
    var responseReturn = {};
    const masterDataSchema = mongoose.Schema({}, { strict: false });
    let mesterDataCollection = await dbConnection.model(
      msaterCollectionName,
      masterDataSchema,
      msaterCollectionName
    );
    let details = await mesterDataCollection.find({});
    var mastersData = details["0"]._doc["data"];
    if (method == "post") {
    } else if (method == "put") {
    } else if (method == "get") {
      let itemsPerPage = parseInt(queryData.limit);
      let currentPage = parseInt(queryData.page);
      let skipItems = parseInt(itemsPerPage * currentPage - itemsPerPage);
      const aggregateData = [
        {
          $lookup: {
            from: "students",
            localField: "studentId",
            foreignField: "_id",
            as: "student",
          },
        },
        {
          $lookup: {
            from: "feestructures",
            localField: "feeStructureId",
            foreignField: "_id",
            as: "feeStructure",
          },
        },
        // { $unwind: "$feeStructure" },
        {
          $lookup: {
            from: "feetypes",
            localField: "feeStructure.feeTypeIds",
            foreignField: "_id",
            as: "feeType",
          },
        },
        {
          $lookup: {
            from: "feemanagers",
            let: { feeTypeId: "$feeType._id", programPlanId: "$programPlanId" },
            pipeline: [
              {
                $match: {
                  $expr: {
                    // $and:
                    feeTypeId: "$feeType._id",
                    programPlanId: "$programPlanId",
                    // [
                    //   { $feeTypeId: "$$feeType._id" },
                    //   { $programPlanId: "$programPlanId" }
                    // ]
                  },
                },
              },
            ],
            as: "feeManager",
          },
        },
        // { $unwind: "$feeManager" },
        {
          $lookup: {
            from: "reminderplans",
            localField: "feeManager.reminderPlanId",
            foreignField: "_id",
            as: "reminderPlan",
          },
        },
        {
          $lookup: {
            from: "programplans",
            localField: "programPlanId",
            foreignField: "_id",
            as: "programPlan",
          },
        },
        {
          $lookup: {
            from: "paymentschedules",
            localField: "feeManager.paymentScheduleId",
            foreignField: "_id",
            as: "paymentSchedule",
          },
        },
        {
          $lookup: {
            from: "concessionplans",
            localField: "feeManager.concessionPlanId",
            foreignField: "_id",
            as: "concessionPlan",
          },
        },
        {
          $lookup: {
            from: "latefees",
            localField: "feeManager.lateFeePlanId",
            foreignField: "_id",
            as: "lateFee",
          },
        },
        {
          $lookup: {
            from: "installments",
            localField: "feeManager.installmentPlanId",
            foreignField: "_id",
            as: "installment",
          },
        },
        {
          $lookup: {
            from: "feetypes",
            localField: "feeManager.feeTypeId",
            foreignField: "_id",
            as: "feeType",
          },
        },
        {
          $lookup: {
            from: "institutedetails",
            localField: "createdBy",
            foreignField: "_id",
            as: "instituteDetails",
          },
        },
        {
          $project: {
            displayName: 1,
            studentId: {
              $arrayElemAt: ["$student.displayName", 0],
            },
            regId: {
              $arrayElemAt: ["$student.regId", 0],
            },
            dueDate: 1,
            studentDetails: "$student",
            studentName: {
              $arrayElemAt: ["$student.firstName", 0],
            },
            feeStructureId: {
              $arrayElemAt: ["$feeStructure.displayName", 0],
            },
            feeStructure: {
              $arrayElemAt: ["$feeStructure.title", 0],
            },
            feeStructureDescription: {
              $arrayElemAt: ["$feeStructure.description", 0],
            },
            programPlanDetails: "$programPlan",
            feeDetails: "$feeManager.feeDetails",
            totalAmount: "$amount",
            paidAmount: "$paid",
            pendingAmount: "$pending",
            createdBy: "$createdBy",
            createdAt: "$createdAt",
            programPlanId: {
              $arrayElemAt: ["$programPlan.displayName", 0],
            },
            reminderPlanId: {
              $arrayElemAt: ["$reminderPlan.displayName", 0],
            },
            reminderPlan: {
              $arrayElemAt: ["$reminderPlan.title", 0],
            },
            reminderPlanDescription: {
              $arrayElemAt: ["$reminderPlan.description", 0],
            },
            paymentScheduleId: {
              $arrayElemAt: ["$paymentSchedule.displayName", 0],
            },
            paymentSchedule: {
              $arrayElemAt: ["$paymentSchedule.title", 0],
            },
            paymentScheduleDescription: {
              $arrayElemAt: ["$paymentSchedule.description", 0],
            },
            concessionPlanId: {
              $arrayElemAt: ["$concessionPlan.displayName", 0],
            },
            concessionPlan: {
              $arrayElemAt: ["$concessionPlan.title", 0],
            },
            concessionPlanDescription: {
              $arrayElemAt: ["$concessionPlan.description", 0],
            },
            lateFeePlanId: {
              $arrayElemAt: ["$lateFee.displayName", 0],
            },
            lateFeePlan: {
              $arrayElemAt: ["$lateFee.title", 0],
            },
            lateFeePlanDescription: {
              $arrayElemAt: ["$lateFee.description", 0],
            },
            installmentPlanId: {
              $arrayElemAt: ["$installment.displayName", 0],
            },
            installment: {
              $arrayElemAt: ["$installment.title", 0],
            },
            installmentDescription: {
              $arrayElemAt: ["$installment.description", 0],
            },
            programPlan: {
              $arrayElemAt: ["$programPlan.title", 0],
            },
            programPlanCode: {
              $arrayElemAt: ["$programPlan.programCode", 0],
            },
            feeTypeName: {
              $arrayElemAt: ["$feeType.title", 0],
            },
            feeTypeId: {
              $arrayElemAt: ["$feeType.displayName", 0],
            },
            feeTypeDescription: {
              $arrayElemAt: ["$feeType.description", 0],
            },
          },
        },
        {
          $facet: {
            metadata: [
              { $count: "total" },
              {
                $addFields: {
                  page: currentPage,
                  itemsPerPage,
                  totalPages: {
                    $ceil: { $divide: ["$total", itemsPerPage] },
                  },
                  nextPage: {
                    $cond: {
                      if: {
                        $gt: [
                          { $ceil: { $divide: ["$total", itemsPerPage] } },
                          currentPage,
                        ],
                      },
                      then: currentPage + 1,
                      else: null,
                    },
                  },
                },
              },
            ],
            data: [
              { $skip: skipItems < 0 ? 0 : skipItems },
              { $limit: itemsPerPage },
            ], // add projection here wish you re-shape the docs
          },
        },
      ];
      const sfmData = await modelData.aggregate(aggregateData);
      const instituteSchema = mongoose.Schema({}, { strict: false });
      let instituteModel = dbConnection.model(
        "institutedetails",
        instituteSchema,
        "institutedetails"
      );
      let instituteData = await instituteModel.find({});
      instituteData = instituteData[0]._doc.instituteContact[0].contactname;
      var sfmDetails =
        sfmData["0"] != undefined
          ? sfmData["0"]["data"] != undefined
            ? sfmData["0"]["data"]
            : []
          : [];
      var pageDetails =
        sfmData["0"] != undefined
          ? sfmData["0"]["metadata"] != undefined
            ? sfmData["0"]["metadata"]["0"]
            : {
                page: null,
                nextPage: null,
                total: null,
                totalPages: null,
              }
          : {
              page: null,
              nextPage: null,
              total: null,
              totalPages: null,
            };
      responseReturn = {
        status: "success",
        message: `${master} data`,
        data: sfmDetails.length
          ? sfmDetails.map((item) => ({
              ...item,
              createdBy: instituteData,
            }))
          : sfmDetails,
        currentPage: pageDetails.page,
        perPage: itemsPerPage,
        nextPage: pageDetails.nextPage,
        totalRecord: pageDetails.total,
        totalPages: pageDetails.totalPages,
      };
    } else {
      responseReturn = {
        status: "failure",
        message: "master not found",
      };
    }
    return responseReturn;
  } catch (e) {
    return e;
  }
}

async function sendDemandNote(
  master,
  method,
  inputData,
  dbConnection,
  modelData,
  queryData
) {
  try {
    var responseReturn = {};
    const masterDataSchema = mongoose.Schema({}, { strict: false });
    let mesterDataCollection = await dbConnection.model(
      msaterCollectionName,
      masterDataSchema,
      msaterCollectionName
    );
    let details = await mesterDataCollection.find({});
    var mastersData = details["0"]._doc["data"];
    if (method == "post") {
    } else if (method == "put") {
    } else if (method == "get") {
      var studentFeeMngerData = [];
      let studentModel = dbConnection.model("students", StudentSchema);
      let feeManagerSchema = dbConnection.model(
        "feemanagers",
        FeeManagerSchema
      );
      let feeStructureSchema = dbConnection.model(
        "feestructures",
        FeeStructureSchema
      );
      let programPlanSchema = dbConnection.model(
        "programplans",
        ProgramPlanSchema
      );
      let guardianModel = dbConnection.model("guardian", GuardianSchema);
      let feeTypeModel = dbConnection.model("feeTypes", FeeTypeSchema);
      var getDatasDetails =
        queryData.id == undefined
          ? await modelData.find({})
          : await modelData.findOne({ _id: queryData.id });
      const { page, limit } = queryData;
      let paginationDatas = await Paginator(getDatasDetails, page, limit);
      let getDatas = paginationDatas.data;
      for (let i = 0; i < getDatas.length; i++) {
        var gd = getDatas[i];
        var studentFeeMngerDataObj = {};
        studentFeeMngerDataObj["displayName"] = gd["displayName"];
        studentFeeMngerDataObj["studentId"] = gd["studentId"];
        let studentDatas = await studentModel.findOne({ _id: gd.studentId });
        // let particularStudent = {};
        let feeStructData = {};
        let ftDatas = [];
        var dueDate = {};
        // for (let w = 0; w < mastersData["studentDetails"].length; w++) {
        //   if (
        //     studentDatas["regId"] ==
        //     mastersData["studentDetails"][w]["Reg No *"]
        //   ) {
        //     particularStudent = mastersData["studentDetails"][w];
        //   }
        // }
        // for (let j = 0; j < gd["feeStructureId"].length; j++) {
        //   var fsData = gd["feeStructureId"][j];
        //   if (j == 0) {
        //     dueDate = {
        //       dueDate:
        //         gd["feeStructureId"][j]["paymentSchedule"]["0"]["dueDate"],
        //       type: "Year",
        //     };
        //   }
        //   let fsDatas = await feeStructureSchema.findOne({
        //     _id: gd["feeStructureId"][j]["id"],
        //   });
        let fsDatas = await feeStructureSchema.findOne({
          _id: gd["feeStructureId"],
        });
        feeStructData = fsDatas;
        for (let k = 0; k < fsDatas["feeTypeIds"].length; k++) {
          // let fmDatas = await feeManagerSchema.findOne({
          //   feeTypeId: fsDatas["feeTypeIds"][k],
          //   programPlanId: gd["programPlanId"]
          // });
          let fsItems = await feeTypeModel.findOne({
            _id: fsDatas["feeTypeIds"][k],
          });
          var ftMerge = {
            ...fsItems._doc,
          };
          ftDatas.push(ftMerge);
        }
        // }

        let ppDetails = await programPlanSchema.findOne({
          _id: gd["programPlanId"],
        });
        studentFeeMngerDataObj["programPlanDetails"] = ppDetails._doc;
        studentFeeMngerDataObj["dueDate"] =
          gd["dueDate"] != undefined ? gd["dueDate"] : null;
        studentFeeMngerDataObj["studentDetails"] = studentDatas;
        var guardianDetails = [];
        for (let i = 0; i < studentDatas.guardianDetails.length; i++) {
          var gdd = await guardianModel.findOne({
            _id: studentDatas.guardianDetails[i],
          });
          guardianDetails.push(gdd);
        }
        studentFeeMngerDataObj["guardianDetails"] = guardianDetails;
        studentFeeMngerDataObj["studentName"] = `${studentDatas["firstName"]}${
          studentDatas["middleName"] == null
            ? ""
            : " " + studentDatas["middleName"]
        } ${studentDatas["lastName"]}`;
        studentFeeMngerDataObj["feeStructureId"] = feeStructData["displayName"];
        studentFeeMngerDataObj["feeStructure"] = feeStructData["title"];
        studentFeeMngerDataObj["feeStructureDescription"] =
          feeStructData["description"];
        studentFeeMngerDataObj["feeDetails"] = ftDatas;
        studentFeeMngerDataObj["totalAmount"] = gd["amount"];
        studentFeeMngerDataObj["paidAmount"] = gd["paid"];
        studentFeeMngerDataObj["pendingAmount"] = gd["pending"];
        studentFeeMngerDataObj["createdBy"] = gd["createdBy"];
        studentFeeMngerDataObj["createdAt"] = gd["createdAt"];
        studentFeeMngerData.push(studentFeeMngerDataObj);
      }
      const instituteSchema = mongoose.Schema({}, { strict: false });
      let instituteModel = dbConnection.model(
        "institutedetails",
        instituteSchema,
        "institutedetails"
      );
      let instituteData = await instituteModel.find({});
      instituteData = instituteData[0]._doc.instituteContact[0].contactname;
      responseReturn = {
        status: "success",
        message: `${master} data`,
        data: studentFeeMngerData.length
          ? studentFeeMngerData.map((item) => ({
              ...item,
              createdBy: instituteData,
            }))
          : studentFeeMngerData,
        currentPage: paginationDatas.page,
        perPage: paginationDatas.perPage,
        nextPage: paginationDatas.next_page,
        totalRecord: paginationDatas.total,
        totalPages: paginationDatas.total_pages,
      };
    } else {
      responseReturn = {
        status: "failure",
        message: "master not found",
      };
    }
    return responseReturn;
  } catch (e) {
    return e;
  }
}

async function getDisplayId(req, res) {
  var dbUrl = req.headers.resource;
  const { type } = req.params;
  const { orgId } = req.query;
  let dbConnection = await createDatabase(orgId, dbUrl);
  var getDatas = [];
  var transType = "";
  if (type == "paymentSchedule") {
    let paymentScheduleModel = dbConnection.model(
      "paymentSchedule",
      PaymentScheduleSchema
    );
    // getDatas = await paymentScheduleModel.find({ parentId: id })
    getDatas = await paymentScheduleModel.find({});
    transType = "PSCH";
  } else if (type == "installments") {
    let installmentSchema = dbConnection.model(
      "installments",
      InstallmentSchema
    );
    getDatas = await installmentSchema.find({});
    transType = "INST";
  } else if (type == "lateFees") {
    let lateFeeSchema = dbConnection.model("lateFees", LateFeeSchema);
    getDatas = await lateFeeSchema.find({});
    transType = "LTFEE";
  } else if (type == "reminders") {
    let reminderSchema = dbConnection.model("reminderPlan", ReminderSchema);
    getDatas = await reminderSchema.find({});
    transType = "REM";
  } else if (type == "category") {
    let cateSchema = dbConnection.model("categoryplans", categorySchema);
    getDatas = await cateSchema.find({});
    transType = "CAT";
  } else if (type == "concession") {
    let conSchema = dbConnection.model("concessionplans", concessionSchema);
    getDatas = await conSchema.find({});
    transType = "CON";
  } else if (type == "feeStructure") {
    let fsSchema = dbConnection.model("feestructures", FeeStructureSchema);
    getDatas = await fsSchema.find({});
    transType = "FS";
  } else if (type == "feeTypes") {
    let ftSchema = dbConnection.model("feeTypes", FeeTypeSchema);
    getDatas = await ftSchema.find({});
    transType = "FT";
  }
  var date = new Date();
  var month = date.getMonth();
  var finYear = "";
  if (month > 2) {
    var current = date.getFullYear();
    var prev = Number(date.getFullYear()) + 1;
    prev = String(prev).substr(String(prev).length - 2);
    finYear = `${current}-${prev}`;
  } else {
    var current = date.getFullYear();
    current = String(current).substr(String(current).length - 2);
    var prev = Number(date.getFullYear()) - 1;
    finYear = `${prev}-${current}`;
  }
  let initial = `${transType}001`;
  let dataArr = [];
  let check;
  let finalVal;
  const sortAlphaNum = (a, b) => a.localeCompare(b, "en", { numeric: true });
  getDatas.forEach((el) => {
    if (el["displayName"]) {
      // let filStr = el["displayName"].split("");
      let typeStr = String(el["displayName"]).slice(
        0,
        String(transType).length
      );
      // let typeYear = filStr[1];
      // if (typeStr == transType && typeYear == finYear) {
      if (typeStr == transType) {
        check = true;
        dataArr.push(el["displayName"]);
      }
    }
  });
  if (!check) {
    finalVal = initial;
  } else {
    let lastCount = String(
      dataArr.sort(sortAlphaNum)[dataArr.length - 1]
    ).slice(String(transType).length);
    let lastCountNo = Number(lastCount) + 1;
    if (lastCountNo.toString().length == 1) lastCountNo = "00" + lastCountNo;
    if (lastCountNo.toString().length == 2) lastCountNo = "0" + lastCountNo;
    // lastCount[2] = lastCountNo;
    finalVal = `${transType}${lastCountNo}`;
  }
  res.status(200).send({
    status: "success",
    message: `ID generated`,
    data: finalVal,
  });
}

async function getStudents(dbConnection, itemsPerPage, currentPage) {
  let skipItems = itemsPerPage * currentPage - itemsPerPage;
  const aggregateData = [
    { $unwind: "$guardianDetails" },
    {
      $lookup: {
        from: "guardians",
        localField: "guardianDetails",
        foreignField: "_id",
        as: "guardianDetails",
      },
    },
    {
      $lookup: {
        from: "programplans",
        localField: "programPlanId",
        foreignField: "_id",
        as: "programPlan",
      },
    },
    {
      $facet: {
        metadata: [
          { $count: "total" },
          {
            $addFields: {
              page: currentPage,
              itemsPerPage,
              totalPages: {
                $ceil: { $divide: ["$total", itemsPerPage] },
              },
              nextPage: {
                $cond: {
                  if: {
                    $gt: [
                      { $ceil: { $divide: ["$total", itemsPerPage] } },
                      currentPage,
                    ],
                  },
                  then: currentPage + 1,
                  else: null,
                },
              },
            },
          },
        ],
        data: [
          { $skip: skipItems < 0 ? 0 : skipItems },
          { $limit: itemsPerPage },
        ], // add projection here wish you re-shape the docs
      },
    },
  ];
  const studentModel = dbConnection.model(
    "students",
    StudentSchema,
    "students"
  );
  const studentData = await studentModel.aggregate(aggregateData);
  return studentData;
}

async function getFeeManagers(dbConnection, itemsPerPage, currentPage) {
  let skipItems = itemsPerPage * currentPage - itemsPerPage;

  const aggregateData = [
    // { $unwind: "$reminderPlan" },
    // { $unwind: "$programPlan" },
    // { $unwind: "$paymentSchedule" },
    // { $unwind: "$concessionPlan" },
    // { $unwind: "$lateFee" },
    // { $unwind: "$installment" },
    {
      $lookup: {
        from: "reminderplans",
        localField: "reminderPlanId",
        foreignField: "_id",
        as: "reminderPlan",
      },
    },
    {
      $lookup: {
        from: "programplans",
        localField: "programPlanId",
        foreignField: "_id",
        as: "programPlan",
      },
    },
    {
      $lookup: {
        from: "paymentschedules",
        localField: "paymentScheduleId",
        foreignField: "_id",
        as: "paymentSchedule",
      },
    },
    {
      $lookup: {
        from: "concessionplans",
        localField: "concessionPlanId",
        foreignField: "_id",
        as: "concessionPlan",
      },
    },
    {
      $lookup: {
        from: "latefees",
        localField: "lateFeePlanId",
        foreignField: "_id",
        as: "lateFee",
      },
    },
    {
      $lookup: {
        from: "installments",
        localField: "installmentPlanId",
        foreignField: "_id",
        as: "installment",
      },
    },
    {
      $lookup: {
        from: "feetypes",
        localField: "feeTypeId",
        foreignField: "_id",
        as: "feeType",
      },
    },
    {
      $lookup: {
        from: "institutedetails",
        localField: "createdBy",
        foreignField: "_id",
        as: "instituteDetails",
      },
    },
    {
      $addFields: {
        programPlanDetails: "$programPlan",
        reminderPlanDetails: "$reminderPlan",
        paymentScheduleDetails: "$paymentSchedule",
        concessionPlanDetais: "$concessionPlan",
        feeTypeDet: "$feeType",
        lateFeePlanDetails: "$lateFee",
        installmentPlanDetails: "$installment",
        createdBy: "$instituteDetails",
      },
    },
    {
      $project: {
        // projection
        programPlan: {
          $arrayElemAt: ["$programPlanDetails", 0],
        },
        reminderPlan: {
          $arrayElemAt: ["$reminderPlanDetails", 0],
        },
        paymentSchedule: {
          $arrayElemAt: ["$paymentScheduleDetails", 0],
        },
        concessionPlan: {
          $arrayElemAt: ["$concessionPlanDetais", 0],
        },
        lateFeePlan: {
          $arrayElemAt: ["$lateFeePlanDetails", 0],
        },
        installmentPlan: {
          $arrayElemAt: ["$installmentPlanDetails", 0],
        },
        feeType: {
          $arrayElemAt: ["$feeTypeDet", 0],
        },
        displayName: 1,
        title: 1,
        description: 1,
        feeDetails: 1,
        createdBy: {
          $arrayElemAt: ["$createdBy.instituteContact[0].contactname", 0],
        },
        createdAt: 1,
      },
    },
    {
      $facet: {
        metadata: [
          { $count: "total" },
          {
            $addFields: {
              page: currentPage,
              itemsPerPage,
              totalPages: {
                $ceil: { $divide: ["$total", itemsPerPage] },
              },
              nextPage: {
                $cond: {
                  if: {
                    $gt: [
                      { $ceil: { $divide: ["$total", itemsPerPage] } },
                      currentPage,
                    ],
                  },
                  then: currentPage + 1,
                  else: null,
                },
              },
            },
          },
        ],
        data: [
          { $skip: skipItems < 0 ? 0 : skipItems },
          { $limit: itemsPerPage },
        ], // add projection here wish you re-shape the docs
      },
    },
  ];
  let feesManagerModel = dbConnection.model(
    "feemanagers",
    FeeManagerSchema,
    "feemanagers"
  );
  const fmData = await feesManagerModel.aggregate(aggregateData);

  return fmData;
}

async function dueDateCalculation(req, res) {
  const { collectEvery, dueDate } = req.query;
  // let arrFeesBreakup = inputData.feesBreakup;
  var monthRange = [12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
  var currentMonth = new Date("2020-06-01");
  var monthIndex =
    Number(
      monthRange[
        currentMonth.getMonth() >= 5
          ? Number(currentMonth.getMonth()) - Number(5)
          : Number(currentMonth.getMonth()) + Number(7)
      ]
    ) - 1;
  let dating = [];
  var quarterIndex = monthIndex / 4;
  var yearIndex = monthIndex / 12;
  var collectEveryRatio =
    collectEvery.toLowerCase() == "month"
      ? monthIndex
      : collectEvery.toLowerCase() == "quarter"
      ? 4
      : 1;
  var typeOfDueDate =
    dueDate.toLowerCase() == "first"
      ? 1
      : dueDate.toLowerCase() == "second"
      ? 2
      : dueDate.toLowerCase() == "third"
      ? 3
      : 4;
  var adjAmt = 0;
  _.times(collectEveryRatio, function (value) {
    let re = value + 1;
    var mainDate =
      collectEvery.toLowerCase() == "month"
        ? new moment().add(value + 1, "months").date(typeOfDueDate)
        : collectEvery.toLowerCase() == "quarter"
        ? new moment().add(re * quarterIndex, "months").date(typeOfDueDate)
        : new moment("2020-06-01").add(yearIndex, "year").date(typeOfDueDate);
    var obj = {
      dueDate: mainDate,
      id: dueDate,
      percentage: Number((100 / collectEveryRatio).toFixed(2)),
    };
    adjAmt = Number((100 / collectEveryRatio).toFixed(2)) + Number(adjAmt);
    dating.push(obj);
  });
  dating["0"]["percentage"] = Number(
    (Number(dating["0"]["percentage"]) + Number(100 - adjAmt)).toFixed(2)
  );
  res.status(200).json({ data: dating, status: "success" });
}

async function installmentDueDateCalculation(req, res) {
  let { noOfInst, frequency, dueDate } = req.query;
  let dating = [];
  var installmentRadtio = Number((100 / Number(noOfInst)).toFixed(2));
  var typeOfDueDate =
    dueDate.toLowerCase() == "first"
      ? 1
      : dueDate.toLowerCase() == "second"
      ? 2
      : dueDate.toLowerCase() == "third"
      ? 3
      : 4;
  var startDate =
    frequency.toLowerCase() == "fortnight"
      ? moment()
          .day(7 + typeOfDueDate)
          .add(7, "days")
      : moment().day(7 + typeOfDueDate);
  var defaultDays = frequency.toLowerCase() == "fortnight" ? 14 : 7;
  var adjAmt = 0;
  if (
    frequency.toLowerCase() == "week" ||
    frequency.toLowerCase() == "fortnight"
  ) {
    _.times(noOfInst, function (value) {
      let re = value + 1;
      var mainDate = new moment(new Date(startDate)).add(
        value * defaultDays,
        "days"
      );
      var obj = {
        dueDate: mainDate,
        id: dueDate,
        percentage: installmentRadtio,
      };

      adjAmt = Number(installmentRadtio) + Number(adjAmt);
      dating.push(obj);
    });
  } else if (frequency.toLowerCase() == "month") {
    _.times(noOfInst, function (value) {
      let re = value + 1;
      var mainDate = new moment().add(value + 1, "months").date(typeOfDueDate);
      var obj = {
        dueDate: mainDate,
        id: dueDate,
        percentage: installmentRadtio,
      };

      adjAmt = Number(installmentRadtio) + Number(adjAmt);
      dating.push(obj);
    });
  }
  dating["0"]["percentage"] = Number(
    (Number(dating["0"]["percentage"]) + Number(100 - adjAmt)).toFixed(2)
  );
  res.status(200).json({ data: dating, status: "success" });
}
function Paginator(items, page, per_page) {
  let current_page = page;
  let perPage = per_page;
  (offset = (current_page - 1) * perPage),
    (paginatedItems = items.slice(offset).slice(0, perPage)),
    (total_pages = Math.ceil(items.length / perPage));
  return {
    page: Number(current_page),
    perPage: Number(perPage),
    pre_page: Number(current_page) - 1 ? Number(current_page) - 1 : null,
    next_page:
      total_pages > Number(current_page) ? Number(current_page) + 1 : null,
    total: items.length,
    total_pages: total_pages,
    data: paginatedItems,
  };
}
async function onDateFormat(d) {
  let dateField = new Date(String(d));
  let month = dateField.getMonth() + 1;
  month = String(month).length == 1 ? `0${String(month)}` : String(month);
  let date = dateField.getDate();
  date = String(date).length == 1 ? `0${String(date)}` : String(date);
  let year = dateField.getFullYear();
  return `${date}/${month}/${year}`;
}

module.exports = {
  createMaster: createMaster,
  getMaster: getMaster,
  updateMaster: updateMaster,
  getDisplayId: getDisplayId,
  dueDateCalculation: dueDateCalculation,
  installmentDueDateCalculation: installmentDueDateCalculation,
};

/*
Program Plan - PRGPLN
Payment Schedule - PSCH
Installment - INST
Late fee - LTFEE
Reminder - REM
*/

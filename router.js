const express = require("express");
const router = express.Router();
var multer = require("multer");
var multerS3 = require("multer-s3");
var AWS = require("aws-sdk");
var storage = multer.memoryStorage()
var upload = multer({ storage: storage })
var storagemulti = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, '/path/to/uploads')
  },
  filename: function (req, file, callback) {
    callback(null, file.fieldname + '-' + Date.now())
  }
});
var uploadmulti = multer({ storage: storage })


//Upload Student Portal
const {
  uploadMaster,
  getFileRecords,
  getUserProfileInfo,
  createUsers,
  getFeeAmount,
  convertJsonArray,
} = require("./controllers/studentPortalController");
//ken42 Upload Student portal
const {
  uploadMasterken42,
  createMasterken42,
  showAllMaster,
  getMasterDetails,
  updateMasterDetails,
  mergeAllData,
  uploadMasterLink,
  addinsttutePplans,
  uploadMasterCSV
} = require("./controllers/studentPortalken42Controller");
// Demand Note

router.get("/check", (req, res) => {
  res.status(200).send({
    status: "success",
    message: "working",
    resource: req.headers.resource,
  });
});


//Multer configuration
AWS.config.update({
  region: process.env.region,
  aws_access_key_id: process.env.accessKeyId,
  aws_secret_access_key: process.env.secretAccessKey,
});

const S3 = new AWS.S3({
  region: process.env.region,
  aws_access_key_id: process.env.accessKeyId,
  aws_secret_access_key: process.env.secretAccessKey,
});

let uploads = multer({
  limits: {
    fileSize: 20000000,
    fieldSize: 20000000,
  },
  storage: multerS3({
    s3: S3,
    bucket: process.env.S3_BUCKET,
    metadata: function (req, file, cb) {
      cb(null, { fieldName: file.fieldname });
    },
    key: function (req, file, cb) {
      if (file.mimetype == "application/json")
        cb(null, "masterken42" + Date.now() + ".json");
      else cb(null, false);
    },
  }),
});

let uploadsken42 = multer({
  limits: {
    fileSize: 20000000,
    fieldSize: 20000000,
  },
  storage: multerS3({
    s3: S3,
    bucket: process.env.S3_BUCKET,
    metadata: function (req, file, cb) {
      console.log("file", file.mimetype);
      cb(null, { fieldName: file.fieldname });
    },
    key: function (req, file, cb) {
      if (file.mimetype == "application/json")
        cb(null, "masterken42" + Date.now() + ".json");
      else if (file.mimetype == "text/csv")
        cb(null, "masterken42" + Date.now() + ".csv");
      else if (file.mimetype == "application/vnd.ms-excel")
        cb(null, "masterken42" + Date.now() + ".csv");
      else cb(null, false);
    },
  }),
});




router.post("/uploadMaster",  upload.single('file'), uploadMaster);
router.get("/getFeeAmount", getFeeAmount);
router.post("/convertJsonArray",convertJsonArray);
//Master Controller
router.post(
  "/uploadMasterken42",
  uploadsken42.fields([{ name: "file" }]),
  uploadMasterken42
);

router.get("/ken42datafetch", uploadMasterLink);
router.post("/ken42datafetchCSV",uploadmulti.array('uploadedImages',12), uploadMasterCSV);

module.exports = router;

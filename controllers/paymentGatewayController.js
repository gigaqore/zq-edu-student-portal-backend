const PaymentGatewayModel = require("../models/PaymentGatewayModel");
const { createDatabase } = require("../utils/db_creation");
const rq = require("request-promise");
const axios = require("axios");
let CryptoJS = require("crypto-js");
const credentialsModelName = "credentials";
const orgListSchema = require("../models/orglists-schema");
const settingsModelName = "settings";
const mongoose = require("mongoose");
const userSettings = require("../utils/helper_jsons/settings.json");
const paymentMappingJSON = require("../payloads/paymentPayloads/paymentPayloadMapping.json");
const paymentLinks = require("../payloads/paymentPayloads/paymentLinks.json");

exports.makePayment = async function (reqBody, dbConnection) {
  let credentialSchema = mongoose.Schema({}, { strict: false });
  const credentialsModel = dbConnection.model(
    credentialsModelName,
    credentialSchema,
    credentialsModelName
  );
  const credentialsData = await credentialsModel.findOne({ type: "payment" });
  const settingsModel = dbConnection.model(
    settingsModelName,
    credentialSchema,
    settingsModelName
  );
  const settingsData = await settingsModel.find({});
  const {
    paymentGateway,
    smsNotification,
    enableReminder,
    emailNotification,
  } = settingsData[0]._doc.paymentGateway;
  let reqBodyData = { ...reqBody };
  switch (paymentGateway) {
    case "razorpay":
      try {
        // const credentials = CryptoJS.AES.decrypt(credentialsData._doc.password, process.env.enc_secret_key).toString(CryptoJS.enc.Utf8);
        const credentials = credentialsData._doc.password;
        let payloadMapper = paymentMappingJSON[paymentGateway];
        reqBodyData = {
          ...reqBodyData,
          amount: Number(reqBodyData.amount + reqBodyData.paisa),
          smsNotification,
          emailNotification,
          enableReminder,
          expirationTimeStamp: Date.now(),
          callbackMethod: "get",
        };
        let payloadData = createPayloadData(reqBodyData, payloadMapper);
        console.log(credentialsData._doc.username, credentials);
        var auth =
          "Basic " +
          Buffer.from(
            credentialsData._doc.userName + ":" + credentials
          ).toString("base64");
        var options = {
          method: "POST",
          uri: paymentLinks[paymentGateway].create,
          body: payloadData,
          json: true,
          headers: {
            "Content-Type": "application/json",
            Authorization: auth,
          },
        };
        return await rq(options)
          .then(async (success) => {
            const paymentModel = dbConnection.model(
              "payments",
              PaymentGatewayModel,
              "payments"
            );
            const paymentData = new paymentModel({
              ...reqBody,
              ...success,
              status: success.status,
              paymentId: success.id,
              amount: Number(reqBody.amount),
            });
            const paymentFinalData = await paymentData.save();
            console.log(paymentFinalData);
            return success;
          })
          .catch((err) => {
            throw err;
          });
      } catch (e) {
        console.log(e);
        throw e;
      }
      break;
    default:
      throw "payment gateway provider not found";
  }
};

exports.getPaymentStatus = async function (req, res) {
  let razorpayId = req.params.id;
  let orgId = req.query.orgId;
  const centralDbConnection = await createDatabase(
    "edu-central",
    process.env.central_mongoDbUrl
  );
  const orgListModel = centralDbConnection.model(
    "orglists",
    orgListSchema,
    "orglists"
  );
  const orgData = await orgListModel.findOne({
    _id: orgId,
  });
  if (!orgData || orgData == null) {
    centralDbConnection.close();
    res.status(500).send({
      status: "failure",
      message: "Organization not found",
    });
  } else {
    let dbConnection = await createDatabase(
      String(orgData._id),
      orgData.connUri
    );
    let credentialSchema = mongoose.Schema({}, { strict: false });
    const credentialModel = dbConnection.model("credentials", credentialSchema);
    const credentialData = await credentialModel.findOne({ type: "payment" });
    var username = credentialData._doc.userName;
    var password = credentialData._doc.password;
    var auth =
      "Basic " + Buffer.from(username + ":" + password).toString("base64");
    var options = {
      method: "GET",
      uri: "https://api.razorpay.com/v1/payments/" + razorpayId,
      headers: {
        "Content-Type": "application/json",
        Authorization: auth,
      },
    };
    rq(options)
      .then((success) => {
        dbConnection.close();
        res.status(200).json({
          status: "success",
          Data: success,
        });
      })
      .catch((err) => {
        dbConnection.close();
        res.status(400).json({ Message: "Failed", Error: err });
        return;
      });
  }
};

function createPayloadData(payload, mapper) {
  let payloadData = {};
  function createPayload(data, mapping, key) {
    Object.keys(mapping).forEach((item) => {
      if (typeof mapping[item] == "object") {
        payloadData[item] = {};
        createPayload(data, mapping[item], item);
      } else {
        if (key) payloadData[key][item] = data[mapping[item]];
        else payloadData[item] = data[mapping[item]];
      }
    });
  }
  createPayload(payload, mapper);
  return payloadData;
}

const transactionsSchema = require("../models/transactionsModel");
const feesLedgerSchema = require("../models/feesLedgerModel");
const StudentSchema = require("../models/studentModel");
const GuardianSchema = require("../models/guardianModel");
const StudentFeeMapSchema = require("../models/studentFeeMapModel");
const { createDatabase } = require("../utils/db_creation");
exports.getrefundDetails = async function (req, res) {
  var dbUrl = req.headers.resource;
  let id = req.params.id;
  if (!id || !req.query.orgId) {
    res.status(400).json({
      status: "failed",
      message: "Please provide all required parameters.",
      type: "error",
    });
  } else {
    let dbName = req.query.orgId;
    let dbConnection = await createDatabase(dbName, dbUrl);

    let transactionModel = dbConnection.model(
      "transactions",
      transactionsSchema
    );
    let feeMapModel = dbConnection.model(
      "studentfeesmaps",
      StudentFeeMapSchema
    );
    let ledgerModel = dbConnection.model("feesledgers", feesLedgerSchema);
    let refundData = await ledgerModel.find({
      status: "Refunded",
      primaryTransaction: paymentId,
      transactionSubType: "refund",
      studentRegId: id,
    });
    if (refundData.length == 0) {
      dbConnection.close();
      res.status(400).json({
        status: "failed",
        message: "refunded already for this payment",
      });
    } else {
      var ledgerDetails = await ledgerModel.find({
        $or: [{ status: "Paid" }, { status: "Partial" }],
        studentRegId: id,
        transactionSubType: "feePayment",
      });
    }
  }
};

exports.getPaidDetails = async function (req, res) {
  var dbUrl = req.headers.resource;
  let id = req.params.id;
  if (!id || !req.query.orgId) {
    res.status(400).json({
      status: "failed",
      message: "Please provide all required parameters.",
      type: "error",
    });
  } else {
    let dbName = req.query.orgId;
    let dbConnection = await createDatabase(dbName, dbUrl);

    let transactionModel = dbConnection.model(
      "transactions",
      transactionsSchema
    );
    let studentModel = dbConnection.model("students", StudentSchema);
    let guardianModel = dbConnection.model("guardians", GuardianSchema);

    let feeMapModel = dbConnection.model(
      "studentfeesmaps",
      StudentFeeMapSchema
    );
    let studentDetails = await studentModel.findOne({
      regId: id,
    });
    let demandNoteDetails = await transactionModel.findOne({
      regId: id,
    });
    if (studentDetails) {
      var studentFeeMapDetails = await feeMapModel.find({
        studentId: studentDetails._id,
      });
      let ledgerModel = dbConnection.model("feesledgers", feesLedgerSchema);
      let feeLedgerD = await ledgerModel.find({
        $or: [{ status: "Paid" }, { status: "Partial" }],
        transactionSubType: "feePayment",
        studentRegId: id,
      });
      var allRemainLedger = [];
      for (ledgerD of feeLedgerD) {
        let allLDetails = await ledgerModel.findOne({
          primaryTransaction: ledgerD.transactionDisplayName,
          transactionSubType: "refund",
        });
        let demandNoteDetails = await transactionModel.findOne({
          displayName: ledgerD.primaryTransaction,
        });

        if (allLDetails == null) {
          let obj = {
            demandNote: demandNoteDetails,
            paymentDetails: ledgerD,
          };
          allRemainLedger.push(obj);
        }
      }
      if (allRemainLedger.length == 0) {
        dbConnection.close();
        res
          .status(404)
          .json({ status: "failed", message: "No receipt for this student" });
      }

      let guardianDetails = await guardianModel.findOne({
        _id: studentDetails.guardianDetails[0],
      });

      if (feeLedgerD.length == 0) {
        dbConnection.close();
        res
          .status(404)
          .json({ status: "failed", message: "Payment not yet received" });
      } else {
        let obj = {
          payment: allRemainLedger,
          guardianDetails: guardianDetails,
          studentDetails: studentDetails,
          studentFeeMapDetails: studentFeeMapDetails,
        };
        dbConnection.close();
        res.status(200).json({ status: "success", data: obj });
      }
    } else {
      dbConnection.close();
      res.status(404),
        json({ status: "failed", message: "student data not found" });
    }

    // let feeLedgerData = [];
    // for (oneLedger of transactionDetails) {
    //   let ledgerModel = dbConnection.model("feesledgers", feesLedgerSchema);
    //   let feeLedgerD = await ledgerModel.find({
    //     $or: [{ status: "Paid" }, { status: "Partial" }],
    //     primaryTransaction: oneLedger.displayName,
    //     transactionSubType: "feePayment",
    //     studentRegId: oneLedger.studentRegId,
    //   });
    //   let studentDetails = await studentModel.findOne({
    //     _id: oneLedger.studentId,
    //   });
    //   let guardianDetails = await guardianModel.findOne({
    //     _id: studentDetails.guardianDetails[0],
    //   });

    //   if (feeLedgerD.length == 0) {
    //     let obj = {
    //       status: "failed",
    //       message: "Payment not yet received",
    //     };
    //     // feeLedgerData.push(obj);
    //     return res.status(404).json(obj);
    //   } else {
    //     // for (ledg of feeLedgerD) {
    //     let obj = {
    //       demandNote: oneLedger,
    //       payment: feeLedgerD,
    //       guardianDetails: guardianDetails,
    //       studentDetails: studentDetails,
    //       studentFeeMapDetails: studentFeeMapDetails,
    //     };
    //     feeLedgerData.push(obj);
    //     // }
    //   }
    // }
    // res.status(200).json(feeLedgerData);
  }
};

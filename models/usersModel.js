const mongoose = require("mongoose");
const usersSchema = new mongoose.Schema(
    {
        payment_Amt: {
            type: String,
            required: true
        },
        payment_Exp_date: {
            type: String,
            required: true
        },
        firstName:{
            type: String,
            required: true
        },
        lastName: {
            type: String,
            required: false
        },
        institute: {
            type: String,
            required: true
        },
        organiZation:{
            type: String,
            required: true
        },
        orgId: {
            type: mongoose.Types.ObjectId,
            required: true
        },
        gstin: {
            type: String,
            required: false
        },
        PAN: {
            type: String,
            required: false
        },
        account_Status: {
            type: String,
            required: true
        },
        inviteUrl:{
            type: String,
            required: true
        },
        userName:{
            type: String,
            required: true
        },
        password:{
            type: String,
            required: false
        },
        last_Login:{
            type: String,
            required: false
        },
        campusId: {type:mongoose.Types.ObjectId, required:false},
        role:{
            type: String,
            required: true
        },
        register_date:{
            type: String,
            required: true
        }
    },
    { timestamps: true }, { _id: false }
);
mongoose.set("useFindAndModify", false);
usersSchema.pre("update", function () {
    this.update({}, { $set: { updatedAt: new Date() } });
});
module.exports = usersSchema

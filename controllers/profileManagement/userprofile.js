const orgListSchema = require("../../models/orglists-schema");
const { createDatabase } = require("../../utils/db_creation");
const mongoose = require("mongoose");

module.exports.getUserProfile = async (req, res) => {
  const { resource, orgId } = req.headers;
  let dbConnectionp = await createDatabase(orgId, resource);
  const settingsSchema = mongoose.Schema({}, { strict: false });
  const userProfileModel = dbConnectionp.model(
    "settings",
    settingsSchema,
    "settings"
  );
  userProfileModel.find({}, async function (err, data) {
    if (err) {
      dbConnectionp.close();
      res.status(400).send({
        status: "failure",
        message: "could not get user profile",
        data: err.toString(),
      });
    } else {
      dbConnectionp.close();
      if (data.length > 0) {
        let result = {
          instituteName: data[0]._doc.instituteDetails.instituteName,
          logo: data[0]._doc.logo,
          orgId: orgId,
        };
        res.status(200).send({
          status: "success",
          message: "user profile data",
          data: result,
        });
      } else {
        dbConnectionp.close();
        let result = {
          instituteName: null,
          logo: null,
          orgId: orgId,
        };
        res.status(200).send({
          status: "success",
          message: "user profile data",
          data: result,
        });
      }
    }
  });
  // res.status(200).send({ orgId });
};

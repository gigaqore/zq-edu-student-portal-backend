const mongoose = require("mongoose");
const feeplanInstallmentschema = new mongoose.Schema(
    {
        feePlanId: { type: mongoose.Types.ObjectId },
        label: { type: String },
        title: {type: String},
        description: { type: String },
        studentRegId: {type: String},
        dueDate: { type: String },
        lateFeeStartDate: { type: String },
        percentage: { type: Number },
        totalAmount: { type: Number },
        academicYear: {type: String},
        term: {type:Number},
        year: {type: Number},
        plannedAmount: { type: Number },
        plannedAmountBreakup: { type: Object },
        paidAmount: { type: Number },
        paidAmountBreakup: { type: Object },
        pendingAmount: { type: Number },
        pendingAmountBreakup: { type: Object },
        discountType: { type: String },
        discountPercentage: { type: Number },
        discountAmount: { type: Number },
        discountAmountBreakup: { type: Array },
        status: { type: String },
        transactionId: { type: String },
        remarks: { type: Object },
        campusId: {type:String},
    },
    { timestamps: true },
    { _id: false }

)


module.exports = feeplanInstallmentschema;
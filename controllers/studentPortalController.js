const StudentSchema = require("../models/studentModel");
const { createDatabase } = require("../utils/db_creation");
const masterUploadSchema = require("../models/masterUploadModel");
const settingsSchema = require("../models/settings/feesetting")
const settingsSchemawithversion = require("../models/settings-model")
var _ = require("lodash");
// var moment = require("moment");
const moment = require('moment-timezone')

const csvtojson = require("csvtojson");
const instituteDetailsSchema = require("../models/instituteDetailsModel")
const FeeTypeSchema = require("../models/feeTypeModel");
// const StudentSchema = require("../models/studentModel");
const ProgramPlanSchema = require("../models/programPlanModel");
const statecodes = require('../helper_jsons/stateCodes')
const FeeManagerSchema = require("../models/feesManagerModel");
const FeeStructureSchema = require("../models/feeStructureModel");
// const { getDisplayId } = require("./displayIdController");
const StudentFeeMapSchema = require("../models/studentFeeMapModel");
const paymentScheduleSchema = require("../models/paymentScheduleModel");
const ReminderScheduleSchema = require("../models/reminderModel");
const GuardianSchema = require("../models/guardianModel");
const OrgListSchema = require("../models/orglists-schema");
const CategorySchema = require("../models/categoryModel");
const LateFeesSchema = require("../models/lateFeeModel");
const ConcessionSchema = require("../models/concessionModel");
const InstallmentSchema = require("../models/installmentModel");
const bankDetailsSchema = require("../models/bankDetailsModel");
const templateversion = require("../config/templateVersion");
const transactionsSchema = require("../models/transactionsModel");
const feesLedgerSchema = require("../models/feesLedgerModel");
const usersSchema = require("../models/usersModel");
const campusSchema = require("../models/campusModel");
// const { createOtcPayment } = require("./transactions/otcController");
const journeysSchema = require("../models/journeyModel");
const feeplanschema = require("../models/feeplanModel");
const feeplanInstallmentschema = require("../models/feeplanInstallment");
const orgListSchema = require("../models/orglists-schema");


const programPlanSchema = require("../models/programPlanModel");
const reconciliationTransactionsSchema = require("../models/reconciliationTransactionsModel");

const mongoose = require("mongoose");
// var AWS = require("aws-sdk");
let axios = require("axios")
const PubNub = require("pubnub");
var jsonDiff = require('json-diff')
const fs = require('fs');
// const { ConfigurationServicePlaceholders } = require("aws-sdk/lib/config_service_placeholders");
// var pubnub = new PubNub({
//     subscribeKey: "sub-c-982dbaba-1d98-11ea-8c76-2e065dbe5941",
//     publishKey: "pub-c-87ae3cc8-8d0a-40e0-8e0f-dbb286306b21",
//     secretKey: "sec-c-ODRhYWJjZmYtZGQ0MS00ZjY2LTkzMGMtY2VhNGZhYjYzOWRi",
//     ssl: false,
// });
var pubnub = new PubNub({
    subscribeKey: "sub-c-40815e58-bc97-11eb-9c3c-fe487e55b6a4",
    publishKey: "pub-c-2d5b6cbe-9af0-4733-be3e-90aad2cd9485",
    secretKey: "sec-c-ZDQ2OTI0MzAtMDllMS00NTQ2LTg5NmQtMDM4YzU3OTAxZDhj",
    ssl: false,
});


//Multer configuration

module.exports.uploadMaster1 = async (req, res) => {
    let dbConnectionp = await createDatabase(req.query.orgId, req.query.resource);
    let transactionModel = dbConnectionp.model("transactions", transactionsSchema);
    let trdata = await transactionModel.find({})
    console.log(trdata.length)
    for (let i = 0; i < trdata.length; i++) {
        let receiptid = trdata[i]._doc.receiptNo
        receiptid = receiptid.replace("2020-21", "2021-22")
        await transactionModel.updateOne({ receiptNo: trdata[i]._doc.receiptNo }, { $set: { receiptNo: receiptid } })
        if (i + 1 == trdata.length) {
            console.log("receiptNo updated")
        }
    }
}

module.exports.uploadMaster = async (req, res) => {
    // console.log(req.headers)
    let response = req.file;
    let date = new Date()
    var current = date.getFullYear();
    var prev = Number(date.getFullYear()) + 1;
    prev = String(prev).substr(String(prev).length - 2);
    finYear = `${current}-${prev}`;
    let result = { instituteDetails: {}, programPlans: [], feeManagers: [], feeStructures: [], feeTypes: [], studentDetails: [], reminderPlan: [], installmentPlan: [], studentFeeMaps: [] }
    //console.log("req",response.file[0].key)
    // let dbConnection = await createDatabase("edu-central", process.env.central_mongoDbUrl);
    // const instituteModel = dbConnection.model("orglists", OrgListSchema);
    console.log(req.query.orgId, req.query.resource)
    // let dbConnectionp = await createDatabase(req.query.orgId, "mongodb+srv://admin:R6BbEn8UUJjoeDQq@mongo-cluster.orkv6.mongodb.net");
    let dbConnectionp = await createDatabase(req.query.orgId, req.query.resource);

    // let feeInstallmentPlanModel = dbConnectionp.model("studentfeeinstallmentplans", feeplanInstallmentschema);
    // await feeInstallmentPlanModel.updateMany({percentage:60}, {$set:{"plannedAmountBreakup.0.title":"Term 1 Fees","pendingAmountBreakup.0.title":"Term 1 Fees","paidAmountBreakup.0.title":"Term 1 Fees"}})
    // await feeInstallmentPlanModel.updateMany({percentage:40}, {$set:{"plannedAmountBreakup.0.title":"Term 2 Fees","pendingAmountBreakup.0.title":"Term 2 Fees","paidAmountBreakup.0.title":"Term 2 Fees"}})
    // console.log("update",upd)
    if (response == undefined) {
        res.status(400).send({ Message: "Please Upload file" });
    } else {
        let filename = req.query.filename
        let input = JSON.parse(req.file.buffer.toString())
        let stddata = input.apidata
        // let ctemplate = await updateTemplate(stddata, res)
        // console.log("template version status", ctemplate)
        let uidata = input.uidata
        console.log("file info", stddata["Student Details"].length, req.query.orgId, req.query.resource)
        // let academicyear = stddata["Program Plan"][0]["Academic Year"].toString()
        let dbname = req.headers.orgId
        // console.log("dbname", dbname, req.headers.resource)
        // await dbConnectionp.dropDatabase
        // dbConnectionp.close()
        // console.log(we)
        let masterUploadModel = dbConnectionp.model("masteruploads", masterUploadSchema);
        let programPlanSchema = dbConnectionp.model("programplans", ProgramPlanSchema);
        let feeStructureModel = dbConnectionp.model("feestructures", FeeStructureSchema);
        let paymentScheduleModel = dbConnectionp.model("paymentschedules", paymentScheduleSchema);
        let reminderModel = dbConnectionp.model("reminderplans", ReminderScheduleSchema);
        let lateFeeModel = dbConnectionp.model("latefees", LateFeesSchema);
        let installmentModel = dbConnectionp.model("installments", InstallmentSchema);
        let categoryModel = dbConnectionp.model("categoryplans", CategorySchema);
        let concessionModel = dbConnectionp.model("concessionplans", ConcessionSchema);
        let studentModel = dbConnectionp.model("students", StudentSchema);
        let feeManagerSchema = dbConnectionp.model("feemanagers", FeeManagerSchema);
        let feeMapModel = dbConnectionp.model("studentFeesMap", StudentFeeMapSchema);
        let feeTypeModel = dbConnectionp.model("feeTypes", FeeTypeSchema);
        let guardianSchema = dbConnectionp.model("guardian", GuardianSchema);
        let bankdetailsModel = dbConnectionp.model("bankDetails", bankDetailsSchema)
        let states = ""
        // let states = statecodes.options.find(item => item.label.toLowerCase() == stddata["Institute Details"].legalAddress.state.value.toLowerCase())
        let institutionModel = dbConnectionp.model("instituteDetails", instituteDetailsSchema);
        let campusModel = dbConnectionp.model("campuses", campusSchema);

        bankDetails = []
        // for (let j = 0; j < 5; j++) {

        // }
        // for (let i = 0; i < 5; i++) {
        //     let bkey = `bankDetails${i + 1}`
        //     console.log(bkey, stddata["Institute Details"][bkey])
        //     await bankDetails.push({
        //         bankName: stddata["Institute Details"][bkey].bankName.value,
        //         bankAccountName: stddata["Institute Details"][bkey].bankAccountName.value,
        //         bankAccountNumber: stddata["Institute Details"][bkey].bankAccountNumber.value,
        //         bankIFSC: stddata["Institute Details"][bkey].bankIFSC.value,
        //         status: "Active",
        //     })
        // }
        let campuses = []
        for (let k = 0; k < stddata["Campuses"].length; k++) {
            let campid = mongoose.Types.ObjectId()
            let campbankDetails = []
            for (let i = 0; i < 5; i++) {
                if (stddata["Campuses"][k][`Bank Name ${i + 1}`] !== "-") {
                    await bankDetails.push({
                        bankName: stddata["Campuses"][k][`Bank Name ${i + 1}`],
                        bankAccountName: stddata["Campuses"][k][`Bank Account Name ${i + 1}`],
                        bankAccountNumber: stddata["Campuses"][k][`Bank Accunt Number ${i + 1}`],
                        bankIFSC: stddata["Campuses"][k][`Bank IFSC ${i + 1}`],
                        campusId: campid,
                        status: "Active",
                    })
                }
            }
            let campdata = stddata["Campuses"][k]
            let campusdetail = {
                _id: campid,
                campusId: `CAMP_${finYear}_${(Number(k) + 1).toString().length == 1 ? "00" : (Number(k) + 1).toString().length == 2 ? "0" : ""
                    }${Number(k) + 1}`,
                headaId: campdata["Institute ID"],
                displayName: campdata["Display Name"],
                name: campdata["Legal Name"],
                legalName: campdata["Legal Name"],
                dateOfRegistration: campdata["Date Of Registration"],
                legalAddress: {
                    address1: campdata["Address 1"],
                    address2: campdata["Address 2"],
                    address3: campdata["Address 3"],
                    city: campdata["City/Town"],
                    state: campdata["State"],
                    country: campdata["Country"],
                    pincode: campdata["Pincode"],
                },
                financialDetails: {
                    GSTIN: campdata["GSTIN"],
                    PAN: campdata["PAN"]
                },
                logo: campdata["Logo"],
                bankDetails: [
                    {
                        bankName: campdata["Bank Name 1"],
                        bankAccountName: campdata["Bank Account Name 1"],
                        bankAccountNumber: campdata["Bank Accunt Number 1"],
                        bankIFSC: campdata["Bank IFSC 1"],
                        status: "Active",
                    },
                    {
                        bankName: campdata["Bank Name 2"],
                        bankAccountName: campdata["Bank Account Name 2"],
                        bankAccountNumber: campdata["Bank Accunt Number 2"],
                        bankIFSC: campdata["Bank IFSC 2"],
                        status: "Active",
                    },
                    {
                        bankName: campdata["Bank Name 3"],
                        bankAccountName: campdata["Bank Account Name 3"],
                        bankAccountNumber: campdata["Bank Accunt Number 3"],
                        bankIFSC: campdata["Bank IFSC 3"],
                        status: "Active",
                    },
                    {
                        bankName: campdata["Bank Name 4"],
                        bankAccountName: campdata["Bank Account Name 4"],
                        bankAccountNumber: campdata["Bank Accunt Number 4"],
                        bankIFSC: campdata["Bank IFSC 4"],
                        status: "Active",
                    },
                    {
                        bankName: campdata["Bank Name 5"],
                        bankAccountName: campdata["Bank Account Name 5"],
                        bankAccountNumber: campdata["Bank Accunt Number 5"],
                        bankIFSC: campdata["Bank IFSC 5"],
                        status: "Active",
                    }
                ],
                instituteContact: [{
                    contactname: campdata["Institute Contact Name 1 *"],
                    designation: campdata["Designation 1"],
                    department: campdata["Department 1"],
                    emailAddress: campdata["Email Address 1 *"],
                    phoneNumber: campdata["Phone Number 1"],
                    mobileNumber: campdata["Mobile Number 1"]
                },
                {
                    contactname: campdata["Institute Contact Name 2"],
                    designation: campdata["Designation 2"],
                    department: campdata["Department 2"],
                    emailAddress: campdata["Email Address 2"],
                    phoneNumber: campdata["Phone Number 2"],
                    mobileNumber: null,
                }
                ],
                headApprover: [{
                    type: Object, required: true,
                    headApproverName: campdata["Head Approver Name"],
                    designation: campdata["Head Approver Designation"],
                    emailAddress: campdata["Head Approver Email"],
                    phoneNumber: campdata["Head Approver Phone Number"],
                    mobileNumber: campdata["Head Approver Mobile Number"]
                }]
            }
            let newcampus = new campusModel(campusdetail)
            await campuses.push(campusdetail)
            await newcampus.save()
        }
        let instituteDetail = {
            legalName: stddata["Institute Details"].legalName.legalName.value,
            organizationType: "Trust",
            dateOfRegistration: stddata["Institute Details"].legalName.dateOfRegistration.value,
            legalAddress: {
                address1: stddata["Institute Details"].legalAddress.address1.value,
                address2: stddata["Institute Details"].legalAddress.address2.value,
                address3: stddata["Institute Details"].legalAddress.address3.value,
                city: stddata["Institute Details"].legalAddress.city.value,
                state: stddata["Institute Details"].legalAddress.state.value,
                country: stddata["Institute Details"].legalAddress.country.value,
                pincode: stddata["Institute Details"].legalAddress.pincode.value,
            },
            financialDetails: {
                GSTIN: stddata["Institute Details"]["gstPan"].GSTIN.value,
                PAN: stddata["Institute Details"]["gstPan"].PAN.value
            },
            bankDetails: bankDetails,
            instituteContact: [{
                contactname: stddata["Institute Details"].instituteContactDetail1.contactname.value,
                designation: stddata["Institute Details"].instituteContactDetail1.designation.value,
                department: stddata["Institute Details"].instituteContactDetail1.department.value,
                emailAddress: stddata["Institute Details"].instituteContactDetail1.emailAddress.value,
                phoneNumber: stddata["Institute Details"].instituteContactDetail1.phoneNumber.value,
                mobileNumber: stddata["Institute Details"].instituteContactDetail1.mobileNumber.value
            },
            {
                contactname: stddata["Institute Details"].instituteContactDetail2.contactname.value,
                designation: stddata["Institute Details"].instituteContactDetail2.designation.value,
                department: stddata["Institute Details"].instituteContactDetail2.department.value,
                emailAddress: stddata["Institute Details"].instituteContactDetail2.emailAddress.value,
                phoneNumber: stddata["Institute Details"].instituteContactDetail2.phoneNumber.value,
                // mobileNumber: stddata["Institute Details"].instituteContactDetail2.mobileNumber.value
            }
            ]
        }

        // console.log("bankdetails",bankDetails)
        await bankdetailsModel.insertMany(bankDetails)
        newinst = new institutionModel(instituteDetail)
        await newinst.save()
        result.instituteDetails = newinst
        result.feeTypes = stddata["Fee Types"]
        // result.paymentSchedule = stddata["Payment Schedules"]
        // result.reminderPlan = stddata["Reminder Plan"]
        result.lateFeePlan = stddata["Late Fee Plans"]
        // result.installmentPlan = stddata["Installment plan"]
        result.categoryPlan = stddata["Categories"]
        result.cencessionPlan = stddata["Concession Plans"]
        result.programPlans = stddata["Program Plans"]
        result.studentFeeMaps = stddata["Students Fee Mapping"]
        let festrdata = []
        let fmang = []
        let stds = []
        let rmlans = []
        let inplans = []
        let paymentshedules = []
        stddata["Payment Schedules"].forEach(async function (item) {
            item["Percentage"] = item["Percentage"].split(",")
            paymentshedules.push(item)
        })
        stddata["Fee Structures"].forEach(async function (item) {
            item["Fee Types *"] = item["Fee Types *"].split(",")
            festrdata.push(item)
        })
        stddata["Fee Inventory"].forEach(async function (item) {
            item["Program Plan ID *"] = item["Program Plan ID *"].split(",")
            item["Fee Type ID *"] = item["Fee Type ID *"].split(",")
            fmang.push(item)
        })
        stddata["Student Details"].forEach(async function (item) {
            item["feeStructure"] = item["Fee Structure ID *"]
            delete item["Fee Structure ID *"]
            stds.push(item)
        })
        stddata["Reminder Plans"].forEach(async function (item) {
            item["Other Reminders"] = item["Other Reminders"].split(",")
            item["nor"] = item["No. of Reminders"]
            delete item["No. of Reminders"]
            rmlans.push(item)
        })
        stddata["Installment Plans"].forEach(async function (item) {
            item["Percentage Breakup"] = item["Percentage Breakup"].split(",")
            item["noi"] = item["No. of Installments"]
            delete item["No. of Installments"]
            inplans.push(item)
        })
        result.paymentSchedule = paymentshedules
        result.feeStructures = festrdata
        result.feeManagers = fmang
        result.studentDetails = stds
        result.reminderPlan = rmlans
        result.installmentPlan = inplans
        let masters = await masterUploadModel.find({})
        let urmplans = []
        let uinplans = []
        uidata["Reminder Plans"].forEach(async function (item) {
            item["Number of Reminders"] = item["No. of Reminders"]
            delete item["No. of Reminders"]
            urmplans.push(item)
        })
        uidata["Installment Plans"].forEach(async function (item) {
            item["Number of Installments"] = item["No. of Installments"]
            delete item["No. of Installments"]
            uinplans.push(item)
        })
        uidata["Reminder Plan"] = urmplans
        uidata["Installment Plan"] = uinplans
        let masteruploadData = {
            fileName: filename,
            uploadedFileName: req.file.originalname,
            fileSize: `${req.file.buffer.length * 0.001} KB`,
            totalRecords: stddata["Student Details"].length,
            totalStudents: stddata["Student Details"].length,
            totalFeetypes: stddata["Fee Types"].length,
            totalFeeStructures: stddata["Fee Structures"].length,
            totalPrograPlans: stddata["Program Plans"].length,
            totalFeeManagers: stddata["Fee Inventory"].length,
            data: result,
            uidata: uidata,
            version: 1
        }
        let newMasterUpload = new masterUploadModel(masteruploadData)
        // if (masters.length > 0) {
        //     // res.status(200).send({ status: "success", message: "Student Data Already Exists", cause: "data exists" })
        //     let mastId = masters[0]._id
        //     let ppdatas = await programPlanSchema.find({})
        //     let fstrdata = await feeStructureModel.find({})
        //     let paysch = await paymentScheduleModel.find({})
        //     let remdata = await reminderModel.find({})
        //     let latefdata = await lateFeeModel.find({})
        //     let installmdata = await installmentModel.find({})
        //     let catgdata = await categoryModel.find({})
        //     let concessdata = await concessionModel.find({})
        //     let stdsdata = await studentModel.find({})
        //     let feemandata = await feeManagerSchema.find({})
        //     let feemapdata = await feeMapModel.find({})
        //     let feetdata = await feeTypeModel.find({})
        //     let guarddata = await guardianSchema.find({})
        //     let insts = await institutionModel.find({})
        //     let inputData = req.headers
        //     let instName = stddata["Institute Details"].legalName.legalName.value
        //     let doc = [newMasterUpload]
        //     //   return res.status(200).json({ status: "success", data: doc });
        //     let pubnubConfig = {
        //         channel: inputData.orgId.toString(),
        //         message: {
        //             description: { message: "Setup Initiated", data: 0 },
        //             status: 0,
        //         },
        //     };
        //     let feeType = doc[0]["data"]["feeTypes"];
        //     let feeStructure = doc[0]["data"]["feeStructures"];
        //     let paymentSchedule = doc[0]["data"]["paymentSchedule"];
        //     let reminderPlan = doc[0]["data"]["reminderPlan"];
        //     let lateFeePlan = doc[0]["data"]["lateFeePlan"];
        //     let installmentPlan = doc[0]["data"]["installmentPlan"];
        //     let categoryPlan = doc[0]["data"]["categoryPlan"];
        //     let cencessionPlan = doc[0]["data"]["cencessionPlan"];
        //     let programPlanDatas = doc[0]["data"]["programPlans"];
        //     let feeManagerDatas = doc[0]["data"]["feeManagers"];
        //     let studentDetails = doc[0]["data"]["studentDetails"];
        //     //   let displayId = await getDisplayId("feeTypes", dbName, dbUrl);
        //     newftypes = []
        //     oldftypes = []
        //     let fcount = feetdata.length
        //     for (let i = 0; i < feeType.length; i++) {
        //         let ft = feetdata.find(item => item.regId == feeType[i]["Fee Id *"])
        //         if (ft) {
        //             var newFeeTypes = {
        //                 displayName: ft.displayName,
        //                 title: feeType[i]["Fee Type *"],
        //                 description: feeType[i]["Description"],
        //                 refId: feeType[i]["Fee Id *"],
        //                 createdBy: instName,
        //             };
        //             oldftypes.push(newFeeTypes)
        //             await feeTypeModel.findOneAndUpdate({ _id: ft._id }, newFeeTypes, { new: true })
        //         }
        //         else {
        //             fcount++
        //             var newFeeTypes = {
        //                 displayName: `FT${
        //                     fcount.toString().length == 1 ? "00" : fcount.toString().length == 2 ? "0" : ""
        //                     }${Number(fcount)}`,
        //                 title: x["Fee Type *"],
        //                 description: x["Description"],
        //                 refId: x["Fee Id *"],
        //                 createdBy: instName,
        //             };
        //             newftypes.push(newFeeTypes)
        //         }
        //     }
        //     await feeTypeModel.insertMany(newftypes);
        //     pubnubConfig.message.description = {
        //         message: `Fee Type has been added successfully.`,
        //     };
        //     pubnubConfig.message.status = 8;
        //     await pubnub.publish(pubnubConfig);

        //     //Fee Structure Add
        //     var allFeeStructure = [];
        //     var alloldFeeStructure = [];
        //     for (let j = 0; j < feeStructure.length; j++) {
        //         let ofs = fstrdata.find(item => item.feeStructureId == feeStructure[j]["id"])
        //         if (ofs) {
        //             var ppInputData = feeStructure[j];
        //             var feeTypeData = [];
        //             for (let ftData = 0; ftData < ppInputData["Fee Types *"].length; ftData++) {
        //                 var ftDet = await feeTypeModel.findOne({
        //                     refId: ppInputData["Fee Types *"][ftData],
        //                 });
        //                 if (ftDet == null) {
        //                     feeTypeData.push(null);
        //                 }
        //                 else {
        //                     feeTypeData.push(ftDet._id);
        //                 }
        //                 // console.log("ftDet", ftDet);
        //             }
        //             if (feeTypeData.includes(null)) {
        //                 dbConnectionp.dropDatabase(function (err, result) {
        //                     if (err) {
        //                         res.send(400).json({
        //                             status: "failure",
        //                             message: "database delete error",
        //                             count: 0,
        //                         })
        //                     }
        //                     else {
        //                         res.status(409).send({ status: "failure", message: `error in fee type id please check the feetype of fees structure ${feeStructure[j]["id"]}` })
        //                     }
        //                 })
        //             } else {
        //                 var ppData = {
        //                     displayName: ofs.displayName,
        //                     title: ppInputData["Title *"],
        //                     description: ppInputData["Description"],
        //                     feeTypeIds: feeTypeData,
        //                     createdBy: instName,
        //                 };
        //                 await feeStructureModel.findOneAndUpdate({ _id: ofs._id }, ppData, { new: true })
        //             }
        //         }
        //         else {
        //             var ppInputData = feeStructure[j];
        //             var feeTypeData = [];
        //             for (
        //                 let ftData = 0;
        //                 ftData < ppInputData["Fee Types *"].length;
        //                 ftData++
        //             ) {
        //                 var ftDet = await feeTypeModel.findOne({
        //                     refId: ppInputData["Fee Types *"][ftData],
        //                 });
        //                 if (ftDet == null) {
        //                     feeTypeData.push(null);
        //                 }
        //                 else {
        //                     feeTypeData.push(ftDet._id);
        //                 }
        //                 // console.log("ftDet", ftDet);

        //             }
        //             if (feeTypeData.includes(null)) {
        //                 dbConnectionp.dropDatabase(function (err, result) {
        //                     if (err) {
        //                         res.send(400).json({
        //                             status: "failure",
        //                             message: "database delete error",
        //                             count: 0,
        //                         })
        //                     }
        //                     else {
        //                         res.status(409).send({ status: "failure", message: `error in fee type id please check the feetype of fees structure ${feeStructure[j]["id"]}` })
        //                     }
        //                 })
        //             } else {
        //                 var ppData = {
        //                     displayName: `FS${
        //                         j.toString().length == 1 ? "00" : j.toString().length == 2 ? "0" : ""
        //                         }${Number(j) + 1}`,
        //                     title: ppInputData["Title *"],
        //                     description: ppInputData["Description"],
        //                     feeTypeIds: feeTypeData,
        //                     createdBy: instName,
        //                 };
        //                 allFeeStructure.push(ppData);
        //             }
        //         }
        //     }
        //     if (allFeeStructure.length > 0) {
        //         await feeStructureModel.insertMany(allFeeStructure);
        //         pubnubConfig.message.description = {
        //             message: `fees Structure has been added successfully.`,
        //         };
        //         pubnubConfig.message.status = 16;
        //         await pubnub.publish(pubnubConfig);
        //     }
        //     //Program Plan Add
        //     var allProgramPlan = [];
        //     for (let j = 0; j < programPlanDatas.length; j++) {
        //         let pps = ppdatas.find(item => item.programCode == programPlanDatas[j]["Program code *"])
        //         if (pps) {
        //             var ppInputData = programPlanDatas[j];
        //             var ppData = {
        //                 displayName: pps.displayName,
        //                 programCode: ppInputData["Program code *"],
        //                 title: ppInputData["Program Name *"],
        //                 academicYear: ppInputData["academicYear"],
        //                 description: ppInputData["Description "],
        //                 createdBy: instName,
        //             };
        //             await programPlanSchema.findOneAndUpdate({ _id: pps._id }, ppData, { new: true })
        //         }
        //         else {
        //             var ppInputData = programPlanDatas[j];
        //             var ppData = {
        //                 displayName: `PP${
        //                     j.toString().length == 1 ? "00" : j.toString().length == 2 ? "0" : ""
        //                     }${Number(j) + 1}`,
        //                 programCode: ppInputData["Program code *"],
        //                 title: ppInputData["Program Name *"],
        //                 academicYear: ppInputData["academicYear"],
        //                 description: ppInputData["Description "],
        //                 createdBy: instName,
        //             };
        //             allProgramPlan.push(ppData);
        //         }

        //     }
        //     if (allProgramPlan.length > 0) {
        //         await programPlanSchema.insertMany(allProgramPlan);
        //         pubnubConfig.message.description = {
        //             message: `Program Plan has been added successfully.`,
        //         };
        //         pubnubConfig.message.status = 24;
        //         await pubnub.publish(pubnubConfig);
        //     }
        //     //ReminderPlan Add
        //     var allReminderPlam = [];
        //     for (let j = 0; j < reminderPlan.length; j++) {
        //         let rps = await remdata.find(item => item.reminderPlanId == reminderPlan[i]["Reminder id"])
        //         if (rps) {
        //             var ppInputData = reminderPlan[j];
        //             var ppData = {
        //                 displayName: rps.displayName,
        //                 reminderPlanId: ppInputData["Reminder id"],
        //                 title: ppInputData["Title"],
        //                 description: ppInputData["description"],
        //                 numberOfReminders: ppInputData["nor"],
        //                 scheduleDetails: [
        //                     {
        //                         daysBefore: ppInputData["Days before due date"],
        //                     },
        //                     {
        //                         daysAfter: ppInputData["Days after demand note due date"],
        //                     },
        //                     {
        //                         daysAfter: ppInputData["Days after 1st reminder"],
        //                     },
        //                     {
        //                         daysAfter: ppInputData["Days after 2nd Reminder"],
        //                     },
        //                 ],
        //                 createdBy: instName,
        //             };
        //             allReminderPlam.push(ppData);
        //         }
        //         else {
        //             var ppInputData = reminderPlan[j];
        //             var ppData = {
        //                 displayName: `REM${
        //                     j.toString().length == 1 ? "00" : j.toString().length == 2 ? "0" : ""
        //                     }${Number(j) + 1}`,
        //                 reminderPlanId: ppInputData["Reminder id"],
        //                 title: ppInputData["Title"],
        //                 description: ppInputData["description"],
        //                 numberOfReminders: ppInputData["no of reminders"],
        //                 scheduleDetails: [
        //                     {
        //                         daysBefore: ppInputData["Days before due date"],
        //                     },
        //                     {
        //                         daysAfter: ppInputData["Days after demand note due date"],
        //                     },
        //                     {
        //                         daysAfter: ppInputData["Days after 1st reminder"],
        //                     },
        //                     {
        //                         daysAfter: ppInputData["Days after 2nd Reminder"],
        //                     },
        //                 ],
        //                 createdBy: instName,
        //             };
        //             allReminderPlam.push(ppData);
        //         }

        //     }
        //     if (allReminderPlam.length > 0) {
        //         await reminderModel.insertMany(allReminderPlam);
        //         pubnubConfig.message.description = {
        //             message: `Reminder Plan has been added successfully.`,
        //         };
        //         pubnubConfig.message.status = 40;
        //         await pubnub.publish(pubnubConfig);
        //     }
        //     // await feeTypeModel.updateMany({newftypes});
        //     masteruploadData.version = parseInt(masters[0].version) + 1
        //     newMasterUpload.save(function (err, data) {
        //         if (err) {
        //             return res.status(500).json({
        //                 message: `database upation failed`,
        //                 type: "failure",
        //                 data: err.toString(),
        //             });
        //         } else {
        //             newinst.save(async function (err3, data3) {
        //                 if (err3) {
        //                     return res.status(500).json({
        //                         message: `database updation failed`,
        //                         type: "failure",
        //                         data: err1.toString(),
        //                     });
        //                 } else {
        //                     res.status(201).send({ status: "success", message: "Excel Uploaded Successfully", data: newMasterUpload })

        //                     // let input2 = req.headers
        //                     // console.log(input2)
        //                     // init(input2, masterUploadModel, dbConnectionp).then(studentinitialize => {
        //                     //     console.log("studentinitialize", studentinitialize)
        //                     // res.status(201).send({ status: "success", message: "Excel Uploaded Successfully", data: newMasterUpload })
        //                     // }).catch(function (err) {
        //                     //     res.status(500).send({
        //                     //         status: "failure",
        //                     //         message: "Failed to Create Student Mapping Setup",
        //                     //         data: req.body,
        //                     //         cause: err,
        //                     //     });
        //                     // });
        //                 }
        //             })
        //         }
        //     })

        // }
        // else {
        let input2 = { orgId: req.query.orgId, resource: req.query.resource }
        newMasterUpload.save(async function (err, data) {
            // console.log("err", err, "data", data)
            if (err) {
                await dbConnectionp.close()
                return res.status(500).send({
                    message: "Mongoose error",
                    type: "error",
                    cause: err.toString(),
                });
            } else {
                initCheck(input2, masterUploadModel, dbConnectionp, newMasterUpload, stddata["Institute Details"].legalName.legalName.value, result, states, campuses, finYear, res, req.query).then(studentinitialize => {
                    newinst.save(async function (err3, data3) {
                        if (err3) {
                            res.status(500).send({
                                message: "Mongoose error",
                                type: "error",
                                cause: err3.toString(),
                            });
                        } else {
                            console.log("initcheck function executed successfully")
                            try {
                                console.log("fee map created")
                                let credentailsDataValue = {
                                    "userName": "rzp_test_41fqHdVK5H7axO",
                                    "password": "i2v7iT3Y6TuEySilGJOglBwT",
                                    "type": "payment"
                                }
                                console.log("init master done")
                                let instituteDetail = result.instituteDetails;
                                const credentials = mongoose.Schema({}, { strict: false });
                                // const msettingModel = dbConnectionp.model("settings", credentials, "settings")
                                const credentialsModel = dbConnectionp.model("credentials", credentials, "credentials")
                                const settingsModel = dbConnectionp.model("settings", settingsSchema, "settings");
                                // const settingsActualModel = dbConnectionp.model("settingsActual", settingsSchema, 'settingsActual')
                                const msettingsDat = {
                                    "orgName": instituteDetail.legalName,
                                    "logo": "https://supportings.s3.amazonaws.com/hkbk.png",
                                    "address": {
                                        "address": `${instituteDetail.legalAddress.address1}, ${instituteDetail.legalAddress.address2}, ${instituteDetail.legalAddress.address3}`,
                                        "district": instituteDetail.legalAddress.city,
                                        "country": instituteDetail.legalAddress.country,
                                        "pincode": instituteDetail.legalAddress.pincode
                                    },
                                    "email": instituteDetail.instituteContact[0].emailAddress,
                                    "phoneNumber1": instituteDetail.instituteContact[0].phoneNumber,
                                    "phoneNumber2": instituteDetail.instituteContact[1].phoneNumber,
                                    "emailService": {
                                        "provider": "sendgrid",
                                        "sender": "noreply@zenqore.com"
                                    },
                                    "paymentService": {
                                        "provider": "razorpay"
                                    }
                                }
                                // let newmsetting = new msettingModel(msettingsDat)
                                const sanSettingsData = {
                                    "instituteDetails": {
                                        "instituteName": instituteDetail.legalName,
                                        organizationType: "Trust",
                                        "gstin": instituteDetail.financialDetails.GSTIN,
                                        "pan": instituteDetail.financialDetails.PAN,
                                        "address1": instituteDetail.legalAddress.address1,
                                        "address2": instituteDetail.legalAddress.address2,
                                        "address3": instituteDetail.legalAddress.address3,
                                        "cityTown": instituteDetail.legalAddress.city,
                                        "stateName": instituteDetail.legalAddress.state,
                                        "stateCode": states == undefined ? 29 : states.value,
                                        "pinCode": instituteDetail.legalAddress.pincode,
                                        "firstName": instituteDetail.instituteContact[0].contactname,
                                        "lastName": "",
                                        "email": instituteDetail.instituteContact[0].emailAddress,
                                        "phoneNumber1": instituteDetail.instituteContact[0].phoneNumber,
                                        "phoneNumber2": instituteDetail.instituteContact[1].phoneNumber,
                                        "academicYear": null,
                                        "dateFormat": null
                                    },
                                    "logo": {
                                        "logo": "https://supportings.blob.core.windows.net/zenqore-supportings/5fe0667d8c36be3698b32ca4.png"
                                    },
                                    "favicon": {
                                        "favicon": ""
                                    },
                                    "portalLogin": "",
                                    "smsGateway": {
                                        "phoneNumber": null,
                                        "senderName": null,
                                        "apiKey": null
                                    },
                                    "fees": {
                                        "feeMapType": "Program Plan & Date of Join",
                                        "sendReceipt": "After Reconciliation",
                                        "partialAmount": "Yes",
                                        "logos": "Parent on Left"
                                    },
                                    "paymentGateway": {
                                        "paymentGateway": "razorpay",
                                        "accessKey": null,
                                        "secretKey": null
                                    },
                                    "headApprover": [
                                        {
                                            "name": null,
                                            "designation": null,
                                            "email": null,
                                            "phoneNumber": null
                                        }
                                    ],
                                    "bankDetails": null,
                                    "labels": [
                                        {
                                            "regId": "REG ID",
                                            "classBatch": "CLASS/BATCH"
                                        }
                                    ],
                                    "receipts": {
                                        "send": "immediately",
                                        "partialAmount": true,
                                        "feesMapping": "0",
                                        "logos": "0",
                                        "content": "totalAmount",
                                        "demandNoteContent": "1",
                                        "bank": "0"
                                    },
                                    "emailServer": [
                                        {
                                            "emailServer": "sendgrid",
                                            "emailAddress": "noreply@zenqore.com",
                                            "config": [
                                                ""
                                            ],
                                            "fileName": ""
                                        },
                                        {
                                            "emailServer": "sendgrid",
                                            "emailAddress": "",
                                            "config": [
                                                ""
                                            ],
                                            "fileName": ""
                                        },
                                        {
                                            "emailServer": "sendgrid",
                                            "emailAddress": "",
                                            "config": [
                                                ""
                                            ],
                                            "fileName": ""
                                        },
                                        {
                                            "emailServer": "sendgrid",
                                            "emailAddress": "",
                                            "config": [
                                                ""
                                            ],
                                            "fileName": ""
                                        },
                                        {
                                            "emailServer": "sendgrid",
                                            "emailAddress": "",
                                            "config": [
                                                ""
                                            ],
                                            "fileName": ""
                                        }
                                    ],
                                    "label": [
                                        {
                                            "regId": "REG ID",
                                            "classBatch": "CLASS/BATCH"
                                        }
                                    ],
                                    "receipts": {
                                        "send": "immediately",
                                        "partialAmount": true
                                    },
                                    "logoPositions": {
                                        "position": "0"
                                    }
                                }
                                const newsettingsModel = new settingsModel(sanSettingsData);
                                // await settingsDataversion.save();
                                await newsettingsModel.save();
                                // const initialSettingsModel = dbConnectionp.model("settings", credentials, "settings");
                                const credentialData = credentialsModel(credentailsDataValue)
                                await credentialData.save()
                                // await campusModel.insertMany(campuses)
                                console.log("credential saved")
                                await dbConnectionp.close()
                                res.status(201).send({ status: "success", message: "Excel Uploaded Successfully", data: newMasterUpload })

                            }
                            catch (err) {
                                res.json({ status: "failure", message: "reconciliation: " + err.message });
                            }
                        }
                    })
                }).catch(async function (err) {
                    await dbConnectionp.close()
                    res.status(500).send({
                        status: "failure",
                        message: "Failed to Create Student Mapping Setup",
                        data: req.body,
                        cause: err.toString(),
                    });
                });
            }

        })
        // }


    }
};
module.exports.uploadMasterAcademic = async (req, res) => {
    // console.log(req.headers)
    let response = req.file;
    let date = new Date()
    var current = date.getFullYear();
    var prev = Number(date.getFullYear()) + 1;
    prev = String(prev).substr(String(prev).length - 2);
    finYear = `${current}-${prev}`;
    let result = { instituteDetails: {}, programPlans: [], feeManagers: [], feeStructures: [], feeTypes: [], studentDetails: [], reminderPlan: [], installmentPlan: [], studentFeeMaps: [] }
    console.log(req.query.orgId, req.query.resource)
    let dbConnectionp = await createDatabase(req.query.orgId, "mongodb://localhost:27017");
    console.log("after connected")
    console.log(dbConnectionp)
    if (response == undefined) {
        res.status(400).send({ Message: "Please Upload file" });
    } else {
        let filename = req.query.filename
        let input = JSON.parse(req.file.buffer.toString())
        let stddata = input.apidata
        let uidata = input.uidata
        console.log("file info", stddata["Student Details"].length, req.query.orgId, req.query.resource)
        let dbname = req.headers.orgId
        let masterUploadModel = dbConnectionp.model("masteruploads", masterUploadSchema);
        let programPlanSchema = dbConnectionp.model("programplans", ProgramPlanSchema);
        let feeStructureModel = dbConnectionp.model("feestructures", FeeStructureSchema);
        let paymentScheduleModel = dbConnectionp.model("paymentschedules", paymentScheduleSchema);
        let reminderModel = dbConnectionp.model("reminderplans", ReminderScheduleSchema);
        let lateFeeModel = dbConnectionp.model("latefees", LateFeesSchema);
        let installmentModel = dbConnectionp.model("installments", InstallmentSchema);
        let categoryModel = dbConnectionp.model("categoryplans", CategorySchema);
        let concessionModel = dbConnectionp.model("concessionplans", ConcessionSchema);
        let studentModel = dbConnectionp.model("students", StudentSchema);
        let feeManagerSchema = dbConnectionp.model("feemanagers", FeeManagerSchema);
        let feeMapModel = dbConnectionp.model("studentFeesMap", StudentFeeMapSchema);
        let feeTypeModel = dbConnectionp.model("feeTypes", FeeTypeSchema);
        let guardianSchema = dbConnectionp.model("guardian", GuardianSchema);
        let bankdetailsModel = dbConnectionp.model("bankDetails", bankDetailsSchema)
        let states = ""
        let institutionModel = dbConnectionp.model("instituteDetails", instituteDetailsSchema);
        let campusModel = dbConnectionp.model("campuses", campusSchema);

        bankDetails = []
        let campuses = []
        for (let k = 0; k < stddata["Campuses"].length; k++) {
            let campid = mongoose.Types.ObjectId()
            let campbankDetails = []
            for (let i = 0; i < 5; i++) {
                if (stddata["Campuses"][k][`Bank Name ${i + 1}`] !== "-") {
                    await bankDetails.push({
                        bankName: stddata["Campuses"][k][`Bank Name ${i + 1}`],
                        bankAccountName: stddata["Campuses"][k][`Bank Account Name ${i + 1}`],
                        bankAccountNumber: stddata["Campuses"][k][`Bank Accunt Number ${i + 1}`],
                        bankIFSC: stddata["Campuses"][k][`Bank IFSC ${i + 1}`],
                        campusId: campid,
                        status: "Active",
                    })
                }
            }
            let campdata = stddata["Campuses"][k]
            let campusdetail = {
                _id: campid,
                campusId: `CAMP_${finYear}_${(Number(k) + 1).toString().length == 1 ? "00" : (Number(k) + 1).toString().length == 2 ? "0" : ""
                    }${Number(k) + 1}`,
                headaId: campdata["Institute ID"],
                displayName: campdata["Display Name"],
                name: campdata["Legal Name"],
                legalName: campdata["Legal Name"],
                dateOfRegistration: campdata["Date Of Registration"],
                legalAddress: {
                    address1: campdata["Address 1"],
                    address2: campdata["Address 2"],
                    address3: campdata["Address 3"],
                    city: campdata["City/Town"],
                    state: campdata["State"],
                    country: campdata["Country"],
                    pincode: campdata["Pincode"],
                },
                financialDetails: {
                    GSTIN: campdata["GSTIN"],
                    PAN: campdata["PAN"]
                },
                logo: campdata["Logo"],
                bankDetails: [
                    {
                        bankName: campdata["Bank Name 1"],
                        bankAccountName: campdata["Bank Account Name 1"],
                        bankAccountNumber: campdata["Bank Accunt Number 1"],
                        bankIFSC: campdata["Bank IFSC 1"],
                        status: "Active",
                    },
                    {
                        bankName: campdata["Bank Name 2"],
                        bankAccountName: campdata["Bank Account Name 2"],
                        bankAccountNumber: campdata["Bank Accunt Number 2"],
                        bankIFSC: campdata["Bank IFSC 2"],
                        status: "Active",
                    },
                    {
                        bankName: campdata["Bank Name 3"],
                        bankAccountName: campdata["Bank Account Name 3"],
                        bankAccountNumber: campdata["Bank Accunt Number 3"],
                        bankIFSC: campdata["Bank IFSC 3"],
                        status: "Active",
                    },
                    {
                        bankName: campdata["Bank Name 4"],
                        bankAccountName: campdata["Bank Account Name 4"],
                        bankAccountNumber: campdata["Bank Accunt Number 4"],
                        bankIFSC: campdata["Bank IFSC 4"],
                        status: "Active",
                    },
                    {
                        bankName: campdata["Bank Name 5"],
                        bankAccountName: campdata["Bank Account Name 5"],
                        bankAccountNumber: campdata["Bank Accunt Number 5"],
                        bankIFSC: campdata["Bank IFSC 5"],
                        status: "Active",
                    }
                ],
                instituteContact: [{
                    contactname: campdata["Institute Contact Name 1 *"],
                    designation: campdata["Designation 1"],
                    department: campdata["Department 1"],
                    emailAddress: campdata["Email Address 1 *"],
                    phoneNumber: campdata["Phone Number 1"],
                    mobileNumber: campdata["Mobile Number 1"]
                },
                {
                    contactname: campdata["Institute Contact Name 2"],
                    designation: campdata["Designation 2"],
                    department: campdata["Department 2"],
                    emailAddress: campdata["Email Address 2"],
                    phoneNumber: campdata["Phone Number 2"],
                    mobileNumber: null,
                }
                ],
                headApprover: [{
                    type: Object, required: true,
                    headApproverName: campdata["Head Approver Name"],
                    designation: campdata["Head Approver Designation"],
                    emailAddress: campdata["Head Approver Email"],
                    phoneNumber: campdata["Head Approver Phone Number"],
                    mobileNumber: campdata["Head Approver Mobile Number"]
                }]
            }
            await campuses.push(campusdetail)
        }
        let instituteDetail = {
            legalName: stddata["Institute Details"].legalName.legalName.value,
            organizationType: "Trust",
            dateOfRegistration: stddata["Institute Details"].legalName.dateOfRegistration.value,
            legalAddress: {
                address1: stddata["Institute Details"].legalAddress.address1.value,
                address2: stddata["Institute Details"].legalAddress.address2.value,
                address3: stddata["Institute Details"].legalAddress.address3.value,
                city: stddata["Institute Details"].legalAddress.city.value,
                state: stddata["Institute Details"].legalAddress.state.value,
                country: stddata["Institute Details"].legalAddress.country.value,
                pincode: stddata["Institute Details"].legalAddress.pincode.value,
            },
            financialDetails: {
                GSTIN: stddata["Institute Details"]["gstPan"].GSTIN.value,
                PAN: stddata["Institute Details"]["gstPan"].PAN.value
            },
            bankDetails: bankDetails,
            instituteContact: [{
                contactname: stddata["Institute Details"].instituteContactDetail1.contactname.value,
                designation: stddata["Institute Details"].instituteContactDetail1.designation.value,
                department: stddata["Institute Details"].instituteContactDetail1.department.value,
                emailAddress: stddata["Institute Details"].instituteContactDetail1.emailAddress.value,
                phoneNumber: stddata["Institute Details"].instituteContactDetail1.phoneNumber.value,
                mobileNumber: stddata["Institute Details"].instituteContactDetail1.mobileNumber.value
            },
            {
                contactname: stddata["Institute Details"].instituteContactDetail2.contactname.value,
                designation: stddata["Institute Details"].instituteContactDetail2.designation.value,
                department: stddata["Institute Details"].instituteContactDetail2.department.value,
                emailAddress: stddata["Institute Details"].instituteContactDetail2.emailAddress.value,
                phoneNumber: stddata["Institute Details"].instituteContactDetail2.phoneNumber.value,
            }
            ]
        }
        newinst = new institutionModel(instituteDetail)
        // await newinst.save()
        result.instituteDetails = newinst
        result.feeTypes = stddata["Fee Types"]
        result.lateFeePlan = stddata["Late Fee Plans"]
        // result.installmentPlan = stddata["Installment plan"]
        result.categoryPlan = stddata["Categories"]
        result.cencessionPlan = stddata["Concession Plans"]
        result.programPlans = stddata["Program Plans"]
        result.studentFeeMaps = stddata["Students Fee Mapping"]
        let nonexist = []
        // for (let j = 0; j < studentFeeMaps.length; j++) {
        //     let stddata = await studentModel.findOne({regId:studentFeeMaps[j]["USN"]})
        //     if(!stddata){
        //         nonexist.push(studentFeeMaps[j]["USN"].toString())
        //     }
        // }
        let festrdata = []
        let fmang = []
        let stds = []
        let rmlans = []
        let inplans = []
        let paymentshedules = []
        stddata["Payment Schedules"].forEach(async function (item) {
            item["Percentage"] = item["Percentage"].split(",")
            paymentshedules.push(item)
        })
        stddata["Fee Structures"].forEach(async function (item) {
            item["Fee Types *"] = item["Fee Types *"].split(",")
            festrdata.push(item)
        })
        stddata["Fee Inventory"].forEach(async function (item) {
            item["Program Plan ID *"] = item["Program Plan ID *"].split(",")
            item["Fee Type ID *"] = item["Fee Type ID *"].split(",")
            fmang.push(item)
        })
        stddata["Student Details"].forEach(async function (item) {
            item["feeStructure"] = item["Fee Structure ID *"]
            delete item["Fee Structure ID *"]
            stds.push(item)
        })
        stddata["Reminder Plans"].forEach(async function (item) {
            item["Other Reminders"] = item["Other Reminders"].split(",")
            item["nor"] = item["No. of Reminders"]
            delete item["No. of Reminders"]
            rmlans.push(item)
        })
        stddata["Installment Plans"].forEach(async function (item) {
            item["Percentage Breakup"] = item["Percentage Breakup"].split(",")
            item["noi"] = item["No. of Installments"]
            delete item["No. of Installments"]
            inplans.push(item)
        })
        result.paymentSchedule = paymentshedules
        result.feeStructures = festrdata
        result.feeManagers = fmang
        result.studentDetails = stds
        result.reminderPlan = rmlans
        result.installmentPlan = inplans
        let masters = await masterUploadModel.find({})
        let urmplans = []
        let uinplans = []
        uidata["Reminder Plans"].forEach(async function (item) {
            item["Number of Reminders"] = item["No. of Reminders"]
            delete item["No. of Reminders"]
            urmplans.push(item)
        })
        uidata["Installment Plans"].forEach(async function (item) {
            item["Number of Installments"] = item["No. of Installments"]
            delete item["No. of Installments"]
            uinplans.push(item)
        })
        uidata["Reminder Plan"] = urmplans
        uidata["Installment Plan"] = uinplans
        let masteruploadData = {
            fileName: filename,
            uploadedFileName: req.file.originalname,
            fileSize: `${req.file.buffer.length * 0.001} KB`,
            totalRecords: stddata["Student Details"].length,
            totalStudents: stddata["Student Details"].length,
            totalFeetypes: stddata["Fee Types"].length,
            totalFeeStructures: stddata["Fee Structures"].length,
            totalPrograPlans: stddata["Program Plans"].length,
            totalFeeManagers: stddata["Fee Inventory"].length,
            data: result,
            uidata: uidata,
            version: 1
        }
        let newMasterUpload = new masterUploadModel(masteruploadData)
        let input2 = { orgId: req.query.orgId, resource: req.query.resource }
        initUpdate(input2, masterUploadModel, dbConnectionp, newMasterUpload, stddata["Institute Details"].legalName.legalName.value, result, states, campuses, finYear, res).then(async studentinitialize => {
            console.log("initcheck function executed successfully")
            try {
                console.log("fee map created")
                let credentailsDataValue = {
                    "userName": "rzp_test_41fqHdVK5H7axO",
                    "password": "i2v7iT3Y6TuEySilGJOglBwT",
                    "type": "payment"
                }
                console.log("init master done")
                let instituteDetail = result.instituteDetails;
                const credentials = mongoose.Schema({}, { strict: false });
                const credentialsModel = dbConnectionp.model("credentials", credentials, "credentials")
                const settingsModel = dbConnectionp.model("settings", settingsSchema, "settings");
                console.log("credential saved")
                await dbConnectionp.close()
                res.status(201).send({ status: "success", message: "Excel Uploaded Successfully", data: newMasterUpload })

            }
            catch (err) {
                res.json({ status: "failure", message: "reconciliation: " + err.message });
            }
        }).catch(async function (err) {
            await dbConnectionp.close()
            res.status(500).send({
                status: "failure",
                message: "Failed to Create Student Mapping Setup",
                data: req.body,
                cause: err.toString(),
            });
        });



        // }


    }
};

module.exports.updateMaster = async (req, res) => {
    // console.log(req.headers)
    let response = req.file;
    let date = new Date()
    var current = date.getFullYear();
    var prev = Number(date.getFullYear()) + 1;
    prev = String(prev).substr(String(prev).length - 2);
    finYear = `${current}-${prev}`;
    let result = { instituteDetails: {}, programPlans: [], feeManagers: [], feeStructures: [], feeTypes: [], studentDetails: [], reminderPlan: [], installmentPlan: [], studentFeeMaps: [] }
    //console.log("req",response.file[0].key)
    let centralDbConnection;
    let orgId = req.query.orgId;
    // try {
    console.log("before central db connected")
    centralDbConnection = await createDatabase(
        "edu-central",
        process.env.central_mongoDbUrl
    );
    const orgListModel = centralDbConnection.model(
        "orglists",
        orgListSchema,
        "orglists"
    );
    const orgData = await orgListModel.findOne({
        _id: mongoose.Types.ObjectId(orgId),
    });
    console.log("central db connected")
    // let dbConnection = await createDatabase("edu-central", process.env.central_mongoDbUrl);
    // const instituteModel = dbConnection.model("orglists", OrgListSchema);
    console.log(req.query.orgId)
    let dbConnectionp = await createDatabase(orgData._doc._id.toString(),
        orgData._doc.connUri
    );
    dbConnection.close()
    console.log("closed")
    // dbConnection = await createDatabase(
    //   "5faa2d6d83774b0007e6537d",
    //   "mongodb://20.44.39.232:30000"
    // );

    if (response == undefined) {
        res.status(400).send({ Message: "Please Upload file" });
    } else {
        let filename = req.query.filename
        let input = JSON.parse(req.file.buffer.toString())
        let stddata = input.apidata
        // let ctemplate = await updateTemplate(stddata, res)
        // console.log("template version status", ctemplate)
        let uidata = input.uidata
        console.log("file info", stddata["Student Details"].length, req.query.orgId, orgData._doc.connUri)
        // let academicyear = stddata["Program Plan"][0]["Academic Year"].toString()
        let dbname = orgData._doc._id.toString()
        // console.log("dbname", dbname, req.headers.resource)
        // await dbConnectionp.dropDatabase
        // dbConnectionp.close()
        // console.log(we)
        let masterUploadModel = dbConnectionp.model("masteruploads", masterUploadSchema);
        let programPlanSchema = dbConnectionp.model("programplans", ProgramPlanSchema);
        let feeStructureModel = dbConnectionp.model("feestructures", FeeStructureSchema);
        let paymentScheduleModel = dbConnectionp.model("paymentschedules", paymentScheduleSchema);
        let reminderModel = dbConnectionp.model("reminderplans", ReminderScheduleSchema);
        let lateFeeModel = dbConnectionp.model("latefees", LateFeesSchema);
        let installmentModel = dbConnectionp.model("installments", InstallmentSchema);
        let categoryModel = dbConnectionp.model("categoryplans", CategorySchema);
        let concessionModel = dbConnectionp.model("concessionplans", ConcessionSchema);
        let studentModel = dbConnectionp.model("students", StudentSchema);
        let feeManagerSchema = dbConnectionp.model("feemanagers", FeeManagerSchema);
        let feeMapModel = dbConnectionp.model("studentFeesMap", StudentFeeMapSchema);
        let feeTypeModel = dbConnectionp.model("feeTypes", FeeTypeSchema);
        let guardianSchema = dbConnectionp.model("guardian", GuardianSchema);
        let bankdetailsModel = dbConnectionp.model("bankDetails", bankDetailsSchema)
        let states = ""
        // let states = statecodes.options.find(item => item.label.toLowerCase() == stddata["Institute Details"].legalAddress.state.value.toLowerCase())
        let institutionModel = dbConnectionp.model("instituteDetails", instituteDetailsSchema);
        let campusModel = dbConnectionp.model("campuses", campusSchema);

        bankDetails = []
        result.feeTypes = stddata["Fee Types"]
        // result.paymentSchedule = stddata["Payment Schedules"]
        // result.reminderPlan = stddata["Reminder Plan"]
        result.lateFeePlan = stddata["Late Fee Plans"]
        // result.installmentPlan = stddata["Installment plan"]
        result.categoryPlan = stddata["Categories"]
        result.cencessionPlan = stddata["Concession Plans"]
        result.programPlans = stddata["Program Plans"]
        result.studentFeeMaps = stddata["Students Fee Mapping"]
        let festrdata = []
        let fmang = []
        let stds = []
        let rmlans = []
        let inplans = []
        let paymentshedules = []
        stddata["Payment Schedules"].forEach(async function (item) {
            item["Percentage"] = item["Percentage"].split(",")
            paymentshedules.push(item)
        })
        stddata["Fee Structures"].forEach(async function (item) {
            item["Fee Types *"] = item["Fee Types *"].split(",")
            festrdata.push(item)
        })
        stddata["Fee Inventory"].forEach(async function (item) {
            item["Program Plan ID *"] = item["Program Plan ID *"].split(",")
            item["Fee Type ID *"] = item["Fee Type ID *"].split(",")
            fmang.push(item)
        })
        stddata["Student Details"].forEach(async function (item) {
            item["feeStructure"] = item["Fee Structure ID *"]
            delete item["Fee Structure ID *"]
            stds.push(item)
        })
        stddata["Reminder Plans"].forEach(async function (item) {
            item["Other Reminders"] = item["Other Reminders"].split(",")
            item["nor"] = item["No. of Reminders"]
            delete item["No. of Reminders"]
            rmlans.push(item)
        })
        stddata["Installment Plans"].forEach(async function (item) {
            item["Percentage Breakup"] = item["Percentage Breakup"].split(",")
            item["noi"] = item["No. of Installments"]
            delete item["No. of Installments"]
            inplans.push(item)
        })
        result.paymentSchedule = paymentshedules
        result.feeStructures = festrdata
        result.feeManagers = fmang
        result.studentDetails = stds
        result.reminderPlan = rmlans
        result.installmentPlan = inplans
        let masters = await masterUploadModel.find({})
        let urmplans = []
        let uinplans = []
        uidata["Reminder Plans"].forEach(async function (item) {
            item["Number of Reminders"] = item["No. of Reminders"]
            delete item["No. of Reminders"]
            urmplans.push(item)
        })
        uidata["Installment Plans"].forEach(async function (item) {
            item["Number of Installments"] = item["No. of Installments"]
            delete item["No. of Installments"]
            uinplans.push(item)
        })
        uidata["Reminder Plan"] = urmplans
        uidata["Installment Plan"] = uinplans
        let masteruploadData = {
            fileName: filename,
            uploadedFileName: req.file.originalname,
            fileSize: `${req.file.buffer.length * 0.001} KB`,
            totalRecords: stddata["Student Details"].length,
            totalStudents: stddata["Student Details"].length,
            totalFeetypes: stddata["Fee Types"].length,
            totalFeeStructures: stddata["Fee Structures"].length,
            totalPrograPlans: stddata["Program Plans"].length,
            totalFeeManagers: stddata["Fee Inventory"].length,
            data: result,
            uidata: uidata,
            version: 1
        }
        let newMasterUpload = new masterUploadModel(masteruploadData)
        let input2 = { orgId: req.query.orgId, resource: orgData._doc.connUri }
        // newMasterUpload.save(async function (err, data) {
        //     // console.log("err", err, "data", data)
        //     if (err) {
        //         await dbConnectionp.close()
        //         return res.status(500).send({
        //             message: "Mongoose error",
        //             type: "error",
        //             cause: err.toString(),
        //         });
        // } 
        // else {
        let campuses = await campusModel.find({})
        initCheck2(input2, masterUploadModel, dbConnectionp, newMasterUpload, stddata["Institute Details"].legalName.legalName.value, result, states, campuses, finYear, res).then(studentinitialize => {
            newinst.save(async function (err3, data3) {
                if (err3) {
                    res.status(500).send({
                        message: "Mongoose error",
                        type: "error",
                        cause: err3.toString(),
                    });
                } else {
                    console.log("initcheck function executed successfully")
                    try {
                        console.log("New Students saved")
                        await dbConnectionp.close()
                        await centralDbConnection.close()
                        res.status(201).send({ status: "success", message: "Excel Uploaded Successfully", data: newMasterUpload })

                    }
                    catch (err) {
                        res.json({ status: "failure", message: "reconciliation: " + err.message });
                    }
                }
            })
        }).catch(async function (err) {
            await dbConnectionp.close()
            res.status(500).send({
                status: "failure",
                message: "Failed to Create Student Mapping Setup",
                data: req.body,
                cause: err.toString(),
            });
        });
        // }

        // })
        // }


    }
};
module.exports.getFileRecords = async (req, res) => {
    console.log("connuri", req.headers.resource)
    console.log("sdfsfd", req.headers)
    console.log("orgid", req.headers.orgId)
    let dbConnectionp = await createDatabase(req.headers.orgId, req.headers.resource);
    console.log("dbConnectionp", dbConnectionp)
    let masterUploadModel = dbConnectionp.model("masteruploads", masterUploadSchema);
    masterUploadModel.find({}, async function (err, uplloaddata) {
        console.log(uplloaddata)
        if (err) {
            await dbConnectionp.close()
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
            res
                .status(500)
                .send({ status: "failure", Message: "could not get records", cause: err.toString() });
        }
        else if (uplloaddata.length == 0) {
            await dbConnectionp.close()
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
            res
                .status(200)
                .send({ status: "success", Message: "Get uploaded records", data: uplloaddata });
        }
        else {
            await dbConnectionp.close()
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
            res
                .status(200)
                .send({ status: "success", Message: "Get uploaded records", data: uplloaddata });
        }
    })
};
module.exports.getUserProfileInfo = async (req, res) => {
    let dbConnectionp = await createDatabase(req.headers.orgId, req.headers.resource);
    const settingsSchema = mongoose.Schema({}, { strict: false });
    const userProfileModel = dbConnectionp.model("feeSettings", settingsSchema, "feeSettings");
    userProfileModel.find({}, async function (err, data) {
        if (err) {
            res.status(201).send({ status: "failure", message: "could not get user profile", data: err.toString() })
        }
        else {
            console.log("data", data[0]._doc)
            let result = {
                instituteName: data[0]._doc.instituteDetails.instituteName,
                logo: data[0]._doc.logo,
                orgId: req.headers.orgId
            }
            data[0].orgId = req.headers.orgId
            res.status(201).send({ status: "success", message: "user profile data", data: result })
        }
    })
};
async function updateTemplate(stddata, res) {
    // let file = templateversion
    let filepath = "./config/templateVersion.json"
    let result = {
        "version": Number(templateversion.version) + 1,
        "template": {
            "sheets": []
        }
    }
    let count = 0
    for (let key in stddata) {
        let sheetd
        if (count == 0) {
            sheetd = {
                "sheet": key,
                "columns": Object.keys(stddata[key])
            }
        }
        else {
            sheetd = {
                "sheet": key,
                "columns": Object.keys(stddata[key][0])
            }
        }

        result.template.sheets.push(sheetd)
        count++
    }
    let diff = jsonDiff.diff(templateversion.template, result.template)
    if (count == Object.keys(stddata).length && diff !== undefined) {
        fs.writeFile(filepath, JSON.stringify(result), function (err, data) {
            if (err) {
                res.send(err.toString())
            }
            else {
                console.log("updated")
                return { "status": 1 }
            }
        });
    }
    else {
        return { "status": 0 }
    }

}
async function initCheck(req, masterUpladModel, dbConnectionp, newMasterUpload, instName, result, states, campuses, finYear, res, query) {
    let inputData = req;
    let dbName = req.orgId
    // let dbConnection = await createDatabase(dbName, dbUrl);
    // let masterUpladModel = dbConnection.model(
    //     "masteruploads",
    //     masterUploadSchema
    // );
    let programPlanSchema = dbConnectionp.model("programplans", ProgramPlanSchema);
    let feeStructureModel = dbConnectionp.model("feestructures", FeeStructureSchema);
    let paymentScheduleModel = dbConnectionp.model("paymentschedules", paymentScheduleSchema);
    let reminderModel = dbConnectionp.model("reminderplans", ReminderScheduleSchema);
    let lateFeeModel = dbConnectionp.model("latefees", LateFeesSchema);
    let installmentModel = dbConnectionp.model("installments", InstallmentSchema);
    let categoryModel = dbConnectionp.model("categoryplans", CategorySchema);
    let concessionModel = dbConnectionp.model("concessionplans", ConcessionSchema);
    let studentModel = dbConnectionp.model("students", StudentSchema);
    let feeManagerSchema = dbConnectionp.model("feemanagers", FeeManagerSchema);
    let feeMapModel = dbConnectionp.model("studentFeesMap", StudentFeeMapSchema);
    let feeTypeModel = dbConnectionp.model("feeTypes", FeeTypeSchema);
    let guardianSchema = dbConnectionp.model("guardian", GuardianSchema);
    let transactionModel = dbConnectionp.model("transactions", transactionsSchema);
    let feeLedgerModel = dbConnectionp.model("feeledgers", feesLedgerSchema)
    let campusModel = dbConnectionp.model("campuses", campusSchema)
    let transData = await transactionModel.find({})
    let tcount = transData.length
    return new Promise(async function (resolve, reject) {
        doc = [newMasterUpload]
        if (doc) {
            //   return res.status(200).json({ status: "success", data: doc });
            let pubnubConfig = {
                channel: inputData.orgId.toString(),
                message: {
                    description: { message: "Setup Initiated", data: 0 },
                    status: 0,
                },
            };
            let feeType = doc[0]["data"]["feeTypes"];
            let feeStructure = doc[0]["data"]["feeStructures"];
            let paymentSchedule = doc[0]["data"]["paymentSchedule"];
            let reminderPlan = doc[0]["data"]["reminderPlan"];
            let lateFeePlan = doc[0]["data"]["lateFeePlan"];
            let installmentPlan = doc[0]["data"]["installmentPlan"];
            let categoryPlan = doc[0]["data"]["categoryPlan"];
            let cencessionPlan = doc[0]["data"]["cencessionPlan"];
            let programPlanDatas = doc[0]["data"]["programPlans"];
            let feeManagerDatas = doc[0]["data"]["feeManagers"];
            let studentDetails = doc[0]["data"]["studentDetails"];
            let stdfeeMaps = doc[0]["data"]["studentFeeMaps"]
            //   let displayId = await getDisplayId("feeTypes", dbName, dbUrl);
            pubnubConfig.message.description = {
                message: `Institute Details has been added successfully.`,
                current: "Institute Details",
                next: "Fee Type"
            };
            pubnubConfig.message.status = 2;
            // await pubnub.publish(pubnubConfig);
            var feeTypeDetails = []
            let i = 0
            let cc = 0;
            for (let i = 0; i < feeType.length; i++) {
                // console.log(i)
                let x = feeType[i];
                console.log(x["Campus ID"])
                if (x["Campus ID"].toLowerCase() == "all") {
                    let campid
                    campuses.forEach(async function (ktm) {
                        cc++
                        campid = ktm["_id"]
                        var newFeeTypes = {
                            _id: mongoose.Types.ObjectId(),
                            displayName: `FT_${finYear}_${(Number(cc)).toString().length == 1 ? "00" : (Number(cc)).toString().length == 2 ? "0" : ""
                                }${Number(cc)}`,
                            refid: x["ID *"],
                            title: x["Fee Type *"],
                            description: x["Description"],
                            roleView: x["Role View"].split(":"),
                            partialAllowed: x["Partial Allowed"],
                            campusId: campid,
                            status: x["Status"].toLowerCase() == "active" ? 1 : 0,
                            createdBy: instName,
                        };
                        feeTypeDetails.push(newFeeTypes);
                    })
                }
                else {
                    let campid
                    let campid1 = await campuses.find(item => item.campusId == x["Campus ID"])
                    campid = campid1["_id"]
                    var newFeeTypes = {
                        _id: mongoose.Types.ObjectId(),
                        displayName: `FT_${finYear}_${(Number(i) + 1).toString().length == 1 ? "00" : (Number(i) + 1).toString().length == 2 ? "0" : ""
                            }${Number(i) + 1}`,
                        refid: x["ID *"],
                        title: x["Fee Type *"],
                        description: x["Description"],
                        roleView: x["Role View"].split(":"),
                        partialAllowed: x["Partial Allowed"],
                        campusId: campid,
                        status: x["Status"].toLowerCase() == "active" ? 1 : 0,
                        createdBy: instName,
                    };
                    feeTypeDetails.push(newFeeTypes);
                }


                // var newFeeTypes = {
                //     _id: mongoose.Types.ObjectId(),
                //     displayName: `FT${
                //         (Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                //         }${Number(j) + 1}`,
                //     refid: x["ID *"],
                //     title: x["Fee Type *"],
                //     description: x["Description"],
                //     roleView: x["Role View"].split(":"),
                //     partialAllowed: x["Partial Allowed"],
                //     campusId: campid,
                //     status: x["Status"].toLowerCase() == "active" ? 1 : 0,
                //     createdBy: instName,
                // };
                // return newFeeTypes;
            }
            // console.log(feeTypeDetails)
            if (feeTypeDetails.length > 0) {
                console.log("ad")
                feeTypeModel.insertMany(feeTypeDetails, async function (error, docs) {
                    if (error) {
                        if (error.name === "BulkWriteError" && error.code === 11000) {
                            // reject(res.status(200).json({
                            //     success: true,
                            //     message: error.toString(),
                            //     count: 0,
                            // }))
                            console.log("error", error)
                        }
                    } else {
                        //    let feeStructure = await createFeeStructure(docs,feeStructure)
                        console.log("stresr")
                        pubnubConfig.message.description = {
                            message: `Fee Type has been added successfully.`,
                            current: "Fee Types",
                            next: "Fee Structures"
                        };
                        pubnubConfig.message.status = 8;
                        // await pubnub.publish(pubnubConfig);
                        //Fee Structure Add
                        let allFeeStructure = [];
                        let ccft = 0;
                        for (let j = 0; j < feeStructure.length; j++) {
                            var ppInputData = feeStructure[j];
                            if (ppInputData["Campus ID"].toLowerCase() == "all") {
                                for (let k = 0; k < campuses.length; k++) {
                                    ccft++
                                    var feeTypeData = [];
                                    let ftd = 0
                                    for (ftd; ftd < feeStructure[j]["Fee Types *"].length; ftd++) {
                                        var ftDet = await feeTypeDetails.find(item => item.refid.trim() == feeStructure[j]["Fee Types *"][ftd].trim() && item["campusId"] == campuses[k]["_id"]);
                                        if (ftDet == null) {
                                            feeTypeData.push(null);
                                        }
                                        else {
                                            feeTypeData.push(ftDet._id);
                                        }
                                        // console.log("ftDet", ftDet);
                                    }
                                    // console.log(feeTypeData)
                                    let campid = campuses[k]["_id"];
                                    var ppData = {
                                        _id: mongoose.Types.ObjectId(),
                                        displayName: `FS_${finYear}_${(Number(ccft)).toString().length == 1 ? "00" : (Number(ccft)).toString().length == 2 ? "0" : ""
                                            }${Number(ccft)}`,
                                        refid: ppInputData["ID *"],
                                        title: ppInputData["Title *"],
                                        description: ppInputData["Description"],
                                        feeTypeIds: feeTypeData,
                                        campusId: campid,
                                        status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                        createdBy: instName,
                                    };
                                    await allFeeStructure.push(ppData);
                                    // console.log("allFeeStructure1", allFeeStructure)

                                }
                            }
                            else {
                                let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                                let campid = campid1["_id"];
                                var feeTypeData = [];
                                let ftd = 0
                                for (ftd = 0; ftd < feeStructure[j]["Fee Types *"].length; ftd++) {
                                    var ftDet = await feeTypeDetails.find(item => item.refid.trim() == feeStructure[j]["Fee Types *"][ftd].trim() && item["campusId"] == campid1["_id"]);
                                    if (ftDet == null) {
                                        feeTypeData.push(null);
                                    }
                                    else {
                                        feeTypeData.push(ftDet._id);
                                    }

                                }
                                var ppData = {
                                    _id: mongoose.Types.ObjectId(),
                                    displayName: `FS_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                        }${Number(j) + 1}`,
                                    refid: ppInputData["ID *"],
                                    title: ppInputData["Title *"],
                                    description: ppInputData["Description"],
                                    feeTypeIds: feeTypeData,
                                    campusId: campid,
                                    status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                    createdBy: instName,
                                };
                                // console.log("allFeeStructure1", allFeeStructure)
                                await allFeeStructure.push(ppData);
                                console.log("ftDet", allFeeStructure.length);
                            }
                            console.log(j + 1 == feeStructure.length, allFeeStructure.length)
                            if (allFeeStructure.length > 0) {
                                feeStructureModel.insertMany(allFeeStructure, async function (error2, docs2) {
                                    if (error2) {
                                        // if (error2.name === "BulkWriteError" && error2.code === 11000) {
                                        //     reject(res.status(200).json({
                                        //         success: true,
                                        //         message: error2.toString(),
                                        //         count: 0,
                                        //     }))
                                        // }
                                    } else {
                                        console.log(`Fees Structure has been added successfully.`, docs2)
                                        pubnubConfig.message.description = {
                                            message: `Fees Structure has been added successfully.`,
                                            current: "Fee Structures",
                                            next: "Program Plans"
                                        };
                                        pubnubConfig.message.status = 16;
                                        // await pubnub.publish(pubnubConfig);
                                        // pubnubConfig.message.description = {
                                        //     message: `fees Structure has been added successfully.`,
                                        // };
                                        // pubnubConfig.message.status = 16;
                                        // await pubnub.publish(pubnubConfig);

                                        //Program Plan Add
                                        var allProgramPlan = [];
                                        for (let j = 0; j < programPlanDatas.length; j++) {
                                            var ppInputData = programPlanDatas[j];
                                            if (ppInputData["Campus ID"].toLowerCase() == "all") {
                                                campuses.forEach(async function (ktm) {
                                                    let campid = ktm["_id"]
                                                    let splitfromdate = ppInputData["From Date"].split("-")
                                                    let splittodate = ppInputData["To Date"].split("-")
                                                    let frdata = new Date(ppInputData["From Date"])
                                                    let trdata = new Date(ppInputData["To Date"])
                                                    let academicyear = `${frdata.getFullYear()}-${trdata.getFullYear().toString().slice(2, 4)}`
                                                    var ppData = {
                                                        _id: mongoose.Types.ObjectId(),
                                                        displayName: `PP_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                            }${Number(j) + 1}`,
                                                        refid: ppInputData["Program Code *"],
                                                        hedaId: ppInputData["Program Code *"],
                                                        title: ppInputData["Program Name *"],
                                                        fromDate: ppInputData["From Date"],
                                                        toDate: ppInputData["To Date"],
                                                        academicYear: academicyear,
                                                        description: ppInputData["Description"],
                                                        campusId: campid,
                                                        status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                        createdBy: instName,
                                                    };
                                                    allProgramPlan.push(ppData);
                                                })
                                            }
                                            else {
                                                let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                                                let campid = campid1["_id"]
                                                let splitfromdate = ppInputData["From Date"].split("-")
                                                let splittodate = ppInputData["To Date"].split("-")
                                                let frdata = new Date(ppInputData["From Date"])
                                                let trdata = new Date(ppInputData["To Date"])
                                                let academicyear = `${frdata.getFullYear()}-${trdata.getFullYear().toString().slice(2, 4)}`
                                                var ppData = {
                                                    _id: mongoose.Types.ObjectId(),
                                                    displayName: `PP_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                        }${Number(j) + 1}`,
                                                    refid: ppInputData["Program Code *"],
                                                    hedaId: ppInputData["Program Code *"],
                                                    title: ppInputData["Program Name *"],
                                                    fromDate: ppInputData["From Date"],
                                                    toDate: ppInputData["To Date"],
                                                    academicYear: academicyear,
                                                    description: ppInputData["Description"],
                                                    course: ppInputData["Course"],
                                                    coursePeriod: ppInputData["Course Period"],
                                                    campusId: campid,
                                                    status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                    createdBy: instName,
                                                };
                                                allProgramPlan.push(ppData);
                                            }

                                        }
                                        if (allProgramPlan.length > 0) {
                                            await programPlanSchema.insertMany(allProgramPlan);
                                            pubnubConfig.message.description = {
                                                message: `Program Plan has been added successfully.`,
                                                current: "Program Plans",
                                                next: "Payment Schedules"
                                            };
                                            pubnubConfig.message.status = 24;
                                            // await pubnub.publish(pubnubConfig);
                                        }
                                        console.log("pp")
                                        //Payment Schdule Add
                                        var allPaymentSchedule = [];
                                        let ccps = 0;
                                        for (let j = 0; j < paymentSchedule.length; j++) {
                                            var ppInputData = paymentSchedule[j];

                                            if (ppInputData["Campus ID"].toLowerCase() == "all") {
                                                campuses.forEach(async function (ktm) {
                                                    ccps++
                                                    let campid = ktm["_id"]
                                                    // let campisd = {
                                                    //     "displayName": "PYMSCH_2021-22_004",
                                                    //     "title": "Payment Schedule 3",
                                                    //     "description": "Quarter",
                                                    //     "scheduleDetails": {
                                                    //         "collectEvery": "Quarter",
                                                    //         "startMonth": "March",
                                                    //         "startDate": "First",
                                                    //         "endDate": "Third",
                                                    //         "dueDate": "Twenty Five",
                                                    //         "penaltyStartDate": "5",
                                                    //         "oneTimeDateRange": {
                                                    //             "fromStartMonth": "",
                                                    //             "fromStartDate": "",
                                                    //             "fromDueDate": "",
                                                    //             "toStartMonth": "",
                                                    //             "toStartDate": "",
                                                    //             "toDueDate": ""
                                                    //         }
                                                    //     },
                                                    //     "campusId": "60caeb240000da181db44339",
                                                    //     "parentId": "",
                                                    //     "feesBreakUp": [
                                                    //         40, 20, 20, 20
                                                    //     ],
                                                    //     "createdBy": "60377798737cdf7d33caf118",
                                                    //     "orgId": "60377798737cdf7d33caf118"
                                                    // }
                                                    var ppData = {
                                                        _id: mongoose.Types.ObjectId(),
                                                        displayName: `PYMSCH_${finYear}_${(Number(ccps)).toString().length == 1 ? "00" : (Number(ccps)).toString().length == 2 ? "0" : ""
                                                            }${Number(ccps)}`,
                                                        title: ppInputData["Title"],
                                                        refid: ppInputData["ID *"],
                                                        description: ppInputData["Description"],
                                                        scheduleDetails: {
                                                            collectEvery: ppInputData["Collection Period"],
                                                            dueDate: ppInputData["Due By"],
                                                            penaltyStartDate: ppInputData["Penalty Start Date"],
                                                            "startMonth": ppInputData["Start Month"],
                                                            "startDate": ppInputData["Start Date"],
                                                            "endDate": null,
                                                            "oneTimeDateRange": {
                                                                "fromStartMonth": "",
                                                                "fromStartDate": "",
                                                                "fromDueDate": "",
                                                                "toStartMonth": "",
                                                                "toStartDate": "",
                                                                "toDueDate": ""
                                                            }
                                                        },
                                                        period: ppInputData["Period"],
                                                        startMonth: ppInputData["Start Month"],
                                                        feesBreakUp: ppInputData["Percentage"],
                                                        campusId: campid,
                                                        status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                        createdBy: instName,
                                                    };
                                                    allPaymentSchedule.push(ppData);
                                                })
                                            }
                                            else {
                                                let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                                                let campid = campid1["_id"]
                                                var ppData = {
                                                    _id: mongoose.Types.ObjectId(),
                                                    displayName: `PYMSCH_${finYear}_${(Number(ccps)).toString().length == 1 ? "00" : (Number(ccps)).toString().length == 2 ? "0" : ""
                                                        }${Number(ccps)}`,
                                                    title: ppInputData["Title"],
                                                    refid: ppInputData["ID *"],
                                                    description: ppInputData["Description"],
                                                    scheduleDetails: {
                                                        collectEvery: ppInputData["Collection Period"],
                                                        dueDate: ppInputData["Due By"],
                                                        penaltyStartDate: ppInputData["Penalty Start Date"],
                                                        "startMonth": ppInputData["Start Month"],
                                                        "startDate": ppInputData["Start Date"],
                                                        "endDate": null,
                                                        "oneTimeDateRange": {
                                                            "fromStartMonth": "",
                                                            "fromStartDate": "",
                                                            "fromDueDate": "",
                                                            "toStartMonth": "",
                                                            "toStartDate": "",
                                                            "toDueDate": ""
                                                        }
                                                    },
                                                    period: ppInputData["Period"],
                                                    startMonth: ppInputData["Start Month"],
                                                    feesBreakUp: ppInputData["Percentage"],
                                                    campusId: campid,
                                                    status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                    createdBy: instName,
                                                };
                                                allPaymentSchedule.push(ppData);
                                            }
                                            // let feesbup = [];
                                            // ppInputData["Percentage"].forEach(async function(it){

                                            // })

                                        }
                                        if (allPaymentSchedule.length > 0) {
                                            await paymentScheduleModel.insertMany(allPaymentSchedule);
                                            pubnubConfig.message.description = {
                                                message: `Payment Schdule has been added successfully.`,
                                                current: "Payment Schedules",
                                                next: "Reminder Plans"
                                            };
                                            pubnubConfig.message.status = 32;
                                            // await pubnub.publish(pubnubConfig);
                                        }

                                        //ReminderPlan Add
                                        console.log("rr")
                                        var allReminderPlam = [];
                                        let ccrm = 0;
                                        for (let j = 0; j < reminderPlan.length; j++) {
                                            var ppInputData = reminderPlan[j];
                                            if (ppInputData["Campus ID"].toLowerCase() == "all") {
                                                campuses.forEach(async function (ktm) {
                                                    ccrm++
                                                    let campid = ktm["_id"]
                                                    var ppData = {
                                                        _id: mongoose.Types.ObjectId(),
                                                        displayName: `REM_${finYear}_${(Number(ccrm)).toString().length == 1 ? "00" : (Number(ccrm)).toString().length == 2 ? "0" : ""
                                                            }${Number(ccrm)}`,
                                                        refid: ppInputData["ID *"],
                                                        title: ppInputData["Title"],
                                                        description: ppInputData["Description"],
                                                        numberOfReminders: ppInputData["nor"],
                                                        scheduleDetails: [
                                                            {
                                                                daysBefore: ppInputData["Days Before Due Date"],
                                                            },
                                                            {
                                                                daysAfter: ppInputData["Days After Demand Note"],
                                                            },
                                                            {
                                                                daysAfter: ppInputData["Days After 1st Reminder"],
                                                            },
                                                            {
                                                                daysAfter: ppInputData["Days After 2nd Reminder"],
                                                            },
                                                        ],
                                                        demandNoteReminder: ppInputData["Demand Note Reminder"],
                                                        otherReminders: ppInputData["Other Reminders"],
                                                        campusId: campid,
                                                        status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                        createdBy: instName,
                                                    };
                                                    allReminderPlam.push(ppData);
                                                })
                                            }
                                            else {
                                                let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                                                let campid = campid1["_id"]
                                                var ppData = {
                                                    _id: mongoose.Types.ObjectId(),
                                                    displayName: `REM_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                        }${Number(j) + 1}`,
                                                    refid: ppInputData["ID *"],
                                                    title: ppInputData["Title"],
                                                    description: ppInputData["Description"],
                                                    numberOfReminders: ppInputData["nor"],
                                                    scheduleDetails: [
                                                        {
                                                            daysBefore: ppInputData["Days Before Due Date"],
                                                        },
                                                        {
                                                            daysAfter: ppInputData["Days After Demand Note"],
                                                        },
                                                        {
                                                            daysAfter: ppInputData["Days After 1st Reminder"],
                                                        },
                                                        {
                                                            daysAfter: ppInputData["Days After 2nd Reminder"],
                                                        },
                                                    ],
                                                    demandNoteReminder: ppInputData["Demand Note Reminder"],
                                                    otherReminders: ppInputData["Other Reminders"],
                                                    campusId: campid,
                                                    status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                    createdBy: instName,
                                                };
                                                allReminderPlam.push(ppData);
                                            }

                                        }
                                        if (allReminderPlam.length > 0) {
                                            await reminderModel.insertMany(allReminderPlam);
                                            pubnubConfig.message.description = {
                                                message: `Reminder Plan has been added successfully.`,
                                                current: "Reminder Plans",
                                                next: "Late Fee Plans"
                                            };
                                            pubnubConfig.message.status = 40;
                                            // await pubnub.publish(pubnubConfig);
                                            console.log(`Reminder Plan has been added successfully.`)
                                        }
                                        //LateFee Add
                                        var allLateFee = [];
                                        let cclf = 0;
                                        for (let j = 0; j < lateFeePlan.length; j++) {
                                            var ppInputData = lateFeePlan[j];
                                            if (ppInputData["Campus ID"].toLowerCase() == "all") {
                                                campuses.forEach(async function (ktm) {
                                                    cclf++
                                                    let campid = ktm["_id"]
                                                    var ppData = {
                                                        _id: mongoose.Types.ObjectId(),
                                                        displayName: `LTFEE_${finYear}_${(Number(cclf)).toString().length == 1 ? "00" : (Number(cclf)).toString().length == 2 ? "0" : ""
                                                            }${Number(cclf)}`,
                                                        refid: ppInputData["ID *"],
                                                        title: ppInputData["Title"],
                                                        description: ppInputData["Description"],
                                                        type: ppInputData["Type"],
                                                        amount: ppInputData["Charges"],
                                                        every: ppInputData["Frequency"],
                                                        campusId: campid,
                                                        status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                        createdBy: instName,
                                                    };
                                                    allLateFee.push(ppData);
                                                })
                                            }
                                            else {
                                                let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                                                let campid = campid1["_id"]
                                                var ppData = {
                                                    _id: mongoose.Types.ObjectId(),
                                                    displayName: `LTFEE_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                        }${Number(j) + 1}`,
                                                    refid: ppInputData["ID *"],
                                                    title: ppInputData["Title"],
                                                    description: ppInputData["Description"],
                                                    type: ppInputData["Type"],
                                                    amount: ppInputData["Charges"],
                                                    every: ppInputData["Frequency"],
                                                    campusId: campid,
                                                    status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                    createdBy: instName,
                                                };
                                                allLateFee.push(ppData);
                                            }
                                        }
                                        if (allLateFee.length > 0) {
                                            await lateFeeModel.insertMany(allLateFee);
                                            pubnubConfig.message.description = {
                                                message: `Late Fee has been added successfully.`,
                                                current: "Late Fee Plans",
                                                next: "Categories"
                                            };
                                            pubnubConfig.message.status = 48;
                                            // await pubnub.publish(pubnubConfig);
                                        }
                                        console.log("cc")
                                        //Category Add
                                        var allCategory = [];
                                        let ccct = 0;
                                        for (let j = 0; j < categoryPlan.length; j++) {
                                            var ppInputData = categoryPlan[j];
                                            if (ppInputData["Campus ID"].toLowerCase() == "all") {
                                                campuses.forEach(async function (ktm) {
                                                    ccct++
                                                    let campid = ktm["_id"]
                                                    var ppData = {
                                                        _id: mongoose.Types.ObjectId(),
                                                        displayName: `CAT_${finYear}_${(Number(ccct)).toString().length == 1 ? "00" : (Number(ccct)).toString().length == 2 ? "0" : ""
                                                            }${Number(ccct)}`,
                                                        refid: ppInputData["ID"],
                                                        title: ppInputData["Title"],
                                                        description: ppInputData["Description"],
                                                        campusId: campid,
                                                        createdBy: instName,
                                                        status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                    };
                                                    allCategory.push(ppData);
                                                })
                                            }
                                            else {
                                                let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                                                let campid = campid1["_id"]
                                                var ppData = {
                                                    _id: mongoose.Types.ObjectId(),
                                                    displayName: `CAT_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                        }${Number(j) + 1}`,
                                                    refid: ppInputData["ID"],
                                                    title: ppInputData["Title"],
                                                    description: ppInputData["Description"],
                                                    campusId: campid,
                                                    createdBy: instName,
                                                    status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                };
                                                allCategory.push(ppData);
                                            }
                                        }
                                        if (allCategory.length > 0) {
                                            await categoryModel.insertMany(allCategory);
                                            pubnubConfig.message.description = {
                                                message: `Category has been added successfully.`,
                                                current: "Categories",
                                                next: "Installment Plans"
                                            };
                                            pubnubConfig.message.status = 56;
                                            // await pubnub.publish(pubnubConfig);
                                            // pubnubConfig.message.description = {
                                            //     message: `Category has been added successfully.`,
                                            // };
                                            // pubnubConfig.message.status = 56;
                                            // await pubnub.publish(pubnubConfig);
                                        }
                                        console.log("inst")
                                        //InstallmentPlan Add
                                        var allInstallment = [];
                                        let ccin = 0;
                                        for (let j = 0; j < installmentPlan.length; j++) {
                                            var ppInputData = installmentPlan[j];
                                            if (ppInputData["Campus ID"].toLowerCase() == "all") {
                                                campuses.forEach(async function (ktm) {
                                                    ccin++
                                                    let campid = ktm["_id"]
                                                    console.log(ppInputData["noi"])
                                                    var ppData = {
                                                        _id: mongoose.Types.ObjectId(),
                                                        displayName: `INST_${finYear}_${(Number(ccin)).toString().length == 1 ? "00" : (Number(ccin)).toString().length == 2 ? "0" : ""
                                                            }${Number(ccin)}`,
                                                        refid: ppInputData["ID *"],
                                                        title: ppInputData["Title"],
                                                        description: ppInputData["Description"],
                                                        numberOfInstallments: ppInputData["noi"],
                                                        frequency: ppInputData["Frequency"],
                                                        dueDate: ppInputData["Due Date"],
                                                        percentageBreakup: ppInputData["Percentage Breakup"],
                                                        campusId: campid,
                                                        status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                        createdBy: instName,
                                                        updatedBy: instName,
                                                    };
                                                    allInstallment.push(ppData);
                                                })
                                            }
                                            else {
                                                let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                                                let campid = campid1["_id"]
                                                console.log(ppInputData["noi"])
                                                var ppData = {
                                                    _id: mongoose.Types.ObjectId(),
                                                    displayName: `INST_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                        }${Number(j) + 1}`,
                                                    refid: ppInputData["ID *"],
                                                    title: ppInputData["Title"],
                                                    description: ppInputData["Description"],
                                                    numberOfInstallments: ppInputData["noi"],
                                                    frequency: ppInputData["Frequency"],
                                                    dueDate: ppInputData["Due Date"],
                                                    percentageBreakup: ppInputData["Percentage Breakup"],
                                                    campusId: campid,
                                                    status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                    createdBy: instName,
                                                    updatedBy: instName,
                                                };
                                                allInstallment.push(ppData);
                                            }
                                        }
                                        if (allInstallment.length > 0) {
                                            await installmentModel.insertMany(allInstallment);
                                            pubnubConfig.message.description = {
                                                message: `Installment has been added successfully.`,
                                                current: "Installment Plans",
                                                next: "Concession Plans"
                                            };
                                            pubnubConfig.message.status = 64;
                                            // await pubnub.publish(pubnubConfig);
                                            // pubnubConfig.message.description = {
                                            //     message: `Installment has been added successfully.`,
                                            // };
                                            // pubnubConfig.message.status = 64;
                                            // await pubnub.publish(pubnubConfig);
                                        }
                                        console.log("con")
                                        //Concession Add
                                        let allConcession = [];
                                        console.log(cencessionPlan.length)
                                        let cccn = 0;
                                        for (let j = 0; j < cencessionPlan.length; j++) {
                                            // var ppInputData = cencessionPlan[j];
                                            if (cencessionPlan[j]["Campus ID"].toLowerCase() == "all") {
                                                campuses.forEach(async function (ktm) {
                                                    cccn++
                                                    let categoryData = await allCategory.find(item => item.refid == cencessionPlan[j]["Category ID"] && item.campusId == ktm["_id"]);
                                                    let campid = ktm["_id"]
                                                    var ppData = new concessionModel({
                                                        _id: mongoose.Types.ObjectId(),
                                                        displayName: `CON_${finYear}_${(Number(cccn)).toString().length == 1 ? "00" : (Number(cccn)).toString().length == 2 ? "0" : ""
                                                            }${Number(cccn)}`,
                                                        refid: cencessionPlan[j]["ID"],
                                                        title: cencessionPlan[j]["Title"],
                                                        description: cencessionPlan[j]["Description"],
                                                        categoryId: categoryData ? categoryData._id : "none",
                                                        concessionType: cencessionPlan[j]["Concession Type"],
                                                        concessionValue: cencessionPlan[j]["Concession Value"],
                                                        campusId: campid,
                                                        status: cencessionPlan[j]["Status"].toLowerCase() == "active" ? 1 : 0,
                                                        createdBy: instName,
                                                    });
                                                    await ppData.save()
                                                    // await allConcession.push(ppData);
                                                    // console.log(allConcession)
                                                })
                                            }
                                            else {
                                                let campid1 = await campuses.find(item => item.campusId == cencessionPlan[j]["Campus ID"])
                                                let categoryData = await allCategory.find(item => item.refid == cencessionPlan[j]["Category ID"] && item.campusId == campid1["_id"]);
                                                let campid = campid1["_id"]
                                                var ppData = new concessionModel({
                                                    _id: mongoose.Types.ObjectId(),
                                                    displayName: `CON_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                        }${Number(j) + 1}`,
                                                    refid: cencessionPlan[j]["ID *"],
                                                    title: cencessionPlan[j]["Title"],
                                                    description: cencessionPlan[j]["Description"],
                                                    categoryId: categoryData._id,
                                                    concessionType: cencessionPlan[j]["Concession Type"],
                                                    concessionValue: cencessionPlan[j]["Concession Value"],
                                                    campusId: campid,
                                                    status: cencessionPlan[j]["Status"].toLowerCase() == "active" ? 1 : 0,
                                                    createdBy: instName,
                                                });
                                                await ppData.save()
                                                // await allConcession.push(ppData);
                                            }
                                            console.log(j + 1 == cencessionPlan.length)
                                            // if(j+1==cencessionPlan.length){
                                            //     console.log("allcon",allConcession)
                                            //     if (allConcession.length > 0) {
                                            //         console.log("sdfs")
                                            //        let addcon = await concessionModel.insertMany(allConcession);
                                            //         console.log("addes")
                                            //         pubnubConfig.message.description = {
                                            //             message: `Concession Plan has been added successfully.`,
                                            //             current: "Concession Plans",
                                            //             next: "Student Details"
                                            //         };
                                            //         pubnubConfig.message.status = 72;
                                            //         // await pubnub.publish(pubnubConfig);
                                            //         // pubnubConfig.message.description = {
                                            //         //     message: `Concession Plan has been added successfully.`,
                                            //         // };
                                            //         // pubnubConfig.message.status = 72;
                                            //         // await pubnub.publish(pubnubConfig);

                                            //     }
                                            // }
                                        }

                                        console.log("std")
                                        //student Add
                                        var allStudents = [];
                                        for (let j = 0; j < studentDetails.length; j++) {
                                            // for (let j = 2608; j < studentDetails.length; j++) {

                                            var ppInputData = studentDetails[j];
                                            // let campid1 = await campuses.find(item => item.headaId == ppInputData["Institute ID"])
                                            // if(!campid1){

                                            let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                                            // }
                                            if (!campid1) {
                                                console.log(ppInputData["Campus ID"], campuses)
                                            }
                                            let campid = campid1["_id"];
                                            if (!campid) {
                                                ppInputData
                                            }
                                            var guardianDetails = {
                                                isPrimary: true,
                                                firstName: ppInputData["Parent Name"],
                                                lastName: ppInputData["Parent Name"],
                                                fullName: ppInputData["Parent Name"],
                                                mobile: ppInputData["Parent Phone Number"],
                                                email: ppInputData["Parent Email Address"],
                                                PIIUsageFlag: true,
                                                PIIUsageFlagUpdated: new Date(),
                                                fatherDetails: {},
                                                motherDetails: {},
                                                guardianDetails: {},
                                                relation: "Parent",
                                                createdBy: instName,
                                            };
                                            let guardianData = new guardianSchema(guardianDetails);
                                            var guardianResponse = await guardianData.save();
                                            // console.log("guardiandetails", ppInputData["feeStructure"], ppInputData["Program Plan ID"]);
                                            let programPlanData = await allProgramPlan.find(item => item.refid.trim() == ppInputData["Program Plan ID"]);
                                            // console.log("prorgamplan", programPlanData);
                                            let feeStructureData = await allFeeStructure.find(item => item.refid == ppInputData["feeStructure"] && item.campusId.toString() == campid.toString());
                                            // console.log(feeStructureData,programPlanData)

                                            if (programPlanData == null || feeStructureData == null) {
                                                console.log(ppInputData, allProgramPlan, feeStructureData)
                                                // dbConnectionp.dropDatabase(async function (err, result) {
                                                //     if (err) {
                                                //         res.status(400).send({
                                                //             status: "failure",
                                                //             message: "database delete error",
                                                //             count: 0,
                                                //         })
                                                //     }
                                                //     else {
                                                await dbConnectionp.close()
                                                console.log({ status: "failure", message: `error in program plan or fee structure please check the program plan and fee structure of student ${studentDetails[j]["Reg ID *"]} ${ppInputData["Program Plan ID"]} ${ppInputData["feeStructure"]}` })
                                                res.status(409).send({ status: "failure", message: `error in program plan or fee structure please check the program plan and fee structure of student ${studentDetails[j]["Reg ID *"]}` })
                                                //     }
                                                // })
                                            }
                                            else {
                                                // console.log("feeStructure", feeStructureData);
                                                // let categoryId = await categoryModel.findOne({
                                                //   programCode: ppInputData["Program Plan ID"],
                                                // });
                                                let dob
                                                let admdate
                                                // if (ppInputData["DOB"] !== "") {
                                                //     dob = new Date(ppInputData["DOB"]).toISOString(),
                                                //         admdate = new Date(ppInputData["Admitted Date "]).toISOString()
                                                // }
                                                // else {
                                                //     dob = null
                                                //     admdate = null
                                                // }
                                                // let rollnum = await stdfeeMaps.find(item => item["Student ID"] == ppInputData["HEDA ID *"])
                                                // console.log("dob", dob)
                                                // console.log("admitted date",admdate)
                                                // if (ppInputData["Campus ID"].toLowerCase() == "all") {
                                                //     campuses.forEach(async function (ktm) {
                                                //         campid.push(ktm["_id"])
                                                //     })
                                                // }
                                                // else {

                                                // }
                                                var ppData = {
                                                    displayName: `STU_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                        }${Number(j) + 1}`,
                                                    regId: ppInputData["Reg ID *"] == "-" ? ppInputData["HEDA ID *"] : ppInputData["Reg ID *"],
                                                    rollNumber: ppInputData["HEDA ID *"] == "-" ? ppInputData["Reg ID *"] : ppInputData["HEDA ID"] ? ppInputData["HEDA ID"] : ppInputData["HEDA ID *"],
                                                    salutation:
                                                        ppInputData["Salutation"] != undefined
                                                            ? ppInputData["Salutation"]
                                                            : null, // salutation
                                                    category: ppInputData["Category"], // Category
                                                    section: ppInputData["Section"] ? ppInputData["Section"] : "A",
                                                    firstName: ppInputData["First Name *"], //First Name *
                                                    middleName: ppInputData["Middle Name"], //
                                                    lastName: ppInputData["Last Name *"], //Last Name *
                                                    guardianDetails: [guardianData._id],
                                                    gender: ppInputData["Gender"], //Gender
                                                    dob: null,
                                                    citizenship: ppInputData["Citizenship"], //
                                                    currency: ppInputData["Currency"], //
                                                    FOREX: ppInputData["FOREX"], //
                                                    admittedOn: null,
                                                    // admittedOn: new Date(ppInputData["Admitted Date"]) instanceof Date ? new Date(ppInputData["Admitted Date"]) : null, //Admitted Date *
                                                    programPlanId: programPlanData._id,
                                                    feeStructureId: feeStructureData._id,
                                                    phoneNo: ppInputData["Phone Number *"], //Phone Number *
                                                    email: ppInputData["Email Address *"], // Email Address *
                                                    alternateEmail:
                                                        ppInputData["alterEmail"] != undefined
                                                            ? ppInputData["alterEmail"]
                                                            : null,
                                                    parentName: ppInputData["Parent Name"],
                                                    parentPhone: ppInputData["Parent Phone Number"],
                                                    parentEmail: ppInputData["Parent Email Address"],
                                                    relation: "parent",
                                                    addressDetails: {
                                                        address1: ppInputData["Address 1"],
                                                        address2: ppInputData["Address 2"],
                                                        address3: ppInputData["Address 3"],
                                                        city: ppInputData["City/Town"],
                                                        state: ppInputData["State"],
                                                        country: ppInputData["Country"], //Country
                                                        pincode: ppInputData["PIN Code"], //PIN Code
                                                    },
                                                    isFinalYear: ppInputData["Is Final Year"].toLowerCase() == "yes" ? true : false,
                                                    final: ppInputData["Final"],
                                                    campusId: campid,
                                                    status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                    createdBy: instName,
                                                };
                                                allStudents.push(ppData);
                                            }
                                        }
                                        if (allStudents.length > 0) {
                                            await studentModel.insertMany(allStudents);
                                            pubnubConfig.message.description = {
                                                message: `Students has been added successfully.`,
                                                current: "Student Details",
                                                next: "Fee Manager"
                                            };
                                            pubnubConfig.message.status = 88;
                                            // await pubnub.publish(pubnubConfig);
                                            console.log("students added successfully")
                                            // pubnubConfig.message.description = {
                                            //     message: `Students has been added successfully.`,
                                            // };
                                            // pubnubConfig.message.status = 80;
                                            // await pubnub.publish(pubnubConfig);
                                        }

                                        // Fee Manager added
                                        var allFeeManager = [];
                                        let ccfm = 0;
                                        for (let j = 0; j < feeManagerDatas.length; j++) {
                                            var pfmInputData = feeManagerDatas[j];
                                            // console.log(pfmInputData["Campus ID"].toLowerCase() == "all",pfmInputData["Campus ID"].toLowerCase())
                                            if (pfmInputData["Campus ID"].toLowerCase() == "all") {
                                                for (m = 0; m < campuses.length; m++) {
                                                    ccfm++;
                                                    let ktm = campuses[m];
                                                    let campid = campuses[m]["_id"];
                                                    var ppDetails = await allProgramPlan.find(item => item.refid == pfmInputData["Program Plan ID *"]["0"] && item.campusId == ktm["_id"]);
                                                    if (ppDetails) {
                                                        // console.log("ppDetails", ppDetails)
                                                        var feeTypesDetails = await feeTypeDetails.find(item => item.refid == pfmInputData["Fee Type ID *"]["0"] && item.campusId == ktm["_id"]);
                                                        //   console.log("feeTypesDetails",feeTypesDetails)
                                                        var concessionPlans = await allConcession.find(item => item.refid == pfmInputData["Concession ID"] && item.campusId == ktm["_id"])
                                                        //   console.log("concessionPlans",concessionPlans)
                                                        var installmentPlans = await allInstallment.find(item => item.refid == pfmInputData["Installment ID"] && item.campusId == ktm["_id"])
                                                        //   console.log("installmentPlans",installmentPlans)
                                                        var reminderPlans = await allReminderPlam.find(item => item.refid == pfmInputData["Reminder Plan ID *"] && item.campusId == ktm["_id"])
                                                        //   console.log("reminderPlans",reminderPlans)
                                                        var latefees = await allLateFee.find(item => item.refid == pfmInputData["Late Fee Plan ID *"] && item.campusId == ktm["_id"])
                                                        //   console.log("latefees",latefees)
                                                        var paymentschedules = await allPaymentSchedule.find(item => item.refid == pfmInputData["Payment Schedule ID *"] && item.campusId == ktm["_id"])
                                                        //   console.log("paymentschedules",paymentschedules)

                                                        let apldates = []
                                                        let appdates = pfmInputData["Apply Dates"].split(",")
                                                        if (appdates.length > 1) {
                                                            apldates.push(new Date(appdates[0]))
                                                            apldates.push(new Date(appdates[1]))
                                                        }
                                                        else {
                                                            apldates.push(new Date())
                                                            apldates.push(new Date())
                                                        }
                                                        // console.log(apldates)
                                                        var pfmData = {
                                                            displayName: `FM_${finYear}_${(Number(ccfm)).toString().length == 1 ? "00" : (Number(ccfm)).toString().length == 2 ? "0" : ""
                                                                }${Number(ccfm)}`,
                                                            title: pfmInputData["Title *"],
                                                            description: pfmInputData["Description"],
                                                            feeTypeId: feeTypesDetails !== undefined ? feeTypesDetails._id : null,
                                                            programPlanId: ppDetails !== undefined ? ppDetails._id : null,
                                                            reminderPlanId: reminderPlans !== undefined ? reminderPlans._id : null,
                                                            paymentScheduleId: paymentschedules !== undefined ? paymentschedules._id : null,
                                                            concessionPlanId: concessionPlans !== undefined ? concessionPlans._id : null,
                                                            lateFeePlanId: latefees !== undefined ? latefees._id : null,
                                                            installmentPlanId: installmentPlans !== undefined ? installmentPlans._id : null,
                                                            campusId: campid,
                                                            feeMapType: pfmInputData["Fee Map Type"],
                                                            applyDates: apldates,
                                                            feeDetails: {
                                                                units: null,
                                                                perUnitAmount: null,
                                                                totalAmount: pfmInputData["Total Fees *"],
                                                            },
                                                            status: pfmInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                            createdBy: instName,
                                                        };
                                                        allFeeManager.push(pfmData);
                                                    }
                                                }
                                            }
                                            else {
                                                let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                                                let campid = campid1["_id"];
                                                var ppDetails = await allProgramPlan.find(item => item.refid == pfmInputData["Program Plan ID *"]["0"] && item.campusId == campid1["_id"]);
                                                // console.log(campuses,ppInputData["Campus ID"],campid,pfmInputData["Program Plan ID *"],ppDetails)
                                                //   console.log("ppDetails",ppDetails)
                                                var feeTypesDetails = await feeTypeDetails.find(item => item.refid == pfmInputData["Fee Type ID *"]["0"] && item.campusId == campid1["_id"]);
                                                //   console.log("feeTypesDetails",feeTypesDetails)
                                                var concessionPlans = await allConcession.find(item => item.refid == pfmInputData["Concession ID"] && item.campusId == campid1["_id"])
                                                //   console.log("concessionPlans",concessionPlans)
                                                var installmentPlans = await allInstallment.find(item => item.refid == pfmInputData["Installment ID"] && item.campusId == campid1["_id"])
                                                //   console.log("installmentPlans",installmentPlans)
                                                var reminderPlans = await allReminderPlam.find(item => item.refid == pfmInputData["Reminder Plan ID *"] && item.campusId == campid1["_id"])
                                                //   console.log("reminderPlans",reminderPlans)
                                                var latefees = await allLateFee.find(item => item.refid == pfmInputData["Late Fee Plan ID *"] && item.campusId == campid1["_id"])
                                                //   console.log("latefees",latefees)
                                                var paymentschedules = await allPaymentSchedule.find(item => item.refid == pfmInputData["Payment Schedule ID *"] && item.campusId == campid1["_id"])
                                                //   console.log("paymentschedules",paymentschedules)

                                                let apldates = []
                                                let appdates = pfmInputData["Apply Dates"].split(",")
                                                if (appdates.length > 0) {
                                                    apldates.push(new Date(appdates[0]))
                                                    apldates.push(new Date(appdates[1]))
                                                    // console.log("appdates", appdates)
                                                }
                                                else {
                                                    apldates.push(new Date())
                                                    apldates.push(new Date())
                                                    // console.log("appdates", appdates) 
                                                }

                                                var pfmData = {
                                                    displayName: `FM_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                        }${Number(j) + 1}`,
                                                    title: pfmInputData["Title *"],
                                                    description: pfmInputData["Description"],
                                                    feeTypeId: feeTypesDetails !== undefined ? feeTypesDetails._id : null,
                                                    programPlanId: ppDetails !== undefined ? ppDetails._id : null,
                                                    reminderPlanId: reminderPlans !== undefined ? reminderPlans._id : null,
                                                    paymentScheduleId: paymentschedules !== undefined ? paymentschedules._id : null,
                                                    concessionPlanId: concessionPlans !== undefined ? concessionPlans._id : null,
                                                    lateFeePlanId: latefees !== undefined ? latefees._id : null,
                                                    installmentPlanId: installmentPlans !== undefined ? installmentPlans._id : null,
                                                    campusId: campid,
                                                    feeMapType: pfmInputData["Fee Map Type"],
                                                    applyDates: apldates,
                                                    feeDetails: {
                                                        units: null,
                                                        perUnitAmount: null,
                                                        totalAmount: pfmInputData["Total Fees *"],
                                                    },
                                                    status: pfmInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                    createdBy: instName,
                                                };
                                                allFeeManager.push(pfmData);
                                            }


                                        }
                                        // console.log("allFeeManager", allFeeManager);
                                        if (allFeeManager.length > 0) {
                                            await feeManagerSchema.insertMany(allFeeManager);
                                            pubnubConfig.message.description = {
                                                message: `Fee Managers added successfully.`,
                                                current: "Fee Manager",
                                                next: "Student Fee Map"
                                            };
                                            pubnubConfig.message.status = 100;
                                            // await pubnub.publish(pubnubConfig);
                                            console.log("Fee Manager added successfully")
                                            // pubnubConfig.message.description = {
                                            //     message: `Fee Managers added successfully.`,
                                            // };
                                            // pubnubConfig.message.status = 88
                                            // await pubnub.publish(pubnubConfig);
                                        }

                                        //Student Fees Map
                                        console.log("student fee map started")
                                        var allStudentMap = [];
                                        let feetypesd = await feeTypeModel.findOne({ displayName: "FT001" })
                                        for (let j = 0; j < stdfeeMaps.length; j++) {
                                            // for (let j = 2608; j < stdfeeMaps.length; j++) {

                                            // console.log(Object.keys(stdfeeMaps[j]))
                                            // console.log(sds)
                                            let transdata = await transactionModel.find({})
                                            let transcount = transdata.length
                                            var ppInputData = stdfeeMaps[j];
                                            let studentData = await studentModel.findOne({
                                                rollNumber: ppInputData["Student ID"],
                                            });
                                            if (studentData === null) {
                                                studentData = await studentModel.findOne({
                                                    regId: ppInputData["USN"],
                                                });
                                            }
                                            if (studentData === null) {
                                                studentData = await studentModel.findOne({
                                                    regId: ppInputData["Reg ID *"],
                                                });
                                            }
                                            if (studentData === null) {
                                                studentData = await studentModel.findOne({
                                                    regId: ppInputData["HEDA ID"],
                                                });
                                            }
                                            if (!studentData) {
                                                console.log(ppInputData["Student ID"])
                                            }
                                            else {
                                                // let campid1 = await campuses.find(item => item._id.toString() == studentData._doc["campusId"].toString())
                                                let campid = studentData._doc["campusId"];
                                                let months = ["First", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
                                                let duemonths = { "Year": 12, "Half-Year": 6, "Quarter": 3, "Month": 1, "One time date": 0, "One time date range": 0 }
                                                let dueDates = { "Second": 2, "Third": 3, "Fourth": 4, "Fifth": 5, "Last": 30, "Second Last": 28, "Third last": 27, "Fourth last": 2 }
                                                if (!studentData) {
                                                    console.log(stdfeeMaps[j])
                                                }
                                                let prplan = allProgramPlan.find(item => item._id.toString() == studentData.programPlanId.toString())
                                                if (!prplan) {
                                                    stdfeeMaps[j]
                                                }
                                                let paymentSchedule = allPaymentSchedule[0]
                                                // let prspl = prplan.fromDate.split(" ")
                                                // let duddd = new Date(prplan.fromDate)
                                                // let prmonth = prspl[0]
                                                // let mindex = months.indexOf(prmonth)
                                                // let pryear = prspl[2]
                                                // let dueD = paymentSchedule.scheduleDetails.collectEvery
                                                // let dueM = duemonths[paymentSchedule.scheduleDetails.collectEvery]
                                                // let nextDueD = dueM + mindex
                                                // let dueDatem
                                                // if (dueM == 12) {
                                                //     dueDatem = new Date("2021-04-01")
                                                // }
                                                // else {
                                                //     dueDatem = `${months[nextDueD]} ${Number(dueDates[paymentSchedule.scheduleDetails.dueDate]) + 1}, ${pryear}`
                                                // }
                                                var d = new Date("2021-04-01");
                                                var d2 = new Date("2021-04-01");
                                                let amountp = stdfeeMaps[j]["Amount"] ? stdfeeMaps[j]["Amount"].replace(",", "") : "0";
                                                amountp = amountp.replace(/ +/g, "");
                                                let amountr = isNaN(Number(stdfeeMaps[j]["Amount Received"])) ? "0" : stdfeeMaps[j]["Amount Received"].replace(",", "");
                                                amountr = amountr.replace(/ +/g, "");
                                                let pend = parseFloat(amountp) - parseFloat(amountr)
                                                if (isNaN(pend) && !isNaN(amountp)) {
                                                    pend = amountp
                                                }
                                                else if (isNaN(amountp)) {
                                                    pend = 0.00
                                                }
                                                let paidamt = isNaN(amountr) ? "0" : amountr
                                                let rid = 0;
                                                // console.log()
                                                // let trdates = stdfeeMaps[j][`Date1`].split("/")
                                                // let trdate
                                                // if (!trdates.includes("-")) {
                                                //     trdate = new Date(`20${trdates[2]}-${trdates[0]}-${trdates[1]}`)
                                                // }
                                                // else {
                                                trdate = null
                                                // }
                                                let feesbkp = []
                                                let paidamts = []
                                                for (ll = 0; ll < feeTypeDetails.length; ll++) {
                                                    if (feeTypeDetails[ll].title.trim().toLowerCase().includes("tuition") && campid.toString() == feeTypeDetails[ll]["campusId"].toString()) {
                                                        feesbkp.push({
                                                            amount: isNaN(amountp) ? 0.00 : amountp,
                                                            paid: 0.00,
                                                            pending: isNaN(amountp) ? 0.00 : amountp,
                                                            feeTypeCode: feeTypeDetails[ll]["displayName"],
                                                            title: feeTypeDetails[ll]["title"],
                                                        })
                                                        paidamts.push(0)
                                                    }
                                                    else if (campid.toString() == feeTypeDetails[ll]["campusId"].toString()) {
                                                        feesbkp.push({
                                                            amount: 0.00,
                                                            paid: 0.00,
                                                            pending: 0.00,
                                                            feeTypeCode: feeTypeDetails[ll]["displayName"],
                                                            title: feeTypeDetails[ll]["title"],
                                                        })
                                                        paidamts.push(0)
                                                    }
                                                }
                                                var ppData = {
                                                    displayName: `SFM_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                        }${Number(j) + 1}`,
                                                    studentId: studentData._id,
                                                    usn: stdfeeMaps[j]["USN"],
                                                    programPlanId: studentData.programPlanId,
                                                    feeStructureId: studentData.feeStructureId,
                                                    feeManagerId: undefined,
                                                    dueDate: d2.toISOString(),
                                                    amount: isNaN(amountp) ? 0.00 : amountp,
                                                    paid: 0.00,
                                                    receivedDate: trdate,
                                                    receiptNumbers: stdfeeMaps[j]["Receipt Number"],
                                                    concession: 0,
                                                    fine: 0,
                                                    pending: isNaN(amountp) ? 0.00 : amountp,
                                                    transactionPlan: {
                                                        feesBreakUp: feesbkp,
                                                        totalAmount: isNaN(amountp) ? 0.00 : amountp,
                                                        paidAmount: paidamts,
                                                        totalPaid: 0.00,
                                                        totalPending: isNaN(amountp) ? 0.00 : amountp
                                                    },
                                                    campusId: campid,
                                                    status: 1,
                                                    createdBy: req.orgId,
                                                }
                                                allStudentMap.push(ppData);
                                            }

                                            // d.setMonth(d.getMonth() + 3);
                                            // d.setDate(1);


                                            // tcount = tcount + 1
                                            // let amtt = isNaN(stdfeeMaps[j]["Amount"]) ? 0.00 : stdfeeMaps[j]["Amount"]
                                            // let paystatus
                                            // if(Number(amtt)>Number(paidamt)){
                                            //     paystatus = "Partial"
                                            // }
                                            // else if(Number(amtt)==Number(paidamt)){
                                            //     paystatus = "Paid"
                                            // }
                                            // let transactionPayload = {
                                            //     "feesLedgerIds": [],
                                            //     "emailCommunicationRefIds": [mm.email],
                                            //     "smsCommunicationRefIds": [std.phone],
                                            //     "relatedTransactions": [],
                                            //     "softwareReconciled": false,
                                            //     "currency": "INR",
                                            //     "exchangeRate": 1,
                                            //     "displayName": `RCPT_2020-21_${
                                            //         tcount < 9 ? "00" : tcount < 99 ? "0" : ""
                                            //         }${Number(tcount) + 1}`,
                                            //     "transactionDate": trdate,
                                            //     "transactionType": "eduFees",
                                            //     "transactionSubType": "feePayment",
                                            //     "studentId": studentData._id,
                                            //     "studentName": `${studentData.firstName} ${studentData.lastName}`,
                                            //     "class": prplan.title,
                                            //     "academicYear": prplan.academicYear,
                                            //     "amount": paidamt,
                                            //     "studentRegId": studentData.regId,
                                            //     "receiptNo": `RCPT_2020-21_${
                                            //         tcount < 9 ? "00" : tcount < 99 ? "0" : ""
                                            //         }${Number(tcount) + 1}`,
                                            //     "programPlan": studentData.programPlanId,
                                            //     paymentRefId: stdfeeMaps[j]["Receipt Number"],
                                            //     "data": {
                                            //         "orgId": req.orgId,
                                            //         "displayName": `RCPT_2020-21_${
                                            //             tcount < 9 ? "00" : tcount < 99 ? "0" : ""
                                            //             }${Number(tcount) + 1}`,
                                            //         "transactionType": "eduFees",
                                            //         "transactionSubType": "feePayment",
                                            //         "mode": "cash",
                                            //         "method": "otc",
                                            //         "modeDetails": {
                                            //             "netBankingType": null,
                                            //             "walletType": null,
                                            //             "instrumentNo": null,
                                            //             "instrumentDate": trdate,
                                            //             "bankName": null,
                                            //             "cardDetails": {
                                            //                 "cardType": "",
                                            //                 "nameOnCard": `${studentData.firstName} ${studentData.lastName}`,
                                            //                 "cardNumber": ""
                                            //             },
                                            //             "transactionId": stdfeeMaps[j]["Receipt Number"],
                                            //             "remarks": "test"
                                            //         },
                                            //         "feesBreakUp": [{
                                            //             "feeTypeId": feetypesd._id,
                                            //             "feeType": "Tuition Fee ",
                                            //             "amount": paidamt,
                                            //             "feeTypeCode": "FT001"
                                            //         }]
                                            //     },
                                            //     "paymentTransactionId": stdfeeMaps[j]["Receipt Number"],
                                            //     "status": "Paid"
                                            // }
                                            // let newtransaction = new transactionModel(transactionPayload)

                                            // let feeLedgerPayload = {
                                            //     "transactionId": newtransaction._id,
                                            //     "transactionDate": trdate,
                                            //     "transactionDisplayName": newtransaction.displayName,
                                            //     "primaryTransaction": "",
                                            //     "feeTypeCode": "FT001",
                                            //     "dueAmount": pend,
                                            //     "pendingAmount": pend,
                                            //     "transactionType": "eduFees",
                                            //     "transactionSubType": "feePayment",
                                            //     "studentId": studentData._id,
                                            //     "studentRegId": studentData.regId,
                                            //     "studentName": `${studentData.firstName} ${studentData.lastName}`,
                                            //     "academicYear": prplan.academicYear,
                                            //     "class": prplan.title,
                                            //     "programPlan": prplan._id,
                                            //     "status": paystatus
                                            // }
                                            // let newFeeledger = new feesLedgerModel(feeLedgerPayload)
                                            // await newFeeledger.save()
                                            // newtransaction.feesLedgerIds = [newFeeledger._id]
                                            // await newtransaction.save();
                                            // if(Number(paidamt)>0){
                                            //     transcount++
                                            //     let newLedger = new feeLedgerModel({
                                            //         transactionDate: trdate,
                                            //         transactionDisplayName: `DN_2020-21_${
                                            //             transcount.toString().length == 1 ? "00" : transcount.toString().length == 2 ? "0" : ""
                                            //             }${Number(transcount) + 1}`,
                                            //         primaryTransaction: `DN_2020-21_001${
                                            //             transcount.toString().length == 1 ? "00" : transcount.toString().length == 2 ? "0" : ""
                                            //             }${Number(transcount) + 1}`,
                                            //         transactionType: "eduFees",
                                            //         transactionSubType: "demandNote",
                                            //         feeTypeCode: "FT001",
                                            //         dueAmount: Number(pend),
                                            //         paidAmount: Number(paidamt),
                                            //         refundAmount: 0,
                                            //         pendingAmount: Number(pend), // may remain unused?
                                            //         paymentTransactionId: stdfeeMaps[j]["Receipt Number"], // RazorPay or other txn id to be populeted on payment
                                            //         status: Number(pend)>0 ? "partially paid": "paid",
                                            //         studentId: studentData._id,
                                            //         studentRegId: stdfeeMaps[j]["USN"],
                                            //         studentName: `${studentData.firstName} ${studentData.lastName}`,
                                            //         academicYear: prplan.academicYear,
                                            //         class: prplan.description,
                                            //         programPlan: prplan._id,
                                            //       })
                                            //     let newTransaction = new transactionModel({
                                            //         displayName: `DN_2020-21_${
                                            //             transcount.toString().length == 1 ? "00" : transcount.toString().length == 2 ? "0" : ""
                                            //             }${Number(transcount) + 1}`,
                                            //         transactionType: "eduFees",
                                            //         transactionSubType: "demandNote",
                                            //         transactionDate: trdate,
                                            //         studentId: studentData._id,
                                            //         studentRegId: stdfeeMaps[j]["USN"],
                                            //         studentName: `${studentData.firstName} ${studentData.lastName}`,
                                            //         academicYear: prplan.academicYear,
                                            //         class: prplan.description,
                                            //         programPlan: studentData.programPlanId,
                                            //         paymentRefId: null,
                                            //         receiptNo: null,
                                            //         amount: Number(paidamt),
                                            //         feesLedgerIds: [newLedger._id],
                                            //         emailCommunicationRefIds: [studentData.email],
                                            //         smsCommunicationRefIds: [studentData.phoneNo],
                                            //         status: Number(pend)>0 ? "Partial": "Paid",
                                            //         relatedTransactions: '',
                                            //         paymentTransactionId: ''
                                            //       })
                                            //       newLedger.transactionId = newTransaction._id,

                                            //     await newTransaction.save()  
                                            //     await newLedger.save()  
                                            // }


                                            // await studentModel.updateOne({regId: ppInputData["Student ID"]},{rollNumber:stdfeeMaps[j]["USN"]});
                                        }
                                        if (allStudentMap.length > 0) {
                                            feeMapModel.insertMany(allStudentMap, async function (err2, fmp) {
                                                if (err2) {
                                                    if (err2) {
                                                        console.log(err2)
                                                        res.send(err2.toString())
                                                        // reject(res.status(200).json({
                                                        //     success: true,
                                                        //     message: err2.toString(),
                                                        //     count: 0,
                                                        // }))
                                                    }
                                                } else {
                                                    // resolve({ status: "success", message: "Excel Uploaded Successfully", data: newMasterUpload })
                                                    // createFeePlans(req, dbConnectionp, feeManagerDatas, feeTypeDetails, studentDetails, feeManagerSchema, feeStructureModel, studentModel, stdfeeMaps, campuses, allProgramPlan, allPaymentSchedule, finYear).then(ctresult => {
                                                    //     resolve({ status: "success", message: "Excel Uploaded Successfully", data: newMasterUpload })
                                                    // })
                                                    createFeePlans(req, dbConnectionp, feeManagerDatas, feeTypeDetails, studentDetails, feeManagerSchema, feeStructureModel, studentModel, stdfeeMaps, campuses, allProgramPlan, allPaymentSchedule, finYear, allStudentMap).then(ctresult => {
                                                        if (query.transactions == "true") {
                                                            createTransactions(req, stdfeeMaps, studentModel, allProgramPlan, allPaymentSchedule, feeTypeDetails, dbConnectionp, feeMapModel).then(ctresult => {
                                                                console.log("resolving")
                                                                resolve({ status: "success", message: "Excel Uploaded Successfully", data: newMasterUpload })
                                                            })
                                                        } else {
                                                            console.log("resolving")
                                                            resolve({ status: "success", message: "Excel Uploaded Successfully", data: newMasterUpload })
                                                        }
                                                    })
                                                }
                                            })
                                            // await feeMapModel.insertMany(allStudentMap);

                                        }
                                    }
                                })
                            }

                        }

                    }
                });
            }

        } else {
            return res.status(400).json({
                status: "failure",
                message: "Upload file does not exist",
                Error: err,
            });
        }
    })

};

async function initCheck2(req, masterUpladModel, dbConnectionp, newMasterUpload, instName, result, states, campuses, finYear, res) {
    let inputData = req;
    let dbName = req.orgId
    let response = { pp: [], stdsnew: [], stdsold: [], stdfmaps: [] }
    let programPlanSchema = dbConnectionp.model("programplans", ProgramPlanSchema);
    let feeStructureModel = dbConnectionp.model("feestructures", FeeStructureSchema);
    let paymentScheduleModel = dbConnectionp.model("paymentschedules", paymentScheduleSchema);
    let reminderModel = dbConnectionp.model("reminderplans", ReminderScheduleSchema);
    let lateFeeModel = dbConnectionp.model("latefees", LateFeesSchema);
    let installmentModel = dbConnectionp.model("installments", InstallmentSchema);
    let categoryModel = dbConnectionp.model("categoryplans", CategorySchema);
    let concessionModel = dbConnectionp.model("concessionplans", ConcessionSchema);
    let studentModel = dbConnectionp.model("students", StudentSchema);
    let feeManagerSchema = dbConnectionp.model("feemanagers", FeeManagerSchema);
    let feeMapModel = dbConnectionp.model("studentFeesMap", StudentFeeMapSchema);
    let feeTypeModel = dbConnectionp.model("feeTypes", FeeTypeSchema);
    let guardianSchema = dbConnectionp.model("guardian", GuardianSchema);
    let transactionModel = dbConnectionp.model("transactions", transactionsSchema);
    let feeLedgerModel = dbConnectionp.model("feeledgers", feesLedgerSchema)
    let campusModel = dbConnectionp.model("campuses", campusSchema)
    let transData = await transactionModel.find({})
    let tcount = transData.length
    return new Promise(async function (resolve, reject) {
        doc = [newMasterUpload]
        if (doc) {
            //   return res.status(200).json({ status: "success", data: doc });
            let pubnubConfig = {
                channel: inputData.orgId.toString(),
                message: {
                    description: { message: "Setup Initiated", data: 0 },
                    status: 0,
                },
            };
            let feeTypeDetails = await feeTypeModel.find({})
            let feeStructure = await feeStructureModel.find({})
            let paymentSchedule = await paymentScheduleModel.find({})
            let reminderPlan = await reminderModel.find({})
            let lateFeePlan = await lateFeeModel.find({})
            let installmentPlan = await installmentModel.find({})
            let categoryPlan = await categoryModel.find({})
            let cencessionPlan = await concessionModel.find({})
            let programPlanDatas = doc[0]["data"]["programPlans"];
            let feeManagerDatas = await feeManagerSchema.find({})
            let studentDetails = doc[0]["data"]["studentDetails"];
            let stdfeeMaps = doc[0]["data"]["studentFeeMaps"]
            //   let displayId = await getDisplayId("feeTypes", dbName, dbUrl);
            pubnubConfig.message.description = {
                message: `Institute Details has been added successfully.`,
                current: "Institute Details",
                next: "Fee Type"
            };
            pubnubConfig.message.status = 2;
            // await pubnub.publish(pubnubConfig);
            // var feeTypeDetails = []
            let i = 0
            //Fee Structure Add
            let allFeeStructure

            //Program Plan Add
            var allProgramPlan = [];
            var allPPlan = [];
            for (let j = 0; j < programPlanDatas.length; j++) {
                var ppInputData = programPlanDatas[j];
                let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                let campid = campid1._doc["_id"]
                let splitfromdate = ppInputData["From Date"].split("-")
                let splittodate = ppInputData["To Date"].split("-")
                let frdata = new Date(ppInputData["From Date"])
                let trdata = new Date(ppInputData["To Date"])
                let academicyear = `${frdata.getFullYear()}-${trdata.getFullYear().toString().slice(2, 4)}`
                var ppData = {
                    _id: mongoose.Types.ObjectId(),
                    displayName: `PP_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                        }${Number(j) + 1}`,
                    refid: ppInputData["Program Code *"],
                    title: ppInputData["Program Name *"],
                    fromDate: ppInputData["From Date"],
                    toDate: ppInputData["To Date"],
                    academicYear: academicyear,
                    description: ppInputData["Description"],
                    campusId: campid,
                    status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                    createdBy: instName,
                };
                let existPP = await programPlanSchema.findOne({ title: ppInputData["Program Name *"] })
                if (!existPP) {
                    allProgramPlan.push(ppData);
                }
                allPPlan.push(ppData)

            }
            if (allProgramPlan.length > 0) {
                // await programPlanSchema.insertMany(allProgramPlan);
                response.pp = allProgramPlan
                pubnubConfig.message.description = {
                    message: `Program Plan has been added successfully.`,
                    current: "Program Plans",
                    next: "Payment Schedules"
                };
                pubnubConfig.message.status = 24;
                // await pubnub.publish(pubnubConfig);
            }
            console.log("pp", allProgramPlan)
            //Payment Schdule Add
            var allPaymentSchedule = [];

            //ReminderPlan Add
            console.log("rr")
            var allReminderPlam = [];

            //LateFee Add
            var allLateFee = [];
            console.log("cc")
            //Category Add
            var allCategory = [];
            console.log("inst")
            //InstallmentPlan Add
            var allInstallment = [];
            console.log("con")
            var allConcession = [];
            console.log("std")
            var allStudents = [];
            for (let j = 0; j < studentDetails.length; j++) {
                let existStd = await studentModel.findOne({ regId: studentDetails[j]["Reg ID *"] })
                var ppInputData = studentDetails[j];
                let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                if (!campid1) {
                    console.log(ppInputData["Campus ID"], campuses)
                }
                let campid = campid1._doc["_id"];
                if (!campid) {
                    ppInputData
                }
                var guardianDetails = {
                    isPrimary: true,
                    firstName: ppInputData["Parent Name"],
                    lastName: ppInputData["Parent Name"],
                    fullName: ppInputData["Parent Name"],
                    mobile: ppInputData["Parent Phone Number"],
                    email: ppInputData["Parent Email Address"],
                    PIIUsageFlag: true,
                    PIIUsageFlagUpdated: new Date(),
                    fatherDetails: {},
                    motherDetails: {},
                    guardianDetails: {},
                    relation: "Parent",
                    createdBy: instName,
                };
                if (existStd) {
                    // var guardianResponse = await guardianData.save();
                    // console.log("guardiandetails", ppInputData["feeStructure"], ppInputData["Program Plan ID"]);
                    // console.log(feeStructureData,programPlanData)
                    var ppData = {
                        displayName: `STU_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                            }${Number(j) + 1}`,
                        regId: ppInputData["Reg ID *"] == "-" ? ppInputData["HEDA ID"] : ppInputData["Reg ID *"],
                        rollNumber: ppInputData["HEDA ID"] == "-" ? ppInputData["Reg ID *"] : ppInputData["HEDA ID"],
                        salutation:
                            ppInputData["Salutation"] != undefined
                                ? ppInputData["Salutation"]
                                : null, // salutation
                        category: ppInputData["Category"], // Category
                        section: ppInputData["Section"] ? ppInputData["Section"] : "A",
                        firstName: ppInputData["First Name *"], //First Name *
                        middleName: ppInputData["Middle Name"], //
                        lastName: ppInputData["Last Name *"], //Last Name *
                        guardianDetails: existStd._doc.guardianDetails,
                        gender: ppInputData["Gender"], //Gender
                        dob: null,
                        citizenship: ppInputData["Citizenship"], //
                        currency: ppInputData["Currency"], //
                        FOREX: ppInputData["FOREX"], //
                        admittedOn: null,
                        // admittedOn: new Date(ppInputData["Admitted Date"]) instanceof Date ? new Date(ppInputData["Admitted Date"]) : null, //Admitted Date *
                        programPlanId: existStd._doc.programPlanId,
                        feeStructureId: existStd._doc.feeStructureId,
                        phoneNo: ppInputData["Phone Number *"], //Phone Number *
                        email: ppInputData["Email Address *"], // Email Address *
                        alternateEmail:
                            ppInputData["alterEmail"] != undefined
                                ? ppInputData["alterEmail"]
                                : null,
                        parentName: ppInputData["Parent Name"],
                        parentPhone: ppInputData["Parent Phone Number"],
                        parentEmail: ppInputData["Parent Email Address"],
                        relation: "parent",
                        addressDetails: {
                            address1: ppInputData["Address 1"],
                            address2: ppInputData["Address 2"],
                            address3: ppInputData["Address 3"],
                            city: ppInputData["City/Town"],
                            state: ppInputData["State"],
                            country: ppInputData["Country"], //Country
                            pincode: ppInputData["PIN Code"], //PIN Code
                        },
                        isFinalYear: ppInputData["Is Final Year"].toLowerCase() == "yes" ? true : false,
                        final: ppInputData["Final"],
                        campusId: campid,
                        status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                        createdBy: instName,
                    };
                    allStudents.push(ppData);
                    response.stdsold.push(ppData);
                    await studentModel.updateOne({ regId: ppInputData["Reg ID *"] }, { $set: ppData })
                    let updateguardian = await guardianSchema.updateOne({ _id: existStd._doc.guardianDetails[0] }, { $set: guardianDetails });
                }
                else if (!existStd) {
                    let guardianData = new guardianSchema(guardianDetails);
                    await guardianData.save();
                    // var guardianResponse = await guardianData.save();
                    // console.log("guardiandetails", ppInputData["feeStructure"], ppInputData["Program Plan ID"]);
                    let programPlanData = await allPPlan.find(item => item.refid.trim() == ppInputData["Program Plan ID"]);
                    if (!programPlanData) {
                        let programPlanData1 = await allPPlan.find(item => item.refid.trim() == ppInputData["Program Plan ID"]);
                        programPlanData = await programPlanSchema.findOne({ title: programPlanData1.title });
                    }
                    // console.log("prorgamplan", programPlanData);
                    let feeStructureData = await feeStructureModel.findOne({ campusId: campid.toString() });
                    // console.log(feeStructureData,programPlanData)
                    var ppData = {
                        _id: mongoose.Types.ObjectId(),
                        displayName: `STU_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                            }${Number(j) + 1}`,
                        regId: ppInputData["Reg ID *"] == "-" ? ppInputData["HEDA ID"] : ppInputData["Reg ID *"],
                        rollNumber: ppInputData["HEDA ID"] == "-" ? ppInputData["Reg ID *"] : ppInputData["HEDA ID"],
                        salutation:
                            ppInputData["Salutation"] != undefined
                                ? ppInputData["Salutation"]
                                : null, // salutation
                        category: ppInputData["Category"], // Category
                        section: ppInputData["Section"] ? ppInputData["Section"] : "A",
                        firstName: ppInputData["First Name *"], //First Name *
                        middleName: ppInputData["Middle Name"], //
                        lastName: ppInputData["Last Name *"], //Last Name *
                        guardianDetails: [guardianData._id],
                        gender: ppInputData["Gender"], //Gender
                        dob: null,
                        citizenship: ppInputData["Citizenship"], //
                        currency: ppInputData["Currency"], //
                        FOREX: ppInputData["FOREX"], //
                        admittedOn: null,
                        // admittedOn: new Date(ppInputData["Admitted Date"]) instanceof Date ? new Date(ppInputData["Admitted Date"]) : null, //Admitted Date *
                        programPlanId: programPlanData._id,
                        feeStructureId: feeStructureData._doc._id,
                        phoneNo: ppInputData["Phone Number *"], //Phone Number *
                        email: ppInputData["Email Address *"], // Email Address *
                        alternateEmail:
                            ppInputData["alterEmail"] != undefined
                                ? ppInputData["alterEmail"]
                                : null,
                        parentName: ppInputData["Parent Name"],
                        parentPhone: ppInputData["Parent Phone Number"],
                        parentEmail: ppInputData["Parent Email Address"],
                        relation: "parent",
                        addressDetails: {
                            address1: ppInputData["Address 1"],
                            address2: ppInputData["Address 2"],
                            address3: ppInputData["Address 3"],
                            city: ppInputData["City/Town"],
                            state: ppInputData["State"],
                            country: ppInputData["Country"], //Country
                            pincode: ppInputData["PIN Code"], //PIN Code
                        },
                        isFinalYear: ppInputData["Is Final Year"].toLowerCase() == "yes" ? true : false,
                        final: ppInputData["Final"],
                        campusId: campid,
                        status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                        createdBy: instName,
                    };
                    allStudents.push(ppData);
                    response.stdsnew.push(ppData);
                    let newstd = new studentModel(ppData);
                    await newstd.save()
                }

            }
            if (allStudents.length > 0) {
                // await studentModel.insertMany(allStudents);
                pubnubConfig.message.description = {
                    message: `Students has been added successfully.`,
                    current: "Student Details",
                    next: "Fee Manager"
                };
                pubnubConfig.message.status = 88;
                // await pubnub.publish(pubnubConfig);
                console.log("students added successfully")
            }

            // Fee Manager added
            var allFeeManager = [];

            console.log("student fee map started")
            var allStudentMap = [];
            let feetypesd = await feeTypeModel.findOne({ displayName: "FT001" })
            for (let j = 0; j < stdfeeMaps.length; j++) {
                let transdata = await transactionModel.find({})
                let transcount = transdata.length
                var ppInputData = stdfeeMaps[j];
                let studentData = await studentModel.findOne({
                    rollNumber: ppInputData["Student ID"],
                });

                if (studentData === null) {
                    studentData = await studentModel.findOne({
                        regId: ppInputData["USN"],
                    });
                }
                if (studentData === null) {
                    studentData = await studentModel.findOne({
                        regId: ppInputData["Reg ID *"],
                    });
                }
                if (studentData === null) {
                    studentData = await studentModel.findOne({
                        regId: ppInputData["HEDA ID"],
                    });
                }
                if (studentData === null) {
                    studentData = await response.stdsnew.find(item => item["regId"] == ppInputData["USN"]);
                }
                let existStdmaps
                let campid
                if (studentData._doc) {
                    campid = studentData._doc["campusId"];
                    existStdmaps = await feeMapModel.findOne({ studentId: studentData._doc._id })
                }
                else {
                    campid = studentData["campusId"];
                    existStdmaps = await feeMapModel.findOne({ studentId: studentData._id })
                }
                // let existStdmaps = await feeMapModel.findOne({studentId:studentData._doc._id})

                // let campid = studentData._doc["campusId"];
                let months = ["First", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
                let duemonths = { "Year": 12, "Half-Year": 6, "Quarter": 3, "Month": 1, "One time date": 0, "One time date range": 0 }
                let dueDates = { "Second": 2, "Third": 3, "Fourth": 4, "Fifth": 5, "Last": 30, "Second Last": 28, "Third last": 27, "Fourth last": 2 }
                if (!studentData) {
                    console.log(stdfeeMaps[j])
                }
                let prplan = await programPlanSchema.findOne({ _id: studentData.programPlanId })

                if (!prplan) {
                    stdfeeMaps[j]
                }
                // let paymentSchedule = allPaymentSchedule[0]
                // let prspl = prplan.fromDate.split(" ")
                // let prmonth = prspl[0]
                // let mindex = months.indexOf(prmonth)
                // let pryear = prspl[2]
                // let dueD = paymentSchedule.scheduleDetails.collectEvery
                // let dueM = duemonths[paymentSchedule.scheduleDetails.collectEvery]
                // let nextDueD = dueM + mindex
                // let dueDatem
                // if (dueM == 12) {
                //     dueDatem = new Date("2021-04-01")
                // }
                // else {
                //     dueDatem = `${months[nextDueD]} ${Number(dueDates[paymentSchedule.scheduleDetails.dueDate]) + 1}, ${pryear}`
                // }
                var d = new Date("2021-04-01");
                var d2 = new Date("2021-04-01");
                let amountp = stdfeeMaps[j]["Amount"] ? stdfeeMaps[j]["Amount"].replace(",", "") : "0";
                amountp = amountp.replace(/ +/g, "");
                let amountr = isNaN(Number(stdfeeMaps[j]["Amount Received"])) ? "0" : stdfeeMaps[j]["Amount Received"].replace(",", "");
                amountr = amountr.replace(/ +/g, "");
                let pend = parseFloat(amountp) - parseFloat(amountr)
                if (isNaN(pend) && !isNaN(amountp)) {
                    pend = amountp
                }
                else if (isNaN(amountp)) {
                    pend = 0.00
                }
                let paidamt = isNaN(amountr) ? "0" : amountr
                let rid = 0;
                trdate = null
                let feesbkp = []
                let paidamts = []
                for (ll = 0; ll < feeTypeDetails.length; ll++) {
                    if (feeTypeDetails[ll]._doc.title.trim().toLowerCase().includes("term") && campid.toString() == feeTypeDetails[ll]._doc["campusId"].toString()) {
                        feesbkp.push({
                            amount: isNaN(amountp) ? 0.00 : amountp,
                            paid: 0.00,
                            pending: isNaN(amountp) ? 0.00 : amountp,
                            feeTypeCode: feeTypeDetails[ll]._doc["displayName"],
                            title: feeTypeDetails[ll]._doc["title"],
                        })
                        paidamts.push(0)
                    }
                    // else if (campid.toString() == feeTypeDetails[ll]["campusId"].toString()) {
                    //     feesbkp.push({
                    //         amount: 0.00,
                    //         paid: 0.00,
                    //         pending: 0.00,
                    //         feeTypeCode: feeTypeDetails[ll]["displayName"],
                    //         title: feeTypeDetails[ll]["title"],
                    //     })
                    //     paidamts.push(0)
                    // }
                }
                var ppData
                if (!existStdmaps && Number(amountp) > 0) {
                    ppData = {
                        displayName: `SFM_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                            }${Number(j) + 1}`,
                        studentId: studentData._id,
                        usn: stdfeeMaps[j]["USN"],
                        programPlanId: studentData.programPlanId,
                        feeStructureId: studentData.feeStructureId,
                        feeManagerId: undefined,
                        dueDate: d2.toISOString(),
                        amount: isNaN(amountp) ? 0.00 : amountp,
                        paid: 0.00,
                        receivedDate: trdate,
                        receiptNumbers: stdfeeMaps[j]["Receipt Number"],
                        concession: 0,
                        fine: 0,
                        pending: isNaN(amountp) ? 0.00 : amountp,
                        transactionPlan: {
                            feesBreakUp: feesbkp,
                            totalAmount: isNaN(amountp) ? 0.00 : amountp,
                            paidAmount: paidamts,
                            totalPaid: 0.00,
                            totalPending: isNaN(amountp) ? 0.00 : amountp
                        },
                        campusId: campid,
                        status: 1,
                        createdBy: req.orgId,
                    }
                    allStudentMap.push(ppData);
                    response.stdfmaps.push(ppData)
                }
            }
            console.log(allStudentMap)
            if (allStudentMap.length > 0) {
                feeMapModel.insertMany(allStudentMap, async function (err2, fmp) {
                    if (err2) {
                        if (err2) {
                            console.log(err2)
                            res.send(err2.toString())
                        }
                    } else {
                        // createTransactions(req, stdfeeMaps, studentModel, allProgramPlan, allPaymentSchedule, feeTypeDetails, dbConnectionp, feeMapModel).then(ctresult => {
                        console.log("resolving")
                        // let buffer = new Buffer(JSON.stringify(response))
                        // fs.writeFile(`resp.json`, buffer, async function (err) {
                        //     resolve(res.send(response))

                        //     // if (err) {
                        //     //     res.send(err);
                        //     // }
                        //     console.log("file saved")
                        // });
                        createFeePlansN(req, dbConnectionp, feeManagerDatas, feeTypeDetails, studentDetails, feeManagerSchema, feeStructureModel, studentModel, stdfeeMaps, campuses, allProgramPlan, allPaymentSchedule, finYear, allStudentMap).then(ctresult => {
                            // resolve({ status: "success", message: "Excel Uploaded Successfully", data: newMasterUpload })
                            resolve({ message: "feeplans updated successfully" })
                        })
                        // })
                    }
                })
                // await feeMapModel.insertMany(allStudentMap);

            }


        } else {
            return res.status(400).json({
                status: "failure",
                message: "Upload file does not exist",
                Error: err,
            });
        }
    })

};


async function initUpdate(req, masterUpladModel, dbConnectionp, newMasterUpload, instName, result, states, campuses, finYear, res) {
    let inputData = req;
    let dbName = req.orgId
    // let dbConnection = await createDatabase(dbName, dbUrl);
    // let masterUpladModel = dbConnection.model(
    //     "masteruploads",
    //     masterUploadSchema
    // );
    let programPlanSchema = dbConnectionp.model("programplans", ProgramPlanSchema);
    let feeStructureModel = dbConnectionp.model("feestructures", FeeStructureSchema);
    let paymentScheduleModel = dbConnectionp.model("paymentschedules", paymentScheduleSchema);
    let reminderModel = dbConnectionp.model("reminderplans", ReminderScheduleSchema);
    let lateFeeModel = dbConnectionp.model("latefees", LateFeesSchema);
    let installmentModel = dbConnectionp.model("installments", InstallmentSchema);
    let categoryModel = dbConnectionp.model("categoryplans", CategorySchema);
    let concessionModel = dbConnectionp.model("concessionplans", ConcessionSchema);
    let studentModel = dbConnectionp.model("students", StudentSchema);
    let feeManagerSchema = dbConnectionp.model("feemanagers", FeeManagerSchema);
    let feeMapModel = dbConnectionp.model("studentFeesMap", StudentFeeMapSchema);
    let feeTypeModel = dbConnectionp.model("feeTypes", FeeTypeSchema);
    let guardianSchema = dbConnectionp.model("guardian", GuardianSchema);
    let transactionModel = dbConnectionp.model("transactions", transactionsSchema);
    let feeLedgerModel = dbConnectionp.model("feeledgers", feesLedgerSchema)
    let campusModel = dbConnectionp.model("campuses", campusSchema)
    let transData = await transactionModel.find({})
    let tcount = transData.length
    return new Promise(async function (resolve, reject) {
        doc = [newMasterUpload]
        if (doc) {
            //   return res.status(200).json({ status: "success", data: doc });
            let pubnubConfig = {
                channel: inputData.orgId.toString(),
                message: {
                    description: { message: "Setup Initiated", data: 0 },
                    status: 0,
                },
            };
            let feeType = doc[0]["data"]["feeTypes"];
            let feeStructure = doc[0]["data"]["feeStructures"];
            let paymentSchedule = doc[0]["data"]["paymentSchedule"];
            let reminderPlan = doc[0]["data"]["reminderPlan"];
            let lateFeePlan = doc[0]["data"]["lateFeePlan"];
            let installmentPlan = doc[0]["data"]["installmentPlan"];
            let categoryPlan = doc[0]["data"]["categoryPlan"];
            let cencessionPlan = doc[0]["data"]["cencessionPlan"];
            let programPlanDatas = doc[0]["data"]["programPlans"];
            let feeManagerDatas = doc[0]["data"]["feeManagers"];
            let studentDetails = await studentModel.find({});
            let stdfeeMaps = doc[0]["data"]["studentFeeMaps"]
            //   let displayId = await getDisplayId("feeTypes", dbName, dbUrl);
            pubnubConfig.message.status = 2;
            // await pubnub.publish(pubnubConfig);
            var feeTypeDetails = []
            let i = 0
            let cc = 0;
            // console.log(feeTypeDetails)
            //    let feeStructure = await createFeeStructure(docs,feeStructure)
            console.log("stresr")
            //Program Plan Add
            var allProgramPlan = [];
            console.log("pp")
            //Payment Schdule Add
            var allPaymentSchedule = [];
            let ccps = 0
            //ReminderPlan Add
            console.log("rr")
            var allReminderPlam = [];
            let ccrm = 0;
            //LateFee Add
            var allLateFee = [];
            let cclf = 0;
            console.log("cc")
            //Category Add
            var allCategory = [];
            let ccct = 0;
            console.log("inst")
            //InstallmentPlan Add
            var allInstallment = [];
            let ccin = 0;
            console.log("con")
            //Concession Add
            let allConcession = [];
            console.log(cencessionPlan.length)
            let cccn = 0;

            console.log("std")
            //student Add
            var allStudents = [];

            // Fee Manager added
            var allFeeManager = [];
            let ccfm = 0;

            //Student Fees Map
            console.log("student fee map started")
            var allStudentMap = [];
            // resolve({ status: "success", message: "Excel Uploaded Successfully", data: newMasterUpload })
            // createFeePlans(req, dbConnectionp, feeManagerDatas, feeTypeDetails, studentDetails, feeManagerSchema, feeStructureModel, studentModel, stdfeeMaps, campuses, allProgramPlan, allPaymentSchedule, finYear).then(ctresult => {
            //     resolve({ status: "success", message: "Excel Uploaded Successfully", data: newMasterUpload })
            // })
            createFeePlansAc(req, dbConnectionp, studentDetails, feeStructureModel, studentModel, stdfeeMaps, campuses, allProgramPlan, allPaymentSchedule, finYear, allStudentMap).then(ctresult => {
                // createTransactions(req, stdfeeMaps, studentModel, allProgramPlan, allPaymentSchedule, feeTypeDetails, dbConnectionp, feeMapModel).then(ctresult => {
                console.log("resolving")
                // resolve({ status: "success", message: "Excel Uploaded Successfully", data: newMasterUpload })
                resolve({ status: "success", message: "Excel Uploaded Successfully", data: newMasterUpload })
                // })
            })

            // await feeMapModel.insertMany(allStudentMap);

        } else {
            return res.status(400).json({
                status: "failure",
                message: "Upload file does not exist",
                Error: err,
            });
        }
    })

};


async function initCheckUpdate(req, masterUpladModel, dbConnectionp, newMasterUpload, instName, result, states, campuses, finYear, res) {
    let inputData = req;
    let dbName = req.orgId
    // let dbConnection = await createDatabase(dbName, dbUrl);
    // let masterUpladModel = dbConnection.model(
    //     "masteruploads",
    //     masterUploadSchema
    // );
    let programPlanSchema = dbConnectionp.model("programplans", ProgramPlanSchema);
    let feeStructureModel = dbConnectionp.model("feestructures", FeeStructureSchema);
    let paymentScheduleModel = dbConnectionp.model("paymentschedules", paymentScheduleSchema);
    let reminderModel = dbConnectionp.model("reminderplans", ReminderScheduleSchema);
    let lateFeeModel = dbConnectionp.model("latefees", LateFeesSchema);
    let installmentModel = dbConnectionp.model("installments", InstallmentSchema);
    let categoryModel = dbConnectionp.model("categoryplans", CategorySchema);
    let concessionModel = dbConnectionp.model("concessionplans", ConcessionSchema);
    let studentModel = dbConnectionp.model("students", StudentSchema);
    let feeManagerSchema = dbConnectionp.model("feemanagers", FeeManagerSchema);
    let feeMapModel = dbConnectionp.model("studentFeesMap", StudentFeeMapSchema);
    let feeTypeModel = dbConnectionp.model("feeTypes", FeeTypeSchema);
    let guardianSchema = dbConnectionp.model("guardian", GuardianSchema);
    let transactionModel = dbConnectionp.model("transactions", transactionsSchema);
    let feeLedgerModel = dbConnectionp.model("feeledgers", feesLedgerSchema)
    let campusModel = dbConnectionp.model("campuses", campusSchema)
    let transData = await transactionModel.find({})
    let tcount = transData.length
    return new Promise(async function (resolve, reject) {
        doc = [newMasterUpload]
        if (doc) {
            //   return res.status(200).json({ status: "success", data: doc });
            let pubnubConfig = {
                channel: inputData.orgId.toString(),
                message: {
                    description: { message: "Setup Initiated", data: 0 },
                    status: 0,
                },
            };
            let feeType = doc[0]["data"]["feeTypes"];
            let feeStructure = doc[0]["data"]["feeStructures"];
            let paymentSchedule = doc[0]["data"]["paymentSchedule"];
            let reminderPlan = doc[0]["data"]["reminderPlan"];
            let lateFeePlan = doc[0]["data"]["lateFeePlan"];
            let installmentPlan = doc[0]["data"]["installmentPlan"];
            let categoryPlan = doc[0]["data"]["categoryPlan"];
            let cencessionPlan = doc[0]["data"]["cencessionPlan"];
            let programPlanDatas = doc[0]["data"]["programPlans"];
            let feeManagerDatas = doc[0]["data"]["feeManagers"];
            let studentDetails = doc[0]["data"]["studentDetails"];
            let stdfeeMaps = doc[0]["data"]["studentFeeMaps"]
            //   let displayId = await getDisplayId("feeTypes", dbName, dbUrl);
            pubnubConfig.message.description = {
                message: `Institute Details has been added successfully.`,
                current: "Institute Details",
                next: "Fee Type"
            };
            pubnubConfig.message.status = 2;
            // await pubnub.publish(pubnubConfig);
            var feeTypeDetails = []
            let i = 0
            for (let i = 0; i < feeType.length; i++) {
                // console.log(i)
                let x = feeType[i];
                console.log(x["Campus ID"])
                if (x["Campus ID"].toLowerCase() == "all") {
                    let campid
                    campuses.forEach(async function (ktm) {
                        campid = ktm["_id"]
                        var newFeeTypes = {
                            _id: mongoose.Types.ObjectId(),
                            displayName: `FT_${finYear}_${(Number(i) + 1).toString().length == 1 ? "00" : (Number(i) + 1).toString().length == 2 ? "0" : ""
                                }${Number(i) + 1}`,
                            refid: x["ID *"],
                            title: x["Fee Type *"],
                            description: x["Description"],
                            roleView: x["Role View"].split(":"),
                            partialAllowed: x["Partial Allowed"],
                            campusId: campid,
                            status: x["Status"].toLowerCase() == "active" ? 1 : 0,
                            createdBy: instName,
                        };
                        feeTypeDetails.push(newFeeTypes);
                    })
                }
                else {
                    let campid
                    let campid1 = await campuses.find(item => item.campusId == x["Campus ID"])
                    campid = campid1["_id"]
                    var newFeeTypes = {
                        _id: mongoose.Types.ObjectId(),
                        displayName: `FT_${finYear}_${(Number(i) + 1).toString().length == 1 ? "00" : (Number(i) + 1).toString().length == 2 ? "0" : ""
                            }${Number(i) + 1}`,
                        refid: x["ID *"],
                        title: x["Fee Type *"],
                        description: x["Description"],
                        roleView: x["Role View"].split(":"),
                        partialAllowed: x["Partial Allowed"],
                        campusId: campid,
                        status: x["Status"].toLowerCase() == "active" ? 1 : 0,
                        createdBy: instName,
                    };
                    feeTypeDetails.push(newFeeTypes);
                }


                // var newFeeTypes = {
                //     _id: mongoose.Types.ObjectId(),
                //     displayName: `FT${
                //         (Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                //         }${Number(j) + 1}`,
                //     refid: x["ID *"],
                //     title: x["Fee Type *"],
                //     description: x["Description"],
                //     roleView: x["Role View"].split(":"),
                //     partialAllowed: x["Partial Allowed"],
                //     campusId: campid,
                //     status: x["Status"].toLowerCase() == "active" ? 1 : 0,
                //     createdBy: instName,
                // };
                // return newFeeTypes;
            }
            // console.log(feeTypeDetails)
            if (feeTypeDetails.length > 0) {
                console.log("ad")

                feeTypeModel.insertMany(feeTypeDetails, async function (error, docs) {
                    if (error) {
                        if (error.name === "BulkWriteError" && error.code === 11000) {
                            // reject(res.status(200).json({
                            //     success: true,
                            //     message: error.toString(),
                            //     count: 0,
                            // }))
                            console.log("error", error)
                        }
                    } else {
                        //    let feeStructure = await createFeeStructure(docs,feeStructure)
                        console.log("stresr")
                        pubnubConfig.message.description = {
                            message: `Fee Type has been added successfully.`,
                            current: "Fee Types",
                            next: "Fee Structures"
                        };
                        pubnubConfig.message.status = 8;
                        // await pubnub.publish(pubnubConfig);
                        //Fee Structure Add
                        let allFeeStructure = [];
                        for (let j = 0; j < feeStructure.length; j++) {
                            var ppInputData = feeStructure[j];
                            if (ppInputData["Campus ID"].toLowerCase() == "all") {
                                for (let k = 0; k < campuses.length; k++) {
                                    var feeTypeData = [];
                                    let ftd = 0
                                    for (ftd; ftd < feeStructure[j]["Fee Types *"].length; ftd++) {
                                        var ftDet = await feeTypeDetails.find(item => item.refid.trim() == feeStructure[j]["Fee Types *"][ftd].trim() && item["campusId"] == campuses[k]["_id"]);
                                        if (ftDet == null) {
                                            feeTypeData.push(null);
                                        }
                                        else {
                                            feeTypeData.push(ftDet._id);
                                        }
                                        // console.log("ftDet", ftDet);
                                    }
                                    // console.log(feeTypeData)
                                    let campid = campuses[k]["_id"];
                                    var ppData = {
                                        _id: mongoose.Types.ObjectId(),
                                        displayName: `FS_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                            }${Number(j) + 1}`,
                                        refid: ppInputData["ID *"],
                                        title: ppInputData["Title *"],
                                        description: ppInputData["Description"],
                                        feeTypeIds: feeTypeData,
                                        campusId: campid,
                                        status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                        createdBy: instName,
                                    };
                                    await allFeeStructure.push(ppData);
                                    // console.log("allFeeStructure1", allFeeStructure)

                                }
                            }
                            else {
                                let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                                let campid = campid1["_id"];
                                var feeTypeData = [];
                                let ftd = 0
                                for (ftd = 0; ftd < feeStructure[j]["Fee Types *"].length; ftd++) {
                                    var ftDet = await feeTypeDetails.find(item => item.refid.trim() == feeStructure[j]["Fee Types *"][ftd].trim() && item["campusId"] == campid1["_id"]);
                                    if (ftDet == null) {
                                        feeTypeData.push(null);
                                    }
                                    else {
                                        feeTypeData.push(ftDet._id);
                                    }

                                }
                                var ppData = {
                                    _id: mongoose.Types.ObjectId(),
                                    displayName: `FS_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                        }${Number(j) + 1}`,
                                    refid: ppInputData["ID *"],
                                    title: ppInputData["Title *"],
                                    description: ppInputData["Description"],
                                    feeTypeIds: feeTypeData,
                                    campusId: campid,
                                    status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                    createdBy: instName,
                                };
                                // console.log("allFeeStructure1", allFeeStructure)
                                await allFeeStructure.push(ppData);
                                console.log("ftDet", allFeeStructure.length);
                            }
                            console.log(j + 1 == feeStructure.length, allFeeStructure.length)
                            if (allFeeStructure.length > 0) {
                                feeStructureModel.insertMany(allFeeStructure, async function (error2, docs2) {
                                    if (error2) {
                                        // if (error2.name === "BulkWriteError" && error2.code === 11000) {
                                        //     reject(res.status(200).json({
                                        //         success: true,
                                        //         message: error2.toString(),
                                        //         count: 0,
                                        //     }))
                                        // }
                                    } else {
                                        console.log(`Fees Structure has been added successfully.`, docs2)
                                        pubnubConfig.message.description = {
                                            message: `Fees Structure has been added successfully.`,
                                            current: "Fee Structures",
                                            next: "Program Plans"
                                        };
                                        pubnubConfig.message.status = 16;
                                        // await pubnub.publish(pubnubConfig);
                                        // pubnubConfig.message.description = {
                                        //     message: `fees Structure has been added successfully.`,
                                        // };
                                        // pubnubConfig.message.status = 16;
                                        // await pubnub.publish(pubnubConfig);

                                        //Program Plan Add
                                        var allProgramPlan = [];
                                        for (let j = 0; j < programPlanDatas.length; j++) {
                                            var ppInputData = programPlanDatas[j];
                                            if (ppInputData["Campus ID"].toLowerCase() == "all") {
                                                campuses.forEach(async function (ktm) {
                                                    let campid = ktm["_id"]
                                                    let splitfromdate = ppInputData["From Date"].split("-")
                                                    let splittodate = ppInputData["To Date"].split("-")
                                                    let frdata = new Date(ppInputData["From Date"])
                                                    let trdata = new Date(ppInputData["To Date"])
                                                    let academicyear = `${frdata.getFullYear()}-${trdata.getFullYear().toString().slice(2, 4)}`
                                                    var ppData = {
                                                        _id: mongoose.Types.ObjectId(),
                                                        displayName: `PP_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                            }${Number(j) + 1}`,
                                                        refid: ppInputData["Program Code *"],
                                                        title: ppInputData["Program Name *"],
                                                        fromDate: ppInputData["From Date"],
                                                        toDate: ppInputData["To Date"],
                                                        academicYear: academicyear,
                                                        description: ppInputData["Description"],
                                                        campusId: campid,
                                                        status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                        createdBy: instName,
                                                    };
                                                    allProgramPlan.push(ppData);
                                                })
                                            }
                                            else {
                                                let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                                                let campid = campid1["_id"]
                                                let splitfromdate = ppInputData["From Date"].split("-")
                                                let splittodate = ppInputData["To Date"].split("-")
                                                let frdata = new Date(ppInputData["From Date"])
                                                let trdata = new Date(ppInputData["To Date"])
                                                let academicyear = `${frdata.getFullYear()}-${trdata.getFullYear().toString().slice(2, 4)}`
                                                var ppData = {
                                                    _id: mongoose.Types.ObjectId(),
                                                    displayName: `PP_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                        }${Number(j) + 1}`,
                                                    refid: ppInputData["Program Code *"],
                                                    title: ppInputData["Program Name *"],
                                                    fromDate: ppInputData["From Date"],
                                                    toDate: ppInputData["To Date"],
                                                    academicYear: academicyear,
                                                    description: ppInputData["Description"],
                                                    campusId: campid,
                                                    status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                    createdBy: instName,
                                                };
                                                allProgramPlan.push(ppData);
                                            }

                                        }
                                        if (allProgramPlan.length > 0) {
                                            await programPlanSchema.insertMany(allProgramPlan);
                                            pubnubConfig.message.description = {
                                                message: `Program Plan has been added successfully.`,
                                                current: "Program Plans",
                                                next: "Payment Schedules"
                                            };
                                            pubnubConfig.message.status = 24;
                                            // await pubnub.publish(pubnubConfig);
                                        }
                                        console.log("pp")
                                        //Payment Schdule Add
                                        var allPaymentSchedule = [];
                                        for (let j = 0; j < paymentSchedule.length; j++) {
                                            var ppInputData = paymentSchedule[j];

                                            if (ppInputData["Campus ID"].toLowerCase() == "all") {
                                                campuses.forEach(async function (ktm) {
                                                    let campid = ktm["_id"]
                                                    var ppData = {
                                                        _id: mongoose.Types.ObjectId(),
                                                        displayName: `PYMYSCH_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                            }${Number(j) + 1}`,
                                                        title: ppInputData["Title"],
                                                        refid: ppInputData["ID *"],
                                                        description: ppInputData["Description"],
                                                        scheduleDetails: {
                                                            collectEvery: ppInputData["Collection Period"],
                                                            dueDate: ppInputData["Due By"],
                                                            penaltyStartDate: ppInputData["Penalty Start Date"],
                                                        },
                                                        period: ppInputData["Period"],
                                                        startMonth: ppInputData["Start Month"],
                                                        feesBreakUp: ppInputData["Percentage"],
                                                        campusId: campid,
                                                        status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                        createdBy: instName,
                                                    };
                                                    allPaymentSchedule.push(ppData);
                                                })
                                            }
                                            else {
                                                let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                                                let campid = campid1["_id"]
                                                var ppData = {
                                                    _id: mongoose.Types.ObjectId(),
                                                    displayName: `PYMYSCH_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                        }${Number(j) + 1}`,
                                                    title: ppInputData["Title"],
                                                    refid: ppInputData["ID *"],
                                                    description: ppInputData["Description"],
                                                    scheduleDetails: {
                                                        collectEvery: ppInputData["Collection Period"],
                                                        dueDate: ppInputData["Due By"],
                                                        penaltyStartDate: ppInputData["Penalty Start Date"],
                                                    },
                                                    period: ppInputData["Period"],
                                                    startMonth: ppInputData["Start Month"],
                                                    feesBreakUp: ppInputData["Percentage"],
                                                    campusId: campid,
                                                    status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                    createdBy: instName,
                                                };
                                                allPaymentSchedule.push(ppData);
                                            }
                                            // let feesbup = [];
                                            // ppInputData["Percentage"].forEach(async function(it){

                                            // })

                                        }
                                        if (allPaymentSchedule.length > 0) {
                                            await paymentScheduleModel.insertMany(allPaymentSchedule);
                                            pubnubConfig.message.description = {
                                                message: `Payment Schdule has been added successfully.`,
                                                current: "Payment Schedules",
                                                next: "Reminder Plans"
                                            };
                                            pubnubConfig.message.status = 32;
                                            // await pubnub.publish(pubnubConfig);
                                        }

                                        //ReminderPlan Add
                                        console.log("rr")
                                        var allReminderPlam = [];
                                        for (let j = 0; j < reminderPlan.length; j++) {
                                            var ppInputData = reminderPlan[j];
                                            if (ppInputData["Campus ID"].toLowerCase() == "all") {
                                                campuses.forEach(async function (ktm) {
                                                    let campid = ktm["_id"]
                                                    var ppData = {
                                                        _id: mongoose.Types.ObjectId(),
                                                        displayName: `REM_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                            }${Number(j) + 1}`,
                                                        refid: ppInputData["ID *"],
                                                        title: ppInputData["Title"],
                                                        description: ppInputData["Description"],
                                                        numberOfReminders: ppInputData["nor"],
                                                        scheduleDetails: [
                                                            {
                                                                daysBefore: ppInputData["Days Before Due Date"],
                                                            },
                                                            {
                                                                daysAfter: ppInputData["Days After Demand Note"],
                                                            },
                                                            {
                                                                daysAfter: ppInputData["Days After 1st Reminder"],
                                                            },
                                                            {
                                                                daysAfter: ppInputData["Days After 2nd Reminder"],
                                                            },
                                                        ],
                                                        demandNoteReminder: ppInputData["Demand Note Reminder"],
                                                        otherReminders: ppInputData["Other Reminders"],
                                                        campusId: campid,
                                                        status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                        createdBy: instName,
                                                    };
                                                    allReminderPlam.push(ppData);
                                                })
                                            }
                                            else {
                                                let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                                                let campid = campid1["_id"]
                                                var ppData = {
                                                    _id: mongoose.Types.ObjectId(),
                                                    displayName: `REM_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                        }${Number(j) + 1}`,
                                                    refid: ppInputData["ID *"],
                                                    title: ppInputData["Title"],
                                                    description: ppInputData["Description"],
                                                    numberOfReminders: ppInputData["nor"],
                                                    scheduleDetails: [
                                                        {
                                                            daysBefore: ppInputData["Days Before Due Date"],
                                                        },
                                                        {
                                                            daysAfter: ppInputData["Days After Demand Note"],
                                                        },
                                                        {
                                                            daysAfter: ppInputData["Days After 1st Reminder"],
                                                        },
                                                        {
                                                            daysAfter: ppInputData["Days After 2nd Reminder"],
                                                        },
                                                    ],
                                                    demandNoteReminder: ppInputData["Demand Note Reminder"],
                                                    otherReminders: ppInputData["Other Reminders"],
                                                    campusId: campid,
                                                    status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                    createdBy: instName,
                                                };
                                                allReminderPlam.push(ppData);
                                            }

                                        }
                                        if (allReminderPlam.length > 0) {
                                            await reminderModel.insertMany(allReminderPlam);
                                            pubnubConfig.message.description = {
                                                message: `Reminder Plan has been added successfully.`,
                                                current: "Reminder Plans",
                                                next: "Late Fee Plans"
                                            };
                                            pubnubConfig.message.status = 40;
                                            // await pubnub.publish(pubnubConfig);
                                            console.log(`Reminder Plan has been added successfully.`)
                                        }
                                        //LateFee Add
                                        var allLateFee = [];
                                        for (let j = 0; j < lateFeePlan.length; j++) {
                                            var ppInputData = lateFeePlan[j];
                                            if (ppInputData["Campus ID"].toLowerCase() == "all") {
                                                campuses.forEach(async function (ktm) {
                                                    let campid = ktm["_id"]
                                                    var ppData = {
                                                        _id: mongoose.Types.ObjectId(),
                                                        displayName: `LTFEE_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                            }${Number(j) + 1}`,
                                                        refid: ppInputData["ID *"],
                                                        title: ppInputData["Title"],
                                                        description: ppInputData["Description"],
                                                        type: ppInputData["Type"],
                                                        amount: ppInputData["Charges"],
                                                        every: ppInputData["Frequency"],
                                                        campusId: campid,
                                                        status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                        createdBy: instName,
                                                    };
                                                    allLateFee.push(ppData);
                                                })
                                            }
                                            else {
                                                let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                                                let campid = campid1["_id"]
                                                var ppData = {
                                                    _id: mongoose.Types.ObjectId(),
                                                    displayName: `LTFEE_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                        }${Number(j) + 1}`,
                                                    refid: ppInputData["ID *"],
                                                    title: ppInputData["Title"],
                                                    description: ppInputData["Description"],
                                                    type: ppInputData["Type"],
                                                    amount: ppInputData["Charges"],
                                                    every: ppInputData["Frequency"],
                                                    campusId: campid,
                                                    status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                    createdBy: instName,
                                                };
                                                allLateFee.push(ppData);
                                            }
                                        }
                                        if (allLateFee.length > 0) {
                                            await lateFeeModel.insertMany(allLateFee);
                                            pubnubConfig.message.description = {
                                                message: `Late Fee has been added successfully.`,
                                                current: "Late Fee Plans",
                                                next: "Categories"
                                            };
                                            pubnubConfig.message.status = 48;
                                            // await pubnub.publish(pubnubConfig);
                                        }
                                        console.log("cc")
                                        //Category Add
                                        var allCategory = [];
                                        for (let j = 0; j < categoryPlan.length; j++) {
                                            var ppInputData = categoryPlan[j];
                                            if (ppInputData["Campus ID"].toLowerCase() == "all") {
                                                campuses.forEach(async function (ktm) {
                                                    let campid = ktm["_id"]
                                                    var ppData = {
                                                        _id: mongoose.Types.ObjectId(),
                                                        displayName: `CAT_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                            }${Number(j) + 1}`,
                                                        refid: ppInputData["ID"],
                                                        title: ppInputData["Title"],
                                                        description: ppInputData["Description"],
                                                        campusId: campid,
                                                        createdBy: instName,
                                                        status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                    };
                                                    allCategory.push(ppData);
                                                })
                                            }
                                            else {
                                                let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                                                let campid = campid1["_id"]
                                                var ppData = {
                                                    _id: mongoose.Types.ObjectId(),
                                                    displayName: `CAT_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                        }${Number(j) + 1}`,
                                                    refid: ppInputData["ID"],
                                                    title: ppInputData["Title"],
                                                    description: ppInputData["Description"],
                                                    campusId: campid,
                                                    createdBy: instName,
                                                    status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                };
                                                allCategory.push(ppData);
                                            }
                                        }
                                        if (allCategory.length > 0) {
                                            await categoryModel.insertMany(allCategory);
                                            pubnubConfig.message.description = {
                                                message: `Category has been added successfully.`,
                                                current: "Categories",
                                                next: "Installment Plans"
                                            };
                                            pubnubConfig.message.status = 56;
                                            // await pubnub.publish(pubnubConfig);
                                            // pubnubConfig.message.description = {
                                            //     message: `Category has been added successfully.`,
                                            // };
                                            // pubnubConfig.message.status = 56;
                                            // await pubnub.publish(pubnubConfig);
                                        }
                                        console.log("inst")
                                        //InstallmentPlan Add
                                        var allInstallment = [];
                                        for (let j = 0; j < installmentPlan.length; j++) {
                                            var ppInputData = installmentPlan[j];
                                            if (ppInputData["Campus ID"].toLowerCase() == "all") {
                                                campuses.forEach(async function (ktm) {
                                                    let campid = ktm["_id"]
                                                    console.log(ppInputData["noi"])
                                                    var ppData = {
                                                        _id: mongoose.Types.ObjectId(),
                                                        displayName: `INST_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                            }${Number(j) + 1}`,
                                                        refid: ppInputData["ID *"],
                                                        title: ppInputData["Title"],
                                                        description: ppInputData["Description"],
                                                        numberOfInstallments: ppInputData["noi"],
                                                        frequency: ppInputData["Frequency"],
                                                        dueDate: ppInputData["Due Date"],
                                                        percentageBreakup: ppInputData["Percentage Breakup"],
                                                        campusId: campid,
                                                        status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                        createdBy: instName,
                                                        updatedBy: instName,
                                                    };
                                                    allInstallment.push(ppData);
                                                })
                                            }
                                            else {
                                                let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                                                let campid = campid1["_id"]
                                                console.log(ppInputData["noi"])
                                                var ppData = {
                                                    _id: mongoose.Types.ObjectId(),
                                                    displayName: `INST_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                        }${Number(j) + 1}`,
                                                    refid: ppInputData["ID *"],
                                                    title: ppInputData["Title"],
                                                    description: ppInputData["Description"],
                                                    numberOfInstallments: ppInputData["noi"],
                                                    frequency: ppInputData["Frequency"],
                                                    dueDate: ppInputData["Due Date"],
                                                    percentageBreakup: ppInputData["Percentage Breakup"],
                                                    campusId: campid,
                                                    status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                    createdBy: instName,
                                                    updatedBy: instName,
                                                };
                                                allInstallment.push(ppData);
                                            }
                                        }
                                        if (allInstallment.length > 0) {
                                            await installmentModel.insertMany(allInstallment);
                                            pubnubConfig.message.description = {
                                                message: `Installment has been added successfully.`,
                                                current: "Installment Plans",
                                                next: "Concession Plans"
                                            };
                                            pubnubConfig.message.status = 64;
                                            // await pubnub.publish(pubnubConfig);
                                            // pubnubConfig.message.description = {
                                            //     message: `Installment has been added successfully.`,
                                            // };
                                            // pubnubConfig.message.status = 64;
                                            // await pubnub.publish(pubnubConfig);
                                        }
                                        console.log("con")
                                        //Concession Add
                                        var allConcession = [];
                                        for (let j = 0; j < cencessionPlan.length; j++) {
                                            var ppInputData = cencessionPlan[j];
                                            if (ppInputData["Campus ID"].toLowerCase() == "all") {
                                                campuses.forEach(async function (ktm) {
                                                    let categoryData = await allCategory.find(item => item.refid == ppInputData["Category ID"] && item.campusId == ktm["_id"]);
                                                    let campid = ktm["_id"]
                                                    var ppData = {
                                                        _id: mongoose.Types.ObjectId(),
                                                        displayName: `CON_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                            }${Number(j) + 1}`,
                                                        refid: ppInputData["ID *"],
                                                        title: ppInputData["Title"],
                                                        description: ppInputData["Description"],
                                                        categoryId: categoryData._id,
                                                        concessionType: ppInputData["Concession Type"],
                                                        concessionValue: ppInputData["Concession Value"],
                                                        campusId: campid,
                                                        status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                        createdBy: instName,
                                                    };
                                                    allConcession.push(ppData);
                                                })
                                            }
                                            else {
                                                let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                                                let categoryData = await allCategory.find(item => item.refid == ppInputData["Category ID"] && item.campusId == campid1["_id"]);
                                                let campid = campid1["_id"]
                                                var ppData = {
                                                    _id: mongoose.Types.ObjectId(),
                                                    displayName: `CON_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                        }${Number(j) + 1}`,
                                                    refid: ppInputData["ID *"],
                                                    title: ppInputData["Title"],
                                                    description: ppInputData["Description"],
                                                    categoryId: categoryData._id,
                                                    concessionType: ppInputData["Concession Type"],
                                                    concessionValue: ppInputData["Concession Value"],
                                                    campusId: campid,
                                                    status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                    createdBy: instName,
                                                };
                                                allConcession.push(ppData);
                                            }

                                        }
                                        if (allConcession.length > 0) {
                                            await concessionModel.insertMany(allConcession);
                                            pubnubConfig.message.description = {
                                                message: `Concession Plan has been added successfully.`,
                                                current: "Concession Plans",
                                                next: "Student Details"
                                            };
                                            pubnubConfig.message.status = 72;
                                            // await pubnub.publish(pubnubConfig);
                                            // pubnubConfig.message.description = {
                                            //     message: `Concession Plan has been added successfully.`,
                                            // };
                                            // pubnubConfig.message.status = 72;
                                            // await pubnub.publish(pubnubConfig);

                                        }
                                        console.log("std")
                                        //student Add
                                        var allStudents = [];
                                        for (let j = 0; j < studentDetails.length; j++) {
                                            var ppInputData = studentDetails[j];
                                            // let campid1 = await campuses.find(item => item.headaId == ppInputData["Institute ID"])
                                            // if(!campid1){

                                            let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                                            // }
                                            if (!campid1) {
                                                console.log(ppInputData["Campus ID"], campuses)
                                            }
                                            let campid = campid1["_id"];
                                            if (!campid) {
                                                ppInputData
                                            }
                                            var guardianDetails = {
                                                isPrimary: true,
                                                firstName: ppInputData["Parent Name"],
                                                lastName: ppInputData["Parent Name"],
                                                fullName: ppInputData["Parent Name"],
                                                mobile: ppInputData["Parent Phone Number"],
                                                email: ppInputData["Parent Email Address"],
                                                PIIUsageFlag: true,
                                                PIIUsageFlagUpdated: new Date(),
                                                fatherDetails: {},
                                                motherDetails: {},
                                                guardianDetails: {},
                                                relation: "Parent",
                                                createdBy: instName,
                                            };
                                            let guardianData = new guardianSchema(guardianDetails);
                                            var guardianResponse = await guardianData.save();
                                            // console.log("guardiandetails", ppInputData["feeStructure"], ppInputData["Program Plan ID"]);
                                            let programPlanData = await allProgramPlan.find(item => item.refid.trim() == ppInputData["Program Plan ID"]);
                                            // console.log("prorgamplan", programPlanData);
                                            let feeStructureData = await allFeeStructure.find(item => item.refid == ppInputData["feeStructure"] && item.campusId.toString() == campid.toString());
                                            // console.log(feeStructureData,programPlanData)

                                            if (programPlanData == null || feeStructureData == null) {
                                                console.log(ppInputData, allProgramPlan, feeStructureData)
                                                // dbConnectionp.dropDatabase(async function (err, result) {
                                                //     if (err) {
                                                //         res.status(400).send({
                                                //             status: "failure",
                                                //             message: "database delete error",
                                                //             count: 0,
                                                //         })
                                                //     }
                                                //     else {
                                                await dbConnectionp.close()
                                                res.status(409).send({ status: "failure", message: `error in program plan or fee structure please check the program plan and fee structure of student ${studentDetails[j]["Reg ID *"]}` })
                                                //     }
                                                // })
                                            }
                                            else {
                                                // console.log("feeStructure", feeStructureData);
                                                // let categoryId = await categoryModel.findOne({
                                                //   programCode: ppInputData["Program Plan ID"],
                                                // });
                                                let dob
                                                let admdate
                                                // if (ppInputData["DOB"] !== "") {
                                                //     dob = new Date(ppInputData["DOB"]).toISOString(),
                                                //         admdate = new Date(ppInputData["Admitted Date "]).toISOString()
                                                // }
                                                // else {
                                                //     dob = null
                                                //     admdate = null
                                                // }
                                                // let rollnum = await stdfeeMaps.find(item => item["Student ID"] == ppInputData["HEDA ID *"])
                                                // console.log("dob", dob)
                                                // console.log("admitted date",admdate)
                                                // if (ppInputData["Campus ID"].toLowerCase() == "all") {
                                                //     campuses.forEach(async function (ktm) {
                                                //         campid.push(ktm["_id"])
                                                //     })
                                                // }
                                                // else {

                                                // }
                                                var ppData = {
                                                    displayName: `STU_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                        }${Number(j) + 1}`,
                                                    regId: ppInputData["Reg ID *"] == "-" ? ppInputData["HEDA ID"] : ppInputData["Reg ID *"],
                                                    rollNumber: ppInputData["HEDA ID"] == "-" ? ppInputData["Reg ID *"] : ppInputData["HEDA ID"],
                                                    salutation:
                                                        ppInputData["Salutation"] != undefined
                                                            ? ppInputData["Salutation"]
                                                            : null, // salutation
                                                    category: ppInputData["Category"], // Category
                                                    section: ppInputData["Section"] ? ppInputData["Section"] : "A",
                                                    firstName: ppInputData["First Name *"], //First Name *
                                                    middleName: ppInputData["Middle Name"], //
                                                    lastName: ppInputData["Last Name *"], //Last Name *
                                                    guardianDetails: [guardianData._id],
                                                    gender: ppInputData["Gender"], //Gender
                                                    dob: null,
                                                    citizenship: ppInputData["Citizenship"], //
                                                    currency: ppInputData["Currency"], //
                                                    FOREX: ppInputData["FOREX"], //
                                                    admittedOn: null,
                                                    // admittedOn: new Date(ppInputData["Admitted Date"]) instanceof Date ? new Date(ppInputData["Admitted Date"]) : null, //Admitted Date *
                                                    programPlanId: programPlanData._id,
                                                    feeStructureId: feeStructureData._id,
                                                    phoneNo: ppInputData["Phone Number *"], //Phone Number *
                                                    email: ppInputData["Email Address *"], // Email Address *
                                                    alternateEmail:
                                                        ppInputData["alterEmail"] != undefined
                                                            ? ppInputData["alterEmail"]
                                                            : null,
                                                    parentName: ppInputData["Parent Name"],
                                                    parentPhone: ppInputData["Parent Phone Number"],
                                                    parentEmail: ppInputData["Parent Email Address"],
                                                    relation: "parent",
                                                    addressDetails: {
                                                        address1: ppInputData["Address 1"],
                                                        address2: ppInputData["Address 2"],
                                                        address3: ppInputData["Address 3"],
                                                        city: ppInputData["City/Town"],
                                                        state: ppInputData["State"],
                                                        country: ppInputData["Country"], //Country
                                                        pincode: ppInputData["PIN Code"], //PIN Code
                                                    },
                                                    isFinalYear: ppInputData["Is Final Year"].toLowerCase() == "yes" ? true : false,
                                                    final: ppInputData["Final"],
                                                    campusId: campid,
                                                    status: ppInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                    createdBy: instName,
                                                };
                                                allStudents.push(ppData);
                                            }
                                        }
                                        if (allStudents.length > 0) {
                                            await studentModel.insertMany(allStudents);
                                            pubnubConfig.message.description = {
                                                message: `Students has been added successfully.`,
                                                current: "Student Details",
                                                next: "Fee Manager"
                                            };
                                            pubnubConfig.message.status = 88;
                                            // await pubnub.publish(pubnubConfig);
                                            console.log("students added successfully")
                                            // pubnubConfig.message.description = {
                                            //     message: `Students has been added successfully.`,
                                            // };
                                            // pubnubConfig.message.status = 80;
                                            // await pubnub.publish(pubnubConfig);
                                        }

                                        // Fee Manager added
                                        var allFeeManager = [];
                                        for (let j = 0; j < feeManagerDatas.length; j++) {
                                            var pfmInputData = feeManagerDatas[j];
                                            // console.log(pfmInputData["Campus ID"].toLowerCase() == "all",pfmInputData["Campus ID"].toLowerCase())
                                            if (pfmInputData["Campus ID"].toLowerCase() == "all") {
                                                for (m = 0; m < campuses.length; m++) {
                                                    let ktm = campuses[m];
                                                    let campid = campuses[m]["_id"];
                                                    var ppDetails = await allProgramPlan.find(item => item.refid == pfmInputData["Program Plan ID *"]["0"] && item.campusId == ktm["_id"]);
                                                    if (ppDetails) {
                                                        // console.log("ppDetails", ppDetails)
                                                        var feeTypesDetails = await feeTypeDetails.find(item => item.refid == pfmInputData["Fee Type ID *"]["0"] && item.campusId == ktm["_id"]);
                                                        //   console.log("feeTypesDetails",feeTypesDetails)
                                                        var concessionPlans = await allConcession.find(item => item.refid == pfmInputData["Concession ID"] && item.campusId == ktm["_id"])
                                                        //   console.log("concessionPlans",concessionPlans)
                                                        var installmentPlans = await allInstallment.find(item => item.refid == pfmInputData["Installment ID"] && item.campusId == ktm["_id"])
                                                        //   console.log("installmentPlans",installmentPlans)
                                                        var reminderPlans = await allReminderPlam.find(item => item.refid == pfmInputData["Reminder Plan ID *"] && item.campusId == ktm["_id"])
                                                        //   console.log("reminderPlans",reminderPlans)
                                                        var latefees = await allLateFee.find(item => item.refid == pfmInputData["Late Fee Plan ID *"] && item.campusId == ktm["_id"])
                                                        //   console.log("latefees",latefees)
                                                        var paymentschedules = await allPaymentSchedule.find(item => item.refid == pfmInputData["Payment Schedule ID *"] && item.campusId == ktm["_id"])
                                                        //   console.log("paymentschedules",paymentschedules)

                                                        let apldates = []
                                                        let appdates = pfmInputData["Apply Dates"].split(",")
                                                        if (appdates.length > 1) {
                                                            apldates.push(new Date(appdates[0]))
                                                            apldates.push(new Date(appdates[1]))
                                                        }
                                                        else {
                                                            apldates.push(new Date())
                                                            apldates.push(new Date())
                                                        }
                                                        // console.log(apldates)
                                                        var pfmData = {
                                                            displayName: `FM_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                                }${Number(j) + 1}`,
                                                            title: pfmInputData["Title *"],
                                                            description: pfmInputData["Description"],
                                                            feeTypeId: feeTypesDetails !== undefined ? feeTypesDetails._id : null,
                                                            programPlanId: ppDetails !== undefined ? ppDetails._id : null,
                                                            reminderPlanId: reminderPlans !== undefined ? reminderPlans._id : null,
                                                            paymentScheduleId: paymentschedules !== undefined ? paymentschedules._id : null,
                                                            concessionPlanId: concessionPlans !== undefined ? concessionPlans._id : null,
                                                            lateFeePlanId: latefees !== undefined ? latefees._id : null,
                                                            installmentPlanId: installmentPlans !== undefined ? installmentPlans._id : null,
                                                            campusId: campid,
                                                            feeMapType: pfmInputData["Fee Map Type"],
                                                            applyDates: apldates,
                                                            feeDetails: {
                                                                units: null,
                                                                perUnitAmount: null,
                                                                totalAmount: pfmInputData["Total Fees *"],
                                                            },
                                                            status: pfmInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                            createdBy: instName,
                                                        };
                                                        allFeeManager.push(pfmData);
                                                    }
                                                }
                                            }
                                            else {
                                                let campid1 = await campuses.find(item => item.campusId == ppInputData["Campus ID"])
                                                let campid = campid1["_id"];
                                                var ppDetails = await allProgramPlan.find(item => item.refid == pfmInputData["Program Plan ID *"]["0"] && item.campusId == campid1["_id"]);
                                                // console.log(campuses,ppInputData["Campus ID"],campid,pfmInputData["Program Plan ID *"],ppDetails)
                                                //   console.log("ppDetails",ppDetails)
                                                var feeTypesDetails = await feeTypeDetails.find(item => item.refid == pfmInputData["Fee Type ID *"]["0"] && item.campusId == campid1["_id"]);
                                                //   console.log("feeTypesDetails",feeTypesDetails)
                                                var concessionPlans = await allConcession.find(item => item.refid == pfmInputData["Concession ID"] && item.campusId == campid1["_id"])
                                                //   console.log("concessionPlans",concessionPlans)
                                                var installmentPlans = await allInstallment.find(item => item.refid == pfmInputData["Installment ID"] && item.campusId == campid1["_id"])
                                                //   console.log("installmentPlans",installmentPlans)
                                                var reminderPlans = await allReminderPlam.find(item => item.refid == pfmInputData["Reminder Plan ID *"] && item.campusId == campid1["_id"])
                                                //   console.log("reminderPlans",reminderPlans)
                                                var latefees = await allLateFee.find(item => item.refid == pfmInputData["Late Fee Plan ID *"] && item.campusId == campid1["_id"])
                                                //   console.log("latefees",latefees)
                                                var paymentschedules = await allPaymentSchedule.find(item => item.refid == pfmInputData["Payment Schedule ID *"] && item.campusId == campid1["_id"])
                                                //   console.log("paymentschedules",paymentschedules)

                                                let apldates = []
                                                let appdates = pfmInputData["Apply Dates"].split(",")
                                                if (appdates.length > 0) {
                                                    apldates.push(new Date(appdates[0]))
                                                    apldates.push(new Date(appdates[1]))
                                                    // console.log("appdates", appdates)
                                                }
                                                else {
                                                    apldates.push(new Date())
                                                    apldates.push(new Date())
                                                    // console.log("appdates", appdates) 
                                                }

                                                var pfmData = {
                                                    displayName: `FM_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                        }${Number(j) + 1}`,
                                                    title: pfmInputData["Title *"],
                                                    description: pfmInputData["Description"],
                                                    feeTypeId: feeTypesDetails !== undefined ? feeTypesDetails._id : null,
                                                    programPlanId: ppDetails !== undefined ? ppDetails._id : null,
                                                    reminderPlanId: reminderPlans !== undefined ? reminderPlans._id : null,
                                                    paymentScheduleId: paymentschedules !== undefined ? paymentschedules._id : null,
                                                    concessionPlanId: concessionPlans !== undefined ? concessionPlans._id : null,
                                                    lateFeePlanId: latefees !== undefined ? latefees._id : null,
                                                    installmentPlanId: installmentPlans !== undefined ? installmentPlans._id : null,
                                                    campusId: campid,
                                                    feeMapType: pfmInputData["Fee Map Type"],
                                                    applyDates: apldates,
                                                    feeDetails: {
                                                        units: null,
                                                        perUnitAmount: null,
                                                        totalAmount: pfmInputData["Total Fees *"],
                                                    },
                                                    status: pfmInputData["Status"].toLowerCase() == "active" ? 1 : 0,
                                                    createdBy: instName,
                                                };
                                                allFeeManager.push(pfmData);
                                            }


                                        }
                                        // console.log("allFeeManager", allFeeManager);
                                        if (allFeeManager.length > 0) {
                                            await feeManagerSchema.insertMany(allFeeManager);
                                            pubnubConfig.message.description = {
                                                message: `Fee Managers added successfully.`,
                                                current: "Fee Manager",
                                                next: "Student Fee Map"
                                            };
                                            pubnubConfig.message.status = 100;
                                            // await pubnub.publish(pubnubConfig);
                                            console.log("Fee Manager added successfully")
                                            // pubnubConfig.message.description = {
                                            //     message: `Fee Managers added successfully.`,
                                            // };
                                            // pubnubConfig.message.status = 88
                                            // await pubnub.publish(pubnubConfig);
                                        }

                                        //Student Fees Map
                                        console.log("student fee map started")
                                        var allStudentMap = [];
                                        let feetypesd = await feeTypeModel.findOne({ displayName: "FT001" })
                                        for (let j = 0; j < stdfeeMaps.length; j++) {
                                            // console.log(Object.keys(stdfeeMaps[j]))
                                            // console.log(sds)
                                            let transdata = await transactionModel.find({})
                                            let transcount = transdata.length
                                            var ppInputData = stdfeeMaps[j];
                                            let studentData = await studentModel.findOne({
                                                rollNumber: ppInputData["Student ID"],
                                            });
                                            if (studentData === null) {
                                                studentData = await studentModel.findOne({
                                                    regId: ppInputData["USN"],
                                                });
                                            }
                                            if (studentData === null) {
                                                studentData = await studentModel.findOne({
                                                    regId: ppInputData["Reg ID *"],
                                                });
                                            }
                                            if (studentData === null) {
                                                studentData = await studentModel.findOne({
                                                    regId: ppInputData["HEDA ID"],
                                                });
                                            }
                                            // let campid1 = await campuses.find(item => item._id.toString() == studentData._doc["campusId"].toString())
                                            let campid = studentData._doc["campusId"];
                                            let months = ["First", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
                                            let duemonths = { "Year": 12, "Half-Year": 6, "Quarter": 3, "Month": 1, "One time date": 0, "One time date range": 0 }
                                            let dueDates = { "Second": 2, "Third": 3, "Fourth": 4, "Fifth": 5, "Last": 30, "Second Last": 28, "Third last": 27, "Fourth last": 2 }
                                            if (!studentData) {
                                                console.log(stdfeeMaps[j])
                                            }
                                            let prplan = allProgramPlan.find(item => item._id.toString() == studentData.programPlanId.toString())
                                            if (!prplan) {
                                                stdfeeMaps[j]
                                            }
                                            let paymentSchedule = allPaymentSchedule[0]
                                            let prspl = prplan.fromDate.split(" ")
                                            // let duddd = new Date(prplan.fromDate)
                                            let prmonth = prspl[0]
                                            let mindex = months.indexOf(prmonth)
                                            let pryear = prspl[2]
                                            let dueD = paymentSchedule.scheduleDetails.collectEvery
                                            let dueM = duemonths[paymentSchedule.scheduleDetails.collectEvery]
                                            let nextDueD = dueM + mindex
                                            let dueDatem
                                            if (dueM == 12) {
                                                dueDatem = new Date("2021-04-01")
                                            }
                                            else {
                                                dueDatem = `${months[nextDueD]} ${Number(dueDates[paymentSchedule.scheduleDetails.dueDate]) + 1}, ${pryear}`
                                            }
                                            var d = new Date("2021-04-01");
                                            var d2 = new Date("2021-04-01");
                                            let amountp = stdfeeMaps[j]["Amount"] ? stdfeeMaps[j]["Amount"].replace(",", "") : "0";
                                            amountp = amountp.replace(/ +/g, "");
                                            let amountr = isNaN(Number(stdfeeMaps[j]["Amount Received"])) ? "0" : stdfeeMaps[j]["Amount Received"].replace(",", "");
                                            amountr = amountr.replace(/ +/g, "");
                                            let pend = parseFloat(amountp) - parseFloat(amountr)
                                            if (isNaN(pend) && !isNaN(amountp)) {
                                                pend = amountp
                                            }
                                            else if (isNaN(amountp)) {
                                                pend = 0.00
                                            }
                                            let paidamt = isNaN(amountr) ? "0" : amountr
                                            let rid = 0;
                                            // console.log()
                                            // let trdates = stdfeeMaps[j][`Date1`].split("/")
                                            // let trdate
                                            // if (!trdates.includes("-")) {
                                            //     trdate = new Date(`20${trdates[2]}-${trdates[0]}-${trdates[1]}`)
                                            // }
                                            // else {
                                            trdate = null
                                            // }
                                            let feesbkp = []
                                            let paidamts = []
                                            for (ll = 0; ll < feeTypeDetails.length; ll++) {
                                                if (feeTypeDetails[ll].title.trim().toLowerCase().includes("tuition") && campid.toString() == feeTypeDetails[ll]["campusId"].toString()) {
                                                    feesbkp.push({
                                                        amount: isNaN(amountp) ? 0.00 : amountp,
                                                        paid: 0.00,
                                                        pending: isNaN(amountp) ? 0.00 : amountp,
                                                        feeTypeCode: feeTypeDetails[ll]["displayName"],
                                                        title: feeTypeDetails[ll]["title"],
                                                    })
                                                    paidamts.push(0)
                                                }
                                                else if (campid.toString() == feeTypeDetails[ll]["campusId"].toString()) {
                                                    feesbkp.push({
                                                        amount: 0.00,
                                                        paid: 0.00,
                                                        pending: 0.00,
                                                        feeTypeCode: feeTypeDetails[ll]["displayName"],
                                                        title: feeTypeDetails[ll]["title"],
                                                    })
                                                    paidamts.push(0)
                                                }
                                            }
                                            var ppData = {
                                                displayName: `SFM_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                                                    }${Number(j) + 1}`,
                                                studentId: studentData._id,
                                                usn: stdfeeMaps[j]["USN"],
                                                programPlanId: studentData.programPlanId,
                                                feeStructureId: studentData.feeStructureId,
                                                feeManagerId: undefined,
                                                dueDate: d2.toISOString(),
                                                amount: isNaN(amountp) ? 0.00 : amountp,
                                                paid: 0.00,
                                                receivedDate: trdate,
                                                receiptNumbers: stdfeeMaps[j]["Receipt Number"],
                                                concession: 0,
                                                fine: 0,
                                                pending: isNaN(amountp) ? 0.00 : amountp,
                                                transactionPlan: {
                                                    feesBreakUp: feesbkp,
                                                    totalAmount: isNaN(amountp) ? 0.00 : amountp,
                                                    paidAmount: paidamts,
                                                    totalPaid: 0.00,
                                                    totalPending: isNaN(amountp) ? 0.00 : amountp
                                                },
                                                campusId: campid,
                                                status: 1,
                                                createdBy: req.orgId,
                                            }
                                            allStudentMap.push(ppData);
                                            // d.setMonth(d.getMonth() + 3);
                                            // d.setDate(1);


                                            // tcount = tcount + 1
                                            // let amtt = isNaN(stdfeeMaps[j]["Amount"]) ? 0.00 : stdfeeMaps[j]["Amount"]
                                            // let paystatus
                                            // if(Number(amtt)>Number(paidamt)){
                                            //     paystatus = "Partial"
                                            // }
                                            // else if(Number(amtt)==Number(paidamt)){
                                            //     paystatus = "Paid"
                                            // }
                                            // let transactionPayload = {
                                            //     "feesLedgerIds": [],
                                            //     "emailCommunicationRefIds": [mm.email],
                                            //     "smsCommunicationRefIds": [std.phone],
                                            //     "relatedTransactions": [],
                                            //     "softwareReconciled": false,
                                            //     "currency": "INR",
                                            //     "exchangeRate": 1,
                                            //     "displayName": `RCPT_2020-21_${
                                            //         tcount < 9 ? "00" : tcount < 99 ? "0" : ""
                                            //         }${Number(tcount) + 1}`,
                                            //     "transactionDate": trdate,
                                            //     "transactionType": "eduFees",
                                            //     "transactionSubType": "feePayment",
                                            //     "studentId": studentData._id,
                                            //     "studentName": `${studentData.firstName} ${studentData.lastName}`,
                                            //     "class": prplan.title,
                                            //     "academicYear": prplan.academicYear,
                                            //     "amount": paidamt,
                                            //     "studentRegId": studentData.regId,
                                            //     "receiptNo": `RCPT_2020-21_${
                                            //         tcount < 9 ? "00" : tcount < 99 ? "0" : ""
                                            //         }${Number(tcount) + 1}`,
                                            //     "programPlan": studentData.programPlanId,
                                            //     paymentRefId: stdfeeMaps[j]["Receipt Number"],
                                            //     "data": {
                                            //         "orgId": req.orgId,
                                            //         "displayName": `RCPT_2020-21_${
                                            //             tcount < 9 ? "00" : tcount < 99 ? "0" : ""
                                            //             }${Number(tcount) + 1}`,
                                            //         "transactionType": "eduFees",
                                            //         "transactionSubType": "feePayment",
                                            //         "mode": "cash",
                                            //         "method": "otc",
                                            //         "modeDetails": {
                                            //             "netBankingType": null,
                                            //             "walletType": null,
                                            //             "instrumentNo": null,
                                            //             "instrumentDate": trdate,
                                            //             "bankName": null,
                                            //             "cardDetails": {
                                            //                 "cardType": "",
                                            //                 "nameOnCard": `${studentData.firstName} ${studentData.lastName}`,
                                            //                 "cardNumber": ""
                                            //             },
                                            //             "transactionId": stdfeeMaps[j]["Receipt Number"],
                                            //             "remarks": "test"
                                            //         },
                                            //         "feesBreakUp": [{
                                            //             "feeTypeId": feetypesd._id,
                                            //             "feeType": "Tuition Fee ",
                                            //             "amount": paidamt,
                                            //             "feeTypeCode": "FT001"
                                            //         }]
                                            //     },
                                            //     "paymentTransactionId": stdfeeMaps[j]["Receipt Number"],
                                            //     "status": "Paid"
                                            // }
                                            // let newtransaction = new transactionModel(transactionPayload)

                                            // let feeLedgerPayload = {
                                            //     "transactionId": newtransaction._id,
                                            //     "transactionDate": trdate,
                                            //     "transactionDisplayName": newtransaction.displayName,
                                            //     "primaryTransaction": "",
                                            //     "feeTypeCode": "FT001",
                                            //     "dueAmount": pend,
                                            //     "pendingAmount": pend,
                                            //     "transactionType": "eduFees",
                                            //     "transactionSubType": "feePayment",
                                            //     "studentId": studentData._id,
                                            //     "studentRegId": studentData.regId,
                                            //     "studentName": `${studentData.firstName} ${studentData.lastName}`,
                                            //     "academicYear": prplan.academicYear,
                                            //     "class": prplan.title,
                                            //     "programPlan": prplan._id,
                                            //     "status": paystatus
                                            // }
                                            // let newFeeledger = new feesLedgerModel(feeLedgerPayload)
                                            // await newFeeledger.save()
                                            // newtransaction.feesLedgerIds = [newFeeledger._id]
                                            // await newtransaction.save();
                                            // if(Number(paidamt)>0){
                                            //     transcount++
                                            //     let newLedger = new feeLedgerModel({
                                            //         transactionDate: trdate,
                                            //         transactionDisplayName: `DN_2020-21_${
                                            //             transcount.toString().length == 1 ? "00" : transcount.toString().length == 2 ? "0" : ""
                                            //             }${Number(transcount) + 1}`,
                                            //         primaryTransaction: `DN_2020-21_001${
                                            //             transcount.toString().length == 1 ? "00" : transcount.toString().length == 2 ? "0" : ""
                                            //             }${Number(transcount) + 1}`,
                                            //         transactionType: "eduFees",
                                            //         transactionSubType: "demandNote",
                                            //         feeTypeCode: "FT001",
                                            //         dueAmount: Number(pend),
                                            //         paidAmount: Number(paidamt),
                                            //         refundAmount: 0,
                                            //         pendingAmount: Number(pend), // may remain unused?
                                            //         paymentTransactionId: stdfeeMaps[j]["Receipt Number"], // RazorPay or other txn id to be populeted on payment
                                            //         status: Number(pend)>0 ? "partially paid": "paid",
                                            //         studentId: studentData._id,
                                            //         studentRegId: stdfeeMaps[j]["USN"],
                                            //         studentName: `${studentData.firstName} ${studentData.lastName}`,
                                            //         academicYear: prplan.academicYear,
                                            //         class: prplan.description,
                                            //         programPlan: prplan._id,
                                            //       })
                                            //     let newTransaction = new transactionModel({
                                            //         displayName: `DN_2020-21_${
                                            //             transcount.toString().length == 1 ? "00" : transcount.toString().length == 2 ? "0" : ""
                                            //             }${Number(transcount) + 1}`,
                                            //         transactionType: "eduFees",
                                            //         transactionSubType: "demandNote",
                                            //         transactionDate: trdate,
                                            //         studentId: studentData._id,
                                            //         studentRegId: stdfeeMaps[j]["USN"],
                                            //         studentName: `${studentData.firstName} ${studentData.lastName}`,
                                            //         academicYear: prplan.academicYear,
                                            //         class: prplan.description,
                                            //         programPlan: studentData.programPlanId,
                                            //         paymentRefId: null,
                                            //         receiptNo: null,
                                            //         amount: Number(paidamt),
                                            //         feesLedgerIds: [newLedger._id],
                                            //         emailCommunicationRefIds: [studentData.email],
                                            //         smsCommunicationRefIds: [studentData.phoneNo],
                                            //         status: Number(pend)>0 ? "Partial": "Paid",
                                            //         relatedTransactions: '',
                                            //         paymentTransactionId: ''
                                            //       })
                                            //       newLedger.transactionId = newTransaction._id,

                                            //     await newTransaction.save()  
                                            //     await newLedger.save()  
                                            // }


                                            // await studentModel.updateOne({regId: ppInputData["Student ID"]},{rollNumber:stdfeeMaps[j]["USN"]});
                                        }
                                        if (allStudentMap.length > 0) {
                                            feeMapModel.insertMany(allStudentMap, async function (err2, fmp) {
                                                if (err2) {
                                                    if (err2) {
                                                        console.log(err2)
                                                        res.send(err2.toString())
                                                        // reject(res.status(200).json({
                                                        //     success: true,
                                                        //     message: err2.toString(),
                                                        //     count: 0,
                                                        // }))
                                                    }
                                                } else {
                                                    // resolve({ status: "success", message: "Excel Uploaded Successfully", data: newMasterUpload })
                                                    // createFeePlans(req, dbConnectionp, feeManagerDatas, feeTypeDetails, studentDetails, feeManagerSchema, feeStructureModel, studentModel, stdfeeMaps, campuses, allProgramPlan, allPaymentSchedule, finYear).then(ctresult => {
                                                    //     resolve({ status: "success", message: "Excel Uploaded Successfully", data: newMasterUpload })
                                                    // })
                                                    createTransactions(req, stdfeeMaps, studentModel, allProgramPlan, allPaymentSchedule, feeTypeDetails, dbConnectionp, feeMapModel).then(ctresult => {
                                                        console.log("resolving")
                                                        // resolve({ status: "success", message: "Excel Uploaded Successfully", data: newMasterUpload })
                                                        createFeePlans(req, dbConnectionp, feeManagerDatas, feeTypeDetails, studentDetails, feeManagerSchema, feeStructureModel, studentModel, stdfeeMaps, campuses, allProgramPlan, allPaymentSchedule, finYear, allStudentMap).then(ctresult => {
                                                            resolve({ status: "success", message: "Excel Uploaded Successfully", data: newMasterUpload })
                                                        })
                                                    })
                                                }
                                            })
                                            // await feeMapModel.insertMany(allStudentMap);

                                        }
                                    }
                                })
                            }

                        }

                    }
                });
            }

        } else {
            return res.status(400).json({
                status: "failure",
                message: "Upload file does not exist",
                Error: err,
            });
        }
    })

};
module.exports.createUsers = async (req, res) => {
    console.log("create users")
    let dbConnectionc = await createDatabase("Zq-EduUser-dev", process.env.central_mongoDbUrl);
    var usersModel = await dbConnectionc.model(
        "Users_dev",
        usersSchema,
        "Users_dev"
    );
    let { firstName, lastName, email, phoneNumber, PAN, instituteName, instituteID, loginClient, nameSpace } = req.body
    let exiemp = await usersModel.findOne({ userName: email, role: "Employee" })
    if (!exiemp) {
        let inviteData = await CreatetinyUrl(email, `${firstName} ${lastName}`, req.query.type)
        var currentdate = new Date();
        var datetime = currentdate.getDate() + "/" + (currentdate.getMonth() + 1) + "/" + currentdate.getFullYear() + " @ " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
        let empProfile = {
            payment_Amt: "-",
            payment_Exp_date: "-",
            firstName: firstName,
            lastName: lastName,
            institute: "HKBK",
            organiZation: "HKBK",
            orgId: req.query.orgId,
            gstin: "-",
            PAN: "-",
            account_Status: "Invitation-send",
            inviteUrl: inviteData.inviteUrl,
            userName: email,
            password: null,
            last_Login: "-",
            role: "Staff",
            register_date: datetime
        }

        let newEmpLogin = new usersModel(empProfile)
        let newEmpLoginResp = await newEmpLogin.save()
        dbConnectionc.close();
        res.send({ status: "success", "message": "users created successfully", data: newEmpLoginResp })
    }
    else {
        dbConnectionc.close();
        res.send({ status: "success", "message": "user already exists", data: "already invitation sent" })
    }

    // var hashpass = bcrypt.hashSync("Welcome1@3$", saltRounds);
    // let empProfile = {
    //     payment_Amt: "-",
    //     payment_Exp_date: "-",
    //     firstName: elt['First Name *'],
    //     lastName: elt['Last Name *'],
    //     institute: req.headers.orgId,
    //     gstin: "-",
    //     PAN: elt['PAN *'],
    //     account_Status: "Active",
    //     inviteUrl: "null",
    //     userName: `${elt['ID *']}@hkbk.com`,
    //     password: hashpass,
    //     last_Login: "-",
    //     role: "Employee",
    //     register_date: datetime
    // }
    // let exiemp = await employeeLoginModel.findOne({ userName: `${elt['ID *']}@hkbk.com`, role: "Employee", institute: req.headers.orgId })
    //     if (!exiemp) {
    //         let newEmpLogin = new employeeLoginModel(empProfile)
    //         let newEmpLoginResp = await newEmpLogin.save()
    //     }


}
async function CreatetinyUrl(email, empname, type) {
    // console.log("email",email)
    let types = { Payroll: "payrollInvite", "Fee Collection": "feeCollectionInvite" }
    return new Promise(async function (resolve, reject) {
        data = JSON.stringify({ "Url": `${process.env[types[type]]}/#/hkbk/zqsignup?${email}` })
        submitfortiny = await axios.post(`${process.env.tinyUrl}`, data, { headers: { 'Content-Type': 'application/json' } })
        var senderAddress = "zenqore@gmail.com"
        var toAddresses = email
        var body_text = `<p>Dear ${empname}, <br><br> You have been onboarded on the ${type} Platform. Click on the following link to complete the onboarding process by setting your password. </p>
    <button style="font-family: 'Google Sans';
    font-style: normal;
    font-weight: 600;
    height: 36px;
    padding: 0px 15px;
    font-size: 14px;
    line-height: 20px;
    display: flex;
    border-radius: 3px;
    background-color: #0052CC;
    align-items: center;
    justify-content: center;
    outline: none;
    border: none;
    cursor: pointer;
    color: #FFFFFF;"> <a style="color: #FFFFFF;line-height: 36px;font-family: 'Google Sans';text-decoration:none;" href="${submitfortiny.data.ShortUrl}"> COMPLETE ONBOARDING </a></button>
    <p>Regards,</p>
    <p><strong>${type} team</strong></p>
    <p>&nbsp;</p>`
        var subject = `${type} onboarding request`
        var charset = "UTF-8";
        console.log(submitfortiny.data)
        // console.log(senderAddress)
        var params = {
            FromEmailAddress: senderAddress,
            Destination: {
                ToAddresses: [toAddresses],
            },
            Content: {
                Simple: {
                    Body: {
                        Html: {
                            Data: body_text,
                            Charset: charset
                        }
                    },
                    Subject: {
                        Data: subject,
                        Charset: charset
                    }
                }
            }
        };
        // console.log("params",params)
        if (toAddresses !== "-" && toAddresses !== undefined && toAddresses !== "") {
            pinpointEmail.sendEmail(params, async function (err, data) {
                if (err) {
                    console.log("error", err.toString())
                    reject({ status: "failure", inviteUrl: null })
                } else {
                    // console.log("success")
                    resolve({ status: "success", inviteUrl: submitfortiny.data.ShortUrl })

                }
            });
        }
        else {
            resolve({ status: "success", inviteUrl: "NA" })
        }

    })
}

async function createTransactions(req, stdfeeMaps, studentModel, allProgramPlan, allPaymentSchedule, feeTypeDetails, dbConnection, feeMapModel) {
    let rid = -1;
    let aid = -1;
    let dfeesBreakUp
    for (let j = 0; j < stdfeeMaps.length; j++) {
        // for (let j = 2608; j < stdfeeMaps.length; j++) {

        let ppInputData = stdfeeMaps[j];
        let studentData = await studentModel.findOne({
            rollNumber: ppInputData["Student ID"],
        });
        if (studentData === null) {
            studentData = await studentModel.findOne({
                regId: ppInputData["USN"],
            });
            console.log("not exist", ppInputData["USN"])
        }
        if (studentData) {
            let months = ["First", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
            let duemonths = { "Year": 12, "Half-Year": 6, "Quarter": 3, "Month": 1, "One time date": 0, "One time date range": 0 }
            let dueDates = { "Second": 2, "Third": 3, "Fourth": 4, "Fifth": 5, "Last": 30, "Second Last": 28, "Third last": 27, "Fourth last": 2 }

            let prplan = await allProgramPlan.find(item => item._id.toString() == studentData.programPlanId.toString())
            let stmapid = await feeMapModel.findOne({ studentId: studentData._id })
            let paymentSchedule = allPaymentSchedule[0]
            let prspl = prplan.fromDate.split(" ")
            // let duddd = new Date(prplan.fromDate)
            let prmonth = prspl[0]
            let mindex = months.indexOf(prmonth)
            let pryear = prspl[2]
            let dueD = paymentSchedule.scheduleDetails.collectEvery
            let dueM = duemonths[paymentSchedule.scheduleDetails.collectEvery]
            let nextDueD = dueM + mindex
            let dueDatem
            if (dueM == 12) {
                dueDatem = new Date("2021-04-01")
            }
            else {
                dueDatem = `${months[nextDueD]} ${Number(dueDates[paymentSchedule.scheduleDetails.dueDate]) + 1}, ${pryear}`
            }
            var d = new Date("2021-04-01");
            var d2 = new Date("2021-04-01");
            let amountp = stdfeeMaps[j]["Amount"].replace(",", "");
            amountp = amountp.replace(/ +/g, "");
            let amountr = stdfeeMaps[j]["Amount Received"].replace(",", "");
            amountr = amountr.replace(/ +/g, "");
            let pend1 = parseFloat(amountp) - parseFloat(amountr)
            if (isNaN(pend1) && !isNaN(amountp)) {
                pend1 = amountp
            }
            else if (isNaN(amountp)) {
                pend1 = 0.00
            }
            let paidamt = isNaN(amountr) ? "0" : amountr
            for (let p = 1; p < 9; p++) {
                // for (let p = 1; p < 2; p++) {
                if (!stdfeeMaps[j][`Payment${p}`]) {
                    stdfeeMaps[j]
                }
                let payment = stdfeeMaps[j][`Payment${p}`].replace(",", "");
                // let payment = stdfeeMaps[j][`Payment1`].replace(",", "");
                payment = payment.trim();
                payment = payment.replace(/ +/g, "");
                let receipt = stdfeeMaps[j][`Receipt ${p}`] !== undefined ? stdfeeMaps[j][`Receipt ${p}`] : stdfeeMaps[j][`Receipt${p}`]

                let paydatesplit = stdfeeMaps[j][`Date ${p}`] !== undefined ? stdfeeMaps[j][`Date ${p}`].split("/") : stdfeeMaps[j][`Date${p}`].split("/")
                if (stdfeeMaps[j][`Date${p}`] && stdfeeMaps[j][`Date${p}`].includes("-")) {
                    paydatesplit = stdfeeMaps[j][`Date${p}`] !== undefined ? stdfeeMaps[j][`Date${p}`].split("-") : stdfeeMaps[j][`Date ${p}`].split("-")
                }
                else if (stdfeeMaps[j][`Date ${p}`] && stdfeeMaps[j][`Date ${p}`].includes("-")) {
                    paydatesplit = stdfeeMaps[j][`Date ${p}`] !== undefined ? stdfeeMaps[j][`Date ${p}`].split("-") : stdfeeMaps[j][`Date${p}`].split("-")
                }
                else if (stdfeeMaps[j][`Date ${p}`] && stdfeeMaps[j][`Date ${p}`].includes(".")) {
                    paydatesplit = stdfeeMaps[j][`Date ${p}`] !== undefined ? stdfeeMaps[j][`Date ${p}`].split(".") : stdfeeMaps[j][`Date${p}`].split(".")
                }
                else if (stdfeeMaps[j][`Date${p}`] && stdfeeMaps[j][`Date${p}`].includes(".")) {
                    paydatesplit = stdfeeMaps[j][`Date${p}`] !== undefined ? stdfeeMaps[j][`Date${p}`].split(".") : stdfeeMaps[j][`Date ${p}`].split(".")
                }
                if (payment !== "" && !isNaN(Number(payment)) && payment !== "-" && Number(payment) > 0) {
                    rid++
                    let paydate
                    if (stdfeeMaps[j][`Date ${p}`] && stdfeeMaps[j][`Date ${p}`].includes("/")) {
                        paydate = new Date(`20${paydatesplit[2]}-${paydatesplit[0]}-${paydatesplit[1]}`)
                    }
                    else if (stdfeeMaps[j][`Date${p}`] && stdfeeMaps[j][`Date${p}`].includes("/")) {
                        paydate = new Date(`20${paydatesplit[2]}-${paydatesplit[0]}-${paydatesplit[1]}`)
                    }
                    else if (stdfeeMaps[j][`Date ${p}`] && stdfeeMaps[j][`Date ${p}`].includes(".")) {
                        paydate = new Date("2021-04-20T18:30:00.000+0000")
                    }
                    else if (stdfeeMaps[j][`Date${p}`] && stdfeeMaps[j][`Date${p}`].includes(".")) {
                        paydate = new Date("2021-04-20T18:30:00.000+0000")
                    }
                    else if (stdfeeMaps[j][`Date ${p}`] && stdfeeMaps[j][`Date ${p}`].includes("-")) {
                        paydate = new Date(`20${paydatesplit[2]}-${paydatesplit[1]}-${paydatesplit[0]}`)
                    }
                    else if (stdfeeMaps[j][`Date${p}`] && stdfeeMaps[j][`Date${p}`].includes("-")) {
                        paydate = new Date(`20${paydatesplit[2]}-${paydatesplit[1]}-${paydatesplit[0]}`)
                    }
                    else if (stdfeeMaps[j][`Date ${p}`] && !stdfeeMaps[j][`Date ${p}`].includes("-") && !stdfeeMaps[j][`Date ${p}`].includes("/") && !stdfeeMaps[j][`Date ${p}`].includes(".")) {
                        paydate = new Date(prplan.fromDate)
                    }
                    else if (stdfeeMaps[j][`Date${p}`] && !stdfeeMaps[j][`Date${p}`].includes("-") && !stdfeeMaps[j][`Date${p}`].includes("/") && !stdfeeMaps[j][`Date${p}`].includes(".")) {
                        paydate = new Date(prplan.fromDate)
                    }
                    if (!paydate) {
                        paydate = new Date(prplan.fromDate)
                    }
                    else if (isNaN(paydate.getTime())) {
                        paydate = new Date(prplan.fromDate)
                    }
                    if (stdfeeMaps[j][`Date${p}`] && stdfeeMaps[j][`Date${p}`].includes(".") && stdfeeMaps[j][`Date${p}`].includes("/")) {
                        paydate = new Date("2021-04-20T18:30:00.000+0000")
                        console.log(paydate)
                    }
                    if (stdfeeMaps[j][`Date ${p}`] && stdfeeMaps[j][`Date ${p}`].includes(".") && stdfeeMaps[j][`Date ${p}`].includes("/")) {
                        paydate = new Date("2021-04-20T18:30:00.000+0000")
                        console.log(paydate)
                    }
                    trdate = paydate
                    // dfeesBreakUp = []
                    // for (let kk = 0; kk < feeTypeDetails.length;) {
                    //     if(kk == 0){
                    //         dfeesBreakUp.push({
                    //             "feeTypeCode": feeTypeDetails[kk].displayName,
                    //             "feeTypeId": feeTypeDetails[kk]._id,
                    //             "amount": payment,
                    //             "feeType": feeTypeDetails[kk].title
                    //         })
                    //     }
                    //     else{
                    //         dfeesBreakUp.push({
                    //             "feeTypeCode": feeTypeDetails[kk].displayName,
                    //             "feeTypeId": feeTypeDetails[kk]._id,
                    //             "amount": 0,
                    //             "feeType": feeTypeDetails[kk].title
                    //         })
                    //     }

                    // }\
                    let inputData
                    // if (feeTypeDetails.length > 14) {
                    //     inputData = {
                    //         stemapdata: stmapid._doc,
                    //         studentFeeMap: stmapid.displayName,
                    //         "displayName": `RCPT_2020-21_${
                    //             rid < 9 ? "00" : rid < 99 ? "0" : ""
                    //             }${Number(rid) + 1}`,
                    //         "transactionDate": paydate,
                    //         "relatedTransactions": [],
                    //         "transactionType": "eduFees",
                    //         "transactionSubType": "feePayment",
                    //         "studentId": studentData._id,
                    //         "studentName": `${studentData.firstName} ${studentData.lastName}`,
                    //         "class": prplan.title,
                    //         "parentName": studentData.parentName,
                    //         "academicYear": prplan.academicYear,
                    //         "amount": payment,
                    //         "studentRegId": studentData.regId,
                    //         "receiptNo": receipt,
                    //         "programPlan": prplan._id,
                    //         "paymentTransactionId": receipt,
                    //         "paymentRefId": receipt,
                    //         "receiptStatus": "",
                    //         "currency": studentData.currency,
                    //         "currencyAmount": parseFloat(payment) * parseFloat(studentData.FOREX),
                    //         "exchangeRate": studentData.FOREX,
                    //         "userName": "",
                    //         "createdBy": req.orgId,
                    //         "campusId": studentData.campusId,
                    //         "data": {
                    //             "feesBreakUp":
                    //                 [
                    //                     {
                    //                         "feeTypeCode": feeTypeDetails[0].displayName,
                    //                         "feeTypeId": feeTypeDetails[0]._id,
                    //                         "amount": payment,
                    //                         "feeType": feeTypeDetails[0].title
                    //                     },
                    //                     {
                    //                         "feeTypeCode": feeTypeDetails[1].displayName,
                    //                         "feeTypeId": feeTypeDetails[1]._id,
                    //                         "amount": 0.00,
                    //                         "feeType": feeTypeDetails[1].title
                    //                     },
                    //                     {
                    //                         "feeTypeCode": feeTypeDetails[2].displayName,
                    //                         "feeTypeId": feeTypeDetails[2]._id,
                    //                         "amount": 0.00,
                    //                         "feeType": feeTypeDetails[2].title
                    //                     },
                    //                     {
                    //                         "feeTypeCode": feeTypeDetails[3].displayName,
                    //                         "feeTypeId": feeTypeDetails[3]._id,
                    //                         "amount": 0.00,
                    //                         "feeType": feeTypeDetails[3].title
                    //                     },
                    //                     {
                    //                         "feeTypeCode": feeTypeDetails[4].displayName,
                    //                         "feeTypeId": feeTypeDetails[4]._id,
                    //                         "amount": 0.00,
                    //                         "feeType": feeTypeDetails[4].title
                    //                     },
                    //                     {
                    //                         "feeTypeCode": feeTypeDetails[5].displayName,
                    //                         "feeTypeId": feeTypeDetails[5]._id,
                    //                         "amount": 0.00,
                    //                         "feeType": feeTypeDetails[5].title
                    //                     },
                    //                     {
                    //                         "feeTypeCode": feeTypeDetails[6].displayName,
                    //                         "feeTypeId": feeTypeDetails[6]._id,
                    //                         "amount": 0.00,
                    //                         "feeType": feeTypeDetails[6].title
                    //                     },
                    //                     {
                    //                         "feeTypeCode": feeTypeDetails[7].displayName,
                    //                         "feeTypeId": feeTypeDetails[7]._id,
                    //                         "amount": 0.00,
                    //                         "feeType": feeTypeDetails[7].title
                    //                     },
                    //                     {
                    //                         "feeTypeCode": feeTypeDetails[8].displayName,
                    //                         "feeTypeId": feeTypeDetails[8]._id,
                    //                         "amount": 0.00,
                    //                         "feeType": feeTypeDetails[8].title
                    //                     },
                    //                     {
                    //                         "feeTypeCode": feeTypeDetails[9].displayName,
                    //                         "feeTypeId": feeTypeDetails[9]._id,
                    //                         "amount": 0.00,
                    //                         "feeType": feeTypeDetails[9].title
                    //                     },
                    //                     {
                    //                         "feeTypeCode": feeTypeDetails[10].displayName,
                    //                         "feeTypeId": feeTypeDetails[10]._id,
                    //                         "amount": 0.00,
                    //                         "feeType": feeTypeDetails[10].title
                    //                     },
                    //                     {
                    //                         "feeTypeCode": feeTypeDetails[11].displayName,
                    //                         "feeTypeId": feeTypeDetails[11]._id,
                    //                         "amount": 0.00,
                    //                         "feeType": feeTypeDetails[11].title
                    //                     },
                    //                     {
                    //                         "feeTypeCode": feeTypeDetails[12].displayName,
                    //                         "feeTypeId": feeTypeDetails[12]._id,
                    //                         "amount": 0.00,
                    //                         "feeType": feeTypeDetails[12].title
                    //                     },
                    //                     {
                    //                         "feeTypeCode": feeTypeDetails[13].displayName,
                    //                         "feeTypeId": feeTypeDetails[13]._id,
                    //                         "amount": 0.00,
                    //                         "feeType": feeTypeDetails[13].title
                    //                     },
                    //                     {
                    //                         "feeTypeCode": feeTypeDetails[14].displayName,
                    //                         "feeTypeId": feeTypeDetails[14]._id,
                    //                         "amount": 0.00,
                    //                         "feeType": feeTypeDetails[14].title
                    //                     },
                    //                     {
                    //                         "feeTypeCode": feeTypeDetails[15].displayName,
                    //                         "feeTypeId": feeTypeDetails[15]._id,
                    //                         "amount": 0.00,
                    //                         "feeType": feeTypeDetails[15].title
                    //                     },
                    //                     {
                    //                         "feeTypeCode": feeTypeDetails[16].displayName,
                    //                         "feeTypeId": feeTypeDetails[16]._id,
                    //                         "amount": 0.00,
                    //                         "feeType": feeTypeDetails[16].title
                    //                     }
                    //                 ],
                    //             "orgId": req.orgId,
                    //             "transactionType": "eduFees",
                    //             "transactionSubType": "feePayment",
                    //             "mode": "cash",
                    //             "method": "otc",
                    //             "modeDetails": {
                    //                 "netBankingType": "",
                    //                 "walletType": "",
                    //                 "instrumentNo": receipt,
                    //                 "cardType": "",
                    //                 "nameOnCard": "",
                    //                 "cardNumber": "",
                    //                 "instrumentDate": paydate,
                    //                 "bankName": "",
                    //                 "branchName": "",
                    //                 "transactionId": "",
                    //                 "remarks": ""
                    //             }
                    //         }
                    //     }
                    // }
                    // else {
                    // inputData = {
                    //     stemapdata: stmapid._doc,
                    //     studentFeeMap: stmapid.displayName,
                    //     "displayName": `RCPT_2020-21_${
                    //         rid < 9 ? "00" : rid < 99 ? "0" : ""
                    //         }${Number(rid) + 1}`,
                    //     "transactionDate": paydate,
                    //     "relatedTransactions": [],
                    //     "transactionType": "eduFees",
                    //     "transactionSubType": "feePayment",
                    //     "studentId": studentData._id,
                    //     "studentName": `${studentData.firstName} ${studentData.lastName}`,
                    //     "class": prplan.title,
                    //     "parentName": studentData.parentName,
                    //     "academicYear": prplan.academicYear,
                    //     "amount": payment,
                    //     "studentRegId": studentData.regId,
                    //     "receiptNo": receipt,
                    //     "programPlan": prplan._id,
                    //     "paymentTransactionId": receipt,
                    //     "paymentRefId": receipt,
                    //     "receiptStatus": "",
                    //     "currency": studentData.currency,
                    //     "currencyAmount": parseFloat(payment) * parseFloat(studentData.FOREX),
                    //     "exchangeRate": studentData.FOREX,
                    //     "userName": "",
                    //     "createdBy": req.orgId,
                    //     "campusId": studentData.campusId,
                    //     "data": {
                    //         "feesBreakUp":
                    //             [
                    //                 {
                    //                     "feeTypeCode": feeTypeDetails[0].displayName,
                    //                     "feeTypeId": feeTypeDetails[0]._id,
                    //                     "amount": payment,
                    //                     "feeType": feeTypeDetails[0].title
                    //                 },
                    //                 // {
                    //                 //     "feeTypeCode": feeTypeDetails[1].displayName,
                    //                 //     "feeTypeId": feeTypeDetails[1]._id,
                    //                 //     "amount": 0.00,
                    //                 //     "feeType": feeTypeDetails[1].title
                    //                 // },
                    //                 // {
                    //                 //     "feeTypeCode": feeTypeDetails[2].displayName,
                    //                 //     "feeTypeId": feeTypeDetails[2]._id,
                    //                 //     "amount": 0.00,
                    //                 //     "feeType": feeTypeDetails[2].title
                    //                 // },
                    //                 // {
                    //                 //     "feeTypeCode": feeTypeDetails[3].displayName,
                    //                 //     "feeTypeId": feeTypeDetails[3]._id,
                    //                 //     "amount": 0.00,
                    //                 //     "feeType": feeTypeDetails[3].title
                    //                 // },
                    //                 // {
                    //                 //     "feeTypeCode": feeTypeDetails[4].displayName,
                    //                 //     "feeTypeId": feeTypeDetails[4]._id,
                    //                 //     "amount": 0.00,
                    //                 //     "feeType": feeTypeDetails[4].title
                    //                 // },
                    //                 // {
                    //                 //     "feeTypeCode": feeTypeDetails[5].displayName,
                    //                 //     "feeTypeId": feeTypeDetails[5]._id,
                    //                 //     "amount": 0.00,
                    //                 //     "feeType": feeTypeDetails[5].title
                    //                 // },
                    //                 // {
                    //                 //     "feeTypeCode": feeTypeDetails[6].displayName,
                    //                 //     "feeTypeId": feeTypeDetails[6]._id,
                    //                 //     "amount": 0.00,
                    //                 //     "feeType": feeTypeDetails[6].title
                    //                 // },
                    //                 // {
                    //                 //     "feeTypeCode": feeTypeDetails[7].displayName,
                    //                 //     "feeTypeId": feeTypeDetails[7]._id,
                    //                 //     "amount": 0.00,
                    //                 //     "feeType": feeTypeDetails[7].title
                    //                 // },
                    //                 // {
                    //                 //     "feeTypeCode": feeTypeDetails[8].displayName,
                    //                 //     "feeTypeId": feeTypeDetails[8]._id,
                    //                 //     "amount": 0.00,
                    //                 //     "feeType": feeTypeDetails[8].title
                    //                 // },
                    //                 // {
                    //                 //     "feeTypeCode": feeTypeDetails[9].displayName,
                    //                 //     "feeTypeId": feeTypeDetails[9]._id,
                    //                 //     "amount": 0.00,
                    //                 //     "feeType": feeTypeDetails[9].title
                    //                 // },
                    //                 // {
                    //                 //     "feeTypeCode": feeTypeDetails[10].displayName,
                    //                 //     "feeTypeId": feeTypeDetails[10]._id,
                    //                 //     "amount": 0.00,
                    //                 //     "feeType": feeTypeDetails[10].title
                    //                 // },
                    //                 // {
                    //                 //     "feeTypeCode": feeTypeDetails[11].displayName,
                    //                 //     "feeTypeId": feeTypeDetails[11]._id,
                    //                 //     "amount": 0.00,
                    //                 //     "feeType": feeTypeDetails[11].title
                    //                 // },
                    //                 // {
                    //                 //     "feeTypeCode": feeTypeDetails[12].displayName,
                    //                 //     "feeTypeId": feeTypeDetails[12]._id,
                    //                 //     "amount": 0.00,
                    //                 //     "feeType": feeTypeDetails[12].title
                    //                 // },
                    //                 // {
                    //                 //     "feeTypeCode": feeTypeDetails[13].displayName,
                    //                 //     "feeTypeId": feeTypeDetails[13]._id,
                    //                 //     "amount": 0.00,
                    //                 //     "feeType": feeTypeDetails[13].title
                    //                 // },
                    //                 // {
                    //                 //     "feeTypeCode": feeTypeDetails[14].displayName,
                    //                 //     "feeTypeId": feeTypeDetails[14]._id,
                    //                 //     "amount": 0.00,
                    //                 //     "feeType": feeTypeDetails[14].title
                    //                 // },
                    //                 // {
                    //                 //     "feeTypeCode": feeTypeDetails[15].displayName,
                    //                 //     "feeTypeId": feeTypeDetails[15]._id,
                    //                 //     "amount": 0.00,
                    //                 //     "feeType": feeTypeDetails[15].title
                    //                 // },
                    //                 // {
                    //                 //     "feeTypeCode": feeTypeDetails[16].displayName,
                    //                 //     "feeTypeId": feeTypeDetails[16]._id,
                    //                 //     "amount": 0.00,
                    //                 //     "feeType": feeTypeDetails[16].title
                    //                 // }
                    //             ],
                    //         "orgId": req.orgId,
                    //         "transactionType": "eduFees",
                    //         "transactionSubType": "feePayment",
                    //         "mode": "cash",
                    //         "method": "otc",
                    //         "modeDetails": {
                    //             "netBankingType": "",
                    //             "walletType": "",
                    //             "instrumentNo": receipt,
                    //             "cardType": "",
                    //             "nameOnCard": "",
                    //             "cardNumber": "",
                    //             "instrumentDate": paydate,
                    //             "bankName": "",
                    //             "branchName": "",
                    //             "transactionId": "",
                    //             "remarks": ""
                    //         }
                    //     }
                    // }
                    // }


                    inputData = {
                        "transactionDate": paydate,
                        "relatedTransactions": [],
                        "studentId": studentData._id,
                        "emailCommunicationRefIds": studentData.parentEmail !== "" ? studentData.parentEmail : studentData.email,
                        "transactionType": "eduFees",
                        "transactionSubType": "feePayment",
                        "studentFeeMap": stmapid.displayName,
                        "amount": payment,
                        "status": "initiated",
                        "userName": "test user",
                        "data": {
                            "feesBreakUp": [
                                {
                                    "feeType": feeTypeDetails[0].title,
                                    "amount": payment,
                                    "feeTypeCode": feeTypeDetails[0].displayName
                                },
                                // {
                                //                     "feeTypeCode": feeTypeDetails[0].displayName,
                                //                     "feeTypeId": feeTypeDetails[0]._id,
                                //                     "amount": payment,
                                //                     "feeType": feeTypeDetails[0].title
                                //                 },
                            ],
                            "orgId": req.orgId,
                            "transactionType": "eduFees",
                            "transactionSubType": "feePayment",
                            "mode": "otc",
                            "method": "cash",
                            "modeDetails": {
                                "netBankingType": null,
                                "walletType": null,
                                "instrumentNo": null,
                                "cardType": null,
                                "nameOnCard": null,
                                "cardNumber": null,
                                "instrumentDate": paydate,
                                "bankName": null,
                                "branchName": null,
                                "transactionId": receipt,
                                "remarks": "first installment payment"
                            }
                        },
                        "paymentTransactionId": receipt,
                        "createdBy": req.orgId,
                        "campusId": studentData.campusId,
                        "academicYear": prplan.academicYear,
                        "class": prplan.title,
                        "studentName": `${studentData.firstName} ${studentData.lastName}`,
                        "studentRegId": studentData.regId,
                        "programPlanId": prplan._id,
                        "parentName": studentData.parentName,
                        "type": "receipt"
                    }

                    let createtrans = await createOtcPayment({ body: inputData }, dbConnection)
                    // var receiptN = ("" + Math.random()).substring(2, 7);
                    // var year2 = moment().year();

                    // var transID = `TXN/${year2}/${receiptN + 1}`;
                    // let imode = inputData.data.mode;
                    // let mode = imode.toLowerCase();
                    // let feeMapModel = dbConnection.model(
                    //     "studentfeesmaps",
                    //     StudentFeeMapSchema
                    // );

                    // let feMapDe = await feeMapModel.findOne({
                    //     displayName: inputData.studentFeeMap,
                    // });
                    // let paidAA = Number(feMapDe.paid) + Number(inputData.amount);

                    // if (Number(feMapDe.amount) < Number(paidAA)) {
                    //     aid++
                    //     let extraAmount = Math.abs(Number(feMapDe.amount) - Number(paidAA));
                    //     let payable = Number(feMapDe.pending);

                    //     var advanceId = `ADV_2020-21_${
                    //         aid < 9 ? "00" : aid < 99 ? "0" : ""
                    //         }${Number(aid) + 1}`
                    //     let transactionId;
                    //     if (mode == "cash") {
                    //         transactionId = transID;
                    //     } else {
                    //         transactionId = inputData.paymentTransactionId;
                    //     }
                    //     let passData = {
                    //         displayName: advanceId,
                    //         transactionDate: inputData.transactionDate,
                    //         relatedTransactions: inputData.relatedTransactions,
                    //         transactionType: "eduFees",
                    //         transactionSubType: "advance",
                    //         studentId: inputData.studentId,
                    //         studentName: inputData.studentName,
                    //         class: inputData.class,
                    //         academicYear: inputData.academicYear,
                    //         amount: extraAmount,
                    //         studentRegId: inputData.studentRegId,
                    //         receiptNo: advanceId,
                    //         programPlan: inputData.programPlanId,
                    //         data: inputData.data,
                    //         paymentTransactionId: transactionId,
                    //         receiptStatus: inputData.receiptStatus,
                    //         currency: inputData.currency,
                    //         currencyAmount: inputData.currencyAmount,
                    //         exchangeRate: inputData.exchangeRate,
                    //         userName: inputData.userName,
                    //         campusId: studentData.campusId,
                    //         createdBy: inputData.createdBy,
                    //         updatedBy: inputData.createdBy,
                    //     };
                    //     let paymentData = await advanceLedgerEntry({ body: passData }, dbConnection, inputData.stemapdata)
                    //     console.log("sdfds", paymentData)
                    //     if (paymentData.status == "failure") {
                    //         return false
                    //     } else {
                    //         var rcptId = inputData.displayName
                    //         let transactionId;
                    //         if (mode == "cash") {
                    //             transactionId = transID;
                    //         } else {
                    //             transactionId = inputData.paymentTransactionId;
                    //         }
                    //         // let receiptNo;
                    //         // if (inputData.type == "receipt") {
                    //         //   receiptNo = `${year2}/${receiptN + 1}`;
                    //         // } else {
                    //         //   receiptNo = transactionId;
                    //         // }
                    //         var afterAdvanceFee = [];
                    //         for (oneFee of inputData.data.feesBreakUp) {
                    //             let obje;
                    //             if (String(oneFee.feeTypeCode) == "FT001") {
                    //                 obje = {
                    //                     feeTypeId: oneFee.feeTypeId,
                    //                     feeType: oneFee.feeType,
                    //                     amount: payable,
                    //                     feeTypeCode: oneFee.feeTypeCode,
                    //                 };
                    //             } else {
                    //                 obje = {
                    //                     feeTypeId: oneFee.feeTypeId,
                    //                     feeType: oneFee.feeType,
                    //                     amount: oneFee.amount,
                    //                     feeTypeCode: oneFee.feeTypeCode,
                    //                 };
                    //             }
                    //             afterAdvanceFee.push(obje);
                    //         }
                    //         let passData = {
                    //             displayName: rcptId,
                    //             transactionDate: inputData.transactionDate,
                    //             relatedTransactions: inputData.relatedTransactions,
                    //             transactionType: "eduFees",
                    //             transactionSubType: "feePayment",
                    //             studentId: inputData.studentId,
                    //             studentName: inputData.studentName,
                    //             class: inputData.class,
                    //             academicYear: inputData.academicYear,
                    //             amount: payable,
                    //             studentRegId: inputData.studentRegId,
                    //             receiptNo: rcptId,
                    //             programPlan: inputData.programPlanId,
                    //             data: {
                    //                 feesBreakUp: afterAdvanceFee,
                    //                 orgId: inputData.data.orgId,
                    //                 transactionType: "eduFees",
                    //                 transactionSubType: "feePayment",
                    //                 mode: inputData.data.mode,
                    //                 method: inputData.data.method,
                    //                 modeDetails: {
                    //                     netBankingType: inputData.data.modeDetails.netBankingType,
                    //                     walletType: inputData.data.modeDetails.walletType,
                    //                     instrumentNo: inputData.data.modeDetails.instrumentNo,
                    //                     cardType: inputData.data.modeDetails.cardType,
                    //                     nameOnCard: inputData.data.modeDetails.nameOnCard,
                    //                     cardNumber: inputData.data.modeDetails.cardNumber,
                    //                     instrumentDate: inputData.data.modeDetails.instrumentDate,
                    //                     bankName: inputData.data.modeDetails.bankName,
                    //                     branchName: inputData.data.modeDetails.branchName,
                    //                     transactionId: inputData.data.modeDetails.transactionId,
                    //                     remarks: inputData.data.modeDetails.remarks,
                    //                 },
                    //             },
                    //             paymentTransactionId: transactionId,
                    //             receiptStatus: inputData.receiptStatus,
                    //             currency: inputData.currency,
                    //             currencyAmount: inputData.currencyAmount,
                    //             exchangeRate: inputData.exchangeRate,
                    //             userName: inputData.userName,
                    //             createdBy: inputData.createdBy,
                    //             updatedBy: inputData.createdBy,
                    //         };
                    //         console.log("pppp")
                    //         paymentData = await ledgerEntry({ body: passData }, dbConnection, inputData.stemapdata)
                    //         if (paymentData.status == "failure") {
                    //             return false
                    //         } else {
                    //             const settingsSchema = mongoose.Schema({}, { strict: false });
                    //             const settingsModel = dbConnection.model(
                    //                 "settings",
                    //                 settingsSchema,
                    //                 "settings"
                    //             );
                    //             let feeMapModel = dbConnection.model(
                    //                 "studentfeesmaps",
                    //                 StudentFeeMapSchema
                    //             );
                    //             let feeStructureModel = dbConnection.model(
                    //                 "feestructures",
                    //                 FeeStructureSchema
                    //             );
                    //             let feeManagerModel = dbConnection.model(
                    //                 "feemanagers",
                    //                 FeeManagerSchema
                    //             );
                    //             let feeTypeModel = dbConnection.model(
                    //                 "feetypes",
                    //                 FeeTypeSchema
                    //             );

                    //             let feMapDe = await feeMapModel.findOne({
                    //                 displayName: inputData.studentFeeMap,
                    //             });
                    //             let feeStructureDetails = await feeStructureModel.findOne({
                    //                 _id: feMapDe.feeStructureId,
                    //             });
                    //             let feeBre = [];
                    //             if (feMapDe.transactionPlan.feesBreakUp.length !== 0) {
                    //                 for (singleData of feMapDe.transactionPlan.feesBreakUp) {
                    //                     let fees = singleData.amount;
                    //                     for (oneFee of afterAdvanceFee) {
                    //                         if (
                    //                             String(singleData.feeTypeCode) ==
                    //                             String(oneFee.feeTypeCode)
                    //                         ) {
                    //                             let fullPaid =
                    //                                 Number(singleData.paid) + Number(oneFee.amount);
                    //                             let fullPending = Number(fees) - fullPaid;
                    //                             let obje = {
                    //                                 amount: fees,
                    //                                 paid: fullPaid,
                    //                                 pending: fullPending,
                    //                                 feeTypeCode: oneFee.feeTypeCode,
                    //                                 title: oneFee.feeType,
                    //                             };
                    //                             feeBre.push(obje);
                    //                         }
                    //                     }
                    //                 }
                    //             } else {
                    //                 let fees = singleData.amount;
                    //                 for (oneFee of afterAdvanceFee) {
                    //                     if (
                    //                         String(singleData.feeTypeCode) ==
                    //                         String(oneFee.feeTypeCode)
                    //                     ) {
                    //                         let fullPaid = Number(oneFee.amount);
                    //                         let fullPending = Number(fees) - fullPaid;
                    //                         let obje = {
                    //                             amount: fees,
                    //                             paid: fullPaid,
                    //                             pending: fullPending,
                    //                             feeTypeCode: oneFee.feeTypeCode,
                    //                             title: oneFee.feeType,
                    //                         };
                    //                         feeBre.push(obje);
                    //                     }
                    //                 }
                    //             }
                    //             var tota = 0;
                    //             var pai = 0;
                    //             var pend = 0;
                    //             for (oneFees of feeBre) {
                    //                 tota += oneFees.amount;
                    //                 pai += oneFees.paid;
                    //                 pend += oneFees.pending;
                    //             }
                    //             let feeTypesPaid = {
                    //                 feesBreakUp: feeBre,
                    //                 totalAmount: tota,
                    //                 totalPaid: pai,
                    //                 totalPending: pend,
                    //             };
                    //             let paidA = Number(feMapDe.paid) + Number(inputData.amount);
                    //             if (Number(feMapDe.amount) - Number(paidA) < 0) {
                    //                 await feeMapModel.updateOne(
                    //                     { displayName: inputData.studentFeeMap },
                    //                     {
                    //                         $set: {
                    //                             paid: paidA,
                    //                             pending: 0,
                    //                             transactionPlan: feeTypesPaid,
                    //                         },
                    //                     });
                    //             } else {
                    //                 await feeMapModel.updateOne(
                    //                     { displayName: inputData.studentFeeMap },
                    //                     {
                    //                         $set: {
                    //                             paid: paidA,
                    //                             pending: Number(feMapDe.amount) - Number(paidA),
                    //                             transactionPlan: feeTypesPaid,
                    //                         },
                    //                     });
                    //             }
                    //         }

                    //     }

                    // }
                    // else {
                    //     // let ctdata = await createOtcPayment(transactionPayload, dbConnectionp)
                    //     var rcptId = inputData.displayName
                    //     let transactionId;
                    //     if (mode == "cash") {
                    //         transactionId = transID;
                    //     } else {
                    //         transactionId = inputData.paymentTransactionId;
                    //     }
                    //     let passData = {
                    //         displayName: rcptId,
                    //         transactionDate: inputData.transactionDate,
                    //         relatedTransactions: inputData.relatedTransactions,
                    //         transactionType: "eduFees",
                    //         transactionSubType: "feePayment",
                    //         studentId: inputData.studentId,
                    //         studentName: inputData.studentName,
                    //         parentName: inputData.parentName,
                    //         class: inputData.class,
                    //         academicYear: inputData.academicYear,
                    //         amount: inputData.amount,
                    //         studentRegId: inputData.studentRegId,
                    //         receiptNo: rcptId,
                    //         programPlan: inputData.programPlanId,
                    //         data: inputData.data,
                    //         paymentTransactionId: transactionId,
                    //         receiptStatus: inputData.receiptStatus,
                    //         currency: inputData.currency,
                    //         currencyAmount: inputData.currencyAmount,
                    //         exchangeRate: inputData.exchangeRate,
                    //         userName: inputData.userName,
                    //         campusId: studentData.campusId,
                    //         createdBy: inputData.createdBy,
                    //         updatedBy: inputData.createdBy,
                    //     };
                    //     let paymentData = await ledgerEntry({ body: passData }, dbConnection, inputData.stemapdata)
                    //     if (paymentData == false) {
                    //         console.log("fail to", inputData.studentRegId)
                    //         return false
                    //     } else {
                    //         let feeMapModel = dbConnection.model(
                    //             "studentfeesmaps",
                    //             StudentFeeMapSchema
                    //         );
                    //         let feeStructureModel = dbConnection.model(
                    //             "feestructures",
                    //             FeeStructureSchema
                    //         );
                    //         let feeManagerModel = dbConnection.model(
                    //             "feemanagers",
                    //             FeeManagerSchema
                    //         );
                    //         let feeTypeModel = dbConnection.model("feetypes", FeeTypeSchema);

                    //         let feMapDe = await feeMapModel.findOne({
                    //             displayName: inputData.studentFeeMap,
                    //         });
                    //         let feeStructureDetails = await feeStructureModel.findOne({
                    //             _id: feMapDe.feeStructureId,
                    //         });
                    //         let feeBre = [];
                    //         if (feMapDe.transactionPlan.feesBreakUp.length !== 0) {
                    //             for (singleData of feMapDe.transactionPlan.feesBreakUp) {
                    //                 let fees = singleData.amount;
                    //                 for (oneFee of inputData.data.feesBreakUp) {
                    //                     if (
                    //                         String(singleData.feeTypeCode) == String(oneFee.feeTypeCode)
                    //                     ) {
                    //                         let fullPaid =
                    //                             Number(singleData.paid) + Number(oneFee.amount);
                    //                         let fullPending = Number(fees) - fullPaid;
                    //                         let obje;
                    //                         if (Number(fullPending) < 0) {
                    //                             obje = {
                    //                                 amount: fees,
                    //                                 paid: fullPaid,
                    //                                 pending: 0,
                    //                                 feeTypeCode: oneFee.feeTypeCode,
                    //                                 title: oneFee.feeType,
                    //                             };
                    //                         } else {
                    //                             obje = {
                    //                                 amount: fees,
                    //                                 paid: fullPaid,
                    //                                 pending: fullPending,
                    //                                 feeTypeCode: oneFee.feeTypeCode,
                    //                                 title: oneFee.feeType,
                    //                             };
                    //                         }
                    //                         feeBre.push(obje);
                    //                     }
                    //                 }
                    //             }
                    //         } else {
                    //             let fees = singleData.amount;
                    //             for (oneFee of inputData.data.feesBreakUp) {
                    //                 if (
                    //                     String(singleData.feeTypeCode) == String(oneFee.feeTypeCode)
                    //                 ) {
                    //                     let fullPaid = Number(oneFee.amount);
                    //                     let fullPending = Number(fees) - fullPaid;
                    //                     let obje;
                    //                     if (Number(fullPending) < 0) {
                    //                         obje = {
                    //                             amount: fees,
                    //                             paid: fullPaid,
                    //                             pending: 0,
                    //                             feeTypeCode: oneFee.feeTypeCode,
                    //                             title: oneFee.feeType,
                    //                         };
                    //                     } else {
                    //                         obje = {
                    //                             amount: fees,
                    //                             paid: fullPaid,
                    //                             pending: fullPending,
                    //                             feeTypeCode: oneFee.feeTypeCode,
                    //                             title: oneFee.feeType,
                    //                         };
                    //                     }
                    //                     await feeBre.push(obje);
                    //                 }
                    //             }
                    //         }
                    //         var tota = 0;
                    //         var pai = 0;
                    //         var pend = 0;
                    //         for (oneFees of feeBre) {
                    //             tota += oneFees.amount;
                    //             pai += oneFees.paid;
                    //             pend += oneFees.pending;
                    //         }
                    //         let feeTypesPaid = {
                    //             feesBreakUp: feeBre,
                    //             totalAmount: tota,
                    //             totalPaid: pai,
                    //             totalPending: pend,
                    //         };
                    //         let paidA = Number(feMapDe.paid) + Number(inputData.amount);
                    //         let pendingAmountTotal =
                    //             Number(feMapDe.amount) -
                    //             Number(paidA);
                    //         let upd = await feeMapModel.updateOne(
                    //             { studentId: studentData._id },
                    //             {
                    //                 $set: {
                    //                     paid: paidA,
                    //                     pending: pendingAmountTotal,
                    //                     "transactionPlan.totalPaid": paidA,
                    //                     "transactionPlan.paidAmount.0": paidA,
                    //                     "transactionPlan.totalPending": pendingAmountTotal,
                    //                     "transactionPlan.feesBreakUp.0.paid": paidA,
                    //                     "transactionPlan.feesBreakUp.0.pending": pendingAmountTotal,
                    //                 },
                    //             });
                    //     }
                    // }
                }
                if (p == 8) {

                }
            }
            if (j == stdfeeMaps.length) {
                // console.log("sdfdsfdsfdsf")
                return { message: "transactions created" };
            }
        }
    }
}


async function createOtcPayment(req, dbConnection) {
    let inputData = req.body;
    // orgData = orgData._doc 
    // console.log("orgData",orgData)
    var receiptN = ("" + Math.random()).substring(2, 7);
    var year2 = moment().year();
    var transactionDate = moment
        .utc(inputData.transactionDate)
        .tz("Asia/Kolkata");

    // let transactionDate = moment(inputData.transactionDate).format();

    var transID = `TXN/${year2}/${receiptN + 1}`;
    let imode = inputData.data.method;
    let mode = imode.toLowerCase();
    let transactId;
    if (mode == "cash") {
        transactId = transID;
    } else {
        transactId = inputData.paymentTransactionId;
    }

    // if (!orgData || orgData == null) {
    //   return false
    // } else {
    let transactionModel = dbConnection.model(
        "transactions",
        transactionsSchema
    );

    let guardianModel = dbConnection.model("guardians", GuardianSchema);

    let programPlanModel = dbConnection.model(
        "programplans",
        programPlanSchema
    );
    let feeMapModel = dbConnection.model(
        "studentfeesmaps",
        StudentFeeMapSchema
    );
    let settingsModel = dbConnection.model("settings", settingsSchema);
    let studentModel = dbConnection.model("students", StudentSchema);
    let feePlanModel = dbConnection.model("studentfeeplan", feeplanschema);
    let installFeePlanModel = dbConnection.model(
        "studentfeeinstallmentplans",
        feeplanInstallmentschema
    );

    let studentDetails = await studentModel.findOne({
        regId: inputData.studentRegId,
    });
    const orgSettings = await settingsModel.findOne({});
    if (studentDetails) {
        let studentFeeMapDetails = await feeMapModel.findOne({
            studentId: studentDetails._id,
        });
        let programPlanDetails = await programPlanModel.findOne({
            _id: studentDetails.programPlanId,
        });
        let feePlanData = await feePlanModel.findOne({
            studentRegId: studentDetails.regId,
        });
        if (
            Number(feePlanData.pendingAmount) == 0 ||
            Number(feePlanData.pendingAmount) < 0
        ) {
            console.log({
                success: false,
                Message: `Already Paid Full Payment for the student registration ID : ${studentDetails.regId}`,
            })
            return {
                success: false,
                Message: `Already Paid Full Payment for the student registration ID : ${studentDetails.regId}`,
            }
        } else {
            var transactionDetails = await transactionModel.find({
                $or: [{ status: "Pending" }, { status: "Partial" }],
                studentRegId: studentDetails.regId,
                transactionSubType: "demandNote",
            });
            let demandNoteId;
            if (transactionDetails.length == 0) {
                demandNoteId = [];
            } else {
                demandNoteId = transactionDetails.displayName;
            }
            let installmentPlanData = await installFeePlanModel.find({
                feePlanId: feePlanData._id,
            });
            let feesBreak = [];
            var totalLoan = Number(inputData.amount);
            for (oneInsta of installmentPlanData) {
                if (oneInsta.status.toLowerCase() !== "paid") {
                    let payAmnt;
                    if (Number(oneInsta.plannedAmount) <= Number(totalLoan)) {
                        payAmnt = oneInsta.plannedAmount;
                    } else {
                        payAmnt = Number(totalLoan);
                    }
                    totalLoan = Number(totalLoan) - Number(payAmnt);
                    let paid = Number(oneInsta.paidAmount) + Number(payAmnt);
                    let pending = Number(oneInsta.pendingAmount) - Number(payAmnt);
                    let allPending;
                    if (Number(pending) < 0) {
                        allPending = 0;
                    } else {
                        allPending = pending;
                    }
                    if (Number(payAmnt) !== 0 || Number(payAmnt) > 0) {
                        let feesBreakup = {
                            amount: payAmnt,
                            paid: paid,
                            pending: allPending,
                            feeTypeCode: oneInsta.plannedAmountBreakup[0].feeTypeCode,
                            title: oneInsta.plannedAmountBreakup[0].title,
                            installment: oneInsta.label,
                            dueDate: oneInsta.dueDate,
                        };
                        feesBreak.push(feesBreakup);
                    }
                }
            }
            var rcptId = await getDisplayId(dbConnection);
            let passData = {
                displayName: rcptId,
                transactionDate: transactionDate,
                relatedTransactions: [],
                transactionType: "eduFees",
                transactionSubType: "feePayment",
                studentId: studentDetails._id,
                emailCommunicationRefIds: studentDetails.email,
                studentName: studentDetails.firstName + " " + studentDetails.lastName,
                class: inputData.class,
                academicYear: inputData.academicYear,
                amount: inputData.amount,
                studentRegId: studentDetails.regId,
                receiptNo: rcptId,
                programPlan: studentDetails.programPlanId,
                data: {
                    orgId: inputData.data.orgId,
                    displayName: rcptId,
                    transactionType: "eduFees",
                    transactionSubType: "feePayment",
                    mode: "OTC",
                    method: inputData.data.method,
                    modeDetails: inputData.data.modeDetails,
                    feesBreakUp: feesBreak,
                },
                paymentTransactionId: transactId,
                receiptStatus: "",
                currency: "INR",
                currencyAmount: totalLoan,
                exchangeRate: totalLoan,
                userName: studentDetails.firstName + " " + studentDetails.lastName,
                createdBy: studentDetails.createdBy,
                updatedBy: studentDetails.createdBy,
                campusId: studentDetails.campusId,
                status: "",
                type: "After Reconciliation",
            };
            let createDemand = await ledgerEntry({ body: passData }, dbConnection);
            if (createDemand.status == "success") {
                let feMapDe = await feeMapModel.findOne({
                    studentId: studentDetails._id,
                });
                let feePlanData = await feePlanModel.findOne({
                    studentRegId: studentDetails.regId,
                });
                let updateFeeMap = await feeMapModel.updateOne(
                    { displayName: feMapDe.displayName },
                    {
                        $set: {
                            paid: Number(feePlanData.paidAmount),
                            pending: Number(feePlanData.pendingAmount),
                        },
                    }
                );
                if (updateFeeMap.nModified) {
                    return {
                        status: "success",
                        message: "Transaction Created Successfully",
                        data: createDemand,
                    }
                } else {
                    return {
                        success: false,
                        message: "Unable to update the student fees mapping",
                    }
                }
            } else {
                return {
                    success: false,
                    Message: `Unable to make the payment for the student registration ID ${studentDetails.regId}`,
                    // Error: createDemand,
                }
            }
        }
    } else {
        return { success: false, message: "Invalid Registration ID" }
    }
    // }
}

async function ledgerEntry(req, dbConnection) {
    let txnData = req.body;
    checkTransactionPayload(txnData);
    transactionSubType = txnData.transactionSubType;
    let TxnModel = dbConnection.model("transactions", transactionsSchema);
    let FeesLedgerModel = dbConnection.model("feesledgers", feesLedgerSchema);
    let journeyModel = dbConnection.model("journeys", journeysSchema);
    let reconciliationTransactionsModel = dbConnection.model(
        "reconciliationTransactions",
        reconciliationTransactionsSchema
    );
    let studentModel = dbConnection.model("students", StudentSchema);
    let feeMapModel = dbConnection.model("studentfeesmaps", StudentFeeMapSchema);
    let feePlanModel = dbConnection.model("studentfeeplan", feeplanschema);
    let installFeePlan = dbConnection.model(
        "studentfeeinstallmentplans",
        feeplanInstallmentschema
    );
    let studentData = await studentModel.findOne({ _id: txnData.studentId });
    var savedTxnData;
    var ledgerIds;
    var status = "Pending";
    var journeysData;
    var feePlanData;
    var installmentFeePlan;
    try {
        let studentFeesDetails = await feePlanModel.findOne({
            studentRegId: txnData.studentRegId,
        });
        savedTxnData = await insertTransaction(txnData, TxnModel);
        ledgerIds = await insertFeesPaymentLedgerEntries(
            savedTxnData,
            FeesLedgerModel,
            studentFeesDetails
        );
        journeysData = await journeyEntry(
            txnData,
            savedTxnData,
            ledgerIds,
            journeyModel,
            studentFeesDetails.pendingAmount
        );

        feePlanData = await updateFeePlan(
            txnData,
            savedTxnData,
            ledgerIds,
            feePlanModel
        );

        installmentFeePlan = await updateInstallmentFeePlan(
            savedTxnData,
            installFeePlan,
            feePlanData
        );
        let studentFeesDetails1 = await feePlanModel.findOne({
            studentRegId: txnData.studentRegId,
        });
        if (Number(studentFeesDetails1.pendingAmount) == 0) {
            status = "Paid";
        } else {
            status = "Partial";
        }
        await TxnModel.findByIdAndUpdate(
            { _id: savedTxnData._id },
            { feesLedgerIds: ledgerIds, status: status }
        );
        msg =
            "feesTransactionsController: Created " +
            ledgerIds.length +
            " ledger entries for transaction: " +
            txnData.displayName;
        return { status: "success", message: msg, data: savedTxnData };
    } catch (err) {
        msg = "feesTransactionsController: Error: " + err.message;
        // need to do cleanup in case transaction (and some ledger entries) were inserted
        if (savedTxnData) {
            msg =
                "feesTransactionsController: Error: " +
                err.message +
                " Rolling back transaction " +
                savedTxnData._id +
                " and ledgerIds: " +
                ledgerIds;

            if (TxnModel) {
                await TxnModel.deleteOne({ _id: savedTxnData._id });
            }
            if (FeesLedgerModel && ledgerIds) {
                await FeesLedgerModel.deleteMany({ _id: { $in: ledgerIds } });
            }
        }
        return { status: "failure", message: msg, data: txnData };
    } finally {
        // dbConnection.close();
    }
}

async function getDisplayId(dbConnection) {
    var getDatas = [];
    var transType = "";
    const rcptSchema = dbConnection.model(
        "transactions",
        transactionsSchema,
        "transactions"
    );
    //   let rcptSchema = await dbConnection.model(
    //     "transactions",
    //     rcptModel,
    //     "transactions"
    //   );
    getDatas = await rcptSchema.find({});
    transType = "RCPT";
    var date = new Date();
    var month = date.getMonth();
    var finYear = "";
    // if (month >= 2) {

    // } else {
    //   var current = date.getFullYear();
    //   current = String(current).substr(String(current).length - 2);
    //   var prev = Number(date.getFullYear()) - 1;
    //   finYear = `${prev}-${current}`;
    // }
    var current = date.getFullYear();
    var prev = Number(date.getFullYear()) + 1;
    prev = String(prev).substr(String(prev).length - 2);
    finYear = `${current}-${prev}`;

    let initial = `${transType}_${finYear}_001`;
    let dataArr = [];
    let check;
    let finalVal;
    const sortAlphaNum = (a, b) => a.localeCompare(b, "en", { numeric: true });
    getDatas.forEach((el) => {
        if (el["displayName"]) {
            let filStr = el["displayName"].split("_");
            let typeStr = filStr[0];
            let typeYear = filStr[1];
            if (typeStr == transType && typeYear == finYear) {
                check = true;
                dataArr.push(el["displayName"]);
            }
        }
    });
    if (!check) {
        finalVal = initial;
    } else {
        let lastCount = dataArr.sort(sortAlphaNum)[dataArr.length - 1].split("_");
        let lastCountNo = Number(lastCount[2]) + 1;
        if (lastCountNo.toString().length == 1) lastCountNo = "00" + lastCountNo;
        if (lastCountNo.toString().length == 2) lastCountNo = "0" + lastCountNo;
        lastCount[2] = lastCountNo;
        finalVal = lastCount.join("_");
    }
    return finalVal;
}

/**
 * This function performs all sanity checks on input payload
 * req.body should have payload for transaction and corresponding ledger entries
 * @param {*} txnData - from httpRequest object - txnData = req.body
 */
function checkTransactionPayload(txnData) {
    let displayName = txnData.displayName;
    let txnType = txnData.transactionType;
    let txnSubType = txnData.transactionSubType;
    // check for null or empty fields in the httprequest payload
    if (!displayName || displayName == "") {
        errMsg =
            "transactionController: displayName is null in the Transaction payload";
        throw new Error(errMsg);
    }
    if (!txnType || txnType == "") {
        errMsg =
            "transactionController: transactionType is null in the Transaction payload";
        throw new Error(errMsg);
    }
    if (!txnSubType || txnSubType == "") {
        errMsg =
            "transactionController: transactionSubType is null in the Transaction payload";
        throw new Error(errMsg);
    }
} // checkTransactionPayload

async function insertTransaction(txnData, TxnModel) {
    try {
        let txnModel = new TxnModel(txnData);
        var savedTxnData = await txnModel.save();
        msg =
            "feesTransactionsController: Created: " +
            "_id: " +
            savedTxnData._id +
            ", '" +
            savedTxnData.displayName +
            "', type: " +
            savedTxnData.transactionType +
            "/" +
            savedTxnData.transactionSubType;
        return savedTxnData;
    } catch (err) {
        throw err;
    }
} // insertTransaction

async function insertFeesPaymentLedgerEntries(
    savedTxnData,
    FeesLedgerModel,
    studentFeeDetails
) {
    let totalPendingAmount = studentFeeDetails.pendingAmount;
    var status = "Paid";
    let pada = Number(studentFeeDetails.paidAmount) + Number(savedTxnData.amount);
    if (pada < totalPendingAmount) {
        status = "Partial";
    }

    var ledgerIds = [];
    var pending = totalPendingAmount;
    for (feeItem of savedTxnData.data.feesBreakUp) {
        let ans = Number(pending) - Number(feeItem.amount);
        if (Number(feeItem.amount) !== 0) {
            let primary;
            if (savedTxnData.relatedTransactions.length == 0) {
                primary = "";
            } else {
                primary = savedTxnData.relatedTransactions[0];
            }
            feesLedgerData = {
                transactionId: savedTxnData._id,
                transactionDate: savedTxnData.transactionDate,
                transactionDisplayName: savedTxnData.displayName,
                primaryTransaction: primary,
                feeTypeCode: feeItem.feeTypeCode,
                paidAmount: Number(feeItem.amount),
                pendingAmount: ans,
                transactionType: savedTxnData.transactionType,
                transactionSubType: savedTxnData.transactionSubType,
                studentId: savedTxnData.studentId,
                studentRegId: savedTxnData.studentRegId,
                studentName: savedTxnData.studentName,
                academicYear: savedTxnData.academicYear,
                class: savedTxnData.class,
                programPlan: savedTxnData.programPlan,
                campusId: savedTxnData.campusId,
                status: status,
            };
            let feesLedgerModel = new FeesLedgerModel(feesLedgerData);
            ledgerResponse = await feesLedgerModel.save();
            ledgerIds.push(ledgerResponse._id);
        }
    } // for
    return ledgerIds;
} // insertFeesPaymentLedgerEntries

//jounrey entry
async function journeyEntry(
    inputData,
    txnData,
    ledgerData,
    journeyModel,
    totalPendingAmount
) {
    let primary;
    if (txnData.relatedTransactions.length == 0) {
        primary = "";
    } else {
        primary = txnData.relatedTransactions[0];
    }
    let journeyData = {
        primaryCoaCode: inputData.studentId,
        primaryTransaction: primary,
        transaction: txnData.displayName,
        transactionDate: txnData.transactionDate,
        ledgerId: ledgerData,
        creditAmount: 0,
        debitAmount: inputData.amount,
        campusId: inputData.campusId,
        pendingAmount: totalPendingAmount,
    };
    let journeyLedgerData = new journeyModel(journeyData);
    journeyResponse = await journeyLedgerData.save();
    return journeyResponse;
}

//updateFeeplan
async function updateFeePlan(inputData, txnData, ledgerData, feePlanModel) {
    let studentFeesDetails = await feePlanModel.findOne({
        studentRegId: txnData.studentRegId,
    });
    if (studentFeesDetails == null || studentFeesDetails == undefined) {
        console.log("no student fee plan");
        return;
    }

    let pendingAmount =
        Number(studentFeesDetails.pendingAmount) - Number(txnData.amount);
    let totalPaid =
        Number(studentFeesDetails.paidAmount) + Number(txnData.amount);

    for (oneFees of txnData.data.feesBreakUp) {
        // Find item index using _.findIndex (thanks @Muniyaraj for comment)
        var index = _.findIndex(studentFeesDetails.paidAmountBreakup, {
            feeTypeCode: oneFees.feeTypeCode,
        });

        let indexData = studentFeesDetails.paidAmountBreakup[index];

        // Replace item at index using native splice
        studentFeesDetails.paidAmountBreakup.splice(index, 1, {
            amount: Number(indexData.amount) + Number(oneFees.amount),
            feeTypeCode: indexData.feeTypeCode,
            title: indexData.title,
        });
    }

    for (onePending of txnData.data.feesBreakUp) {
        // Find item index using _.findIndex (thanks @Muniyaraj for comment)
        var index = _.findIndex(studentFeesDetails.pendingAmountBreakup, {
            feeTypeCode: onePending.feeTypeCode,
        });

        let indexData = studentFeesDetails.pendingAmountBreakup[index];

        let pendingAm = Number(indexData.amount) - Number(onePending.amount);

        let tot;
        if (Number(pendingAm) < 0) {
            tot = 0;
        } else {
            tot = Number(pendingAm);
        }
        // Replace item at index using native splice
        studentFeesDetails.pendingAmountBreakup.splice(index, 1, {
            amount: tot,
            feeTypeCode: indexData.feeTypeCode,
            title: indexData.title,
        });
    }

    let totalPending;
    if (Number(pendingAmount) < 0) {
        totalPending = 0;
    } else {
        totalPending = pendingAmount;
    }

    let feePlanId = await feePlanModel.findOne({
        studentRegId: txnData.studentRegId,
    });

    let datum = await feePlanModel.updateOne(
        { studentRegId: txnData.studentRegId },
        {
            $set: {
                paidAmount: totalPaid,
                paidAmountBreakup: studentFeesDetails.paidAmountBreakup,
                pendingAmount: totalPending,
                pendingAmountBreakup: studentFeesDetails.pendingAmountBreakup,
            },
        },
        async function (err, resultData) {
            if (resultData.nModified) {
                //   console.log("updated successfully", feePlanId._id);
                return feePlanId._id;
            } else {
                return err;
            }
        }
    );

    return feePlanId._id;
}

async function updateInstallmentFeePlan(
    txnData,
    installmentFeePlanModel,
    feePlanId
) {
    //transaction loop
    for (oneBreakUp of txnData.data.feesBreakUp) {
        let oneInstallment = await installmentFeePlanModel.findOne({
            label: String(oneBreakUp.installment),
            feePlanId: feePlanId,
        });
        let oneInstallmentPaid = Number(oneInstallment.paidAmount);
        let oneInstallmentPending = Number(oneInstallment.pendingAmount);
        if (
            Number(oneInstallmentPending) < 0 ||
            Number(oneInstallmentPending) == 0
        ) {
            return { status: "Fee Installment Already Paid" };
        } else {
            let newPaid = Number(oneInstallmentPaid) + Number(oneBreakUp.amount);
            let newPending =
                Number(oneInstallmentPending) - Number(oneBreakUp.amount);
            let status;
            if (Number(newPending) == 0 || Number(newPending) < 0) {
                status = "Paid";
            } else {
                status = "Planned";
            }

            // Find item index using _.findIndex (thanks @Muniyaraj for comment)
            var index = _.findIndex(oneInstallment.paidAmountBreakup, {
                feeTypeCode: oneBreakUp.feeTypeCode,
            });

            let indexData = oneInstallment.paidAmountBreakup[index];

            // Replace item at index using native splice
            oneInstallment.paidAmountBreakup.splice(index, 1, {
                amount: Number(indexData.amount) + Number(oneBreakUp.amount),
                feeTypeCode: indexData.feeTypeCode,
                title: indexData.title,
            });

            // Find item index using _.findIndex (thanks @Muniyaraj for comment)
            var index1 = _.findIndex(oneInstallment.pendingAmountBreakup, {
                feeTypeCode: oneBreakUp.feeTypeCode,
            });

            let indexData1 = oneInstallment.pendingAmountBreakup[index1];

            // Replace item at index using native splice
            oneInstallment.pendingAmountBreakup.splice(index, 1, {
                amount: Number(indexData1.amount) - Number(oneBreakUp.amount),
                feeTypeCode: indexData1.feeTypeCode,
                title: indexData1.title,
            });

            installmentFeePlanModel.updateOne(
                { _id: oneInstallment._id },
                {
                    $set: {
                        paidAmount: newPaid,
                        pendingAmount: newPending,
                        status: status,
                        paidAmountBreakup: oneInstallment.paidAmountBreakup,
                        pendingAmountBreakup: oneInstallment.pendingAmountBreakup,
                    },
                },
                async function (err, resultData) {
                    if (resultData.nModified) {
                        //   console.log("updated installment successfully", resultData);
                    } else {
                        console.log("nothing updated", err);
                    }
                }
            );
        }
    }
}

module.exports.getFeeAmount = async (req, res) => {
    let student = req.query.id
    let dbConnectionp = await createDatabase(req.headers.orgId, req.headers.resource);
    let feeStructureModel = dbConnectionp.model("feestructures", FeeStructureSchema);
    let studentModel = dbConnectionp.model("students", StudentSchema);
    let feeManagerSchema = dbConnectionp.model("feemanagers", FeeManagerSchema);
    const settingsModel = dbConnectionp.model("settings", settingsSchema, "settings");

    let stddata = await studentModel.findOne({ "regId": req.query.id });
    let feestrData = await feeStructureModel.findOne({ _id: stddata._doc.feeStructureId });
    let settingsData = await settingsModel.find({})
    let feemapType = settingsData[0]._doc.fees.feeMapType;
    if (feemapType == "Program Plan") {
        let feeAmount = 0;
        for (let i = 0; i < feestrData._doc.feeTypeIds.length; i++) {
            let feeData = await feeManagerSchema.findOne({ feeTypeId: feestrData._doc.feeTypeIds[i], programPlanId: stddata._doc.programPlanId });
            // let feeInv = await feeData.find(item=>item._doc.feeMapType==feemapType)
            feeAmount = feeAmount + Number(feeData._doc.feeDetails.totalAmount)
            if (i + 1 == feestrData._doc.feeTypeIds.length) {
                res.send({ status: "success", data: `Fee Amount for Student ${req.query.id} is ${feeAmount}` })
            }
        }
    }
    else if (feemapType == "Amount") {
        let feeAmount = 0;
        for (let i = 0; i < feestrData._doc.feeTypeIds.length; i++) {
            let feeData = await feeManagerSchema.find({ feeTypeId: feestrData._doc.feeTypeIds[i], programPlanId: stddata._doc.programPlanId });
            let feeInv = await feeData.find(item => item._doc.feeMapType == feemapType)
            feeAmount = feeAmount + Number(feeInv.feeDetails.totalAmount)
            if (i + 1 == feestrData._doc.feeTypeIds.length) {
                res.send({ status: "success", data: `Fee Amount for Student ${req.query.id} is ${feeAmount}` })
            }
        }
    }
    else if (feemapType == "Program Plan & Date of Join") {
        let feeAmount = 0;
        // for(let i=0;i<feestrData._doc.feeTypeIds.length;i++){
        let feeData = await feeManagerSchema.find({ feeTypeId: feestrData._doc.feeTypeIds[0], programPlanId: stddata._doc.programPlanId });
        console.log(new Date(stddata._doc.admittedOn), feeData[0]._doc.applyDates[0] <= new Date(stddata._doc.admittedOn) && feeData[0]._doc.applyDates[1] >= new Date(stddata._doc.admittedOn))
        for (let j = 0; j < feeData.length; j++) {
            if (feeData[j]._doc.applyDates[0] <= new Date(stddata._doc.admittedOn) && feeData[j]._doc.applyDates[1] >= new Date(stddata._doc.admittedOn) && stddata._doc.campusId[0].toString() == feeData[j]._doc.campusId[0].toString()) {
                feeAmount = feeAmount + Number(feeData[j].feeDetails.totalAmount)
            }
        }
        // let feeInv = await feeData.find(item=>(new Date(item._doc.applyDates[0])<=new Date(stddata._doc.admittedOn) && new Date(item._doc.applyDates[1])>=new Date(stddata._doc.admittedOn)))
        // feeAmount = feeAmount + Number(feeInv.feeDetails.totalAmount)
        // if(i+1==feestrData._doc.feeTypeIds.length){
        res.send({ status: "success", data: `Fee Amount for Student ${req.query.id} is ${feeAmount}` })
        // }
        // }
    }



}

async function createFeePlans1(req, dbConnectionp, feeManagerDatas, feeTypeDetails, studentDetails, feeManagerSchema, feeStructureModel, studentModel, stdfeeMaps, campuses, allProgramPlan, allPaymentSchedule) {
    return new Promise(async function (resolve, reject) {
        let daydigits = { "First": "02", "Second": "03", "Third": "04", "Fourth": "05", "Fifth": "06", "Tenth": "11", "Last": "28", "Second Last": "27", "Third last": "26", "Fourth last": "25" };
        // let student = req.query.id:
        // let dbConnectionp = await createDatabase(req.headers.orgId, req.headers.resource);
        // let feeStructureModel = dbConnectionp.model("feestructures", FeeStructureSchema);
        // let studentModel = dbConnectionp.model("students", StudentSchema);
        // let feeManagerSchema = dbConnectionp.model("feemanagers", FeeManagerSchema);
        // let campid1 = await campuses.find(item => item._id.toString() == studentData._doc["campusId"].toString())
        // let campid = campid1["_id"];
        let feePlanModel = dbConnectionp.model("studentfeeplans", feeplanschema);
        let feeInstallmentPlanModel = dbConnectionp.model("studentfeeinstallmentplans", feeplanInstallmentschema);

        var allStudentsFeePlans = [];
        var allStudentsInstallmentPlans = []
        let count = 0;
        for (let j = 0; j < studentDetails.length; j++) {
            console.log(j)
            let ppInputData = studentDetails[j]
            regId = ppInputData["Reg ID *"] == "-" ? ppInputData["HEDA ID"] : ppInputData["Reg ID *"]
            let stddata = await studentModel.findOne({ "regId": regId });
            let ppdata = await allProgramPlan.find(item => item._id.toString() == stddata._doc.programPlanId);
            let yearsplit = ppdata.academicYear.split("-")
            let year = yearsplit[0]
            let feestrData = await feeStructureModel.findOne({ _id: stddata._doc.feeStructureId, campusId: stddata._doc.campusId });

            let stuFeeplan = {
                _id: mongoose.Types.ObjectId(),
                applicationId: `FPLAN${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                    }${Number(j) + 1}`,
                studentRegId: ppInputData["Reg ID *"] == "-" ? ppInputData["HEDA ID"] : ppInputData["Reg ID *"],
                studentId: stddata._id,
                programPlanHEDAId: ppInputData["Program Plan ID"],
                plannedAmount: 0,
                plannedAmountBreakup: [],
                paidAmount: 0,
                paidAmountBreakup: [],
                pendingAmount: 0,
                pendingAmountBreakup: [],
                currency: stddata._doc.currency,
                forex: stddata._doc.FOREX,
                discountType: "",
                discountPercentage: 0,
                discountAmount: 0,
                dueDate: "",
                penaltyStartDate: "",
                percentageBreakup: [],
                installmentPlan: {},
                discountAmountBreakup: [],
                campusId: stddata._doc.campusId,
                remarks: {
                    seller: "",
                    finance: "",
                    headseller: ""
                }
            }
            let stdFeeInPlan = {
                feePlanId: stuFeeplan._id,
                label: "",
                description: "",
                dueDate: "",
                lateFeeStartDate: "",
                percentage: "",
                totalAmount: 0,
                plannedAmount: 0,
                plannedAmountBreakup: [],
                paidAmount: 0,
                paidAmountBreakup: [],
                pendingAmount: 0,
                pendingAmountBreakup: [],
                discountType: "",
                discountPercentage: 0,
                discountAmount: 0,
                discountAmountBreakup: [],
                status: "",
                transactionId: "",
                campusId: stddata._doc.campusId,
                remarks: {
                    seller: "",
                    finance: "",
                    headseller: ""
                }
            }
            let feeAmount = 0;
            let plannedAmount = 0;
            let plannedAmountBreakup = [];
            let paidAmountBreakup = [];
            let pendingAmountBreakup = [];
            let discountAmountBreakup = [];
            let instplannedAmountBreakup = [];
            let instpaidAmountBreakup = [];
            let instpendingAmountBreakup = [];

            for (let i = 0; i < feestrData._doc.feeTypeIds.length; i++) {
                let feetyped = feeTypeDetails.find(item => item._id.toString() == feestrData._doc.feeTypeIds[i])
                paidAmountBreakup.push({
                    amount: 0.00,
                    feeTypeCode: feetyped.displayName,
                    title: feetyped.title,
                })
                let feeBreakup = [];
                let feeData = await feeManagerSchema.findOne({ feeTypeId: feestrData._doc.feeTypeIds[i], programPlanId: stddata._doc.programPlanId, "applyDates.0": { $lte: new Date(stddata._doc.admittedOn) }, "applyDates.1": { $gte: new Date(stddata._doc.admittedOn) }, campusId: stddata._doc.campusId });
                if (!feeData || feeData == null) {
                    feeData = await feeManagerSchema.findOne({ feeTypeId: feestrData._doc.feeTypeIds[i], campusId: stddata._doc.campusId });
                }
                let pshedule
                if (feeData == undefined || feeData == null) {
                    pshedule = allPaymentSchedule[0]
                } else {
                    pshedule = await allPaymentSchedule.find(item => item._id.toString() == feeData.paymentScheduleId);
                }
                let month = pshedule.startMonth;
                let duedate = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                let penaltyStartdate = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                penaltyStartdate = new Date(penaltyStartdate.setMonth((Number(penaltyStartdate.getMonth()) + 1)));
                let tuitionFee = Number(feeData.feeDetails.totalAmount);
                plannedAmountBreakup.push({
                    amount: Number(feeData.feeDetails.totalAmount),
                    feeTypeCode: feetyped.displayName,
                    title: feetyped.title,
                })
                plannedAmount = plannedAmount + Number(feeData.feeDetails.totalAmount);
                pendingAmountBreakup.push({
                    amount: Number(feeData.feeDetails.totalAmount),
                    feeTypeCode: feetyped.displayName,
                    title: feetyped.title,
                })
                discountAmountBreakup.push({
                    amount: 0.00,
                    feeTypeCode: feetyped.displayName,
                    title: feetyped.title,
                })
                if (i + 1 == feestrData._doc.feeTypeIds.length) {
                    for (l = 0; l < pshedule.feesBreakUp.length; l++) {
                        count = l + 1
                        let duedate1
                        let plannedAmountBreakupinst = [];
                        for (n = 0; n < plannedAmountBreakup.length; n++) {
                            plannedAmountBreakupinst.push({
                                amount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmountBreakup[n].amount) / 100,
                                feeTypeCode: plannedAmountBreakup[n].feeTypeCode,
                                title: plannedAmountBreakup[n].title,
                            })
                        }
                        let penaltyStartdate1
                        if (l > 0) {
                            duedate1 = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                            duedate1 = duedate1.setMonth((Number(duedate1.getMonth()) + (12 / pshedule.feesBreakUp.length)));
                            penaltyStartdate1 = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                            penaltyStartdate1 = penaltyStartdate1.setMonth((Number(penaltyStartdate1.getMonth()) + ((12 / pshedule.feesBreakUp.length) + 1)));
                        }
                        else {
                            duedate1 = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                            penaltyStartdate1 = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                            penaltyStartdate1 = penaltyStartdate1.setMonth((Number(penaltyStartdate1.getMonth()) + 1));
                        }
                        let stuinstallplans = new feeInstallmentPlanModel({
                            feePlanId: stuFeeplan._id,
                            label: `Installment${(Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                                }${Number(count)}`,
                            description: `Installment${(Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                                }${Number(count)}`,
                            dueDate: duedate1,
                            lateFeeStartDate: penaltyStartdate1,
                            percentage: pshedule.feesBreakUp[l],
                            totalAmount: plannedAmount,
                            plannedAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmount) / 100,
                            plannedAmountBreakup: plannedAmountBreakupinst,
                            paidAmount: 0,
                            paidAmountBreakup: paidAmountBreakup,
                            pendingAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmount) / 100,
                            pendingAmountBreakup: plannedAmountBreakupinst,
                            discountType: "",
                            discountPercentage: 0,
                            discountAmount: 0,
                            discountAmountBreakup: discountAmountBreakup,
                            status: "",
                            transactionId: "",
                            campusId: stddata._doc.campusId,
                            remarks: {
                                seller: "",
                                finance: "",
                                headseller: ""
                            }
                        })
                        allStudentsInstallmentPlans.push(stuinstallplans)
                        // feeBreakup.push({ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(tuitionFee) / 100), percent: pshedule.feesBreakUp[l] })

                    }

                }
                // else {
                //     let feeBreakup = [];
                //     let feeData = await feeManagerSchema.findOne({ feeTypeId: feestrData._doc.feeTypeIds[i], programPlanId: stddata._doc.programPlanId, campusId: stddata._doc.campusId });
                //     let pshedule 
                //     if (feeData == undefined || feeData == null) {
                //         feeData = await feeManagerSchema.findOne({ feeTypeId: feestrData._doc.feeTypeIds[i], campusId: stddata._doc.campusId });
                //         pshedule = allPaymentSchedule[0]
                //     } else {
                //         pshedule = await allPaymentSchedule.find(item => item._id.toString() == feeData.paymentScheduleId);
                //     }
                //     // let pshedule = await allPaymentSchedule.find(item => item._id.toString() == feeData.paymentScheduleId);
                //     let month = pshedule.startMonth;
                //     let duedate = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                //     let penaltyStartdate = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                //     penaltyStartdate = new Date(penaltyStartdate.setMonth((Number(penaltyStartdate.getMonth()) + 1)));
                //     for (l = 0; l < pshedule.feesBreakUp.length; l++) {
                //         count++
                //         if (l > 0) {
                //             duedate = duedate.setMonth((Number(duedate.getMonth()) + (12 / pshedule.feesBreakUp.length)));
                //             penaltyStartdate = penaltyStartdate.setMonth((Number(penaltyStartdate.getMonth()) + (12 / pshedule.feesBreakUp.length)));
                //         }
                //         feeBreakup.push({ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100), percent: pshedule.feesBreakUp[l] })
                //         let stuinstallplans = new feeInstallmentPlanModel({
                //             feePlanId: stuFeeplan._id,
                //             label: `Installment${
                //                 (Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                //                 }${Number(count)}`,
                //             description: `Installment${
                //                 (Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                //                 }${Number(count)}`,
                //             dueDate: duedate,
                //             penaltyStartDate: penaltyStartdate,
                //             percentage: pshedule.feesBreakUp[l],
                //             totalAmount: feeData.feeDetails.totalAmount,
                //             plannedAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100,
                //             plannedAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100), percent: pshedule.feesBreakUp[l] }],
                //             paidAmount: 0,
                //             paidAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: 0, percent: pshedule.feesBreakUp[l] }],
                //             pendingAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100,
                //             pendingAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100, percent: pshedule.feesBreakUp[l] }],
                //             discountType: "",
                //             discountPercentage: 0,
                //             discountAmount: 0,
                //             discountAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: 0, percent: pshedule.feesBreakUp[l] }],
                //             status: "",
                //             transactionId: "",
                //             campusId: stddata._doc.campusId,
                //             remarks: {
                //                 seller: "",
                //                 finance: "",
                //                 headseller: ""
                //             }
                //         })
                //         allStudentsInstallmentPlans.push(stuinstallplans)
                //     }
                //     plannedAmountBreakup.push({
                //         amount: Number(feeData.feeDetails.totalAmount),
                //         feeTypeCode: feetyped.displayName,
                //         title: feetyped.title,
                //     })
                //     plannedAmount = plannedAmount + Number(feeData.feeDetails.totalAmount);
                //     pendingAmountBreakup.push({
                //         amount: Number(feeData.feeDetails.totalAmount),
                //         feeTypeCode: feetyped.displayName,
                //         title: feetyped.title,
                //     })
                //     discountAmountBreakup.push({
                //         amount: 0.00,
                //         feeTypeCode: feetyped.displayName,
                //         title: feetyped.title,
                //     })
                // }

                // for (let j = 0; j < feeData.length; j++) {
                //     if (feeData[j]._doc.applyDates[0] <= new Date(stddata._doc.admittedOn) && feeData[j]._doc.applyDates[1] >= new Date(stddata._doc.admittedOn) && stddata._doc.campusId[0].toString() == feeData[j]._doc.campusId[0].toString()) {
                //         feeAmount = feeAmount + Number(feeData[j].feeDetails.totalAmount)
                //     }
                // }
            }
            stuFeeplan.plannedAmount = plannedAmount;
            stuFeeplan.pendingAmount = plannedAmount;
            stuFeeplan.plannedAmountBreakup = plannedAmountBreakup;
            stuFeeplan.paidAmountBreakup = paidAmountBreakup;
            stuFeeplan.pendingAmountBreakup = pendingAmountBreakup;
            stuFeeplan.discountAmountBreakup = discountAmountBreakup;
            let stuFeePlans = new feePlanModel(stuFeeplan)

            allStudentsFeePlans.push(stuFeePlans)
            if (j + 1 == studentDetails.length) {
                feePlanModel.insertMany(allStudentsFeePlans, function (err, result) {
                    if (err) {
                        if (err.name === "BulkWriteError" && err.code === 11000) {
                            reject(res.status(200).json({
                                success: true,
                                message: err.toString(),
                                count: 0,
                            }))
                        }
                    } else {
                        feeInstallmentPlanModel.insertMany(allStudentsInstallmentPlans, function (err2, result2) {
                            if (err2) {
                                if (err2.name === "BulkWriteError" && err2.code === 11000) {
                                    reject(res.status(200).json({
                                        success: true,
                                        message: err2.toString(),
                                        count: 0,
                                    }))
                                }
                            } else {
                                resolve({ status: "success", message: "fee plan created successfully" })
                            }
                        })
                    }
                })
            }
            // await stuFeePlans.save();
        }
    })
}
async function createFeePlans(req, dbConnectionp, feeManagerDatas, feeTypeDetails, studentDetails, feeManagerSchema, feeStructureModel, studentModel, stdfeeMaps, campuses, allProgramPlan, allPaymentSchedule, finYear, allStudentMap1) {
    return new Promise(async function (resolve, reject) {
        let daydigits = { "First": "01", "Second": "02", "Third": "03", "Fourth": "04", "Fifth": "05", "Tenth": "10", "Fifteenth": "15", "Last": "28", "Second Last": "27", "Third last": "26", "Fourth last": "25" };
        let monthdigits = { "January": "01", "February": "02", "March": "03", "April": "04", "May": "05", "June": "06", "July": "07", "August": "08", "September": "09", "October": "10", "November": "11", "December": "12" };
        let digitsmonth = { "01": "January", "02": "February", "03": "March", "04": "April", "05": "May", "06": "June", "07": "July", "08": "August", "09": "September", "10": "October", "11": "November", "12": "December" };

        let InstfeePlans = {
            "Fee Plan 1": {
                amount: 155000,
                feesBreakUp: [100],
                feesBreakUpAmount: [100]
            },
            "Fee Plan 2": {
                amount: 145000,
                feesBreakUp: [8.33, 8.33, 8.33, 8.33, 8.33, 8.33, 8.33, 8.33, 8.33, 8.33, 8.33, 8.33],
                feesBreakUpAmount: [12500, 12500, 12500, 12500, 12500, 12500, 12500, 12500, 12500, 12500, 12500, 12500]
            },
            "Fee Plan 3": {
                amount: 115000,
                feesBreakUp: [100],
                feesBreakUpAmount: [65500, 49500]
            },
            "Fee Plan 4": {
                amount: 115000,
                feesBreakUp: [100],
                feesBreakUpAmount: [35000, 35000, 15000, 15000, 15000]
            },
            "Fee Plan 5": {
                amount: 155000,
                feesBreakUp: [100],
                feesBreakUpAmount: [140500, 14500]
            },
            "Fee Plan 6": {
                amount: 155000,
                feesBreakUp: [100],
                feesBreakUpAmount: [140500, 14500]
            },
            "Fee Plan 7": {
                amount: 145000,
                feesBreakUp: [100],
                feesBreakUpAmount: [100165, 44835]
            },
            "Fee Plan 8": {
                amount: 172280,
                feesBreakUp: [100],
                feesBreakUpAmount: [157780, 14500]
            },
            "Fee Plan 9": {
                amount: 155000,
                feesBreakUp: [100],
                feesBreakUpAmount: [56250, 56250, 21250, 21250]
            },
            "Fee Plan 10": {
                amount: 172280,
                feesBreakUp: [100],
                feesBreakUpAmount: [60570, 60570, 25570, 25570]
            },
            "Fee Plan 11": {
                amount: 115000,
                feesBreakUp: [100],
                feesBreakUpAmount: [55000, 20000, 20000, 20000]
            },
            "Fee Plan 12": {
                amount: 145000,
                feesBreakUp: [100],
                feesBreakUpAmount: [71667, 36667, 36667]
            },
            "Fee Plan 13": {
                amount: 145000,
                feesBreakUp: [100],
                feesBreakUpAmount: [36250, 36250, 36250, 36250]
            },
            "Fee Plan 14": {
                amount: 155000,
                feesBreakUp: [100],
                feesBreakUpAmount: [38750, 38750, 38750, 38750]
            },
            "Fee Plan 15": {
                amount: 172280,
                feesBreakUp: [100],
                feesBreakUpAmount: [43070, 43070, 43070, 43070]
            },
            "Fee Plan 16": {
                amount: 155000,
                feesBreakUp: [100],
                feesBreakUpAmount: [57000, 57000, 20500, 20500]
            },
            "Fee Plan 17": {
                amount: 172280,
                feesBreakUp: [100],
                feesBreakUpAmount: [57000, 57000, 29140, 29140]
            },
            "Fee Plan 18": {
                amount: 155000,
                feesBreakUp: [100],
                feesBreakUpAmount: [77500, 77500]
            },
            "Fee Plan 19": {
                amount: 155000,
                feesBreakUp: [100],
                feesBreakUpAmount: [100]
            },
        }
        // let student = req.query.id:
        // let dbConnectionp = await createDatabase(req.headers.orgId, req.headers.resource);
        // let feeStructureModel = dbConnectionp.model("feestructures", FeeStructureSchema);
        // let studentModel = dbConnectionp.model("students", StudentSchema);
        // let feeManagerSchema = dbConnectionp.model("feemanagers", FeeManagerSchema);
        // let campid1 = await campuses.find(item => item._id.toString() == studentData._doc["campusId"].toString())
        // let campid = campid1["_id"];
        let feePlanModel = dbConnectionp.model("studentfeeplans", feeplanschema);
        let feeInstallmentPlanModel = dbConnectionp.model("studentfeeinstallmentplans", feeplanInstallmentschema);
        let allStudentMap = dbConnectionp.model("studentfeesmaps", StudentFeeMapSchema);
        var allStudentsFeePlans = [];
        var allStudentsInstallmentPlans = []
        let count = 0;
        let dcount = 0;
        let duedates1 = [new Date(`2021-April-10`)];
        let duedates12 = [new Date(`2021-April-10`), new Date(`2021-May-10`), new Date(`2021-June-10`), new Date(`2021-July-10`), new Date(`2021-August-10`), new Date(`2021-September-10`), new Date(`2021-October-10`), new Date(`2021-November-10`), new Date(`2021-December-10`), new Date(`2022-January-10`), new Date(`2022-February-10`), new Date(`2022-March-10`)];
        let duedates4 = [new Date(`2021-April-10`), new Date(`2021-June-10`), new Date(`2021-September-10`), new Date(`2021-November-10`)];
        let duedates3 = [new Date(`2021-April-10`), new Date(`2021-July-10`), new Date(`2021-November-10`)];
        let duedates5 = [new Date(`2021-April-10`), new Date(`2021-June-10`), new Date(`2021-August-10`), new Date(`2021-October-10`), new Date(`2021-December-10`)];
        let latefeesdates4 = [new Date(`2021-April-30`), new Date(`2021-June-30`), new Date(`2021-September-30`), new Date(`2021-November-30`)];
        let duedates2 = [new Date(`2021-June-01`), new Date(`2021-October-01`)];
        let latefeesdates2 = [new Date(`2021-November-30`), new Date(`2022-May-30`)];
        let latefeesdates1 = [new Date(`2021-April-30`)]
        let latefeesdates3 = [new Date(`2021-April-30`), new Date(`2021-July-30`), new Date(`2021-November-30`)];
        let latefeesdates5 = [new Date(`2021-April-30`), new Date(`2021-June-30`), new Date(`2021-August-30`), new Date(`2021-October-30`), new Date(`2021-December-30`)];
        let latefeesdates12 = [new Date(`2021-April-30`), new Date(`2021-May-30`), new Date(`2021-June-30`), new Date(`2021-July-30`), new Date(`2021-August-30`), new Date(`2021-September-30`), new Date(`2021-October-30`), new Date(`2021-November-30`), new Date(`2021-December-30`), new Date(`2022-January-30`), new Date(`2022-February-28`), new Date(`2022-March-30`)];

        let feeDueDates = {
            duedates1: duedates1,
            duedates2: duedates2,
            duedates3: duedates3,
            duedates4: duedates4,
            duedates5: duedates5,
            duedates12: duedates12
        }
        let lateFeeDates = {
            latefeesdates1: latefeesdates1,
            latefeesdates2: latefeesdates2,
            latefeesdates3: latefeesdates3,
            latefeesdates4: latefeesdates4,
            latefeesdates5: latefeesdates5,
            latefeesdates12: latefeesdates12
        }
        for (let j = 0; j < studentDetails.length; j++) {
            // for (let j = 2608; j < studentDetails.length; j++) {
            console.log(j)
            let feePlanBreakUp = [];
            let ppInputData = studentDetails[j]
            // regId = ppInputData["Reg ID *"] == "-" ? ppInputData["HEDA ID"] : ppInputData["Reg ID *"]
            let regId = ppInputData["Reg ID *"]
            let stddata = await studentModel.findOne({ "regId": regId });
            let ppdata = await allProgramPlan.find(item => item._id.toString() == stddata._doc.programPlanId);
            let feeData = await allStudentMap.findOne({ studentId: stddata._doc._id })
            if (!feeData) {
                console.log(stddata._doc.regId)
            }
            let academicperiod
            let feePlanacademicyearwise = false
            for (let q = 0; q < 4; q++) {
                if (feeData[`Tuition Amount ${q + 1}`] && feeData[`Tuition Amount ${q + 1}`].toString() == "-") {
                    feePlanacademicyearwise = true
                    academicperiod = q + 1
                }
            }
            // console.log(stdfeeMaps[j]["Fee Plan"])
            if (stdfeeMaps[j]["Fee Plan"]) {
                let yearsplit = ppdata.academicYear.split("-")
                let year = yearsplit[0]
                let feestrData = await feeStructureModel.findOne({ _id: stddata._doc.feeStructureId, campusId: stddata._doc.campusId });
                let feeData = await allStudentMap.findOne({ studentId: stddata._doc._id })
                if (!feeData) {
                    console.log(stddata._doc.regId)
                }
                let stuFeeplan = {
                    _id: mongoose.Types.ObjectId(),
                    applicationId: `FPLAN_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                        }${Number(j) + 1}`,
                    studentRegId: ppInputData["Reg ID *"] == "-" ? ppInputData["HEDA ID"] : ppInputData["Reg ID *"],
                    studentId: stddata._id,
                    programPlanHEDAId: ppInputData["Program Plan ID"],
                    plannedAmount: 0,
                    plannedAmountBreakup: [],
                    paidAmount: 0,
                    paidAmountBreakup: [],
                    pendingAmount: 0,
                    pendingAmountBreakup: [],
                    currency: stddata._doc.currency,
                    forex: stddata._doc.FOREX,
                    discountType: "",
                    discountPercentage: 0,
                    discountAmount: 0,
                    dueDate: "",
                    penaltyStartDate: "",
                    percentageBreakup: [],
                    installmentPlan: {},
                    discountAmountBreakup: [],
                    campusId: stddata._doc.campusId,
                    concessionPercentage: 0,
                    concessionAmount: 0,
                    remarks: {
                        seller: "",
                        finance: "",
                        headseller: ""
                    }
                }
                let stdFeeInPlan = {
                    feePlanId: stuFeeplan._id,
                    label: "",
                    description: "",
                    dueDate: "",
                    lateFeeStartDate: "",
                    percentage: "",
                    totalAmount: 0,
                    plannedAmount: 0,
                    plannedAmountBreakup: [],
                    paidAmount: 0,
                    paidAmountBreakup: [],
                    pendingAmount: 0,
                    pendingAmountBreakup: [],
                    discountType: "",
                    discountPercentage: 0,
                    discountAmount: 0,
                    discountAmountBreakup: [],
                    status: "",
                    transactionId: "",
                    campusId: stddata._doc.campusId,
                    remarks: {
                        seller: "",
                        finance: "",
                        headseller: ""
                    }
                }
                let feeAmount = 0;
                let plannedAmount = 0;
                let pendingAmount = 0;
                let paidAmount = 0;
                let plannedAmountBreakup = [];
                let paidAmountBreakup = [];
                let pendingAmountBreakup = [];
                let discountAmountBreakup = [];
                let instplannedAmountBreakup = [];
                let instpaidAmountBreakup = [];
                let instpendingAmountBreakup = [];
                let pshedule = { feesBreakUpAmount: [] }
                if (!InstfeePlans[stdfeeMaps[j]["Fee Plan"]]) {
                    console.log(stdfeeMaps[j])
                }
                pshedule.feesBreakUpAmount = InstfeePlans[stdfeeMaps[j]["Fee Plan"]].feesBreakUpAmount
                for (let i = 0; i < feestrData._doc.feeTypeIds.length; i++) {
                    let feetyped = feeTypeDetails.find(item => item._id.toString() == feestrData._doc.feeTypeIds[i])
                    let feeBreakup = [];
                    if (feetyped.title.toLowerCase().includes("tuition") || feetyped.title.includes("Term")) {
                        plannedAmountBreakup.push({
                            amount: Number(feeData["amount"]),
                            feeTypeCode: feetyped.displayName,
                            title: feetyped.title,
                        })
                        plannedAmount = plannedAmount + Number(feeData["amount"]);
                        pendingAmount = pendingAmount + Number(feeData["pending"]);
                        paidAmount = paidAmount + Number(feeData["paid"]);
                        pendingAmountBreakup.push({
                            amount: Number(feeData["pending"]),
                            feeTypeCode: feetyped.displayName,
                            title: feetyped.title,
                        })
                        paidAmountBreakup.push({
                            amount: Number(feeData["paid"]),
                            feeTypeCode: feetyped.displayName,
                            title: feetyped.title,
                        })
                    }
                    else {
                        plannedAmountBreakup.push({
                            amount: 0.00,
                            feeTypeCode: feetyped.displayName,
                            title: feetyped.title,
                        })
                        plannedAmount = plannedAmount + 0;
                        pendingAmount = pendingAmount + 0;
                        paidAmount = paidAmount + 0;
                        pendingAmountBreakup.push({
                            amount: 0.00,
                            feeTypeCode: feetyped.displayName,
                            title: feetyped.title,
                        })
                        paidAmountBreakup.push({
                            amount: 0.00,
                            feeTypeCode: feetyped.displayName,
                            title: feetyped.title,
                        })
                    }
                    discountAmountBreakup.push({
                        amount: 0.00,
                        feeTypeCode: feetyped.displayName,
                        title: feetyped.title,
                    })
                    let temppaid = 0
                    let temppend = 0
                    if (i + 1 == feestrData._doc.feeTypeIds.length) {
                        for (l = 0; l < pshedule.feesBreakUpAmount.length; l++) {
                            count = l + 1
                            let duedate1
                            let penaltyStartdate1
                            let plannedAmountBreakupinst = [];
                            let pendingAmountBreakupinst = [];
                            let paidAmountBreakupinst = [];
                            let title
                            title = `Term Fees`
                            duedate1 = feeDueDates[`duedates${pshedule.feesBreakUpAmount.length}`][l]
                            penaltyStartdate1 = lateFeeDates[`latefeesdates${pshedule.feesBreakUpAmount.length}`][l]
                            let paida
                            let penda
                            paida = 0
                            penda = parseFloat(pshedule.feesBreakUpAmount[l])

                            for (n = 0; n < plannedAmountBreakup.length; n++) {
                                plannedAmountBreakupinst.push({
                                    amount: n == 0 ? parseFloat(pshedule.feesBreakUpAmount[l]) : 0,
                                    feeTypeCode: plannedAmountBreakup[n].feeTypeCode,
                                    // title: `Term ${count<3 ? 1: 2} Fees`,
                                    title: title == null ? plannedAmountBreakup[n].title : title,
                                })
                                pendingAmountBreakupinst.push({
                                    amount: n == 0 ? penda : 0,
                                    feeTypeCode: pendingAmountBreakup[n].feeTypeCode,
                                    // title: `Term ${count<3 ? 1: 2} Fees`,
                                    title: title == null ? plannedAmountBreakup[n].title : title,
                                })
                                paidAmountBreakupinst.push({
                                    amount: n == 0 ? paida : 0,
                                    feeTypeCode: paidAmountBreakup[n].feeTypeCode,
                                    // title: `Term ${count<3 ? 1: 2} Fees`,
                                    title: title == null ? plannedAmountBreakup[n].title : title,
                                })

                            }
                            dcount++
                            let stuinstallplans = new feeInstallmentPlanModel({
                                feePlanId: stuFeeplan._id,
                                label: `Installment${(Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                                    }${Number(count)}`,
                                displayName: `INST_${finYear}_${(Number(dcount) + 1).toString().length == 1 ? "00" : (Number(dcount) + 1).toString().length == 2 ? "0" : ""
                                    }${Number(dcount) + 1}`,
                                description: `Installment${(Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                                    }${Number(count)}`,
                                studentRegId: stddata._doc.regId,
                                dueDate: duedate1,
                                lateFeeStartDate: penaltyStartdate1,
                                percentage: (parseFloat(pshedule.feesBreakUpAmount[l]) / parseFloat(plannedAmount)) * 100,
                                totalAmount: plannedAmount,
                                // plannedAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(pendingAmount) / 100,
                                plannedAmount: parseFloat(pshedule.feesBreakUpAmount[l]),
                                plannedAmountBreakup: plannedAmountBreakupinst,
                                paidAmount: paida,
                                paidAmountBreakup: paidAmountBreakupinst,
                                pendingAmount: penda,
                                pendingAmountBreakup: pendingAmountBreakupinst,
                                discountType: "",
                                discountPercentage: 0,
                                discountAmount: 0,
                                discountAmountBreakup: discountAmountBreakup,
                                status: "Planned",
                                transactionId: "",
                                campusId: stddata._doc.campusId,
                                remarks: {
                                    seller: "",
                                    finance: "",
                                    headseller: ""
                                }
                            })
                            await stuinstallplans.save();
                            allStudentsInstallmentPlans.push(stuinstallplans)
                            // feeBreakup.push({ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(tuitionFee) / 100), percent: pshedule.feesBreakUp[l] })

                        }

                    }
                    // else {
                    //     let feeBreakup = [];
                    //     let feeData = await feeManagerSchema.findOne({ feeTypeId: feestrData._doc.feeTypeIds[i], programPlanId: stddata._doc.programPlanId, campusId: stddata._doc.campusId });
                    //     let pshedule 
                    //     if (feeData == undefined || feeData == null) {
                    //         feeData = await feeManagerSchema.findOne({ feeTypeId: feestrData._doc.feeTypeIds[i], campusId: stddata._doc.campusId });
                    //         pshedule = allPaymentSchedule[0]
                    //     } else {
                    //         pshedule = await allPaymentSchedule.find(item => item._id.toString() == feeData.paymentScheduleId);
                    //     }
                    //     // let pshedule = await allPaymentSchedule.find(item => item._id.toString() == feeData.paymentScheduleId);
                    //     let month = pshedule.startMonth;
                    //     let duedate = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                    //     let penaltyStartdate = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                    //     penaltyStartdate = new Date(penaltyStartdate.setMonth((Number(penaltyStartdate.getMonth()) + 1)));
                    //     for (l = 0; l < pshedule.feesBreakUp.length; l++) {
                    //         count++
                    //         if (l > 0) {
                    //             duedate = duedate.setMonth((Number(duedate.getMonth()) + (12 / pshedule.feesBreakUp.length)));
                    //             penaltyStartdate = penaltyStartdate.setMonth((Number(penaltyStartdate.getMonth()) + (12 / pshedule.feesBreakUp.length)));
                    //         }
                    //         feeBreakup.push({ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100), percent: pshedule.feesBreakUp[l] })
                    //         let stuinstallplans = new feeInstallmentPlanModel({
                    //             feePlanId: stuFeeplan._id,
                    //             label: `Installment${
                    //                 (Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                    //                 }${Number(count)}`,
                    //             description: `Installment${
                    //                 (Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                    //                 }${Number(count)}`,
                    //             dueDate: duedate,
                    //             penaltyStartDate: penaltyStartdate,
                    //             percentage: pshedule.feesBreakUp[l],
                    //             totalAmount: feeData.feeDetails.totalAmount,
                    //             plannedAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100,
                    //             plannedAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100), percent: pshedule.feesBreakUp[l] }],
                    //             paidAmount: 0,
                    //             paidAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: 0, percent: pshedule.feesBreakUp[l] }],
                    //             pendingAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100,
                    //             pendingAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100, percent: pshedule.feesBreakUp[l] }],
                    //             discountType: "",
                    //             discountPercentage: 0,
                    //             discountAmount: 0,
                    //             discountAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: 0, percent: pshedule.feesBreakUp[l] }],
                    //             status: "",
                    //             transactionId: "",
                    //             campusId: stddata._doc.campusId,
                    //             remarks: {
                    //                 seller: "",
                    //                 finance: "",
                    //                 headseller: ""
                    //             }
                    //         })
                    //         allStudentsInstallmentPlans.push(stuinstallplans)
                    //     }
                    //     plannedAmountBreakup.push({
                    //         amount: Number(feeData.feeDetails.totalAmount),
                    //         feeTypeCode: feetyped.displayName,
                    //         title: feetyped.title,
                    //     })
                    //     plannedAmount = plannedAmount + Number(feeData.feeDetails.totalAmount);
                    //     pendingAmountBreakup.push({
                    //         amount: Number(feeData.feeDetails.totalAmount),
                    //         feeTypeCode: feetyped.displayName,
                    //         title: feetyped.title,
                    //     })
                    //     discountAmountBreakup.push({
                    //         amount: 0.00,
                    //         feeTypeCode: feetyped.displayName,
                    //         title: feetyped.title,
                    //     })
                    // }

                    // for (let j = 0; j < feeData.length; j++) {
                    //     if (feeData[j]._doc.applyDates[0] <= new Date(stddata._doc.admittedOn) && feeData[j]._doc.applyDates[1] >= new Date(stddata._doc.admittedOn) && stddata._doc.campusId[0].toString() == feeData[j]._doc.campusId[0].toString()) {
                    //         feeAmount = feeAmount + Number(feeData[j].feeDetails.totalAmount)
                    //     }
                    // }
                }
                stuFeeplan.plannedAmount = plannedAmount;
                stuFeeplan.pendingAmount = pendingAmount;
                stuFeeplan.paidAmount = paidAmount;
                stuFeeplan.feesBreakUp = pshedule.feesBreakUpAmount;
                stuFeeplan.feePlanBreakUp = stdfeeMaps[j]["Fee Structure"]
                stuFeeplan.plannedAmountBreakup = plannedAmountBreakup;
                stuFeeplan.paidAmountBreakup = paidAmountBreakup;
                stuFeeplan.pendingAmountBreakup = pendingAmountBreakup;
                stuFeeplan.discountAmountBreakup = discountAmountBreakup;
                let stuFeePlans = new feePlanModel(stuFeeplan)
                await stuFeePlans.save()
                allStudentsFeePlans.push(stuFeePlans)
                if (j + 1 == studentDetails.length) {
                    // feePlanModel.insertMany(allStudentsFeePlans, function (err, result) {
                    //     if (err) {
                    //         if (err.name === "BulkWriteError" && err.code === 11000) {
                    //             reject(res.status(200).json({
                    //                 success: true,
                    //                 message: err.toString(),
                    //                 count: 0,
                    //             }))
                    //         }
                    //     } else {
                    // feeInstallmentPlanModel.insertMany(allStudentsInstallmentPlans, function (err2, result2) {
                    //     if (err2) {
                    //         if (err2.name === "BulkWriteError" && err2.code === 11000) {
                    //             reject(res.status(200).json({
                    //                 success: true,
                    //                 message: err2.toString(),
                    //                 count: 0,
                    //             }))
                    //         }
                    //     } else {
                    console.log("s")
                    setTimeout(() => { resolve({ status: "success", message: "fee plan created successfully" }) }, 2000)
                    //     }
                    // })
                    //     }
                    // })
                }
                // await stuFeePlans.save();

            }
            else {
                let yearsplit = ppdata.academicYear.split("-")
                let year = yearsplit[0]
                let feestrData = await feeStructureModel.findOne({ _id: stddata._doc.feeStructureId, campusId: stddata._doc.campusId });
                let feeData = await allStudentMap.findOne({ studentId: stddata._doc._id })
                if (!feeData) {
                    console.log(stddata._doc.regId)
                }
                let stuFeeplan = {
                    _id: mongoose.Types.ObjectId(),
                    applicationId: `FPLAN_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                        }${Number(j) + 1}`,
                    studentRegId: ppInputData["Reg ID *"] == "-" ? ppInputData["HEDA ID"] : ppInputData["Reg ID *"],
                    studentId: stddata._id,
                    programPlanHEDAId: ppInputData["Program Plan ID"],
                    plannedAmount: 0,
                    plannedAmountBreakup: [],
                    paidAmount: 0,
                    paidAmountBreakup: [],
                    pendingAmount: 0,
                    pendingAmountBreakup: [],
                    currency: stddata._doc.currency,
                    forex: stddata._doc.FOREX,
                    discountType: "",
                    discountPercentage: 0,
                    discountAmount: 0,
                    dueDate: "",
                    penaltyStartDate: "",
                    percentageBreakup: [],
                    installmentPlan: {},
                    discountAmountBreakup: [],
                    campusId: stddata._doc.campusId,
                    concessionPercentage: 0,
                    concessionAmount: 0,
                    remarks: {
                        seller: "",
                        finance: "",
                        headseller: ""
                    }
                }
                let stdFeeInPlan = {
                    feePlanId: stuFeeplan._id,
                    label: "",
                    description: "",
                    dueDate: "",
                    lateFeeStartDate: "",
                    percentage: "",
                    totalAmount: 0,
                    plannedAmount: 0,
                    plannedAmountBreakup: [],
                    paidAmount: 0,
                    paidAmountBreakup: [],
                    pendingAmount: 0,
                    pendingAmountBreakup: [],
                    discountType: "",
                    discountPercentage: 0,
                    discountAmount: 0,
                    discountAmountBreakup: [],
                    status: "",
                    transactionId: "",
                    campusId: stddata._doc.campusId,
                    remarks: {
                        seller: "",
                        finance: "",
                        headseller: ""
                    }
                }
                let feeAmount = 0;
                let plannedAmount = 0;
                let pendingAmount = 0;
                let paidAmount = 0;
                let plannedAmountBreakup = [];
                let paidAmountBreakup = [];
                let pendingAmountBreakup = [];
                let discountAmountBreakup = [];
                let instplannedAmountBreakup = [];
                let instpaidAmountBreakup = [];
                let instpendingAmountBreakup = [];

                for (let i = 0; i < feestrData._doc.feeTypeIds.length; i++) {
                    let feetyped = feeTypeDetails.find(item => item._id.toString() == feestrData._doc.feeTypeIds[i])
                    let feeBreakup = [];
                    // let feeData = await feeManagerSchema.findOne({ feeTypeId: feestrData._doc.feeTypeIds[i], programPlanId: stddata._doc.programPlanId, "applyDates.0": { $lte: new Date(stddata._doc.admittedOn) }, "applyDates.1": { $gte: new Date(stddata._doc.admittedOn) }, campusId: stddata._doc.campusId });
                    // if (!feeData || feeData == null) {
                    //     feeData = await feeManagerSchema.findOne({ feeTypeId: feestrData._doc.feeTypeIds[i], campusId: stddata._doc.campusId });
                    // }
                    // let feeData = await stdfeeMaps.find(item => item["USN"].toString() == regId.toString())
                    let pshedule = allPaymentSchedule[0]
                    // if (feeData == undefined || feeData == null) {
                    // pshedule = allPaymentSchedule[0]
                    // } else {
                    //     pshedule = await allPaymentSchedule.find(item => item._id.toString() == feeData.paymentScheduleId);
                    // }
                    let month = monthdigits[pshedule.startMonth];
                    let monthss = ["June", "November", "September", "November"]
                    let duedate = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                    let penaltyStartdate = new Date(`${year}-${month}-${pshedule.scheduleDetails.penaltyStartDate}`)
                    // penaltyStartdate = new Date(penaltyStartdate.setMonth((Number(penaltyStartdate.getMonth()) + 1)));
                    // let tuitionFee = Number(feeData.feeDetails.totalAmount);
                    if (feetyped.title.toLowerCase().includes("tuition") || feetyped.title.includes("Term")) {
                        plannedAmountBreakup.push({
                            amount: Number(feeData["amount"]),
                            feeTypeCode: feetyped.displayName,
                            title: feetyped.title,
                        })
                        plannedAmount = plannedAmount + Number(feeData["amount"]);
                        pendingAmount = pendingAmount + Number(feeData["pending"]);
                        paidAmount = paidAmount + Number(feeData["paid"]);
                        pendingAmountBreakup.push({
                            amount: Number(feeData["pending"]),
                            feeTypeCode: feetyped.displayName,
                            title: feetyped.title,
                        })
                        paidAmountBreakup.push({
                            amount: Number(feeData["paid"]),
                            feeTypeCode: feetyped.displayName,
                            title: feetyped.title,
                        })
                    }
                    else {
                        plannedAmountBreakup.push({
                            amount: 0.00,
                            feeTypeCode: feetyped.displayName,
                            title: feetyped.title,
                        })
                        plannedAmount = plannedAmount + 0;
                        pendingAmount = pendingAmount + 0;
                        paidAmount = paidAmount + 0;
                        pendingAmountBreakup.push({
                            amount: 0.00,
                            feeTypeCode: feetyped.displayName,
                            title: feetyped.title,
                        })
                        paidAmountBreakup.push({
                            amount: 0.00,
                            feeTypeCode: feetyped.displayName,
                            title: feetyped.title,
                        })
                    }
                    discountAmountBreakup.push({
                        amount: 0.00,
                        feeTypeCode: feetyped.displayName,
                        title: feetyped.title,
                    })
                    let temppaid = 0
                    let temppend = 0
                    if (i + 1 == feestrData._doc.feeTypeIds.length) {
                        for (l = 0; l < pshedule.feesBreakUp.length; l++) {
                            count = l + 1
                            let duedate1
                            let penaltyStartdate1
                            let plannedAmountBreakupinst = [];
                            let pendingAmountBreakupinst = [];
                            let paidAmountBreakupinst = [];
                            let title
                            if (pshedule.feesBreakUp.length == 4) {
                                title = `Term ${count < 3 ? 1 : 2} Fees`
                                duedate1 = duedates[l]
                                penaltyStartdate1 = latefeesdates[l]
                            }
                            else if (pshedule.feesBreakUp.length == 2) {
                                title = `Term ${count < 2 ? 1 : 2} Fees`
                                duedate1 = duedates2[l]
                                penaltyStartdate1 = latefeesdates2[l]
                            }
                            else {
                                title = null
                                duedate1 = duedates2[l]
                                penaltyStartdate1 = latefeesdates2[l]
                            }
                            let paida
                            let penda
                            if (parseFloat(paidAmount) < (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmount) / 100) && l == 0) {
                                paida = parseFloat(paidAmount)
                                penda = (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmount) / 100) - (parseFloat(paidAmount))
                            }
                            else if (parseFloat(paidAmount) > (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmount) / 100) && parseFloat(paidAmount) !== parseFloat(plannedAmount) && l == 0) {
                                paida = parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmount) / 100
                                penda = 0

                            } else if (parseFloat(paidAmount) == (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmount) / 100) && l == 0) {
                                paida = parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmount) / 100
                                penda = 0
                            }
                            else if (parseFloat(paidAmount) < (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmount) / 100) && l > 0) {
                                paida = 0
                                penda = parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmount) / 100
                            }
                            else if (parseFloat(paidAmount) > (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmount) / 100) && parseFloat(paidAmount) !== parseFloat(plannedAmount) && l > 0) {
                                penda = parseFloat(plannedAmount) - parseFloat(paidAmount)
                                paida = (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmount) / 100) - parseFloat(penda)

                            } else if (parseFloat(paidAmount) == (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmount) / 100) && l > 0) {
                                paida = 0
                                penda = parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmount) / 100
                            }
                            else {
                                paida = paidAmount
                                penda = parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmount) / 100
                            }

                            for (n = 0; n < plannedAmountBreakup.length; n++) {
                                plannedAmountBreakupinst.push({
                                    amount: n == 0 ? parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmount) / 100 : 0,
                                    feeTypeCode: plannedAmountBreakup[n].feeTypeCode,
                                    // title: `Term ${count<3 ? 1: 2} Fees`,
                                    title: title == null ? plannedAmountBreakup[n].title : title,
                                })
                                pendingAmountBreakupinst.push({
                                    amount: n == 0 ? penda : 0,
                                    feeTypeCode: pendingAmountBreakup[n].feeTypeCode,
                                    // title: `Term ${count<3 ? 1: 2} Fees`,
                                    title: title == null ? plannedAmountBreakup[n].title : title,
                                })
                                paidAmountBreakupinst.push({
                                    amount: n == 0 ? paida : 0,
                                    feeTypeCode: paidAmountBreakup[n].feeTypeCode,
                                    // title: `Term ${count<3 ? 1: 2} Fees`,
                                    title: title == null ? plannedAmountBreakup[n].title : title,
                                })

                            }
                            dcount++
                            let stuinstallplans = new feeInstallmentPlanModel({
                                feePlanId: stuFeeplan._id,
                                label: `Installment${(Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                                    }${Number(count)}`,
                                displayName: `INST_${finYear}_${(Number(dcount) + 1).toString().length == 1 ? "00" : (Number(dcount) + 1).toString().length == 2 ? "0" : ""
                                    }${Number(dcount) + 1}`,
                                description: `Installment${(Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                                    }${Number(count)}`,
                                dueDate: duedate1,
                                lateFeeStartDate: penaltyStartdate1,
                                percentage: pshedule.feesBreakUp[l],
                                totalAmount: plannedAmount,
                                term: l + 1,
                                academicYear: ppdata.academicYear,
                                year: 1,
                                // plannedAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(pendingAmount) / 100,
                                plannedAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmount) / 100,
                                plannedAmountBreakup: plannedAmountBreakupinst,
                                paidAmount: paida,
                                paidAmountBreakup: paidAmountBreakupinst,
                                pendingAmount: penda,
                                pendingAmountBreakup: pendingAmountBreakupinst,
                                discountType: "",
                                discountPercentage: 0,
                                discountAmount: 0,
                                discountAmountBreakup: discountAmountBreakup,
                                status: "Planned",
                                transactionId: "",
                                campusId: stddata._doc.campusId,
                                remarks: {
                                    seller: "",
                                    finance: "",
                                    headseller: ""
                                }
                            })
                            await stuinstallplans.save();
                            allStudentsInstallmentPlans.push(stuinstallplans)
                            // feeBreakup.push({ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(tuitionFee) / 100), percent: pshedule.feesBreakUp[l] })

                        }

                    }
                    // else {
                    //     let feeBreakup = [];
                    //     let feeData = await feeManagerSchema.findOne({ feeTypeId: feestrData._doc.feeTypeIds[i], programPlanId: stddata._doc.programPlanId, campusId: stddata._doc.campusId });
                    //     let pshedule 
                    //     if (feeData == undefined || feeData == null) {
                    //         feeData = await feeManagerSchema.findOne({ feeTypeId: feestrData._doc.feeTypeIds[i], campusId: stddata._doc.campusId });
                    //         pshedule = allPaymentSchedule[0]
                    //     } else {
                    //         pshedule = await allPaymentSchedule.find(item => item._id.toString() == feeData.paymentScheduleId);
                    //     }
                    //     // let pshedule = await allPaymentSchedule.find(item => item._id.toString() == feeData.paymentScheduleId);
                    //     let month = pshedule.startMonth;
                    //     let duedate = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                    //     let penaltyStartdate = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                    //     penaltyStartdate = new Date(penaltyStartdate.setMonth((Number(penaltyStartdate.getMonth()) + 1)));
                    //     for (l = 0; l < pshedule.feesBreakUp.length; l++) {
                    //         count++
                    //         if (l > 0) {
                    //             duedate = duedate.setMonth((Number(duedate.getMonth()) + (12 / pshedule.feesBreakUp.length)));
                    //             penaltyStartdate = penaltyStartdate.setMonth((Number(penaltyStartdate.getMonth()) + (12 / pshedule.feesBreakUp.length)));
                    //         }
                    //         feeBreakup.push({ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100), percent: pshedule.feesBreakUp[l] })
                    //         let stuinstallplans = new feeInstallmentPlanModel({
                    //             feePlanId: stuFeeplan._id,
                    //             label: `Installment${
                    //                 (Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                    //                 }${Number(count)}`,
                    //             description: `Installment${
                    //                 (Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                    //                 }${Number(count)}`,
                    //             dueDate: duedate,
                    //             penaltyStartDate: penaltyStartdate,
                    //             percentage: pshedule.feesBreakUp[l],
                    //             totalAmount: feeData.feeDetails.totalAmount,
                    //             plannedAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100,
                    //             plannedAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100), percent: pshedule.feesBreakUp[l] }],
                    //             paidAmount: 0,
                    //             paidAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: 0, percent: pshedule.feesBreakUp[l] }],
                    //             pendingAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100,
                    //             pendingAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100, percent: pshedule.feesBreakUp[l] }],
                    //             discountType: "",
                    //             discountPercentage: 0,
                    //             discountAmount: 0,
                    //             discountAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: 0, percent: pshedule.feesBreakUp[l] }],
                    //             status: "",
                    //             transactionId: "",
                    //             campusId: stddata._doc.campusId,
                    //             remarks: {
                    //                 seller: "",
                    //                 finance: "",
                    //                 headseller: ""
                    //             }
                    //         })
                    //         allStudentsInstallmentPlans.push(stuinstallplans)
                    //     }
                    //     plannedAmountBreakup.push({
                    //         amount: Number(feeData.feeDetails.totalAmount),
                    //         feeTypeCode: feetyped.displayName,
                    //         title: feetyped.title,
                    //     })
                    //     plannedAmount = plannedAmount + Number(feeData.feeDetails.totalAmount);
                    //     pendingAmountBreakup.push({
                    //         amount: Number(feeData.feeDetails.totalAmount),
                    //         feeTypeCode: feetyped.displayName,
                    //         title: feetyped.title,
                    //     })
                    //     discountAmountBreakup.push({
                    //         amount: 0.00,
                    //         feeTypeCode: feetyped.displayName,
                    //         title: feetyped.title,
                    //     })
                    // }

                    // for (let j = 0; j < feeData.length; j++) {
                    //     if (feeData[j]._doc.applyDates[0] <= new Date(stddata._doc.admittedOn) && feeData[j]._doc.applyDates[1] >= new Date(stddata._doc.admittedOn) && stddata._doc.campusId[0].toString() == feeData[j]._doc.campusId[0].toString()) {
                    //         feeAmount = feeAmount + Number(feeData[j].feeDetails.totalAmount)
                    //     }
                    // }
                }
                stuFeeplan.plannedAmount = plannedAmount;
                stuFeeplan.pendingAmount = pendingAmount;
                stuFeeplan.paidAmount = paidAmount;
                stuFeeplan.plannedAmountBreakup = plannedAmountBreakup;
                stuFeeplan.paidAmountBreakup = paidAmountBreakup;
                stuFeeplan.pendingAmountBreakup = pendingAmountBreakup;
                stuFeeplan.discountAmountBreakup = discountAmountBreakup;
                stuFeeplan.academicYear = ppdata.academicYear;
                let stuFeePlans = new feePlanModel(stuFeeplan)
                await stuFeePlans.save()
                allStudentsFeePlans.push(stuFeePlans)
                if (j + 1 == studentDetails.length) {
                    // feePlanModel.insertMany(allStudentsFeePlans, function (err, result) {
                    //     if (err) {
                    //         if (err.name === "BulkWriteError" && err.code === 11000) {
                    //             reject(res.status(200).json({
                    //                 success: true,
                    //                 message: err.toString(),
                    //                 count: 0,
                    //             }))
                    //         }
                    //     } else {
                    // feeInstallmentPlanModel.insertMany(allStudentsInstallmentPlans, function (err2, result2) {
                    //     if (err2) {
                    //         if (err2.name === "BulkWriteError" && err2.code === 11000) {
                    //             reject(res.status(200).json({
                    //                 success: true,
                    //                 message: err2.toString(),
                    //                 count: 0,
                    //             }))
                    //         }
                    //     } else {
                    console.log("s")
                    setTimeout(() => { resolve({ status: "success", message: "fee plan created successfully" }) }, 2000)
                    //     }
                    // })
                    //     }
                    // })
                }
                // await stuFeePlans.save();

            }
        }
    })
}

async function createFeePlansAc(req, dbConnectionp, studentDetails, feeStructureModel, studentModel, stdfeeMaps, campuses, allProgramPlan, allPaymentSchedule, finYear, allStudentMap) {
    return new Promise(async function (resolve, reject) {
        let daydigits = { "First": "01", "Second": "02", "Third": "03", "Fourth": "04", "Fifth": "05", "Tenth": "10", "Fifteenth": "15", "Last": "28", "Second Last": "27", "Third last": "26", "Fourth last": "25" };
        let monthdigits = { "January": "01", "February": "02", "March": "03", "April": "04", "May": "05", "June": "06", "July": "07", "August": "08", "September": "09", "October": "10", "November": "11", "December": "12" };
        let digitsmonth = { "01": "January", "02": "February", "03": "March", "04": "April", "05": "May", "06": "June", "07": "July", "08": "August", "09": "September", "10": "October", "11": "November", "12": "December" };

        let InstfeePlans = {
            "Fee Plan 1": [{
                amount: 155000
            }],
            "Fee Plan 2": [{
                amount: 155000
            }],
            "Fee Plan 3": [{
                amount: 155000
            }],
            "Fee Plan 4": [{
                amount: 155000
            }],
            "Fee Plan 5": [{
                amount: 155000
            }],
            "Fee Plan 6": [{
                amount: 155000
            }],
            "Fee Plan 7": [{
                amount: 155000
            }],
            "Fee Plan 8": [{
                amount: 155000
            }],
            "Fee Plan 9": [{
                amount: 155000
            }],
            "Fee Plan 10": [{
                amount: 155000
            }],
            "Fee Plan 11": [{
                amount: 155000
            }],
            "Fee Plan 12": [{
                amount: 155000
            }],
            "Fee Plan 13": [{
                amount: 155000
            }],
            "Fee Plan 14": [{
                amount: 155000
            }],
            "Fee Plan 15": [{
                amount: 155000
            }],
            "Fee Plan 16": [{
                amount: 155000
            }],
            "Fee Plan 17": [{
                amount: 155000
            }],
            "Fee Plan 18": [{
                amount: 155000
            }],
            "Fee Plan 19": [{
                amount: 155000
            }],
        }
        // let student = req.query.id:
        // let dbConnectionp = await createDatabase(req.headers.orgId, req.headers.resource);
        // let feeStructureModel = dbConnectionp.model("feestructures", FeeStructureSchema);
        // let studentModel = dbConnectionp.model("students", StudentSchema);
        // let feeManagerSchema = dbConnectionp.model("feemanagers", FeeManagerSchema);
        // let campid1 = await campuses.find(item => item._id.toString() == studentData._doc["campusId"].toString())
        // let campid = campid1["_id"];
        let feeplanacademicschema = mongoose.Schema({}, { strict: false });
        let paymentScheduleModel = dbConnectionp.model("paymentschedules", paymentScheduleSchema);
        let feePlanModel = dbConnectionp.model("studentfeeplans", feeplanschema);
        let feePlanModelbyAcademic = dbConnectionp.model("feeplansbyacademic", feeplanacademicschema);

        let feeInstallmentPlanModel = dbConnectionp.model("studentfeeinstallmentplans", feeplanInstallmentschema);
        let allStudentMap = dbConnectionp.model("studentfeesmaps", StudentFeeMapSchema);
        // let allStudentMap = dbConnectionp.model("studentfeesmaps", StudentFeeMapSchema);
        let programPlanModel = dbConnectionp.model("programplans", ProgramPlanSchema);
        let feeTypeDetails = dbConnectionp.model("feeTypes", FeeTypeSchema);

        let allPaymentSchedule = await paymentScheduleModel.find({})
        var allStudentsFeePlans = [];
        var allStudentsInstallmentPlans = []
        let count = 0;
        let dcount = 0;
        let fpacount = 0;
        let duedates = [new Date(`2021-April-10`), new Date(`2021-June-10`), new Date(`2021-September-10`), new Date(`2021-November-10`)];
        let latefeesdates = [new Date(`2021-April-30`), new Date(`2021-June-30`), new Date(`2021-September-30`), new Date(`2021-November-30`)];
        let duedates2 = [new Date(`2021-June-01`), new Date(`2021-October-01`)];
        let latefeesdates2 = [new Date(`2021-November-30`), new Date(`2022-May-30`)];
        await feeInstallmentPlanModel.updateMany({}, { $set: { academicYear: "2020-21" } })
        await feePlanModel.updateMany({}, { $set: { academicYear: "2021-22" } })
        for (let j = 0; j < studentDetails.length; j++) {
            // for (let j = 2608; j < studentDetails.length; j++) {
            console.log(j, studentDetails[j]._doc.regId)
            let feePlanBreakUp = [];
            let ppInputData = studentDetails[j]._doc
            // regId = ppInputData["Reg ID *"] == "-" ? ppInputData["HEDA ID"] : ppInputData["Reg ID *"]
            // let regId = ppInputData["regId"]
            let stddata = studentDetails[j]
            let stuFeeplandata = await feePlanModel.findOne({ studentRegId: stddata._doc.regId });
            let stuFeeplan
            let ppdata = await programPlanModel.findOne({ _id: stddata._doc.programPlanId });
            let cudate = new Date();
            let yearsplit = ppdata.academicYear.split("-")
            let year = yearsplit[0]
            // console.log(cudate.getFullYear()-Number(year))
            // console.log(dfs)
            let cayear = cudate.getFullYear() - Number(year)
            await feeInstallmentPlanModel.updateMany({ feePlanId: stuFeeplandata._doc._id }, { $set: { year: cayear } })
            // cayear
            let feeData = await allStudentMap.findOne({ studentId: stddata._doc._id });
            let feemapdataa = await stdfeeMaps.find(item => item["USN"] == stddata._doc.regId)
            if (!feeData) {
                console.log(stddata._doc.regId)
            }
            let academicperiod = 0
            const conditions = ["$", "USD", "usd"];
            let feePlanacademicyearwise = false
            for (let q = 0; q < 4; q++) {
                // console.log(feemapdataa[`Tuition Amount ${q + 1}`])
                if (feemapdataa && feemapdataa[`Tuition Amount ${q + 1}`] && feemapdataa[`Tuition Amount ${q + 1}`].toString() !== "-") {
                    feePlanacademicyearwise = true
                    academicperiod = academicperiod + 1
                }
            }
            // if (feePlanacademicyearwise && Number(academicperiod) >= 1) {
            let feestrData = await feeStructureModel.findOne({ _id: stddata._doc.feeStructureId, campusId: stddata._doc.campusId });
            // console.log(academicperiod)
            // console.log(stddata._doc.regId, stuFeeplandata)
            if (stuFeeplandata && feemapdataa) {
                stuFeeplan = stuFeeplandata._doc;
                // let stuFeeplan = {
                //     _id: mongoose.Types.ObjectId(),
                //     applicationId: `FPLAN_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                //         }${Number(j) + 1}`,
                //     studentRegId: ppInputData["Reg ID *"] == "-" ? ppInputData["HEDA ID"] : ppInputData["Reg ID *"],
                //     studentId: stddata._id,
                //     programPlanHEDAId: ppdata._doc["hedaId"],
                //     plannedAmount: 0,
                //     plannedAmountBreakup: [],
                //     paidAmount: 0,
                //     paidAmountBreakup: [],
                //     pendingAmount: 0,
                //     pendingAmountBreakup: [],
                //     currency: stddata._doc.currency,
                //     forex: stddata._doc.FOREX,
                //     discountType: "",
                //     discountPercentage: 0,
                //     discountAmount: 0,
                //     dueDate: "",
                //     penaltyStartDate: "",
                //     percentageBreakup: [],
                //     installmentPlan: {},
                //     discountAmountBreakup: [],
                //     campusId: stddata._doc.campusId,
                //     concessionPercentage: 0,
                //     concessionAmount: 0,
                //     remarks: {
                //         seller: "",
                //         finance: "",
                //         headseller: ""
                //     }
                // }
                fpacount++
                let existingfeeplanacademic = {
                    _id: mongoose.Types.ObjectId(),
                    feePlanId: stuFeeplan._id,
                    applicationId: `FPLAN_${finYear}_${(Number(fpacount) + 1).toString().length == 1 ? "00" : (Number(fpacount) + 1).toString().length == 2 ? "0" : ""
                        }${Number(fpacount) + 1}`,
                    studentRegId: stddata._doc.regId,
                    studentId: stddata._id,
                    academicYear: `2021-22`,
                    programPlanHEDAId: ppdata._doc["hedaId"],
                    plannedAmount: stuFeeplandata._doc.plannedAmount,
                    plannedAmountBreakup: stuFeeplandata._doc.plannedAmountBreakup,
                    paidAmount: stuFeeplandata._doc.paidAmount,
                    paidAmountBreakup: stuFeeplandata._doc.paidAmountBreakup,
                    pendingAmount: stuFeeplandata._doc.pendingAmount,
                    pendingAmountBreakup: stuFeeplandata._doc.pendingAmountBreakup,
                    currency: stddata._doc.currency,
                    forex: stddata._doc.FOREX,
                    discountType: stuFeeplandata._doc.discountType,
                    discountPercentage: 0,
                    discountAmount: 0,
                    dueDate: stuFeeplandata._doc.dueDate,
                    penaltyStartDate: stuFeeplandata._doc.penaltyStartDate,
                    percentageBreakup: [],
                    installmentPlan: {},
                    discountAmountBreakup: stuFeeplandata._doc.discountAmountBreakup,
                    campusId: stddata._doc.campusId,
                    concessionPercentage: 0,
                    concessionAmount: 0,
                    remarks: {
                        seller: "",
                        finance: "",
                        headseller: ""
                    }
                }
                let splitacademicyear = ppdata.academicYear.split("-");
                let plannedAmount = 0;
                let pendingAmount = 0;
                let paidAmount = 0;
                let plannedAmountBreakuptmp = [];
                let paidAmountBreakuptmp = [];
                let pendingAmountBreakuptmp = [];
                let discountAmountBreakuptmp = [];
                let paidAmountBreakup = [];
                let pendingAmountBreakup = [];
                let discountAmountBreakup = [];
                let instplannedAmountBreakup = [];
                let instpaidAmountBreakup = [];
                let instpendingAmountBreakup = [];
                let feeAmount = 0;
                let totalPlannedAmount1 = 0
                let totalPendingAmount1 = 0
                let totalPaidAmount1 = 0
                let totalTuitionamt = 0
                let totalMisamt = 0
                let plannedAmountBreakup2 = stuFeeplan.plannedAmountBreakup;
                let paidAmountBreakup2 = stuFeeplan.paidAmountBreakup;
                let pendingAmountBreakup2 = stuFeeplan.pendingAmountBreakup;
                let discountAmountBreakup2 = stuFeeplan.discountAmountBreakup;
                let tuitionAmount = []
                let miscAmount = []
                // console.log("feemapdataa",feemapdataa)
                let currentAcademicYear = "2020-21"
                let acdemicyearcount1 = 2020
                let acdemicyearcount2 = 21
                let instcount = 1
                let newexistingfeeplanacademic = new feePlanModelbyAcademic(existingfeeplanacademic)
                await newexistingfeeplanacademic.save();
                for (let k = 0; k < Number(academicperiod); k++) {
                    let ttuitionamt = feemapdataa[`Tuition Amount ${k + 1}`].replace(",", "");
                    let ootheramt = !feemapdataa[`Other Amount ${k + 1}`].includes("-") ? feemapdataa[`Other Amount ${k + 1}`].replace(",", "") : "0"
                    if (conditions.some(el => feemapdataa[`Tuition Amount ${k + 1}`].includes(el))) {
                        console.log(feemapdataa[`Tuition Amount ${k + 1}`])
                        ttuitionamt = ttuitionamt.replace("$", "")
                        ttuitionamt = ttuitionamt.replace("USD", "")
                        ttuitionamt = ttuitionamt.replace("usd", "")
                        ttuitionamt = Number(ttuitionamt) * 70
                    }
                    if (conditions.some(el => feemapdataa[`Other Amount ${k + 1}`].includes(el))) {
                        ootheramt = ootheramt.replace("$", "")
                        ootheramt = ootheramt.replace("USD", "")
                        ootheramt = ootheramt.replace("usd", "")
                        ootheramt = Number(ootheramt) * 70
                    }
                    ttuitionamt = Number(ttuitionamt)
                    ootheramt = Number(ootheramt)
                    plannedAmountBreakuptmp.push({
                        total: ttuitionamt + ootheramt,
                        academicYear: `${Number(splitacademicyear[0]) + k}-${Number(splitacademicyear[1]) + k}`,
                        amount: ttuitionamt + ootheramt,
                        // feeTypeCode: feetyped.displayName,
                        // title: feetyped.title,
                        breakUp: []
                    })
                    // pendingAmountBreakup.push({
                    //     total: Number(feemapdataa[`Tuition Amount ${k + 1}`].replace(",", "")) + Number(feemapdataa[`Other Amount ${k + 1}`].replace(",", "")),
                    //     academicYear: `${Number(splitacademicyear[0]) + k}-${Number(splitacademicyear[1]) + k}`,
                    //     amount: Number(feemapdataa[`Tuition Amount ${k + 1}`].replace(",", "")) + Number(feemapdataa[`Other Amount ${k + 1}`].replace(",", "")),
                    //     // feeTypeCode: feetyped.displayName,
                    //     // title: feetyped.title,
                    //     // breakUp: []
                    // })
                    // paidAmountBreakup.push({
                    //     total: 0,
                    //     academicYear: `${Number(splitacademicyear[0]) + k}-${Number(splitacademicyear[1]) + k}`,
                    //     amount: 0,
                    //     // feeTypeCode: feetyped.displayName,
                    //     // title: feetyped.title,
                    //     breakUp: []
                    // })
                    // discountAmountBreakup.push({
                    //     total: 0,
                    //     academicYear: `${Number(splitacademicyear[0]) + k}-${Number(splitacademicyear[1]) + k}`,
                    //     amount: 0,
                    //     // feeTypeCode: feetyped.displayName,
                    //     // title: feetyped.title,
                    //     breakUp: []
                    // })
                    feePlanBreakUp.push({
                        academicYear: `${Number(splitacademicyear[0]) + k}-${Number(splitacademicyear[1]) + k}`,
                        amount: ttuitionamt,
                        feeTypeCode: "FT001",
                        title: "Tuition Fee"
                    })
                    // if (!isNaN(Number(feemapdataa[`Other Amount ${k + 1}`].replace(",", "")))) {

                    feePlanBreakUp.push({
                        academicYear: `${Number(splitacademicyear[0]) + k}-${Number(splitacademicyear[1]) + k}`,
                        amount: ootheramt,
                        feeTypeCode: "FT017",
                        title: "Miscellaneous Fee"
                    })
                    // }
                    totalTuitionamt = totalTuitionamt + ttuitionamt
                    totalMisamt = totalMisamt + ootheramt
                    totalPlannedAmount1 = totalPlannedAmount1 + Number(ttuitionamt) + Number(ootheramt)
                    if (isNaN(totalPlannedAmount1)) {
                        console.log(stddata._doc.regId)
                        console.log(dsfdsfss)
                    }
                    totalPendingAmount1 = totalPendingAmount1 + ttuitionamt + ootheramt
                    totalPaidAmount1 = totalPaidAmount1
                    tuitionAmount.push(ttuitionamt)
                    miscAmount.push(ootheramt)
                }
                count = 0
                for (m = 0; m < plannedAmountBreakuptmp.length; m++) {
                    acdemicyearcount1++
                    acdemicyearcount2++
                    instcount++
                    fpacount++
                    let plannedAmountBreakup = [];
                    // console.log(stuFeeplan)
                    if (!feemapdataa[`Tuition Amount ${m + 1}`].includes("-")) {
                        let tuitionamt = feemapdataa[`Tuition Amount ${m + 1}`].replace(",", "")
                        let otheramt = !feemapdataa[`Other Amount ${m + 1}`].includes("-") ? feemapdataa[`Other Amount ${m + 1}`].replace(",", "") : "0"
                        if (conditions.some(el => feemapdataa[`Tuition Amount ${m + 1}`].includes(el))) {
                            tuitionamt = tuitionamt.replace("$", "")
                            tuitionamt = tuitionamt.replace("USD", "")
                            tuitionamt = tuitionamt.replace("usd", "")
                            tuitionamt = Number(tuitionamt) * 70
                        }
                        if (conditions.some(el => feemapdataa[`Other Amount ${m + 1}`].includes(el))) {
                            otheramt = tuitionamt.replace("$", "")
                            otheramt = tuitionamt.replace("USD", "")
                            otheramt = tuitionamt.replace("usd", "")
                            otheramt = Number(otheramt) * 70
                        }
                        tuitionamt = Number(tuitionamt)
                        otheramt = Number(otheramt)
                        let totalamount = Number(tuitionamt) * Number(otheramt)
                        let stuFeeplanbyacademic = {
                            _id: mongoose.Types.ObjectId(),
                            feePlanId: stuFeeplan._id,
                            applicationId: `FPLAN_${finYear}_${(Number(fpacount) + 1).toString().length == 1 ? "00" : (Number(fpacount) + 1).toString().length == 2 ? "0" : ""
                                }${Number(fpacount) + 1}`,
                            studentRegId: stddata._doc.regId,
                            studentId: stddata._id,
                            academicYear: `${acdemicyearcount1}-${acdemicyearcount2}`,
                            programPlanHEDAId: ppdata._doc["hedaId"],
                            plannedAmount: 0,
                            plannedAmountBreakup: [],
                            paidAmount: 0,
                            paidAmountBreakup: [],
                            pendingAmount: 0,
                            pendingAmountBreakup: [],
                            currency: stddata._doc.currency,
                            forex: stddata._doc.FOREX,
                            discountType: "",
                            discountPercentage: 0,
                            discountAmount: 0,
                            dueDate: "",
                            penaltyStartDate: "",
                            percentageBreakup: [],
                            installmentPlan: {},
                            discountAmountBreakup: [],
                            campusId: stddata._doc.campusId,
                            concessionPercentage: 0,
                            concessionAmount: 0,
                            remarks: {
                                seller: "",
                                finance: "",
                                headseller: ""
                            }
                        }
                        let plannedAmountBreakupacademic = [];
                        let pendingAmountBreakupacademic = [];
                        let discountAmountBreakupacademic = [];
                        let paidAmountBreakupacademic = [];
                        stuFeeplanbyacademic.plannedAmount = plannedAmountBreakuptmp[m].total;
                        stuFeeplanbyacademic.pendingAmount = plannedAmountBreakuptmp[m].total;
                        stuFeeplanbyacademic.paidAmount = 0;
                        // stuFeeplanbyacademic.academicYear = plannedAmountBreakuptmp[m].academicYear;

                        let plannedAmount2 = feePlanBreakUp[m]["amount"]
                        for (let i = 0; i < feestrData._doc.feeTypeIds.length; i++) {
                            let feetyped = await feeTypeDetails.findOne({ _id: mongoose.Types.ObjectId(feestrData._doc.feeTypeIds[i]) })
                            // console.log(feetyped)
                            feetyped = feetyped._doc
                            let feeBreakup = [];
                            let pshedule = allPaymentSchedule[0]._doc
                            let month = monthdigits[pshedule.startMonth];
                            let monthss = ["June", "November", "September", "November"]
                            let duedate = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                            let penaltyStartdate = new Date(`${year}-${month}-${pshedule.scheduleDetails.penaltyStartDate}`)
                            plannedAmount = Number(plannedAmountBreakuptmp[m].total);
                            pendingAmount = Number(plannedAmountBreakuptmp[m].total);
                            paidAmount = 0
                            if (feetyped.title.toLowerCase().includes("tuition") || feetyped.title.includes("Term") || feetyped.title.includes("Miscellaneous")) {
                                plannedAmountBreakupacademic.push({
                                    amount: feetyped.title.toLowerCase().includes("tuition") ? tuitionamt : feetyped.title.includes("Miscellaneous") ? otheramt : 0,
                                    feeTypeCode: feetyped.displayName,
                                    title: feetyped.title
                                })
                                pendingAmountBreakupacademic.push({
                                    amount: feetyped.title.toLowerCase().includes("tuition") ? tuitionamt : feetyped.title.includes("Miscellaneous") ? otheramt : 0,
                                    feeTypeCode: feetyped.displayName,
                                    title: feetyped.title
                                })
                                discountAmountBreakupacademic.push({
                                    amount: 0,
                                    feeTypeCode: feetyped.displayName,
                                    title: feetyped.title
                                })
                                paidAmountBreakupacademic.push({
                                    amount: 0,
                                    feeTypeCode: feetyped.displayName,
                                    title: feetyped.title
                                })
                                // if (m + 1 == plannedAmountBreakuptmp.length) {
                                // for(let g; g < plannedAmountBreakup2.length; g++){
                                //     plannedAmountBreakup2[g].amount = plannedAmountBreakup2[g].title.toLowerCase().includes("tuition") ? Number(plannedAmountBreakup2[g].amount) + totalTuitionamt : plannedAmountBreakup2[g].title.includes("Miscellaneous") ? Number(plannedAmountBreakup2[g].amount) + totalMisamt : plannedAmountBreakup2[g].amount
                                // }
                                // for(let g; g < pendingAmountBreakup2.length; g++){
                                //     pendingAmountBreakup2[g].amount = pendingAmountBreakup2[g].title.toLowerCase().includes("tuition") ? Number(pendingAmountBreakup2[g].amount) + totalTuitionamt : pendingAmountBreakup2[g].title.includes("Miscellaneous") ? Number(pendingAmountBreakup2[g].amount) + totalMisamt : pendingAmountBreakup2[g].amount  
                                // }
                                plannedAmountBreakup.push({
                                    amount: feetyped.title.toLowerCase().includes("tuition") ? tuitionamt : feetyped.title.includes("Miscellaneous") ? otheramt : 0,
                                    feeTypeCode: feetyped.displayName,
                                    title: feetyped.title
                                })
                                pendingAmountBreakup.push({
                                    amount: feetyped.title.toLowerCase().includes("tuition") ? tuitionamt : feetyped.title.includes("Miscellaneous") ? otheramt : 0,
                                    feeTypeCode: feetyped.displayName,
                                    title: feetyped.title,
                                })
                                paidAmountBreakup.push({
                                    amount: 0,
                                    feeTypeCode: feetyped.displayName,
                                    title: feetyped.title,
                                })
                                discountAmountBreakup.push({
                                    amount: 0.00,
                                    feeTypeCode: feetyped.displayName,
                                    title: feetyped.title,
                                })
                                // }
                            }
                            else {
                                plannedAmountBreakupacademic.push({
                                    amount: 0,
                                    feeTypeCode: feetyped.displayName,
                                    title: feetyped.title
                                })
                                pendingAmountBreakupacademic.push({
                                    amount: 0,
                                    feeTypeCode: feetyped.displayName,
                                    title: feetyped.title
                                })
                                discountAmountBreakupacademic.push({
                                    amount: 0,
                                    feeTypeCode: feetyped.displayName,
                                    title: feetyped.title
                                })
                                paidAmountBreakupacademic.push({
                                    amount: 0,
                                    feeTypeCode: feetyped.displayName,
                                    title: feetyped.title
                                })
                                // if (m + 1 == plannedAmountBreakuptmp.length) {
                                plannedAmountBreakup.push({
                                    amount: 0.00,
                                    feeTypeCode: feetyped.displayName,
                                    title: feetyped.title,
                                })
                                plannedAmount = plannedAmount + 0;
                                pendingAmount = pendingAmount + 0;
                                paidAmount = paidAmount + 0;
                                pendingAmountBreakup.push({
                                    amount: 0.00,
                                    feeTypeCode: feetyped.displayName,
                                    title: feetyped.title,
                                })
                                paidAmountBreakup.push({
                                    amount: 0.00,
                                    feeTypeCode: feetyped.displayName,
                                    title: feetyped.title,
                                })
                                discountAmountBreakup.push({
                                    amount: 0.00,
                                    feeTypeCode: feetyped.displayName,
                                    title: feetyped.title,
                                })
                                // }
                            }
                            let temppaid = 0
                            let temppend = 0
                            // plannedAmount = plannedAmount2
                            // console.log("sd",pshedule.feesBreakUp.length)
                            if (i + 1 == feestrData._doc.feeTypeIds.length) {
                                for (let q = 0; q < pshedule.feesBreakUp.length; q++) {
                                    count = count + 1
                                    let duedate1
                                    let penaltyStartdate1
                                    let plannedAmountBreakupinst = [];
                                    let pendingAmountBreakupinst = [];
                                    let paidAmountBreakupinst = [];
                                    let discountAmountBreakupinst = [];
                                    let title
                                    if (pshedule.feesBreakUp.length == 4) {
                                        title = `Term ${count < 3 ? 1 : 2} Fees`
                                        duedate1 = duedates[q]
                                        penaltyStartdate1 = latefeesdates[q]
                                    }
                                    else if (pshedule.feesBreakUp.length == 2) {
                                        title = `Semester ${count} Fees`
                                        duedate1 = duedates2[q]
                                        penaltyStartdate1 = latefeesdates2[q]
                                    }
                                    else {
                                        title = `Semester ${count} Fees`
                                        duedate1 = duedates2[q]
                                        penaltyStartdate1 = latefeesdates2[q]
                                    }
                                    let paida = 0
                                    let penda = 0

                                    for (n = 0; n < plannedAmountBreakup.length; n++) {
                                        penda = penda + (Number(pshedule.feesBreakUp[q]) * Number(plannedAmountBreakup[n].amount) / 100)
                                        plannedAmountBreakupinst.push({
                                            // amount: n == 0 ? parseFloat(pshedule.feesBreakUp[q]) * parseFloat(plannedAmount2) / 100 : 0,
                                            // total:  n == 0 ? parseFloat(pshedule.feesBreakUp[q]) * parseFloat(plannedAmount2) / 100 : 0,
                                            // feeTypeCode: plannedAmountBreakup[m].breakUp[n].feeTypeCode,
                                            // // title: `Term ${count<3 ? 1: 2} Fees`,
                                            // title: title == null ? plannedAmountBreakup[m].breakUp[n].title : title,
                                            // breakUp: {
                                            amount: Number(pshedule.feesBreakUp[q]) * Number(plannedAmountBreakup[n].amount) / 100,
                                            feeTypeCode: plannedAmountBreakup[n].feeTypeCode,
                                            // title: `Term ${count<3 ? 1: 2} Fees`,
                                            title: plannedAmountBreakup[n].title
                                            // }
                                        })
                                        pendingAmountBreakupinst.push({
                                            // amount: n == 0 ? penda : 0,
                                            // feeTypeCode: pendingAmountBreakup[m].breakUp[n].feeTypeCode,
                                            // // title: `Term ${count<3 ? 1: 2} Fees`,
                                            // total:  n == 0 ? parseFloat(pshedule.feesBreakUp[q]) * parseFloat(plannedAmount2) / 100 : 0,
                                            // title: title == null ? plannedAmountBreakup[m].breakUp[n].title : title,
                                            // breakUp: {
                                            amount: Number(pshedule.feesBreakUp[q]) * Number(plannedAmountBreakup[n].amount) / 100,
                                            feeTypeCode: plannedAmountBreakup[n].feeTypeCode,
                                            // title: `Term ${count<3 ? 1: 2} Fees`,
                                            title: plannedAmountBreakup[n].title,
                                            // }
                                        })

                                        paidAmountBreakupinst.push({
                                            // amount: n == 0 ? paida : 0, 
                                            // total:  0,
                                            // feeTypeCode: paidAmountBreakup[m].breakUp[n].feeTypeCode,
                                            // // title: `Term ${count<3 ? 1: 2} Fees`,
                                            // title: title == null ? plannedAmountBreakup[m].breakUp[n].title : title,
                                            // breakUp: {
                                            amount: 0,
                                            feeTypeCode: plannedAmountBreakup[n].feeTypeCode,
                                            // title: `Term ${count<3 ? 1: 2} Fees`,
                                            title: plannedAmountBreakup[n].title,
                                            // }
                                        })
                                        discountAmountBreakupinst.push({
                                            amount: 0,
                                            feeTypeCode: plannedAmountBreakup[n].feeTypeCode,
                                            // title: `Term ${count<3 ? 1: 2} Fees`,
                                            title: plannedAmountBreakup[n].title,
                                        })

                                    }
                                    dcount++
                                    // console.log(cayear)
                                    await feeInstallmentPlanModel.updateOne({ feePlanId: stuFeeplan._id }, { $set: { year: cayear } })
                                    cayear++
                                    // console.log(cayear)
                                    let stuinstallplans = new feeInstallmentPlanModel({
                                        feePlanId: stuFeeplan._id,
                                        studentRegId: stddata._doc.regId,
                                        label: `Installment${(Number(instcount)).toString().length == 1 ? "00" : (Number(instcount)).toString().length == 2 ? "0" : ""
                                            }${Number(instcount)}`,
                                        displayName: `INST_${finYear}_${(Number(instcount) + 1).toString().length == 1 ? "00" : (Number(instcount) + 1).toString().length == 2 ? "0" : ""
                                            }${Number(instcount) + 1}`,
                                        description: `Installment${(Number(instcount)).toString().length == 1 ? "00" : (Number(instcount)).toString().length == 2 ? "0" : ""
                                            }${Number(instcount)}`,
                                        dueDate: duedate1,
                                        lateFeeStartDate: penaltyStartdate1,
                                        percentage: pshedule.feesBreakUp[q],
                                        totalAmount: plannedAmount,
                                        year: cayear,
                                        // plannedAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(pendingAmount) / 100,
                                        plannedAmount: parseFloat(pshedule.feesBreakUp[q]) * parseFloat(plannedAmount) / 100,
                                        plannedAmountBreakup: plannedAmountBreakupinst,
                                        paidAmount: paida,
                                        paidAmountBreakup: paidAmountBreakupinst,
                                        pendingAmount: plannedAmount,
                                        pendingAmountBreakup: pendingAmountBreakupinst,
                                        discountType: "",
                                        discountPercentage: 0,
                                        discountAmount: 0,
                                        discountAmountBreakup: discountAmountBreakupinst,
                                        status: "Planned",
                                        transactionId: "",
                                        academicYear: `${acdemicyearcount1}-${acdemicyearcount2}`,
                                        campusId: stddata._doc.campusId,
                                        remarks: {
                                            seller: "",
                                            finance: "",
                                            headseller: ""
                                        }
                                    })
                                    await stuinstallplans.save();
                                    allStudentsInstallmentPlans.push(stuinstallplans)

                                }
                            }
                        }
                        stuFeeplanbyacademic.plannedAmountBreakup = plannedAmountBreakupacademic;
                        stuFeeplanbyacademic.pendingAmountBreakup = pendingAmountBreakupacademic;
                        stuFeeplanbyacademic.discountAmountBreakup = discountAmountBreakupacademic;
                        stuFeeplanbyacademic.paidAmountBreakup = paidAmountBreakupacademic;
                        let newacademicplan = new feePlanModelbyAcademic(stuFeeplanbyacademic)
                        // console.log(stuFeeplanbyacademic)
                        await newacademicplan.save(stuFeeplanbyacademic)
                    }
                }
                console.log("old", stuFeeplan.plannedAmount)
                stuFeeplan.plannedAmount = stuFeeplan.plannedAmount + totalPlannedAmount1;
                stuFeeplan.pendingAmount = stuFeeplan.pendingAmount + totalPendingAmount1;
                // stuFeeplan.paidAmount = 0;
                console.log("new", stuFeeplan.plannedAmount)
                stuFeeplan.studentRegId = stddata._doc.regId;
                stuFeeplan.plannedAmountBreakup = plannedAmountBreakup2;
                stuFeeplan.paidAmountBreakup = paidAmountBreakup2;
                stuFeeplan.pendingAmountBreakup = pendingAmountBreakup2;
                stuFeeplan.discountAmountBreakup = discountAmountBreakup2;
                stuFeeplan.feePlanBreakUp = feePlanBreakUp
                stuFeeplan.academicYear = "2020-21"
                // let stuFeePlans = new feePlanModel(stuFeeplan)
                if (stuFeeplan.plannedAmountBreakup.length > 0) {
                    await feePlanModel.updateOne({ _id: stuFeeplan._id }, { $set: { academicYear: "2021-22", studentRegId: stddata._doc.regId, year: (Number(cudate.getFullYear()) - Number(year) + 1) } })
                    // await stuFeePlans.save()
                }
                allStudentsFeePlans.push(stuFeeplan)
            }
            if (j + 1 == studentDetails.length) {
                console.log("s")
                setTimeout(() => { resolve({ status: "success", message: "fee plan created successfully" }), 7000 })
            }

            // }
        }
    })
}
async function createFeePlansAcad2(req, dbConnectionp, studentDetails, feeStructureModel, studentModel, stdfeeMaps, campuses, allProgramPlan, allPaymentSchedule, finYear, allStudentMap) {
    return new Promise(async function (resolve, reject) {
        let daydigits = { "First": "01", "Second": "02", "Third": "03", "Fourth": "04", "Fifth": "05", "Tenth": "10", "Fifteenth": "15", "Last": "28", "Second Last": "27", "Third last": "26", "Fourth last": "25" };
        let monthdigits = { "January": "01", "February": "02", "March": "03", "April": "04", "May": "05", "June": "06", "July": "07", "August": "08", "September": "09", "October": "10", "November": "11", "December": "12" };
        let digitsmonth = { "01": "January", "02": "February", "03": "March", "04": "April", "05": "May", "06": "June", "07": "July", "08": "August", "09": "September", "10": "October", "11": "November", "12": "December" };
        let feeplanacademicschema = mongoose.Schema({}, { strict: false });
        let paymentScheduleModel = dbConnectionp.model("paymentschedules", paymentScheduleSchema);
        let feePlanModel = dbConnectionp.model("studentfeeplans", feeplanschema);
        let feePlanModelbyAcademic = dbConnectionp.model("feeplansbyacademic", feeplanacademicschema);
        let feeInstallmentPlanModel = dbConnectionp.model("studentfeeinstallmentplans", feeplanInstallmentschema);
        let allStudentMap = dbConnectionp.model("studentfeesmaps", StudentFeeMapSchema);
        let programPlanModel = dbConnectionp.model("programplans", ProgramPlanSchema);
        let feeTypeDetails = dbConnectionp.model("feeTypes", FeeTypeSchema);
        let allPaymentSchedule = await paymentScheduleModel.find({})
        var allStudentsFeePlans = [];
        var allStudentsInstallmentPlans = []
        let count = 0;
        let dcount = 0;
        let fpacount = 0;
        let duedates = [new Date(`2021-April-10`), new Date(`2021-June-10`), new Date(`2021-September-10`), new Date(`2021-November-10`)];
        let latefeesdates = [new Date(`2021-April-30`), new Date(`2021-June-30`), new Date(`2021-September-30`), new Date(`2021-November-30`)];
        let duedates2 = [new Date(`2021-June-01`), new Date(`2021-October-01`)];
        let latefeesdates2 = [new Date(`2021-November-30`), new Date(`2022-May-30`)];
        await feeInstallmentPlanModel.updateMany({}, { $set: { academicYear: "2021-22" } })

        for (let j = 0; j < studentDetails.length; j++) {
            console.log(j, studentDetails[j]._doc.regId)
            let feePlanBreakUp = [];
            let ppInputData = studentDetails[j]._doc
            let stddata = studentDetails[j]
            let stuFeeplandata = await feePlanModel.findOne({ studentRegId: stddata._doc.regId });
            let stuFeeplan
            let ppdata = await programPlanModel.findOne({ _id: stddata._doc.programPlanId });
            let feeData = await allStudentMap.findOne({ studentId: stddata._doc._id });
            let cudate = new Date();
            let yearsplit = ppdata.academicYear.split("-")
            let year = yearsplit[0]
            let acayears = []
            let acacount = 0
            for (let p = Number(year); p < cudate.getFullYear(); p++) {
                acayears.push(`${Number(year) + acacount}-${Number(yearsplit[1]) + acacount}`)
                acacount++
            }

            let feestrData = await feeStructureModel.findOne({ _id: stddata._doc.feeStructureId, campusId: stddata._doc.campusId });
            stuFeeplan = stuFeeplandata._doc;
            fpacount++
            let existingfeeplanacademic = {
                _id: mongoose.Types.ObjectId(),
                feePlanId: stuFeeplan._id,
                applicationId: `FPLAN_${finYear}_${(Number(fpacount) + 1).toString().length == 1 ? "00" : (Number(fpacount) + 1).toString().length == 2 ? "0" : ""
                    }${Number(fpacount) + 1}`,
                studentRegId: stddata._doc.regId,
                studentId: stddata._id,
                academicYear: `2021-22`,
                programPlanHEDAId: ppdata._doc["hedaId"],
                plannedAmount: stuFeeplandata._doc.plannedAmount,
                plannedAmountBreakup: stuFeeplandata._doc.plannedAmountBreakup,
                paidAmount: stuFeeplandata._doc.paidAmount,
                paidAmountBreakup: stuFeeplandata._doc.paidAmountBreakup,
                pendingAmount: stuFeeplandata._doc.pendingAmount,
                pendingAmountBreakup: stuFeeplandata._doc.pendingAmountBreakup,
                currency: stddata._doc.currency,
                forex: stddata._doc.FOREX,
                discountType: stuFeeplandata._doc.discountType,
                discountPercentage: 0,
                discountAmount: 0,
                dueDate: stuFeeplandata._doc.dueDate,
                penaltyStartDate: stuFeeplandata._doc.penaltyStartDate,
                percentageBreakup: [],
                installmentPlan: {},
                discountAmountBreakup: stuFeeplandata._doc.discountAmountBreakup,
                campusId: stddata._doc.campusId,
                concessionPercentage: 0,
                concessionAmount: 0,
                remarks: {
                    seller: "",
                    finance: "",
                    headseller: ""
                }
            }
            let splitacademicyear = ppdata.academicYear.split("-");
            let plannedAmount = 0;
            let pendingAmount = 0;
            let paidAmount = 0;
            let plannedAmountBreakup = [];
            let plannedAmountBreakuptmp = [];
            let paidAmountBreakuptmp = [];
            let pendingAmountBreakuptmp = [];
            let discountAmountBreakuptmp = [];
            let paidAmountBreakup = [];
            let pendingAmountBreakup = [];
            let discountAmountBreakup = [];
            let instplannedAmountBreakup = [];
            let instpaidAmountBreakup = [];
            let instpendingAmountBreakup = [];
            let feeAmount = 0;
            let totalPlannedAmount1 = 0
            let totalPendingAmount1 = 0
            let totalPaidAmount1 = 0
            let totalTuitionamt = 0
            let totalMisamt = 0
            let plannedAmountBreakup2 = stuFeeplan.plannedAmountBreakup;
            let paidAmountBreakup2 = stuFeeplan.paidAmountBreakup;
            let pendingAmountBreakup2 = stuFeeplan.pendingAmountBreakup;
            let discountAmountBreakup2 = stuFeeplan.discountAmountBreakup;
            let tuitionAmount = []
            let miscAmount = []
            let currentAcademicYear = "2021-22"
            let acdemicyearcount1 = 2021
            let acdemicyearcount2 = 22
            let instcount = 1
            let newexistingfeeplanacademic = new feePlanModelbyAcademic(existingfeeplanacademic)
            await newexistingfeeplanacademic.save();
            count = 0
            for (m = 0; m < acayears.length; m++) {
                acdemicyearcount1++
                acdemicyearcount2++
                instcount++
                fpacount++
                if (!feemapdataa[`Tuition Amount ${m + 1}`].includes("-")) {

                    let plannedAmountBreakupacademic = [];
                    let pendingAmountBreakupacademic = [];
                    let discountAmountBreakupacademic = [];
                    let paidAmountBreakupacademic = [];
                    stuFeeplanbyacademic.plannedAmount = plannedAmountBreakuptmp[m].total;
                    stuFeeplanbyacademic.pendingAmount = plannedAmountBreakuptmp[m].total;
                    stuFeeplanbyacademic.paidAmount = 0;
                    let plannedAmount2 = feePlanBreakUp[m]["amount"]
                    for (let i = 0; i < feestrData._doc.feeTypeIds.length; i++) {
                        let feetyped = await feeTypeDetails.findOne({ _id: mongoose.Types.ObjectId(feestrData._doc.feeTypeIds[i]) })
                        feetyped = feetyped._doc
                        let feeBreakup = [];
                        let pshedule = allPaymentSchedule[0]._doc
                        let month = monthdigits[pshedule.startMonth];
                        let monthss = ["June", "November", "September", "November"]
                        let duedate = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                        let penaltyStartdate = new Date(`${year}-${month}-${pshedule.scheduleDetails.penaltyStartDate}`)
                        plannedAmount = Number(plannedAmountBreakuptmp[m].total);
                        pendingAmount = Number(plannedAmountBreakuptmp[m].total);
                        paidAmount = 0
                        if (feetyped.title.toLowerCase().includes("tuition") || feetyped.title.includes("Term") || feetyped.title.includes("Miscellaneous")) {
                            plannedAmountBreakupacademic.push({
                                amount: feetyped.title.toLowerCase().includes("tuition") ? Number(feemapdataa[`Tuition Amount ${m + 1}`].replace(",", "")) : feetyped.title.includes("Miscellaneous") ? Number(feemapdataa[`Other Amount ${m + 1}`].replace(",", "")) : 0,
                                feeTypeCode: feetyped.displayName,
                                title: feetyped.title
                            })
                            pendingAmountBreakupacademic.push({
                                amount: feetyped.title.toLowerCase().includes("tuition") ? Number(feemapdataa[`Tuition Amount ${m + 1}`].replace(",", "")) : feetyped.title.includes("Miscellaneous") ? Number(feemapdataa[`Other Amount ${m + 1}`].replace(",", "")) : 0,
                                feeTypeCode: feetyped.displayName,
                                title: feetyped.title
                            })
                            discountAmountBreakupacademic.push({
                                amount: 0,
                                feeTypeCode: feetyped.displayName,
                                title: feetyped.title
                            })
                            paidAmountBreakupacademic.push({
                                amount: 0,
                                feeTypeCode: feetyped.displayName,
                                title: feetyped.title
                            })
                            plannedAmountBreakup.push({
                                amount: feetyped.title.toLowerCase().includes("tuition") ? Number(feemapdataa[`Tuition Amount ${m + 1}`].replace(",", "")) : feetyped.title.includes("Miscellaneous") ? Number(feemapdataa[`Other Amount ${m + 1}`].replace(",", "")) : 0,
                                feeTypeCode: feetyped.displayName,
                                title: feetyped.title
                            })
                            pendingAmountBreakup.push({
                                amount: feetyped.title.toLowerCase().includes("tuition") ? Number(feemapdataa[`Tuition Amount ${m + 1}`].replace(",", "")) : feetyped.title.includes("Miscellaneous") ? Number(feemapdataa[`Other Amount ${m + 1}`].replace(",", "")) : 0,
                                feeTypeCode: feetyped.displayName,
                                title: feetyped.title,
                            })
                            paidAmountBreakup.push({
                                amount: 0,
                                feeTypeCode: feetyped.displayName,
                                title: feetyped.title,
                            })
                            discountAmountBreakup.push({
                                amount: 0.00,
                                feeTypeCode: feetyped.displayName,
                                title: feetyped.title,
                            })
                        }
                        else {
                            plannedAmountBreakupacademic.push({
                                amount: 0,
                                feeTypeCode: feetyped.displayName,
                                title: feetyped.title
                            })
                            pendingAmountBreakupacademic.push({
                                amount: 0,
                                feeTypeCode: feetyped.displayName,
                                title: feetyped.title
                            })
                            discountAmountBreakupacademic.push({
                                amount: 0,
                                feeTypeCode: feetyped.displayName,
                                title: feetyped.title
                            })
                            paidAmountBreakupacademic.push({
                                amount: 0,
                                feeTypeCode: feetyped.displayName,
                                title: feetyped.title
                            })
                            plannedAmountBreakup.push({
                                amount: 0.00,
                                feeTypeCode: feetyped.displayName,
                                title: feetyped.title,
                            })
                            plannedAmount = plannedAmount + 0;
                            pendingAmount = pendingAmount + 0;
                            paidAmount = paidAmount + 0;
                            pendingAmountBreakup.push({
                                amount: 0.00,
                                feeTypeCode: feetyped.displayName,
                                title: feetyped.title,
                            })
                            paidAmountBreakup.push({
                                amount: 0.00,
                                feeTypeCode: feetyped.displayName,
                                title: feetyped.title,
                            })
                            discountAmountBreakup.push({
                                amount: 0.00,
                                feeTypeCode: feetyped.displayName,
                                title: feetyped.title,
                            })
                            // }
                        }
                        let temppaid = 0
                        let temppend = 0
                        plannedAmount = plannedAmount2
                        if (i + 1 == feestrData._doc.feeTypeIds.length) {
                            for (let q = 0; q < pshedule.feesBreakUp.length; q++) {
                                count = count + 1
                                let duedate1
                                let penaltyStartdate1
                                let plannedAmountBreakupinst = [];
                                let pendingAmountBreakupinst = [];
                                let paidAmountBreakupinst = [];
                                let discountAmountBreakupinst = [];
                                let title
                                if (pshedule.feesBreakUp.length == 4) {
                                    title = `Term ${count < 3 ? 1 : 2} Fees`
                                    duedate1 = duedates[q]
                                    penaltyStartdate1 = latefeesdates[q]
                                }
                                else if (pshedule.feesBreakUp.length == 2) {
                                    title = `Semester ${count} Fees`
                                    duedate1 = duedates2[q]
                                    penaltyStartdate1 = latefeesdates2[q]
                                }
                                else {
                                    title = `Semester ${count} Fees`
                                    duedate1 = duedates2[q]
                                    penaltyStartdate1 = latefeesdates2[q]
                                }
                                let paida = 0
                                let penda = 0

                                for (n = 0; n < plannedAmountBreakup.length; n++) {
                                    penda = penda + (Number(pshedule.feesBreakUp[q]) * Number(plannedAmountBreakup[n].amount) / 100)
                                    plannedAmountBreakupinst.push({
                                        amount: Number(pshedule.feesBreakUp[q]) * Number(plannedAmountBreakup[n].amount) / 100,
                                        feeTypeCode: plannedAmountBreakup[n].feeTypeCode,
                                        title: plannedAmountBreakup[n].title
                                    })
                                    pendingAmountBreakupinst.push({
                                        amount: Number(pshedule.feesBreakUp[q]) * Number(plannedAmountBreakup[n].amount) / 100,
                                        feeTypeCode: plannedAmountBreakup[n].feeTypeCode,
                                        title: plannedAmountBreakup[n].title,
                                    })
                                    paidAmountBreakupinst.push({
                                        amount: 0,
                                        feeTypeCode: plannedAmountBreakup[n].feeTypeCode,
                                        title: plannedAmountBreakup[n].title,
                                    })
                                    discountAmountBreakupinst.push({
                                        amount: 0,
                                        feeTypeCode: plannedAmountBreakup[n].feeTypeCode,
                                        title: plannedAmountBreakup[n].title,
                                    })

                                }
                                dcount++
                                let stuinstallplans = new feeInstallmentPlanModel({
                                    feePlanId: stuFeeplan._id,
                                    studentRegId: stddata._doc.regId,
                                    label: `Installment${(Number(instcount)).toString().length == 1 ? "00" : (Number(instcount)).toString().length == 2 ? "0" : ""
                                        }${Number(instcount)}`,
                                    displayName: `INST_${finYear}_${(Number(instcount) + 1).toString().length == 1 ? "00" : (Number(instcount) + 1).toString().length == 2 ? "0" : ""
                                        }${Number(instcount) + 1}`,
                                    description: `Installment${(Number(instcount)).toString().length == 1 ? "00" : (Number(instcount)).toString().length == 2 ? "0" : ""
                                        }${Number(instcount)}`,
                                    dueDate: duedate1,
                                    lateFeeStartDate: penaltyStartdate1,
                                    percentage: pshedule.feesBreakUp[q],
                                    totalAmount: plannedAmount,
                                    plannedAmount: parseFloat(pshedule.feesBreakUp[q]) * parseFloat(plannedAmount) / 100,
                                    plannedAmountBreakup: plannedAmountBreakupinst,
                                    paidAmount: paida,
                                    paidAmountBreakup: paidAmountBreakupinst,
                                    pendingAmount: plannedAmount,
                                    pendingAmountBreakup: pendingAmountBreakupinst,
                                    discountType: "",
                                    discountPercentage: 0,
                                    discountAmount: 0,
                                    discountAmountBreakup: discountAmountBreakupinst,
                                    status: "Planned",
                                    transactionId: "",
                                    academicYear: `${acdemicyearcount1}-${acdemicyearcount2}`,
                                    campusId: stddata._doc.campusId,
                                    remarks: {
                                        seller: "",
                                        finance: "",
                                        headseller: ""
                                    }
                                })
                                await stuinstallplans.save();
                                allStudentsInstallmentPlans.push(stuinstallplans)

                            }
                        }
                    }
                    stuFeeplanbyacademic.plannedAmountBreakup = plannedAmountBreakupacademic;
                    stuFeeplanbyacademic.pendingAmountBreakup = pendingAmountBreakupacademic;
                    stuFeeplanbyacademic.discountAmountBreakup = discountAmountBreakupacademic;
                    stuFeeplanbyacademic.paidAmountBreakup = paidAmountBreakupacademic;
                    let newacademicplan = new feePlanModelbyAcademic(stuFeeplanbyacademic)
                    await newacademicplan.save(stuFeeplanbyacademic)
                }
            }
            console.log("old", stuFeeplan.plannedAmount)
            stuFeeplan.plannedAmount = stuFeeplan.plannedAmount + totalPlannedAmount1;
            stuFeeplan.pendingAmount = stuFeeplan.pendingAmount + totalPendingAmount1;
            console.log("new", stuFeeplan.plannedAmount)
            stuFeeplan.studentRegId = stddata._doc.regId;
            stuFeeplan.plannedAmountBreakup = plannedAmountBreakup2;
            stuFeeplan.paidAmountBreakup = paidAmountBreakup2;
            stuFeeplan.pendingAmountBreakup = pendingAmountBreakup2;
            stuFeeplan.discountAmountBreakup = discountAmountBreakup2;
            stuFeeplan.feePlanBreakUp = feePlanBreakUp
            stuFeeplan.academicYear = "2021-22"
            if (stuFeeplan.plannedAmountBreakup.length > 0) {
                await feePlanModel.updateOne({ _id: stuFeeplan._id }, { $set: { academicYear: "2021-22", studentRegId: stddata._doc.regId } })
            }
            allStudentsFeePlans.push(stuFeeplan)

            if (j + 1 == studentDetails.length) {
                console.log("s")
                setTimeout(() => { resolve({ status: "success", message: "fee plan created successfully" }), 7000 })
            }
        }
    })
}
async function createFeePlansN(req, dbConnectionp, feeManagerDatas, feeTypeDetails, studentDetails, feeManagerSchema, feeStructureModel, studentModel, stdfeeMaps, campuses, allProgramPlan, allPaymentSchedule, finYear, allStudentMap1) {
    return new Promise(async function (resolve, reject) {
        let feePlanModel = dbConnectionp.model("studentfeeplans", feeplanschema);
        let feeInstallmentPlanModel = dbConnectionp.model("studentfeeinstallmentplans", feeplanInstallmentschema);
        let allStudentMap = dbConnectionp.model("studentfeesmaps", StudentFeeMapSchema);
        var allStudentsFeePlans = [];
        var allStudentsInstallmentPlans = []
        let count = 0;
        let dcount = 0;
        for (let j = 0; j < studentDetails.length; j++) {
            console.log(j)
            let ppInputData = studentDetails[j]
            // regId = ppInputData["Reg ID *"] == "-" ? ppInputData["HEDA ID"] : ppInputData["Reg ID *"]
            let regId = ppInputData["Reg ID *"]
            let stddata = await studentModel.findOne({ "regId": regId });
            let ppdata = await allProgramPlan.find(item => item._id.toString() == stddata._doc.programPlanId);
            let yearsplit = ppdata.academicYear.split("-")
            let year = yearsplit[0]
            let feestrData = await feeStructureModel.findOne({ _id: stddata._doc.feeStructureId, campusId: stddata._doc.campusId });
            let feeData = await allStudentMap.findOne({ studentId: stddata._id })
            let stuFeeplan = {
                _id: mongoose.Types.ObjectId(),
                applicationId: `FPLAN_${finYear}_${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                    }${Number(j) + 1}`,
                studentRegId: ppInputData["Reg ID *"] == "-" ? ppInputData["HEDA ID"] : ppInputData["Reg ID *"],
                studentId: stddata._id,
                programPlanHEDAId: ppInputData["Program Plan ID"],
                plannedAmount: 0,
                plannedAmountBreakup: [],
                paidAmount: 0,
                paidAmountBreakup: [],
                pendingAmount: 0,
                pendingAmountBreakup: [],
                currency: stddata._doc.currency,
                forex: stddata._doc.FOREX,
                discountType: "",
                discountPercentage: 0,
                discountAmount: 0,
                dueDate: "",
                penaltyStartDate: "",
                percentageBreakup: [],
                installmentPlan: {},
                discountAmountBreakup: [],
                campusId: stddata._doc.campusId,
                remarks: {
                    seller: "",
                    finance: "",
                    headseller: ""
                }
            }
            let stdFeeInPlan = {
                feePlanId: stuFeeplan._id,
                label: "",
                description: "",
                dueDate: "",
                lateFeeStartDate: "",
                percentage: "",
                totalAmount: 0,
                plannedAmount: 0,
                plannedAmountBreakup: [],
                paidAmount: 0,
                paidAmountBreakup: [],
                pendingAmount: 0,
                pendingAmountBreakup: [],
                discountType: "",
                discountPercentage: 0,
                discountAmount: 0,
                discountAmountBreakup: [],
                status: "",
                transactionId: "",
                campusId: stddata._doc.campusId,
                remarks: {
                    seller: "",
                    finance: "",
                    headseller: ""
                }
            }
            let feeAmount = 0;
            let plannedAmount = 0;
            let pendingAmount = 0;
            let paidAmount = 0;
            let plannedAmountBreakup = [];
            let paidAmountBreakup = [];
            let pendingAmountBreakup = [];
            let discountAmountBreakup = [];
            let instplannedAmountBreakup = [];
            let instpaidAmountBreakup = [];
            let instpendingAmountBreakup = [];
            let duedates = [new Date(`2021-April-10`), new Date(`2021-June-10`), new Date(`2021-September-10`), new Date(`2021-November-10`)];
            let latefeesdates = [new Date(`2021-April-30`), new Date(`2021-June-30`), new Date(`2021-September-30`), new Date(`2021-November-30`)];
            for (let i = 0; i < feestrData._doc.feeTypeIds.length; i++) {
                let feetyped = feeTypeDetails.find(item => item._id.toString() == feestrData._doc.feeTypeIds[i])

                let feeBreakup = [];
                let pshedule = allPaymentSchedule[0]
                if (feetyped.title.toLowerCase().includes("tuition") || feetyped.title.includes("Term")) {
                    plannedAmountBreakup.push({
                        amount: Number(feeData["amount"]),
                        feeTypeCode: feetyped.displayName,
                        title: feetyped.title,
                    })
                    plannedAmount = plannedAmount + Number(feeData["amount"]);
                    pendingAmount = pendingAmount + Number(feeData["pending"]);
                    paidAmount = paidAmount + Number(feeData["paid"]);
                    pendingAmountBreakup.push({
                        amount: Number(feeData["pending"]),
                        feeTypeCode: feetyped.displayName,
                        title: feetyped.title,
                    })
                    paidAmountBreakup.push({
                        amount: Number(feeData["paid"]),
                        feeTypeCode: feetyped.displayName,
                        title: feetyped.title,
                    })
                }
                else {
                    plannedAmountBreakup.push({
                        amount: 0.00,
                        feeTypeCode: feetyped.displayName,
                        title: feetyped.title,
                    })
                    plannedAmount = plannedAmount + 0;
                    pendingAmount = pendingAmount + 0;
                    paidAmount = paidAmount + 0;
                    pendingAmountBreakup.push({
                        amount: 0.00,
                        feeTypeCode: feetyped.displayName,
                        title: feetyped.title,
                    })
                    paidAmountBreakup.push({
                        amount: 0.00,
                        feeTypeCode: feetyped.displayName,
                        title: feetyped.title,
                    })
                }
                discountAmountBreakup.push({
                    amount: 0.00,
                    feeTypeCode: feetyped.displayName,
                    title: feetyped.title,
                })
                if (i + 1 == feestrData._doc.feeTypeIds.length) {
                    for (l = 0; l < pshedule.feesBreakUp.length; l++) {
                        count = l + 1
                        let plannedAmountBreakupinst = [];
                        let pendingAmountBreakupinst = [];
                        let paidAmountBreakupinst = [];
                        for (n = 0; n < plannedAmountBreakup.length; n++) {
                            plannedAmountBreakupinst.push({
                                amount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmountBreakup[n].amount) / 100,
                                feeTypeCode: plannedAmountBreakup[n].feeTypeCode,
                                // title: `Term ${count<3 ? 1: 2} Fees`,
                                title: plannedAmountBreakup[n].title,
                            })
                            pendingAmountBreakupinst.push({
                                amount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(pendingAmountBreakup[n].amount) / 100,
                                feeTypeCode: pendingAmountBreakup[n].feeTypeCode,
                                // title: `Term ${count<3 ? 1: 2} Fees`,
                                title: pendingAmountBreakup[n].title,
                            })
                            paidAmountBreakupinst.push({
                                amount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(paidAmountBreakup[n].amount) / 100,
                                feeTypeCode: paidAmountBreakup[n].feeTypeCode,
                                // title: `Term ${count<3 ? 1: 2} Fees`,
                                title: paidAmountBreakup[n].title,
                            })

                        }
                        dcount++
                        let stuinstallplans = new feeInstallmentPlanModel({
                            feePlanId: stuFeeplan._id,
                            label: `Installment${(Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                                }${Number(count)}`,
                            displayName: `INST_${finYear}_${(Number(dcount) + 1).toString().length == 1 ? "00" : (Number(dcount) + 1).toString().length == 2 ? "0" : ""
                                }${Number(dcount) + 1}`,
                            description: `Installment${(Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                                }${Number(count)}`,
                            dueDate: dueDates[l],
                            lateFeeStartDate: latefeesdates[l],
                            percentage: pshedule.feesBreakUp[l],
                            totalAmount: plannedAmount,
                            // plannedAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(pendingAmount) / 100,
                            plannedAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(plannedAmount) / 100,
                            plannedAmountBreakup: pendingAmountBreakupinst,
                            paidAmount: paidAmount,
                            paidAmountBreakup: paidAmountBreakupinst,
                            pendingAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(pendingAmount) / 100,
                            pendingAmountBreakup: pendingAmountBreakupinst,
                            discountType: "",
                            discountPercentage: 0,
                            discountAmount: 0,
                            discountAmountBreakup: discountAmountBreakup,
                            status: "Planned",
                            transactionId: "",
                            campusId: stddata._doc.campusId,
                            remarks: {
                                seller: "",
                                finance: "",
                                headseller: ""
                            }
                        })
                        await stuinstallplans.save();
                        allStudentsInstallmentPlans.push(stuinstallplans)
                        // feeBreakup.push({ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(tuitionFee) / 100), percent: pshedule.feesBreakUp[l] })

                    }

                }

            }
            stuFeeplan.plannedAmount = plannedAmount;
            stuFeeplan.pendingAmount = pendingAmount;
            stuFeeplan.paidAmount = paidAmount;
            stuFeeplan.plannedAmountBreakup = plannedAmountBreakup;
            stuFeeplan.paidAmountBreakup = paidAmountBreakup;
            stuFeeplan.pendingAmountBreakup = pendingAmountBreakup;
            stuFeeplan.discountAmountBreakup = discountAmountBreakup;
            let stuFeePlans = new feePlanModel(stuFeeplan)
            await stuFeePlans.save()
            allStudentsFeePlans.push(stuFeePlans)
            if (j + 1 == studentDetails.length) {
                console.log("s")
                resolve({ status: "success", message: "fee plan created successfully" })
            }
            // await stuFeePlans.save();
        }
    })
}
async function createFeePlans2(req, dbConnectionp, feeManagerDatas, feeTypeDetails, studentDetails, feeManagerSchema, feeStructureModel, studentModel, stdfeeMaps, campuses, allProgramPlan, allPaymentSchedule) {
    return new Promise(async function (resolve, reject) {
        let daydigits = { "First": "02", "Second": "03", "Third": "04", "Fourth": "05", "Fifth": "06", "Tenth": "11", "Last": "28", "Second Last": "27", "Third last": "26", "Fourth last": "25" };
        // let student = req.query.id:
        // let dbConnectionp = await createDatabase(req.headers.orgId, req.headers.resource);
        // let feeStructureModel = dbConnectionp.model("feestructures", FeeStructureSchema);
        // let studentModel = dbConnectionp.model("students", StudentSchema);
        // let feeManagerSchema = dbConnectionp.model("feemanagers", FeeManagerSchema);
        // let campid1 = await campuses.find(item => item._id.toString() == studentData._doc["campusId"].toString())
        // let campid = campid1["_id"];
        let feePlanModel = dbConnectionp.model("studentfeeplans", feeplanschema);
        let feeInstallmentPlanModel = dbConnectionp.model("studentfeeinstallmentplans", feeplanInstallmentschema);

        var allStudentsFeePlans = [];
        var allStudentsInstallmentPlans = []
        let count = 0;
        for (let j = 0; j < studentDetails.length; j++) {
            console.log(j)
            let ppInputData = studentDetails[j]
            regId = ppInputData["Reg ID *"] == "-" ? ppInputData["HEDA ID"] : ppInputData["Reg ID *"]
            let stddata = await studentModel.findOne({ "regId": regId });
            let ppdata = await allProgramPlan.find(item => item._id.toString() == stddata._doc.programPlanId);
            let yearsplit = ppdata.academicYear.split("-")
            let year = yearsplit[0]
            let feestrData = await feeStructureModel.findOne({ _id: stddata._doc.feeStructureId, campusId: stddata._doc.campusId });

            let stuFeeplan = {
                _id: mongoose.Types.ObjectId(),
                applicationId: `FPLAN${(Number(j) + 1).toString().length == 1 ? "00" : (Number(j) + 1).toString().length == 2 ? "0" : ""
                    }${Number(j) + 1}`,
                studentRegId: ppInputData["Reg ID *"] == "-" ? ppInputData["HEDA ID"] : ppInputData["Reg ID *"],
                studentId: stddata._id,
                programPlanHEDAId: ppInputData["Program Plan ID"],
                plannedAmount: 0,
                plannedAmountBreakup: [],
                paidAmount: 0,
                paidAmountBreakup: [],
                pendingAmount: 0,
                pendingAmountBreakup: [],
                currency: stddata._doc.currency,
                forex: stddata._doc.FOREX,
                discountType: "",
                discountPercentage: 0,
                discountAmount: 0,
                dueDate: "",
                penaltyStartDate: "",
                percentageBreakup: [],
                installmentPlan: {},
                discountAmountBreakup: [],
                campusId: stddata._doc.campusId,
                remarks: {
                    seller: "",
                    finance: "",
                    headseller: ""
                }
            }
            let stdFeeInPlan = {
                feePlanId: stuFeeplan._id,
                label: "",
                description: "",
                dueDate: "",
                lateFeeStartDate: "",
                percentage: "",
                totalAmount: 0,
                plannedAmount: 0,
                plannedAmountBreakup: [],
                paidAmount: 0,
                paidAmountBreakup: [],
                pendingAmount: 0,
                pendingAmountBreakup: [],
                discountType: "",
                discountPercentage: 0,
                discountAmount: 0,
                discountAmountBreakup: [],
                status: "",
                transactionId: "",
                campusId: stddata._doc.campusId,
                remarks: {
                    seller: "",
                    finance: "",
                    headseller: ""
                }
            }
            let feeAmount = 0;
            let plannedAmount = 0;
            let plannedAmountBreakup = [];
            let paidAmountBreakup = [];
            let pendingAmountBreakup = [];
            let discountAmountBreakup = [];
            let instplannedAmountBreakup = [];
            let instpaidAmountBreakup = [];
            let instpendingAmountBreakup = [];
            for (let i = 0; i < feestrData._doc.feeTypeIds.length; i++) {
                let feetyped = feeTypeDetails.find(item => item._id.toString() == feestrData._doc.feeTypeIds[i])
                paidAmountBreakup.push({
                    amount: 0.00,
                    paid: 0.00,
                    pending: 0.00,
                    feeTypeCode: feetyped.displayName,
                    title: feetyped.title,
                })
                if (i + 1 == feestrData._doc.feeTypeIds.length) {
                    let feeBreakup = [];
                    let feeData = await feeManagerSchema.findOne({ feeTypeId: feestrData._doc.feeTypeIds[i], programPlanId: stddata._doc.programPlanId, "applyDates.0": { $lte: new Date(stddata._doc.admittedOn) }, "applyDates.1": { $gte: new Date(stddata._doc.admittedOn) }, campusId: stddata._doc.campusId });
                    if (!feeData || feeData == null) {
                        feeData = await feeManagerSchema.findOne({ feeTypeId: feestrData._doc.feeTypeIds[i], campusId: stddata._doc.campusId });
                    }
                    let pshedule
                    if (feeData == undefined || feeData == null) {
                        pshedule = allPaymentSchedule[0]
                    } else {
                        pshedule = await allPaymentSchedule.find(item => item._id.toString() == feeData.paymentScheduleId);
                    }
                    let month = pshedule.startMonth;
                    let duedate = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                    let penaltyStartdate = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                    penaltyStartdate = new Date(penaltyStartdate.setMonth((Number(penaltyStartdate.getMonth()) + 1)));
                    let tuitionFee = Number(feeData.feeDetails.totalAmount);
                    for (l = 0; l < pshedule.feesBreakUp.length; l++) {
                        count++
                        if (l > 0) {
                            let duedate1 = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                            duedate1 = duedate1.setMonth((Number(duedate1.getMonth()) + (12 / pshedule.feesBreakUp.length)));
                            let penaltyStartdate1 = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                            penaltyStartdate1 = penaltyStartdate1.setMonth((Number(penaltyStartdate1.getMonth()) + ((12 / pshedule.feesBreakUp.length) + 1)));
                            let stuinstallplans = new feeInstallmentPlanModel({
                                feePlanId: stuFeeplan._id,
                                label: `Installment${(Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                                    }${Number(count)}`,
                                description: `Installment${(Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                                    }${Number(count)}`,
                                dueDate: duedate1,
                                lateFeeStartDate: penaltyStartdate1,
                                percentage: pshedule.feesBreakUp[l],
                                totalAmount: tuitionFee,
                                plannedAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(tuitionFee) / 100,
                                plannedAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(tuitionFee) / 100), percent: pshedule.feesBreakUp[l] }],
                                paidAmount: 0,
                                paidAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: 0, percent: pshedule.feesBreakUp[l] }],
                                pendingAmount: 0,
                                pendingAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: 0, percent: pshedule.feesBreakUp[l] }],
                                discountType: "",
                                discountPercentage: 0,
                                discountAmount: 0,
                                discountAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: 0, percent: pshedule.feesBreakUp[l] }],
                                status: "",
                                transactionId: "",
                                campusId: stddata._doc.campusId,
                                remarks: {
                                    seller: "",
                                    finance: "",
                                    headseller: ""
                                }
                            })
                            allStudentsInstallmentPlans.push(stuinstallplans)
                        }
                        else {
                            let stuinstallplans = new feeInstallmentPlanModel({
                                feePlanId: stuFeeplan._id,
                                label: `Installment${(Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                                    }${Number(count)}`,
                                description: `Installment${(Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                                    }${Number(count)}`,
                                dueDate: duedate,
                                lateFeeStartDate: penaltyStartdate,
                                percentage: pshedule.feesBreakUp[l],
                                totalAmount: tuitionFee,
                                plannedAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(tuitionFee) / 100,
                                plannedAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(tuitionFee) / 100), percent: pshedule.feesBreakUp[l] }],
                                paidAmount: 0,
                                paidAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: 0, percent: pshedule.feesBreakUp[l] }],
                                pendingAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100,
                                pendingAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100, percent: pshedule.feesBreakUp[l] }],
                                discountType: "",
                                discountPercentage: 0,
                                discountAmount: 0,
                                discountAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: 0, percent: pshedule.feesBreakUp[l] }],
                                status: "",
                                transactionId: "",
                                campusId: stddata._doc.campusId,
                                remarks: {
                                    seller: "",
                                    finance: "",
                                    headseller: ""
                                }
                            })
                            allStudentsInstallmentPlans.push(stuinstallplans)
                        }
                        feeBreakup.push({ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(tuitionFee) / 100), percent: pshedule.feesBreakUp[l] })

                    }
                    plannedAmountBreakup.push({
                        amount: tuitionFee,
                        feeTypeCode: feetyped.displayName,
                        title: feetyped.title,
                    })
                    plannedAmount = plannedAmount + Number(feeData.feeDetails.totalAmount);
                    pendingAmountBreakup.push({
                        amount: tuitionFee,
                        feeTypeCode: feetyped.displayName,
                        title: feetyped.title,
                    })
                    discountAmountBreakup.push({
                        amount: 0.00,
                        feeTypeCode: feetyped.displayName,
                        title: feetyped.title,
                    })
                }
                // else {
                //     let feeBreakup = [];
                //     let feeData = await feeManagerSchema.findOne({ feeTypeId: feestrData._doc.feeTypeIds[i], programPlanId: stddata._doc.programPlanId, campusId: stddata._doc.campusId });
                //     let pshedule 
                //     if (feeData == undefined || feeData == null) {
                //         feeData = await feeManagerSchema.findOne({ feeTypeId: feestrData._doc.feeTypeIds[i], campusId: stddata._doc.campusId });
                //         pshedule = allPaymentSchedule[0]
                //     } else {
                //         pshedule = await allPaymentSchedule.find(item => item._id.toString() == feeData.paymentScheduleId);
                //     }
                //     // let pshedule = await allPaymentSchedule.find(item => item._id.toString() == feeData.paymentScheduleId);
                //     let month = pshedule.startMonth;
                //     let duedate = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                //     let penaltyStartdate = new Date(`${year}-${month}-${daydigits[pshedule.scheduleDetails.dueDate]}`)
                //     penaltyStartdate = new Date(penaltyStartdate.setMonth((Number(penaltyStartdate.getMonth()) + 1)));
                //     for (l = 0; l < pshedule.feesBreakUp.length; l++) {
                //         count++
                //         if (l > 0) {
                //             duedate = duedate.setMonth((Number(duedate.getMonth()) + (12 / pshedule.feesBreakUp.length)));
                //             penaltyStartdate = penaltyStartdate.setMonth((Number(penaltyStartdate.getMonth()) + (12 / pshedule.feesBreakUp.length)));
                //         }
                //         feeBreakup.push({ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100), percent: pshedule.feesBreakUp[l] })
                //         let stuinstallplans = new feeInstallmentPlanModel({
                //             feePlanId: stuFeeplan._id,
                //             label: `Installment${
                //                 (Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                //                 }${Number(count)}`,
                //             description: `Installment${
                //                 (Number(count)).toString().length == 1 ? "00" : (Number(count)).toString().length == 2 ? "0" : ""
                //                 }${Number(count)}`,
                //             dueDate: duedate,
                //             penaltyStartDate: penaltyStartdate,
                //             percentage: pshedule.feesBreakUp[l],
                //             totalAmount: feeData.feeDetails.totalAmount,
                //             plannedAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100,
                //             plannedAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: (parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100), percent: pshedule.feesBreakUp[l] }],
                //             paidAmount: 0,
                //             paidAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: 0, percent: pshedule.feesBreakUp[l] }],
                //             pendingAmount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100,
                //             pendingAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: parseFloat(pshedule.feesBreakUp[l]) * parseFloat(feeData.feeDetails.totalAmount) / 100, percent: pshedule.feesBreakUp[l] }],
                //             discountType: "",
                //             discountPercentage: 0,
                //             discountAmount: 0,
                //             discountAmountBreakup: [{ feeTypeCode: feetyped.displayName, title: feetyped.title, amount: 0, percent: pshedule.feesBreakUp[l] }],
                //             status: "",
                //             transactionId: "",
                //             campusId: stddata._doc.campusId,
                //             remarks: {
                //                 seller: "",
                //                 finance: "",
                //                 headseller: ""
                //             }
                //         })
                //         allStudentsInstallmentPlans.push(stuinstallplans)
                //     }
                //     plannedAmountBreakup.push({
                //         amount: Number(feeData.feeDetails.totalAmount),
                //         feeTypeCode: feetyped.displayName,
                //         title: feetyped.title,
                //     })
                //     plannedAmount = plannedAmount + Number(feeData.feeDetails.totalAmount);
                //     pendingAmountBreakup.push({
                //         amount: Number(feeData.feeDetails.totalAmount),
                //         feeTypeCode: feetyped.displayName,
                //         title: feetyped.title,
                //     })
                //     discountAmountBreakup.push({
                //         amount: 0.00,
                //         feeTypeCode: feetyped.displayName,
                //         title: feetyped.title,
                //     })
                // }

                // for (let j = 0; j < feeData.length; j++) {
                //     if (feeData[j]._doc.applyDates[0] <= new Date(stddata._doc.admittedOn) && feeData[j]._doc.applyDates[1] >= new Date(stddata._doc.admittedOn) && stddata._doc.campusId[0].toString() == feeData[j]._doc.campusId[0].toString()) {
                //         feeAmount = feeAmount + Number(feeData[j].feeDetails.totalAmount)
                //     }
                // }
            }
            stuFeeplan.plannedAmount = plannedAmount;
            stuFeeplan.pendingAmount = plannedAmount;
            stuFeeplan.plannedAmountBreakup = plannedAmountBreakup;
            stuFeeplan.paidAmountBreakup = paidAmountBreakup;
            stuFeeplan.pendingAmountBreakup = pendingAmountBreakup;
            stuFeeplan.discountAmountBreakup = discountAmountBreakup;
            let stuFeePlans = new feePlanModel(stuFeeplan)
            allStudentsFeePlans.push(stuFeePlans)
            if (j + 1 == studentDetails.length) {
                feePlanModel.insertMany(allStudentsFeePlans, function (err, result) {
                    if (err) {
                        if (err.name === "BulkWriteError" && err.code === 11000) {
                            reject(res.status(200).json({
                                success: true,
                                message: err.toString(),
                                count: 0,
                            }))
                        }
                    } else {
                        feeInstallmentPlanModel.insertMany(allStudentsInstallmentPlans, function (err2, result2) {
                            if (err2) {
                                if (err2.name === "BulkWriteError" && err2.code === 11000) {
                                    reject(res.status(200).json({
                                        success: true,
                                        message: err2.toString(),
                                        count: 0,
                                    }))
                                }
                            } else {
                                resolve({ status: "success", message: "fee plan created successfully" })
                            }
                        })
                    }
                })
            }
            // await stuFeePlans.save();
        }
    })
}
module.exports.convertJsonArray = async (req, res) => {
    let result = []
    for (i = 0; i < req.body.Sheet3.length; i++) {
        let arry1 = []
        for (let key in req.body.Sheet3[i]) {
            arry1.push(req.body.Sheet3[i][key])
        }
        result.push(arry1)
        if (i + 1 == req.body.Sheet3.length) {
            res.send(result)
        }
    }
}

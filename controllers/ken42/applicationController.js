const rq = require("request-promise");
const axios = require("axios");
const AWS = require("aws-sdk");
const mongoose = require("mongoose");
const ApplicationSchema = require("../../models/ken42/applicationModel");
const orgListSchema = require("../../models/orglists-schema");
const { createDatabase } = require("../../utils/db_creation");
awsCredentials = {
  accessKeyId: "AKIAR6HU7QOXIVHWCXAL",
  secretAccessKey: "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",
  region: "us-east-1",
};
AWS.config.update(awsCredentials);
var ses = new AWS.SES();
var s3 = new AWS.S3();

exports.createApplication = async function (req, res) {
  let inputData = req.body;

  if (
    !inputData.name ||
    !inputData.email ||
    !inputData.mobile ||
    !inputData.amount ||
    !inputData.paisa ||
    !inputData.applicationId ||
    !inputData.callBackUrl ||
    !inputData.currencyCode
  ) {
    res.status(422).json({
      status: "failed",
      message: "Please provide all required parameters.",
      type: "error",
    });
  } else if (!validEmail(inputData.email)) {
    res.json({ message: "Invalid Info", type: "error" });
  } else {
    const centralDbConnection = await createDatabase(
      "edu-central",
      process.env.central_mongoDbUrl
    );
    const orgListModel = centralDbConnection.model(
      "orglists",
      orgListSchema,
      "orglists"
    );
    const orgData = await orgListModel.findOne({
      _id: req.query.orgId,
    });
    if (!orgData || orgData == null) {
      centralDbConnection.close();
      res.status(500).send({
        status: "failure",
        message: "Organization not found",
      });
    } else {
      let dbConnection = await createDatabase(
        String(orgData._id),
        orgData.connUri
      );
      const credentials = mongoose.Schema({}, { strict: false });
      // const msettingModel = dbConnectionp.model("settings", credentials, "settings")
      const credentialsModel = dbConnection.model(
        "credentials",
        credentials,
        "credentials"
      );
      const credentialData = await credentialsModel.findOne({
        type: "payment",
      });

      var username = credentialData._doc.userName;
      var password = credentialData._doc.password;
      var auth =
        "Basic " + Buffer.from(username + ":" + password).toString("base64");
      let amount = inputData.amount + inputData.paisa;
      let today = Date.now();
      var obj;
      if (inputData.accept_partial == true) {
        obj = {
          amount: parseInt(amount),
          currency: "INR",
          accept_partial: true,
          first_min_partial_amount: inputData.min_partial_amount,
          expire_by: today,
          reference_id: inputData.applicationId,
          description: "Payment for " + inputData.applicationId,
          customer: {
            name: inputData.name,
            contact: inputData.mobile,
            email: inputData.email,
          },
          notify: {
            sms: true,
            email: true,
          },
          reminder_enable: true,
          notes: {
            policy_name: inputData.name,
          },
          callback_url:
            inputData.callBackUrl + "?applicationId=" + inputData.applicationId,
          callback_method: "get",
        };
      } else {
        obj = {
          amount: parseInt(amount),
          currency: "INR",
          accept_partial: inputData.accept_partial,
          expire_by: today,
          reference_id: inputData.applicationId,
          description: "Payment for " + inputData.applicationId,
          customer: {
            name: inputData.name,
            contact: inputData.mobile,
            email: inputData.email,
          },
          notify: {
            sms: true,
            email: true,
          },
          reminder_enable: true,
          notes: {
            policy_name: inputData.name,
          },
          callback_url:
            inputData.callBackUrl + "?applicationId=" + inputData.applicationId,
          callback_method: "get",
        };
      }
      var options = {
        method: "POST",
        uri: "https://api.razorpay.com/v1/payment_links",
        body: obj,
        json: true,
        headers: {
          "Content-Type": "application/json",
          Authorization: auth,
        },
      };
      rq(options)
        .then(async (success) => {
          var applicationModel = dbConnection.model(
            "applications",
            ApplicationSchema
          );
          console.log("connections", applicationModel);

          var appDetails = new applicationModel({
            name: inputData.name,
            email: inputData.email,
            mobile: inputData.mobile,
            applicationId: inputData.applicationId,
            amount: Number(inputData.amount),
            paisa: Number(inputData.paisa),
            partial: inputData.accept_partial,
            paymentId: success.id,
            callBackUrl: inputData.callBackUrl,
            currencyCode: inputData.currencyCode,
            razorpay: success,
            webhookStatus: success.status,
          });
          appDetails.save(function (err, applicationDetails) {
            if (err) {
              dbConnection.close();
              return res.status(400).json({
                message: "failed to store application",
                type: "error",
                data: err,
              });
            } else {
              dbConnection.close();
              res.status(200).json({
                success: true,
                Data: success,
                applicationId: inputData.applicationId,
                applicationDetails,
              });
            }
          });
          // res.status(200).json({
          //   success: true,
          //   Data: success,
          //   applicationId: inputData.applicationId,
          // });
        })
        .catch((err) => {
          res.status(400).json({ Message: "Failed", Error: err });
          return;
        })
        .finally(() => {
          centralDbConnection.close();
        });
    }
  }
};

exports.getPaymentStatus = async function (req, res) {
  let razorpayId = req.params.id;
  const centralDbConnection = await createDatabase(
    "edu-central",
    process.env.central_mongoDbUrl
  );
  const orgListModel = centralDbConnection.model(
    "orglists",
    orgListSchema,
    "orglists"
  );
  const orgData = await orgListModel.findOne({
    _id: req.query.orgId,
  });
  if (!orgData || orgData == null) {
    centralDbConnection.close();
    res.status(500).send({
      status: "failure",
      message: "Organization not found",
    });
  } else {
    let dbConnection = await createDatabase(
      String(orgData._id),
      orgData.connUri
    );
    const credentials = mongoose.Schema({}, { strict: false });
    // const msettingModel = dbConnectionp.model("settings", credentials, "settings")
    const credentialsModel = dbConnection.model(
      "credentials",
      credentials,
      "credentials"
    );
    const credentialData = await credentialsModel.findOne({
      type: "payment",
    });

    var username = credentialData.username;
    var password = credentialData.password;
    var auth =
      "Basic " + Buffer.from(username + ":" + password).toString("base64");
    var options = {
      method: "GET",
      uri: "https://api.razorpay.com/v1/payment_links/" + razorpayId,
      headers: {
        "Content-Type": "application/json",
        Authorization: auth,
      },
    };
    rq(options)
      .then((success) => {
        res.status(200).json({
          status: "success",
          Data: success,
        });
      })
      .catch((err) => {
        res.status(400).json({ Message: "Failed", Error: err });
        return;
      });
  }
};

exports.sendReceipt = async function (req, res) {
  let inputData = req.body;
  var username = process.env.razorusername;
  var password = process.env.razorpassword;
  var auth =
    "Basic " + Buffer.from(username + ":" + password).toString("base64");
  var options = {
    method: "GET",
    uri: process.env.razorpay + "/payments/" + inputData.paymentId,
    headers: {
      "Content-Type": "application/json",
      Authorization: auth,
    },
  };
  rq(options)
    .then((success) => {
      let responseRazor = JSON.parse(success);
      let method = responseRazor.method;
      let transId = responseRazor.acquirer_data.bank_transaction_id;
      let payload = {
        email: inputData.email,
        academicYear: inputData.academicYear,
        applicationId: inputData.applicationId,
        transactionId: transId,
        studentName: inputData.studentName,
        class: inputData.class,
        applicationFees: inputData.applicationFees,
        mode: method.toUpperCase(),
      };
      axios
        .post(
          process.env.receiptAPI + "?institute=" + req.query.institute,
          payload
        )
        .then(function (response) {
          res.status(200).json(response.data);
        })
        .catch(function (error) {
          res.status(400).json({ Message: "Failed", Error: error });
        });
    })
    .catch((err) => {
      res.status(400).json({ Message: "Failed", Error: err });
      return;
    });
};

// exports.deleteStudent = function (req, res) {
//   Role.deleteOne({ _id: req.params.id }).then(function (data) {
//     if (data.deletedCount)
//       return res.json("Role has been deleted successfully");
//     else return res.json({ message: "User does not exist" });
//   });
// };
validEmail = function (email) {
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
    return true;
  }
  return false;
};

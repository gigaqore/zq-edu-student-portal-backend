const { BlobServiceClient } = require("@azure/storage-blob");

exports.getBlobData = async function (req, res) {
  const AZURE_STORAGE_CONNECTION_STRING =
    "DefaultEndpointsProtocol=https;AccountName=vksdata;AccountKey=8ZUER1NJE8gVZoC3w6mrknXHnXJXFr2uII7ObvZy45ARpoRxRmLGJ9EbRI2kc1/XIOKyEP/J5PHh/Zlcu2bOcw==;EndpointSuffix=core.windows.net";
  // Create the BlobServiceClient object which will be used to create a container client
  const blobServiceClient = BlobServiceClient.fromConnectionString(
    AZURE_STORAGE_CONNECTION_STRING
  );
  let i = 1;
  for await (const container of blobServiceClient.listContainers()) {
    console.log(`Container ${i++}: ${container.name}`);
    // Get a block blob client
    const containerClient = blobServiceClient.getContainerClient(
      container.name
    );
    for await (const blob of containerClient.listBlobsFlat()) {
      console.log("\t", blob.name);
      // Create a unique name for the blob
      const blobName = blob.name;

      // Get a block blob client
      const blockBlobClient = containerClient.getBlockBlobClient(blobName);
      const downloadBlockBlobResponse = await blockBlobClient.download(0);
      console.log("\nDownloaded blob content...");
      console.log(
        "\t",
        await streamToString(downloadBlockBlobResponse.readableStreamBody)
      );
    }
  }
};

// A helper method used to read a Node.js readable stream into a string
async function streamToString(readableStream) {
  return new Promise((resolve, reject) => {
    const chunks = [];
    readableStream.on("data", (data) => {
      chunks.push(data.toString());
    });
    readableStream.on("end", () => {
      resolve(chunks.join(""));
    });
    readableStream.on("error", reject);
  });
}

const mongoose = require("mongoose");

const bankDetailsSchema = mongoose.Schema(
  {
    bankName: { type: String,default:null},
    bankAccountName: { type: String, default:null},
    bankAccountNumber: {type: String, default:null},
    bankIFSC: {type:String, default:null},
    campusId: {type:String},
    status: { type: String, Default:"Active" },
  },
  { timestamps: true }
);

mongoose.set("useFindAndModify", false);
bankDetailsSchema.pre("update", function () {
  this.update({}, { $set: { updatedAt: new Date() } });
});

module.exports = bankDetailsSchema;
